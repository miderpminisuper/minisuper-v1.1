package main

import (
	"fmt"
	"net/http"

	"github.com/robfig/config"

	"./Controladores"
	"./Controladores/Acl"
	"./Controladores/Ajustes"
	"./Controladores/Almacenes"
	"./Controladores/Articulos"
	"./Controladores/Cliente"
	"./Controladores/Compras"
	"./Controladores/Datosfiscales"
	"./Controladores/Devolucion"
	"./Controladores/Impuestos"
	"./Controladores/Kardex"
	"./Controladores/Medidas"
	"./Controladores/Movimientos"
	"./Controladores/Pagos"
	"./Controladores/Proveedor"
	"./Controladores/Usuario"
	"./Controladores/Venta"
	"./Modelos/Usuarios"
	"github.com/gorilla/mux"
)

const (
	archivo_configuracion = "configuracion_general.cfg"
)

//############################################### F  U  N  C  I  O  N  E S ########################################################
func makeHandler(fn func(w http.ResponseWriter, r *http.Request)) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		fn(w, r)
	}
}

//Inserta usuario por default
func setUsuarioDefault() {
	result, _ := users.GetAllUser()
	if len(result) > 0 {
		//No hacemos nada si existen usuarios
	} else {
		//Insertamos si la tabla de usuarios esta vacio
		//usuario: admin
		//password: admin
		res := users.SetUsuarioDefault()
		fmt.Println("Usuario nuevo insertado: ", res)
	}
}

var router = mux.NewRouter()

//##################################################   M   A   I   N   ############################################################
func main() {
	//Se lee el archivo de configuracion general
	arch_conf, err := config.ReadDefault(archivo_configuracion)
	if err != nil {
		fmt.Println("LECTURA DEL ARCHIVO configuracion_general.cfg falló: %s", err)
	}
	//Verifica el numero de secciones del archivo configuracion
	if len(arch_conf.Sections()) != 3 {
		fmt.Println("NUMERO DE SECCIONES NO SON VALIDOS, DEBERAN SER 3:  \n DEFAULT, CONFIG_DB_MONGO Y CONFIG_DB_POSTGRES", len(arch_conf.Sections()))
	}
	//obtiene el numero de puerto descrito en el archivo de configuracion
	puerto, err := arch_conf.String("DEFAULT", "puerto")
	if err != nil {
		fmt.Println("No se puede leer el puerto")
	}
	//obtiene la ruta descrita en el archivo de configuracion
	ruta, err := arch_conf.String("DEFAULT", "base-url")
	if err != nil {
		fmt.Println("No se puede leer la url")
	}
	//http.Handle("/assets/", http.StripPrefix("/assets/", http.FileServer(http.Dir("assets"))))
	http.Handle("/Recursos/", http.StripPrefix("/Recursos/", http.FileServer(http.Dir("Recursos"))))
	/*
	*Autor: Ramon Cruz Jurez
	*Descripcion: Modulo de administracion e Inventario
	 */
	router.HandleFunc("/administracion", makeHandler(controllers.Administracion))
	router.HandleFunc("/inventario", makeHandler(controllers.Inventario))
	router.HandleFunc("/productos", makeHandler(controllers.Productos))
	router.HandleFunc("/modulocompras", makeHandler(controllers.Compras))
	router.HandleFunc("/moduloventas", makeHandler(controllers.Ventas))
	//Index
	router.HandleFunc("/", makeHandler(controllers.Index))
	router.HandleFunc("/index", makeHandler(controllers.Index))
	/*
	*Autor: Ramon Cruz Juarez
	*En ésta sección se coloca la llamada al controlador
	*para recuperar la contraseña
	 */
	router.HandleFunc("/passwordrecovery", makeHandler(controllers.RecuperacionContraseña))
	/*
	 *@Caleb
	 *Modulo de login de usuarios
	 */
	router.HandleFunc("/login", makeHandler(controllers.Login))
	router.HandleFunc("/logout", makeHandler(controllers.Logout))
	router.HandleFunc("/login_user", makeHandler(controllers.GetUserSession))
	/*
	 *@Caleb
	 *Modulo de clientes
	 */
	router.HandleFunc("/client", makeHandler(client.IndexClient))
	router.HandleFunc("/client_frame", makeHandler(client.IndexClientFrame))
	router.HandleFunc("/client/new", makeHandler(client.ClientNew))
	router.HandleFunc("/client/new_frame", makeHandler(client.ClientNewFrame))
	router.HandleFunc("/client/edit/{id}", makeHandler(client.EditClientId))
	router.HandleFunc("/client/edit_frame/{id}", makeHandler(client.EditClientIdFrame))
	router.HandleFunc("/client/delete/{id}", makeHandler(client.EliminarCliente))
	router.HandleFunc("/client/delete_frame/{id}", makeHandler(client.EliminarClienteFrame))
	/*
	 *@Caleb
	 *Modulo de usuarios
	 */
	router.HandleFunc("/usuarios", makeHandler(user.IndexUser))
	router.HandleFunc("/usuarios_frame", makeHandler(user.MostrarUsuariosFrame))
	router.HandleFunc("/users/new", makeHandler(user.UserNew))
	router.HandleFunc("/users/new_frame", makeHandler(user.UserNew_frame))
	router.HandleFunc("/users/edit/{id}", makeHandler(user.EditUserId))
	router.HandleFunc("/users/delete/{id}", makeHandler(user.EliminarUsuario))
	router.HandleFunc("/users/mostrar/{id}", makeHandler(user.MostrarUsuarioId))
	/*
	 *@Roberto
	 *Modulo de Medidas
	 */
	router.HandleFunc("/medidas", makeHandler(medidas.Index))
	router.HandleFunc("/medidas_frame", makeHandler(medidas.MedidasFrame))
	router.HandleFunc("/editarmedida", makeHandler(medidas.Editar)).Methods("POST")
	router.HandleFunc("/guardarmedida", makeHandler(medidas.Guardar)).Methods("POST")
	router.HandleFunc("/newmedida", makeHandler(medidas.New)).Methods("POST")
	router.HandleFunc("/newermedida", makeHandler(medidas.Newer)).Methods("POST")
	router.HandleFunc("/eliminarmedida", makeHandler(medidas.Eliminar)).Methods("POST")
	/*
	 *@Roberto
	 *Modulo de formas de pago
	 */
	router.HandleFunc("/formadepago", makeHandler(pagos.Index))
	router.HandleFunc("/formadepago_frame", makeHandler(pagos.FormasDePago))
	router.HandleFunc("/crearformapago", makeHandler(pagos.Crear)).Methods("POST")
	router.HandleFunc("/guardarformadepago", makeHandler(pagos.Guardar)).Methods("POST")
	router.HandleFunc("/editarformapago", makeHandler(pagos.Editar)).Methods("POST")
	router.HandleFunc("/guardareditarformapago", makeHandler(pagos.GuardarEditar)).Methods("POST")
	router.HandleFunc("/eliminarformapago", makeHandler(pagos.Eliminar)).Methods("POST")
	/*
	 *@Roberto
	 *Modulo de almacenes
	 */
	router.HandleFunc("/almacenes", makeHandler(almacenes.Index))
	router.HandleFunc("/almacenes_frame", makeHandler(almacenes.AlmacenesFrame))
	router.HandleFunc("/crearalmacen", makeHandler(almacenes.Crear))
	router.HandleFunc("/guardaralmacen", makeHandler(almacenes.Guardar)).Methods("POST")
	router.HandleFunc("/editaralmacen", makeHandler(almacenes.Editar)).Methods("POST")
	router.HandleFunc("/guardareditaralmacen", makeHandler(almacenes.GuardarEditar)).Methods("POST")
	router.HandleFunc("/eliminaralmacen", makeHandler(almacenes.Eliminar)).Methods("POST")
	/*
	 *@Roberto
	 *Modulo de impuestos
	 */
	router.HandleFunc("/impuestos", makeHandler(impuestos.Index))
	router.HandleFunc("/impuestos_frame", makeHandler(impuestos.ImpuestosFrame))
	router.HandleFunc("/editarimpuesto", makeHandler(impuestos.Editar)).Methods("POST")
	router.HandleFunc("/guardarimpuesto", makeHandler(impuestos.Guardar)).Methods("POST")
	router.HandleFunc("/newmetaimpuesto", makeHandler(impuestos.New)).Methods("POST")
	router.HandleFunc("/newermetaimpuesto", makeHandler(impuestos.Newer)).Methods("POST")
	router.HandleFunc("/eliminarimpuesto", makeHandler(impuestos.Eliminar)).Methods("POST")
	/*
	 *@Roberto
	 *Modulo de Ajustes y Traslados
	 */
	router.HandleFunc("/ajustetraslado", makeHandler(ajustes.Index))
	router.HandleFunc("/ajustetraslado_frame", makeHandler(ajustes.AjustesFrame))
	router.HandleFunc("/ajusteotraslado", makeHandler(ajustes.AjusteoTraslado))
	router.HandleFunc("/ajustearticulo", makeHandler(ajustes.GetProductoUnico))
	router.HandleFunc("/realizarmovimiento", makeHandler(ajustes.RealizarMovimiento)).Methods("POST")
	//Busqueda
	router.HandleFunc("/searchAjaxArtiElastic3", makeHandler(venta.SearchArticleElasticInSell3))
	router.HandleFunc("/valuePaginatioElasticResult3", makeHandler(venta.GetProductsIdTo_Info_Paginatio3))

	/*
	 *@Roberto
	 *Modulo de Movimientos
	 */
	router.HandleFunc("/movimientos", makeHandler(movimientos.Index))
	router.HandleFunc("/movimientos_frame", makeHandler(movimientos.MovimientosFrame))
	router.HandleFunc("/obtenermovimientos", makeHandler(movimientos.ObtenerMovimientos)).Methods("POST")
	router.HandleFunc("/productosdemovimiento", makeHandler(movimientos.ProductosMovimiento)).Methods("POST")
	/*
	 *@caleb
	 *Modulo de proveedores
	 */
	router.HandleFunc("/proveedores", makeHandler(proveedor.IndexProveedor))
	router.HandleFunc("/proveedores_frame", makeHandler(proveedor.IndexProveedorFrame))
	router.HandleFunc("/proveedores/new", makeHandler(proveedor.ProveedorNew))
	router.HandleFunc("/proveedores/new_frame", makeHandler(proveedor.ProveedorNewFrame))
	router.HandleFunc("/proveedores/edit/{id}", makeHandler(proveedor.EditProveedorId))
	router.HandleFunc("/proveedores/edit_frame/{id}", makeHandler(proveedor.EditProveedorIdFrame))
	router.HandleFunc("/proveedores/delete/{id}", makeHandler(proveedor.EliminarProveedor))
	router.HandleFunc("/proveedores/delete_frame/{id}", makeHandler(proveedor.EliminarProveedorFrame))
	/*
	 *@caleb
	 *Datos fiscales de la empresa
	 */
	router.HandleFunc("/datosfiscales", makeHandler(fiscal.DatosFiscales))
	router.HandleFunc("/datosfiscales_frame", makeHandler(fiscal.DatosFiscales_frame))
	router.HandleFunc("/eliminar_datos_fiscales", makeHandler(fiscal.EliminarDatosFiscales))
	/*Autor: Ismael
	 *Modulo compras
	 *Llamadas http a cada funcion de compras
	 */
	router.HandleFunc("/compras", makeHandler(Compras.CargaPantalla))
	router.HandleFunc("/compras_frame", makeHandler(Compras.CargaPantalla_frame))
	router.HandleFunc("/compras_edit/{id}", makeHandler(Compras.EditaCompra))
	router.HandleFunc("/compras_edit_frame/{id}", makeHandler(Compras.EditaCompra_frame))
	router.HandleFunc("/BuscaArticulos", makeHandler(Compras.BuscaArticulos)).Methods("POST")
	router.HandleFunc("/getPaginationResultSearch", makeHandler(Compras.GetAllResultSearchElastic)).Methods("POST")
	router.HandleFunc("/nexpagination", makeHandler(Compras.GetSearchPaginationGeneral)).Methods("POST")
	router.HandleFunc("/AgregaCompras", makeHandler(Compras.AgregaCompra)).Methods("POST")
	router.HandleFunc("/AltaArticuloDeCompra", makeHandler(Compras.AltaArticuloDeCompra)).Methods("POST")
	router.HandleFunc("/InsertaCompras", makeHandler(Compras.InsertaCompra))
	router.HandleFunc("/EditaCompras", makeHandler(Compras.EditaCompras))
	router.HandleFunc("/AltaProductoDesdeCompras", makeHandler(Compras.AltaProductoDesdeCompras)).Methods("POST")
	router.HandleFunc("/AltaAlmacenDesdeCompras", makeHandler(Compras.AltaAlmacenDesdeCompras)).Methods("POST")
	router.HandleFunc("/AltaUnidadDesdeCompras", makeHandler(Compras.AltaUnidadDesdeCompras)).Methods("POST")
	router.HandleFunc("/indexcompras", makeHandler(Compras.CargaPantallaIndex))
	router.HandleFunc("/indexcompras_frame", makeHandler(Compras.CargaPantallaIndex_frame))
	router.HandleFunc("/CargaCompras", makeHandler(Compras.CargaCompras)).Methods("POST")
	router.HandleFunc("/getPaginationResultSearchC", makeHandler(Compras.GetAllResultSearchElastic_index)).Methods("POST")
	router.HandleFunc("/nexpaginationC", makeHandler(Compras.GetSearchPaginationGeneral_index)).Methods("POST")
	router.HandleFunc("/EliminaMovimiento", makeHandler(Compras.EliminaMovimiento)).Methods("POST")
	//Productos
	/*Autor: Ismael
	 *Modulo Articulos
	 *Llamadas http a cada funcion de productos
	 */
	router.HandleFunc("/BuscaArticulos", makeHandler(Articulos.BuscaArticulos)).Methods("POST")
	router.HandleFunc("/CargaArticulos", makeHandler(Articulos.CargaArticulos)).Methods("POST")
	router.HandleFunc("/getPaginationResultSearchA", makeHandler(Articulos.GetAllResultSearchElastic)).Methods("POST")
	router.HandleFunc("/nexpaginationA", makeHandler(Articulos.GetSearchPaginationGeneral)).Methods("POST")
	router.HandleFunc("/indexarticulos", makeHandler(Articulos.CargaPantallaIndex))
	router.HandleFunc("/indexarticulos_frame", makeHandler(Articulos.CargaPantallaIndex_frame))
	router.HandleFunc("/articulos", makeHandler(Articulos.CargaPantalla))
	router.HandleFunc("/articulos_frame", makeHandler(Articulos.CargaPantalla_frame))
	router.HandleFunc("/articulos/{code}", makeHandler(Articulos.EditProducto))
	router.HandleFunc("/articulos_frame/{code}", makeHandler(Articulos.EditProducto_frame))
	router.HandleFunc("/ValidaCodigo", makeHandler(Articulos.VerificaCodigo)).Methods("POST")
	router.HandleFunc("/ValidaCodigoEdit", makeHandler(Articulos.VerificaCodigoEdit)).Methods("POST")
	router.HandleFunc("/GeneraCodigo", makeHandler(Articulos.GeneraCodigo)).Methods("POST")
	router.HandleFunc("/InsertaArticulo", makeHandler(Articulos.InsertaArticulo))
	router.HandleFunc("/InsertaArticulo_frame", makeHandler(Articulos.InsertaArticuloFrame))
	router.HandleFunc("/ActualizaArticulo", makeHandler(Articulos.ActualizaArticulo))
	router.HandleFunc("/ActualizaArticulo_frame", makeHandler(Articulos.ActualizaArticuloFrame))
	router.HandleFunc("/del/{code}", makeHandler(Articulos.EliminarDirectorio))
	router.HandleFunc("/del_frame/{code}", makeHandler(Articulos.EliminarDirectorioFrame))
	//Venta
	router.HandleFunc("/venta/new", makeHandler(venta.VentaNew))
	router.HandleFunc("/venta/new_frame", makeHandler(venta.VentaNew_frame))
	router.HandleFunc("/venta/fn1", makeHandler(venta.PrintFields))
	router.HandleFunc("/venta/fn2", makeHandler(venta.ObtenerId))
	router.HandleFunc("/venta/transaccionV", makeHandler(venta.ManejadorCotizacion))
	router.HandleFunc("/venta/modifdcto", makeHandler(venta.ModifDescuento))
	router.HandleFunc("/venta/crearcotizacion", makeHandler(venta.GeneraIdCotizacion))
	router.HandleFunc("/venta/eliminaCot", makeHandler(venta.EliminarCotizacion))
	router.HandleFunc("/searchAjaxArtiElastic", makeHandler(venta.SearchArticleElasticInSell))
	router.HandleFunc("/valuePaginatioElasticResult", makeHandler(venta.GetProductsIdTo_Info_Paginatio))
	router.HandleFunc("/placepagination", makeHandler(venta.PlacePagination))
	router.HandleFunc("/getformasdepago", makeHandler(venta.GetFormasDePagoMongo))
	router.HandleFunc("/CargaVentas", makeHandler(venta.CargaVentas)).Methods("POST")
	router.HandleFunc("/indexventas_frame", makeHandler(venta.CargaPantallaIndex_frame))
	router.HandleFunc("/ventas_edit_frame/{id}", makeHandler(venta.EditaVenta_frame))
	router.HandleFunc("/paginacion_ventas", makeHandler(venta.GetAllResultSearchElastic_index)).Methods("POST")
	router.HandleFunc("/nexpaginationV", makeHandler(venta.GetSearchPaginationGeneral_index)).Methods("POST")
	// router.HandleFunc("/facturacion/consulta", makeHandler(user.UserNew))
	// router.HandleFunc("/facturacion/{id}", makeHandler(user.EditUserId))

	//pagos
	router.HandleFunc("/getformasdepago", makeHandler(venta.GetFormasDePagoMongo))
	router.HandleFunc("/loadformapagodiv", makeHandler(venta.RealizarPagoConFormaDePago))
	router.HandleFunc("/addvaluetotal", makeHandler(venta.SumarValoresDeArrayParaTotal))
	router.HandleFunc("/subvaluetotal", makeHandler(venta.RestarAtotal))
	router.HandleFunc("/inserrtaVenta", makeHandler(venta.InsertaVenta))
	router.HandleFunc("/BuscaClientesV", makeHandler(venta.BuscaClientes)).Methods("POST")
	router.HandleFunc("/getPaginationResultSearchV", makeHandler(venta.GetAllResultSearchElasticV)).Methods("POST")
	router.HandleFunc("/nexpaginationV", makeHandler(venta.GetSearchPaginationGeneralV)).Methods("POST")
	router.HandleFunc("/GeneraTicket", makeHandler(venta.GeneraTicket))
	router.HandleFunc("/GeneraFactura", makeHandler(venta.GeneraFactura))

	router.HandleFunc("/acl", makeHandler(acl.Index))
	router.HandleFunc("/kardex", makeHandler(kardex.Index))
	router.HandleFunc("/kardex_frame", makeHandler(kardex.IndexFrame))
	router.HandleFunc("/getKardex", makeHandler(kardex.GetKardex)).Methods("POST")
	router.HandleFunc("/getDocumento", makeHandler(kardex.GetDocumento)).Methods("POST")
	router.HandleFunc("/searchAjaxArtiElastic2", makeHandler(venta.SearchArticleElasticInSell2))
	router.HandleFunc("/valuePaginatioElasticResult2", makeHandler(venta.GetProductsIdTo_Info_Paginatio2))
	//DEVOLUCION Y GARANTIA
	router.HandleFunc("/devolucion", makeHandler(devolucion.Index))
	router.HandleFunc("/devolucion_frame", makeHandler(devolucion.IndexFrame))
	router.HandleFunc("/getDocumentoDev", makeHandler(devolucion.GetDocumento)).Methods("POST")
	router.HandleFunc("/setDocumentoDev", makeHandler(devolucion.SetDocumento)).Methods("POST")
	//Router Mux
	http.Handle("/", router)
	//Server Run
	server := http.Server{
		Addr: ":" + puerto,
	}
	fmt.Println("Ejecutandose en el puerto: ", puerto)
	fmt.Println("Acceder a la siguiente url: ", ruta)
	//Funcion inserta usuario default
	setUsuarioDefault()
	err_fi := server.ListenAndServe()
	if err_fi != nil {
		fmt.Println("Error durante la ejecucion: ", err_fi)
	}
}
