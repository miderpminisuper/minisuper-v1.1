$(document).ready(function() {
  $("#repeat-group").hide();
  $("#validate-email").attr('disabled','disabled');

  $("#repeticion").change(function() {
        if($(this).is(":checked")) {
            $("#repeat-group").show();
        }else {
          $("#repeat-group").hide();
        }
    });

  $("#go-back").click(function(){
    window.location.replace("/");
  });
  $("#email").focusout(function(){
    var correo = $("#email").val();
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    if(pattern.test(correo)){
        $("#validate-email").prop('disabled', false);
    }else{
      $("#validate-email").attr('disabled','disabled');
      $("#email").text("");
      $("#email").focus();
    }
  }
  );
  $("#validate-email").click(function(){
    var procesaremail = $("#email").val();
    $.ajax({
      url:"/passwordrecovery",
      type: "POST",
      dataType:"text",
      data:{mensaje:procesaremail},
      success: function(data){
          if(data != ""){
            alert(data);
          }else{
            alert("Se ha realizado el envío");
          }
      }
    });
    $("#validate-email").attr('disabled','disabled');
    $("#email").focus();
    $("#email").val("");
  });

  // $("#commit").click(function(){
  //   var procesaremail = $("#email").val();
  //   $.ajax({
  //     url:"/passwordrecovery",
  //     type: "POST",
  //     dataType:"text",
  //     data:{mensaje:procesaremail},
  //     success: function(data){
  //         if(data != ""){
  //           alert(data);
  //         }else{
  //           alert("Se ha realizado el envío");
  //         }
  //     }
  //   });
  //   $("#validate-email").attr('disabled','disabled');
  //   $("#email").focus();
  //   $("#email").val("");
  // });

});
