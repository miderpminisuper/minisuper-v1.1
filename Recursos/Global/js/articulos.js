
            $(document).ready(function () {
                    //Tooltip
                        $('[data-toggle="tooltip"]').tooltip(); 


                    //Oculta divs
                    $('#divimagen').hide();


                    //Si cambia el radio físico muestra div anterior
                    $('#radioFisico').change(function () {
                      if ($(this).is(':checked')) {
                        $('#id_venta_fisico').show('fast');
                      } else {
                        $('#id_venta_fisico').hide('fast');
                      }
                    });


                    //si cambia a radio lógico debe ocultarse el div anterior
                    $('#radioLogico').change(function () {
                      $('#id_venta_fisico').hide('fast');
                    });


                   
                    $('#id_precio').blur(function () {
                      var precio = $('#id_precio').val();
                      var costo = $('#id_costo').val();                      
                      if ( !isNaN(precio) && !isNaN(costo) && Number(precio) <= Number(costo)){
                        $('#error_precio').addClass("alert alert-warning");
                        $('#error_precio').append("Te sugiero que el precio no sea menor o igual que el costo para que no tengas pérdidas.");
                      }
                    });

                    $('#id_precio').focus(function () {
                      $('#error_precio').empty();
                      $('#error_precio').removeClass("alert alert-warning");
                    });


                    //al perder el foco el codigo se valida
                    $('#codigobarra').blur(function () {
                      var code = $('#codigobarra').val();
                      $.ajax({
                        url: '/ValidaCodigo',
                        type: 'POST',
                        dataType: 'html',
                        data: { code: code },
                        success: function (data, status) {
                          if (data != '') {
                            $('#error_codigo').append(data);
                            console.log(status);
                          } else {
                            console.log(status);
                          }
                        },
                      });
                    });

                    //al recibir el foco se debe vaciar el div de error
                    $('#codigobarra').focus(function () {
                      $('#error_codigo').empty();
                    });
                    $('#idecampo,#idevalor').focus(function () {
                      $('#error_campo_nuevo').empty();
                    });

                    //Si cambia la unidad cambia el label de Cantidad
                    $('#id_unidades').change(function () {
                      var unidad = $('#id_unidades').val();
                      if (unidad != 0) {
                        $('#cant').text(unidad + 's');
                      } else {
                        $('#cant').text('');
                      }
                    });                 


                   var MaxInputs = 6; //Número Maximo de Campos
                    var AddButton = $("#AgregaCampo"); //ID del Botón Agregar
                    var CancelButton = $("#cancelarAdd");
                    var saveButton = $("#guardar_attribs")



                    $(AddButton).click(function () {
                      var x = document.getElementById("tbody_etiquetas").children.length;
                      var FieldCount = x + 1; //para el seguimiento de los campos
                      console.log(FieldCount);
                      $("#error_campo_nuevo").empty();

                      if (/^\s*$/.test($("#idecampo").val()) || /^\s*$/.test($("#idevalor").val())) {
                        $("#error_campo_nuevo").append("El nombre del campo o valor no puede estar vacío");
                      } else {
                        if ($("#div_tabla").children.length > 0) {

                        }
                        if (x < MaxInputs) {
                          $("#tbody_etiquetas").append(
                            '<tr>\n\
                            <td><input type="text" class="form-control" name="idecampo" value="' + $("#idecampo").val() + '" readonly></td>\n\
                            <td><input type="text" class="form-control" name="idevalor" value="' + $("#idevalor").val() + '" readonly></td>\n\
                            <td><button type="button" class="btn btn-danger deleteButton">\n\
                            <span class="glyphicon glyphicon-trash btn-xs"> </span></button></td>\n\
                         </tr>');
                          FieldCount++;
                          $("#idevalor").val('');
                          $("#idecampo").val('');
                        }
                      }
                    });

                    $(CancelButton).click(function () {
                      $("#tbody_etiquetas").empty();
                    });

                    $(saveButton).click(function () {
                      $("#tbody_tab_general").append($("#tbody_etiquetas")[0].children);
                    });


                    $(document).on('click', '.deleteButton', function () {
                      $(this).parent().parent().remove();
                    });
                   

                    $('#Myform').submit(function(event){
                    var code = $('#codigobarra').val();
                    if (code === ''){
                      GeneraCodigo();                    
                    }
                    });

                    valida();
                    //blur para modal de unidades
                    addBlurUnidad();

                  });
                  ///////////////////////////////////////////////////////////////////////////////////   
                  var valida = function(){
                     $('#Myform').bootstrapValidator({
                      feedbackIcons: {
                        valid: 'glyphicon glyphicon-ok',
                        invalid: 'glyphicon glyphicon-remove',
                        validating: 'glyphicon glyphicon-refresh'
                      },
                      err: {
				              	container: '#alertas'
		                	},
                      fields: {
                        Descripción: {
                          selector: '#id_descripcion',
                          message: 'Descripción no válida.',
                          validators: {
                            notEmpty: {
                              message: 'La Descripción del Producto no puede estar vacía.'
                            },
                            stringLength: {
                              min: 5,
                              max: 200,
                              message: 'Debe introducir al menos 5 caracteres y a lo más 200.'
                            }
                          }
                        },
                        Costo: {
                          selector: '#id_costo, #id_precio',
                          validators: {
                            numeric: {
                              message: 'Debe introducir un valor numérico.'
                            },
                            notEmpty: {
                              message: 'El Costo no puede estar vacío.'
                            }
                          }
                        },
                        unidades: {
                          validators: {
                            notEmpty: {
                              message: 'Debe seleccionar un valor.'
                            }
                          }
                        }
                      }
                    });
                  }         

                  

                  function GeneraCodigo() {
                    $('#codigobarra').val('');
                    $.ajax({
                      url: '/GeneraCodigo',
                      type: 'POST',
                      dataType: 'html',
                      data: {},
                      success: function (data, status) {
                        if (data != '') {
                          $('#codigobarra').focus();
                          $('#codigobarra').val(data);
                          $('#codigobarra').blur();
                          console.log(status);

                        } else {
                          console.log(status);
                        }
                      },
                    });
                  }

                  function MuestradivImagen() {
                    if ($('#divimagen').is(':visible')) {
                      $('#divimagen').hide();
                    } else {
                      $('#divimagen').show();
                    }
                  }

            
            ////////////////////////////////////// SCRIPT DE AGREGAR UNIDADES////////////////////////////////////////////////
            function guardarUnidad(){          

              // Los campos no deben estar vacios
              var campo1 = validaInputUnidades($('#id_tipo_unidad')[0],$('#error_tipo_unidad')[0]);
              var campo2 = validaInputUnidades($('#id_nombre_unidad')[0],$('#error_nom_unidad')[0]);
              var campo3 = validaInputUnidades($('#id_abrv_unidad')[0],$('#error_abrv_unidad')[0]);
              if(campo1 && campo2 && campo3){


                //Inserta opción vía ajax POST
                var nombre = $('#id_nombre_unidad').val();
                var tipo = $('#id_tipo_unidad').val();
                var abreviatura = $('#id_abrv_unidad').val();

                $.ajax({
                  url: '/AltaUnidadDesdeCompras',
                  type: 'POST',
                  dataType: 'html',
                  data: { nombre: nombre,
                          abreviatura: abreviatura,
                          tipo: tipo
                       },
                  success: function (data) {
                      $('#id_unidades').empty(); 
                      $('#id_unidades').append(data);
                      $("#btnmodalUnidad").trigger("click");
                      resetFormularioUnidad();
                  },
                  error: function(data) {
                    alert("Ocurrió un error interno, intenta dar de alta la unidad más tarde.");
                    $('#guardarUnidad').attr("disabled",true);
                  }
                });

              }else{
                $('#guardarUnidad').attr("disabled",true);
              }
              
            }

            function resetFormularioUnidad(){
              $('#id_tipo_unidad').val("");
              $('#id_tipo_unidad')[0].parentNode.parentNode.setAttribute("class","form-group");
              $('#id_nombre_unidad').val("");
              $('#id_nombre_unidad')[0].parentNode.parentNode.setAttribute("class","form-group");
              $('#id_abrv_unidad').val("");
              $('#id_abrv_unidad')[0].parentNode.parentNode.setAttribute("class","form-group");
              
              $('#error_tipo_unidad').empty();
              $('#error_nom_unidad').empty();
              $('#error_abrv_unidad').empty();
            }

            function addBlurUnidad(){
              $( "#id_tipo_unidad" ).blur(function() {
                if(validaInputUnidades($('#id_tipo_unidad')[0],$('#error_tipo_unidad')[0])){
                  $('#guardarUnidad').attr("disabled",false);
                }else{
                  $('#guardarUnidad').attr("disabled",true);                  
                }                
              });
              $( "#id_nombre_unidad" ).blur(function() {
                if(validaInputUnidades($('#id_nombre_unidad')[0],$('#error_nom_unidad')[0])){
                  $('#guardarUnidad').attr("disabled",false);
                }else{
                  $('#guardarUnidad').attr("disabled",true);                  
                }                 
              });
              $( "#id_abrv_unidad" ).blur(function() {
                if(validaInputUnidades($('#id_abrv_unidad')[0],$('#error_abrv_unidad')[0])){
                  $('#guardarUnidad').attr("disabled",false);
                }else{
                  $('#guardarUnidad').attr("disabled",true);                  
                }                
              });

            }

            function validaInputUnidades(elemento,tag_error){
              var estado = false;
              elemento.value = elemento.value.trim()
              
              if(elemento.value === ""){
                elemento.parentNode.parentNode.setAttribute("class","has-error");      
                tag_error.innerText ="El campo No debe estar vacio";                
              }else{
                elemento.parentNode.parentNode.removeAttribute("class","has-error");
                elemento.parentNode.parentNode.setAttribute("class","has-success");
                tag_error.innerText ="";                    
                estado = true;
              }     
              return estado; 
            };






