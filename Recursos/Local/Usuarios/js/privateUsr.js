$(document).ready(function () {
	$("#selector1").click(function () {
		var iscollapsed = $("#cl").hasClass("collapse");
		if (!iscollapsed) {
			$("#cl").addClass("collapse");
		}
		else {
			$("#cl").removeClass("collapse");
		}
	});
	
	$("#eliminar_datos_fiscales").click(function () {
			bootbox.confirm({
			    message: "¿Esta seguro de eliminar los datos de la empresa?",
			    buttons: {
			        confirm: {
			            label: 'Yes',
			            className: 'btn-primary'
			        },
			        cancel: {
			            label: 'No',
			            className: 'btn-danger'
			        }
			    },
			    callback: function (result) {
					if(result)
					{
			        	$.ajax(
						{
	                        url: '/eliminar_datos_fiscales',
	                        type: 'POST',
	                        dataType: 'html',	                        
	                        success: function (data) 
							{
	                          window.location.reload();
	                        },
                      });
					}					
			    }
		});
	});
		
	$("#cancelar_datos_fiscales").click(function () {
		bootbox.confirm(
		{
			message: "¿Realmente desea cancelar el proceso?",
			buttons:
			{	confirm: 
				{	label: 'Yes',
			            className: 'btn-primary'
			        },
			        cancel: {
			            label: 'No',
			            className: 'btn-danger'
			        }
			    },
			    callback: function (result) 
				{
					if(result)
					{
						window.location.reload();
					}					
			    }
		});
	});
												
	$('[data-toggle="tooltip"]').tooltip();
	
	$("#agregarCampo").click(function () {
		if (/^\s*$/.test($("#campoNuevo").val())) {
			alert("Empty field");
		}
		else {
			
			var elemento = ""+
			"<div>"+
			"	<div class='col-md-4' id='div" + $("#campoNuevo").val() + "'>"+
			"		<div  class='input-group input-group-md'>"+
			"			<label for='" + $("#campoNuevo").val() + "' class='input-group-addon'>" + $("#campoNuevo").val() + "</label>"+
			"			<input id='" + $("#campoNuevo").val() + "' name='" + $("#campoNuevo").val() + "' type='text' value='" + $("#valorCampo").val() + "' class='form-control' />"+
			"		</div> "+
			"	</div>"+
			"	<input type='button' value='X' class='deleteButton btn btn-danger'>"+
			"</div>";
			$(elemento).appendTo($("#nuevosCampos"));
			$('html, body').animate({
				scrollTop: $(this).offset().top
			}, 2000);
			$("#campoNuevo").focus();
		}
	});
	$(document).on('click', '.deleteButton', function () {
		$(this).parent().remove();
	});

	$('#defaultForm').bootstrapValidator({
			feedbackIcons: {
					valid: 'glyphicon glyphicon-ok',
					invalid: 'glyphicon glyphicon-remove',
					validating: 'glyphicon glyphicon-refresh'
			},
			err: {
					container: '#alertas'
			},

			fields: {
					nombre: {
						selector:'#nombre',
							message: 'Usuario no válido.',
							validators: {
									notEmpty: {
											message: 'El nombre es requerido, no debe dejarse vacío'
									},
									stringLength: {
											min: 5,
											max: 300,
											message: 'Debe introducir una cadena válida'
									},

							}
					},
					rfc: {
							selector:'#rfc',
							
							message: 'RFC Invalido',
							validators: {
									regexp: {
											regexp: /^((([A-Z]|[a-z]){3})[0-9]{6})((([A-Z]|[a-z]|[0-9]){3}))|^((([A-Z]|[a-z]){4})[0-9]{6})((([A-Z]|[a-z]|[0-9]){3}))|^\s*$/g,
									}
							}
					},
					
					calle: {
							excluded: true,
							selector:'#Domiciliocalle',
					},
					noExterior: {
						selector:'#DomicilionoExterior',
						validators: {
							digits: {
								message: 'Formato numerico requerido.'
							}
						}
					},
					noInterior: {
							excluded: true,
							selector: '#DomicilionoInterior'
					},
					localidad: {
							excluded: true,
							selector:'#Domiciliolocalidad',
					},
					colonia: {
						excluded: true,
						selector:'#Domiciliocolonia',
					},
					municipio: {
							excluded: true,
							selector: '#Domiciliomunicipio'
					},
					estado: {
							validators:{
								notEmpty: {
									message: 'Seleccione un Valor'
								}
							}

					},
					codigoPostal: {
							validators:{
									regexp:{
											message:'El numero debe contener 5 d&iacute;gitos y no 00000',
											regexp: /^(?!(00000))\d{5}$/g
									}
							}
					},
					email:{
							selector:'#email',
							validators:{
									emailAddress: {
										message: 'e-mail inválido.'
									}
							}
					},
					telefono:{							
							validators:{
									regexp:{
											message:'El numero debe contener 10 digitos',
											regexp: /^(?!(000))\d{3}\d{7}$/g
									},
									stringLength: {
											min: 0,
											max: 10,
											message: 'Minimo 10 numeros'
									},
							}
					},
					celular:{
							
							validators:{
									regexp:{
											message:'El numero debe contener 10 digitos',
											regexp: /^(?!(000))\d{3}\d{7}$/g
									},
									stringLength: {
											min: 0,
											max: 10,
											message: 'Minimo 10 numeros'
									},
							}
					},
				    password: {
							
						validators: {
							notEmpty:{
								message: 'Introducir contraseña.'
							},
							stringLength: {
													min: 6,
													max: 12,
													message: 'Debe introducir entre 6 y 12 caracteres'
											},
						}
					 },
					 confirmPassword: {
							
						validators: {
							identical: {
								field: 'password',
								message: 'The password and its confirm are not the same'
							},
							notEmpty:{
								message: 'Introducir contraseña.'
							},
						}
					},
					usuario: {
							
							validators:{
								notEmpty:{
									message: 'Un password es requerido.'
								},
								stringLength: {
									min: 6,
									max: 12,
									message: 'Debe introducir entre 6 y 12 caracteres'
								}
							}
					}
				
        }

	});	

});


