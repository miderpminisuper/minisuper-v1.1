$( document ).ready(function() {
    console.log("¡ready!");
    message('¡Wow! ¡lo has hecho estupendo!',1); 
    //message('¡Ups! ¡Hicimos algo mal! ¡No se lo digas a nadie!¡Lo arreglaremos!',0);
    message('¡Wow! ¡el proceso fue de pelos!',1);

	/*
	 *Estilo para los menus principales.
	 */
    $("[rel='tooltip']").tooltip();    
    $('.thumbnail').hover(
        function(){
            $(this).find('.caption').fadeIn(250)
        },
        function(){
            $(this).find('.caption').fadeOut(205)
        }
    ); 

    /*
     * Funcion salir
     */
    $( "#logout" ).click(function() {
        bootbox.confirm({
        title: "Mensaje",
        message: "<strong>¡Hola!</strong>, ¿Te vas tan pronto?",
        buttons: {
            confirm: {
                label: 'Si',
                className: 'btn-info'
            },
            cancel: {
                label: 'Cancelar',
                className: 'btn-danger'
            }
        },
        callback: function (result) {
            console.log('¿Te vas tan pronto?: ' + result);
            if(result == true){
                $.ajax({
                /*la URL para la petición*/
                /*url : '/users',*/
                url : '/logout',
                /*la información a enviar(también es posible utilizar una cadena de datos)*/
                data : { id : 123 },    
                /*especifica si será una petición POST o GET*/
                type : 'GET',    
                /*el tipo de información que se espera de respuesta*/
                dataType : 'html',    
                /*código a ejecutar si la petición es satisfactoria;la respuesta es pasada como argumento a la función*/
                success : function(result) {
                    console.log('success OK');
                    location.reload();
                },
                /*código a ejecutar si la petición falla; son pasados como argumentos a la función el objeto de la petición en crudo y código de estatus de la petición*/
                error : function(xhr, status) {
                    console.log('Disculpe, existió un problema');
                },    
                /*código a ejecutar sin importar si la petición falló o no*/
                complete : function(xhr, status) {
                    console.log('Proceso terminado');
                }
                });
            }else{
                //alert('Gracias.');
            }

        }
        });
    });
    /**
     * MENU VENTAS
     */
    $( "#menuventas" ).click(function() {
        $.ajax({
        /*la URL para la petición*/
        /*url : '/users',*/
        url : '',
        /*la información a enviar(también es posible utilizar una cadena de datos)*/
        data : { id : 123 },    
        /*especifica si será una petición POST o GET*/
        type : 'GET',    
        /*el tipo de información que se espera de respuesta*/
        dataType : 'html',    
        /*código a ejecutar si la petición es satisfactoria;la respuesta es pasada como argumento a la función*/
        success : function(result) {
            console.log('success OK');
            $("#contenedor").html('<div class="alert alert-info" role="alert">Aquí el módulo de VENTAS</div>')
        },
        /*código a ejecutar si la petición falla; son pasados como argumentos a la función el objeto de la petición en crudo y código de estatus de la petición*/
        error : function(xhr, status) {
            console.log('Disculpe, existió un problema');
        },    
        /*código a ejecutar sin importar si la petición falló o no*/
        complete : function(xhr, status) {
            console.log('Proceso terminado');
        }
        });
    });
	
    /**
 * @melchormendoza
 * {menu compras}
 * Funcion que trae la vista de compras a la pagina principal.
 * @params: 
 * @return:Vista HTML
 */
$( "#menucompras" ).click(function() {
    $.ajax({
    /*la URL para la petición*/
    url : '',
    /*la información a enviar(también es posible utilizar una cadena de datos)*/
    data : { id : 123 },    
    /*especifica si será una petición POST o GET*/
    type : 'post',    
    /*el tipo de información que se espera de respuesta*/
    dataType : 'html',    
    /*código a ejecutar si la petición es satisfactoria;la respuesta es pasada como argumento a la función*/
    success : function(result) {
        console.log('success OK');
        $("#contenedor").html('<div class="alert alert-info" role="alert">Aquí el módulo de COMPRAS</div>')
    },
    /*código a ejecutar si la petición falla; son pasados como argumentos a la función el objeto de la petición en crudo y código de estatus de la petición*/
    error : function(xhr, status) {
        console.log('Disculpe, existió un problema');
    },    
    /*código a ejecutar sin importar si la petición falló o no*/
    complete : function(xhr, status) {
        console.log('Proceso terminado');
    }
    });
});

/**
 * @melchormendoza
 * {menu compras}
 * Funcion que trae la vista de inventarios a la pagina principal.
 * @params: 
 * @return:Vista HTML
 */
$( "#menuinventario1, #menuinventario2" ).click(function() {
    $.ajax({
    /*la URL para la petición*/
    url : 'inventario',
    /*la información a enviar(también es posible utilizar una cadena de datos)*/
    data : { id : 123 },    
    /*especifica si será una petición POST o GET*/
    type : 'post',    
    /*el tipo de información que se espera de respuesta*/
    dataType : 'html',    
    /*código a ejecutar si la petición es satisfactoria;la respuesta es pasada como argumento a la función*/
    success : function(result) {
        $("#contenedor").html(result);			
		$( "#tabs" ).tabs().addClass( "ui-tabs-vertical ui-helper-clearfix" );
		$( "#tabs li" ).removeClass( "ui-corner-top" ).addClass( "ui-corner-left" );		
    },
    /*código a ejecutar si la petición falla; son pasados como argumentos a la función el objeto de la petición en crudo y código de estatus de la petición*/
    error : function(xhr, status) {
        console.log('Disculpe, existió un problema');
    },    
    /*código a ejecutar sin importar si la petición falló o no*/
    complete : function(xhr, status) {
        console.log('Proceso terminado');
    }
    });	
});

/*
 *Autor: Ramon Cruz Juarez
 *Segemento que realiza la llamada al menú de compras por medio de ajax
 *
*/
$( "#menucompras, #menucompras2" ).click(function() {
	if(verificaAcceso($('#username').html(),"menucompras"))
	{		
	    $.ajax({
	    /*la URL para la petición*/
	    url : 'modulocompras',  
	    /*el tipo de información que se espera de respuesta*/
	    dataType : 'html',    
	    /*código a ejecutar si la petición es satisfactoria;la respuesta es pasada como argumento a la función*/
	    success : function(result) {
	        $("#contenedor").html(result);	

			$( "#tabs" ).tabs().addClass( "ui-tabs-vertical ui-helper-clearfix" );
			$( "#tabs li" ).removeClass( "ui-corner-top" ).addClass( "ui-corner-left" );					
	    },
	    /*código a ejecutar si la petición falla; son pasados como argumentos a la función el objeto de la petición en crudo y código de estatus de la petición*/
	    error : function(xhr, status) {
	        console.log('Disculpe, existió un problema');
	    },    
	    /*código a ejecutar sin importar si la petición falló o no*/
	    complete : function(xhr, status) {
	        console.log('Proceso terminado');
	    }
	    });
	}
	else
		alert("Acceso degenago...");
});

/*
 *Autor: Ramon Cruz Juarez
 *Segemento que realiza la llamada al menú de ventas por medio de ajax
 *
*/
$( "#menuventas, #menuventas2" ).click(function() {
	if(verificaAcceso($('#username').html(),"menuventas"))
	{		
	    $.ajax({
	    /*la URL para la petición*/
	    url : 'moduloventas',  
	    /*el tipo de información que se espera de respuesta*/
	    dataType : 'html',    
	    /*código a ejecutar si la petición es satisfactoria;la respuesta es pasada como argumento a la función*/
	    success : function(result) {
	        $("#contenedor").html(result);	

			$( "#tabs" ).tabs().addClass( "ui-tabs-vertical ui-helper-clearfix" );
			$( "#tabs li" ).removeClass( "ui-corner-top" ).addClass( "ui-corner-left" );					
	    },
	    /*código a ejecutar si la petición falla; son pasados como argumentos a la función el objeto de la petición en crudo y código de estatus de la petición*/
	    error : function(xhr, status) {
	        console.log('Disculpe, existió un problema');
	    },    
	    /*código a ejecutar sin importar si la petición falló o no*/
	    complete : function(xhr, status) {
	        console.log('Proceso terminado');
	    }
	    });
	}
	else
		alert("Acceso degenago...");
});

/*
 *Autor: Ramon Cruz Juarez
 *Segemento que realiza la llamada a los artículos por medio de ajax
 *
*/
$( "#menuproductos" ).click(function() {
	if(verificaAcceso($('#username').html(),"menuproductos"))
	{
    	$.ajax({
    /*la URL para la petición*/
    url : 'productos',  
    /*el tipo de información que se espera de respuesta*/
    dataType : 'html',    
    /*código a ejecutar si la petición es satisfactoria;la respuesta es pasada como argumento a la función*/
    success : function(result) {
        $("#contenedor").html(result);			
    },
    /*código a ejecutar si la petición falla; son pasados como argumentos a la función el objeto de la petición en crudo y código de estatus de la petición*/
    error : function(xhr, status) {
        console.log('Disculpe, existió un problema');
    },    
    /*código a ejecutar sin importar si la petición falló o no*/
    complete : function(xhr, status) {
        console.log('Proceso terminado');
    }
    });
	}
	else
		alert("Acceso degenago...");
});

/**
 * @melchormendoza
 * {menu compras}
 * Funcion que trae la vista de inventarios a la pagina principal.
 * @params: 
 * @return:Vista HTML
 */
	
$( "#menuadministracion, #menuadministracion2, #menuadministracion3" ).click(function() {
	if(verificaAcceso($('#username').html(),"menuadministracion"))
	{
    	$.ajax({
    /*la URL para la petición*/
    url : '/administracion',
    /*la información a enviar(también es posible utilizar una cadena de datos)*/
    data : { id : 123 },    
    /*especifica si será una petición POST o GET*/
    type : 'post',    
    /*el tipo de información que se espera de respuesta*/
    dataType : 'html',    
    /*código a ejecutar si la petición es satisfactoria;la respuesta es pasada como argumento a la función*/
    success : function(result) {
        $("#contenedor").html(result);			
		$( "#tabs" ).tabs().addClass( "ui-tabs-vertical ui-helper-clearfix" );
		$( "#tabs li" ).removeClass( "ui-corner-top" ).addClass( "ui-corner-left" );	
	
		$( "#tabs_secundario" ).tabs().addClass( "ui-tabs-horizontal ui-helper-clearfix" );
	    $( "#tabs_secundario" ).tabs({
		      collapsible: true
    	});	
    },
    /*código a ejecutar si la petición falla; son pasados como argumentos a la función el objeto de la petición en crudo y código de estatus de la petición*/
    error : function(xhr, status) {
        console.log('Disculpe, existió un problema');
    },    
    /*código a ejecutar sin importar si la petición falló o no*/
    complete : function(xhr, status) {
        console.log('Proceso terminado');
    }
    });
	}
	else
		alert("Acceso degenago...");
});

function verificaAcceso(usuario, elemento)
{
	//Conectarse por ajax a Golang
	//Golang se conecta con redis
	//devuelve true/false dependiendo si el usuario tiene permiso al identificador
	if(usuario=="ramon")
		return true
	else
		return false
}

/**
 * @melchormendoza
 * Trae el layout de usuarios
 */
$( "#menuusuarios" ).click(function() {
    alert("aqui estoy");
    $.ajax({
    /*la URL para la petición*/
    url : '/users',
    /*la información a enviar(también es posible utilizar una cadena de datos)*/
     
    /*especifica si será una petición POST o GET*/
    type : 'post',    
    /*el tipo de información que se espera de respuesta*/
    dataType : 'html',    
    /*código a ejecutar si la petición es satisfactoria;la respuesta es pasada como argumento a la función*/
    success : function(result) {
        console.log('success OK');
        $("#contenedor").html('<div class="alert alert-info" role="alert">Aquí el módulo de USUARIOS</div>')
    },
    /*código a ejecutar si la petición falla; son pasados como argumentos a la función el objeto de la petición en crudo y código de estatus de la petición*/
    error : function(xhr, status) {
        console.log('Disculpe, existió un problema');
    },    
    /*código a ejecutar sin importar si la petición falló o no*/
    complete : function(xhr, status) {
        console.log('Proceso terminado');
    }
    });
});
$( "#menudescuentos" ).click(function() {
    $.ajax({
    /*la URL para la petición*/
    url : '',
    /*la información a enviar(también es posible utilizar una cadena de datos)*/
    data : { id : 123 },    
    /*especifica si será una petición POST o GET*/
    type : 'post',    
    /*el tipo de información que se espera de respuesta*/
    dataType : 'html',    
    /*código a ejecutar si la petición es satisfactoria;la respuesta es pasada como argumento a la función*/
    success : function(result) {
        console.log('success OK');
        $("#contenedor").html('<div class="alert alert-info" role="alert">Aquí el módulo de DESCUENTOS</div>')
    },
    /*código a ejecutar si la petición falla; son pasados como argumentos a la función el objeto de la petición en crudo y código de estatus de la petición*/
    error : function(xhr, status) {
        console.log('Disculpe, existió un problema');
    },    
    /*código a ejecutar sin importar si la petición falló o no*/
    complete : function(xhr, status) {
        console.log('Proceso terminado');
    }
    });
});
/**
 * @melchormendoza
 * Trae el layout de facturacion.
 * params:
 * return: view
 */
$( "#menufacturacion" ).click(function() {
    $.ajax({
    /*la URL para la petición*/
    url : '',
    /*la información a enviar(también es posible utilizar una cadena de datos)*/
    data : { id : 123 },    
    /*especifica si será una petición POST o GET*/
    type : 'post',    
    /*el tipo de información que se espera de respuesta*/
    dataType : 'html',    
    /*código a ejecutar si la petición es satisfactoria;la respuesta es pasada como argumento a la función*/
    success : function(result) {
        console.log('success OK');
        $("#contenedor").html('<div class="alert alert-info" role="alert">Aquí el módulo de FACTURACIÓN</div>')
    },
    /*código a ejecutar si la petición falla; son pasados como argumentos a la función el objeto de la petición en crudo y código de estatus de la petición*/
    error : function(xhr, status) {
        console.log('Disculpe, existió un problema');
    },    
    /*código a ejecutar sin importar si la petición falló o no*/
    complete : function(xhr, status) {
        console.log('Proceso terminado');
    }
    });
});
	/*
	 *@melchor
	 *Muestra un modal con el detalle de usuarios.
	 */
	$("#usuariodetalle").click(function(){ alert('hola Ramon');
        $.ajax({
        /*la URL para la petición*/
        /*url : '/users',*/
        url : '',
        /*la información a enviar(también es posible utilizar una cadena de datos)*/
        data : { id : 123 },    
        /*especifica si será una petición POST o GET*/
        type : 'GET',    
        /*el tipo de información que se espera de respuesta*/
        dataType : 'html',    
        /*código a ejecutar si la petición es satisfactoria;la respuesta es pasada como argumento a la función*/
        success : function(result) {
            console.log('success OK');
            var dialog = bootbox.dialog({
			    title: 'Detalle de usuario',
			    message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>'
			});
			dialog.init(function(){
			    setTimeout(function(){
			        dialog.find('.bootbox-body').html('I was loaded after the dialog was shown!');
			    }, 3000);
			});
        },
        /*código a ejecutar si la petición falla; son pasados como argumentos a la función el objeto de la petición en crudo y código de estatus de la petición*/
        error : function(xhr, status) {
            console.log('Disculpe, existió un problema');
        },    
        /*código a ejecutar sin importar si la petición falló o no*/
        complete : function(xhr, status) {
            console.log('Proceso terminado');
        }
        });
});

    });
    
            /*
            * @melchor
            * Función que maneja los mensajes de error
            * @params msg:texto del mensaje,st:estado del mensaje
            * @return mensaje jGrowl
             */
            function message(msg,st) 
            {
                if(st=='0'){
                    var tema = 'jGrowl-error';
                    var stk = true;
                }else if(st=='1'){
                    var tema = 'jGrowl-succes';
                    var stk = false;
                }
            $.jGrowl(msg, {
                    header: 'Mensaje:',
                    theme: tema,
                    sticky:stk,
                    life:'2000'
                });
            
            }
            function reload(){
                location.reload();
            }
			function usuarioDetalle(){
				$.ajax({
		        /*la URL para la petición*/
		        /*url : '/users',*/
		        url : '',
		        /*la información a enviar(también es posible utilizar una cadena de datos)*/
		        data : { id : 123 },    
		        /*especifica si será una petición POST o GET*/
		        type : 'GET',    
		        /*el tipo de información que se espera de respuesta*/
		        dataType : 'html',    
		        /*código a ejecutar si la petición es satisfactoria;la respuesta es pasada como argumento a la función*/
		        success : function(result) {
		            console.log('success OK');
		            var dialog = bootbox.dialog({
					    title: 'Detalle de usuario',
					    message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>'
					});
					dialog.init(function(){
					    setTimeout(function(){
					        dialog.find('.bootbox-body').html('I was loaded after the dialog was shown!');
					    }, 3000);
					});
		        },
		        /*código a ejecutar si la petición falla; son pasados como argumentos a la función el objeto de la petición en crudo y código de estatus de la petición*/
		        error : function(xhr, status) {
		            console.log('Disculpe, existió un problema');
		        },    
		        /*código a ejecutar sin importar si la petición falló o no*/
		        complete : function(xhr, status) {
		            console.log('Proceso terminado');
		        }
		        });
			}