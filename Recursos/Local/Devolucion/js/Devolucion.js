$(document).ready(function(){
/*
$('.fecha').datepicker({
    language: "es"
});
*/
$(document).on('click', '.number-spinner button', function () {    
	var btn = $(this),
		oldValue = btn.closest('.number-spinner').find('input').val().trim(),
		newVal = 0;
	if (btn.attr('data-dir') == 'up') {
		newVal = parseInt(oldValue) + 1;
	} else {
		if (oldValue > 1) {
			newVal = parseInt(oldValue) - 1;
		} else {
			newVal = 0;
		}
	}
	btn.closest('.number-spinner').find('input').val(newVal);
});
/*
$("#nueva_forma").click(function(){

		$.ajax({
			url: '/crearformapago',
			type: 'POST',
			dataType: 'html',
			data:{},
			success : function(data){
				$('#contenedor2').html(data);
				document.getElementById("comision_forma").value="0.0";
			}
		});
	});
*/
});
/*
function GuardarFormaPago(){

	var nombre = document.getElementById("nombre_nueva_forma").value;
	var comision = document.getElementById("comision_forma").value;
	var cambio = $('input:radio[name=cambio_nueva_forma]:checked').val(); 
	var status = document.getElementById("estatus").value;
	

	if (nombre == ""){
		alert("Inserta un nombre para esta forma de pago.");
	}else{

		$.ajax({
			url: '/guardarformadepago',
			type: 'POST',
			dataType: 'html',
			data:{nombre_pago : nombre,	cambio_pago : cambio, comision_pago:comision, estatus : status },
			success : function(data){


				if (data  == "Insertado"){
					alert("Forma de pago dada de alta.");
					location.reload("/formadepago")
				}else{
					alert("Ocurrio un error");
				}
			}
		});
		
	}
	
}
*/
function GetForma(){
	var meta = $("#idDocumento").val();
	if (meta == ""){
		alert("¡Debes seleccionar un Documento!");
	}else{	
		$.ajax({
			url: '/getDocumentoDev',
			type: 'POST',
			dataType: 'html',
			data:{ id : meta},
			success : function(data){
				$('#contenedor1').html(data);				
			}
		});
	}
}
/*

*/
/*
function getDocumento(id){
	if (id == ""){
		alert("¡Debes seleccionar un SKU!");
	}else{
		$.ajax({
			url: '/getDocumento',
			type: 'POST',
			dataType: 'html',
			data:{ id : id},
			success : function(data){
				$('#documento').html(data);
			}
		});
	}
}
*/

function guardaDevolucion(idDocumento){
	//Parametros de entrada
	var tipoMovimiento = $('input:radio[name=movimiento]:checked').val()
	var idAlmacen = $("#idAlmacen").val();
	if(tipoMovimiento == 'devolucion'){
		var idAlmacen = 1;	
	}
	//Valido que se haya seleccionado algun almacen para garantia
	//en caso de ser devolucion, le asigno 1, para que automaticamente vaya validado
	//Aunque este valor no se usa en la insercion del movimiento
	if(idAlmacen != 0){
		//var JSONObject =  new Object;
		//JSONObject.DEVOLUCION = new Array;

		var i = 0;
		articulos = []
		devoluciones = []
		precios = []
		$("table#devolucion tbody tr").each(function() {
			var devolucion = $("input#dev"+this.id).val();
			var precio = $("input#pre"+this.id).val();
			//JSONObject.DEVOLUCION[i]= new Object;
		    //JSONObject.DEVOLUCION[i].idDocumento = idDocumento;
			//JSONObject.DEVOLUCION[i].idArticulo = this.id;
			//JSONObject.DEVOLUCION[i].cantidad = devolucion;
			//Validamos solo articulos donde devolucion sea mayor a cero.
			if(devolucion > 0){
				articulos.push(this.id)
				devoluciones.push(devolucion)
				precios.push(precio)
			}
			i++;
		});
		//console.log(JSONObject);
		//var JSONstring = JSON.stringify(JSONObject);
		if(articulos.length>0){
			$.ajax({
					url: '/setDocumentoDev',
					type: 'POST',
					dataType: 'html',
					data:{idDocumento:idDocumento,tipoMovimiento:tipoMovimiento,idAlmacen:idAlmacen,articulos:articulos,devoluciones:devoluciones,precios:precios},
					success : function(data){
						if (data  != "-1"){
							alert("¡Movimiento registrado correctamente!");
							getDocumento(data);
							//location.reload("/devolucion")
						}else{
							alert("Ocurrio un error");
						}
					}
				});
		}else{alert('¡No has ingresado cantidad a devolver en ningun artículo!')}		
	}else{alert('¡Debes seleccionar un almacen!')}
}

function activaAlmacen(){
	var mov = $('input:radio[name=movimiento]:checked').val()
	if(mov=='garantia'){
		$("#almacenes").css("display","block");
	}else{
		$("#almacenes").css("display","none");
	}
}

function cambiaImporte(idArticulo,max,valor){

	var precio = $("input#pre"+idArticulo).val();
	var cantidad = parseInt($("input#dev"+idArticulo).val()) + parseInt(valor);

	if (cantidad >= max){
		$("#dev"+idArticulo).val(max-1)
		cantidad = max - 1;
	}
	if(cantidad <= 0){
		$("#imp"+idArticulo).attr("value",0)
	}else{
		$("#imp"+idArticulo).attr("value",parseFloat(precio)*parseFloat(cantidad))
	}
}
function sleep(milliseconds) {
  var start = new Date().getTime();
  for (var i = 0; i < 1e7; i++) {
    if ((new Date().getTime() - start) > milliseconds){
      break;
    }
  }
}
function simulaOnKey(idArticulo){
	$("#dev"+idArticulo).trigger("onkeydown");
}
function getDocumento(id){
	if (id == ""){
		alert("¡Debes seleccionar un SKU!");
	}else{
		$("#modal-documento").modal();
		$.ajax({
			url: '/getDocumento',
			type: 'POST',
			dataType: 'html',
			data:{ id : id},
			success : function(data){
				$('#documento').html(data);
			}
		});
	}
}
function imprimir() {
    //$("#documento").print(/*options*/);
	$.print("#documento");
}