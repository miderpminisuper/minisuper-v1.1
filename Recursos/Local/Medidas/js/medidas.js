$(document).ready(function(){

	$("body").on("click",".eliminar", function(e){
		var numcampos = $("#contenedor1 .claves").length;
		if( numcampos > 0 ) {
        	$(this).parent('div').remove();
        }
        return false;
   	});

});

function GetMedida(){
	var meta = $("#s_medida option:selected").val();

	if (meta == "Seleccione Medida.."){
		$('#contenedor1').empty();
		$('#contenedorguardar').empty();
	}else{	
		
		$.ajax({
			url: '/editarmedida',
			type: 'POST',
			dataType: 'html',
			data:{ id : meta},
			success : function(data){
				$('#contenedorguardar').empty();
				$('#contenedor1').html(data);
				$('#contenedorguardar').append('<div class="input-group input-group-md"><span class="input-group-addon"><input type="button" class="btn btn-lg btn-success" onClick="GuardarMedida();" value="Guardar Medida" name="guardarmedida" id="guardarmedida"></span></div>');
			}
		});
	}
}

function Agregarcampomedida(id){
	var contenedor = $("#contenedor1");
	var numcampos = $("#contenedor1 .claves").length;

	$(contenedor).append('<div class="input-group input-group-md claves clearfix" id="valores_'+numcampos+'"><span class="input-group-addon">Nombre:</span><input type="text" name="nombre_'+numcampos+'" id="nombre_'+numcampos+'" value="" class="form-control nombre"><span class="input-group-addon">Abreviatura: </span><input type="text" name="valor_'+numcampos+'" id="valor_'+numcampos+'" value="" class="form-control valor"><span class="input-group-addon btn btn-danger eliminar">-</span></div></div>');

}

function GuardarMedida(){
	var id = $("#id_").val();
	var meta_medida = $("#grupo_").val();
	var nombres = []
	var abreviaturas = []
	var numcampos_extras = $("#contenedor1 .claves").length;
	
	if (numcampos_extras > 0){
		i = 0;
		$("#contenedor1 div.claves").each(function(){
			var nombre = document.getElementById("nombre_"+i).value;
			var valor = document.getElementById("valor_"+i).value;					
			nombres.push(nombre);
			abreviaturas.push(valor);						
			i++;
		});		
	}

		$.ajax({
			url: '/guardarmedida',
			type: 'POST',
			dataType: 'html',
			data:{ id :id, meta_medida : meta_medida, nombres : nombres, abreviaturas : abreviaturas},
			success : function(data){
					if (data  == "Insertado"){
						alert("Operación Exitosa");
						location.reload("/medidas_frame");
					}else{
						alert("Ocurrio un error");
					}
				
			}
		});

}

function AgregarMedida(){

	var contenedor = $("#contenedornewmedida");
	$('#contenedornewmedida').empty();

	$.ajax({
		url: '/newmedida',
		type: 'POST',
		dataType: 'html',
		data:{},
		success : function(data){
			$(contenedor).append(data);
		}
	});

	

}


function GuardarMedidaNueva(){
	var meta_medida = $("#nombre_grupo").val();
	

	console.log(meta_medida);
	

	if (meta_medida == ""){
		alert("Debes introducir un nombre para la nueva meta-unidad");

	}else{

		$.ajax({
		url: '/newermedida',
		type: 'POST',
		dataType: 'html',
		data:{meta_medida : meta_medida},
		success : function(data){
			if (data  == "Insertado"){
				alert("Operación Exitosa");
				location.reload("/medidas_frame");
			}else{
				alert("Ocurrio un error");
			}
			
		}
	});

	}
}

function EliminarMedida(){
	var medida = $("#s_medida option:selected").val();

	if (medida =="Seleccione Medida.."){
		alert("Ninguna MEDIDA seleccionada");
	}else{
		var nombre = document.getElementById("grupo_").value;
		var r = confirm("Deseas eliminar "+nombre+" del inventario:");
		if (r == true) {

			$.ajax({
					url: '/eliminarmedida',
					type: 'POST',
					dataType: 'html',
					data:{id:medida},
					success : function(data){
						if (data  == "Eliminado"){
							alert("Medida eliminada");
							location.reload("/medidas_frame")
						}else{
							alert("Ocurrio un error");
						}
					}
				});
		    
		} else {
		    location.reload("/medidas_frame")
		}
	} 

}