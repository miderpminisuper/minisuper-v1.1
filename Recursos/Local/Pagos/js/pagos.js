$(document).ready(function(){

	$("#nueva_forma").click(function(){

		$.ajax({
			url: '/crearformapago',
			type: 'POST',
			dataType: 'html',
			data:{},
			success : function(data){
				$('#contenedor2').html(data);
				document.getElementById("comision_forma").value="0.0";
			}
		});
	});

});



function GuardarFormaPago(){

	var nombre = document.getElementById("nombre_nueva_forma").value;
	var comision = document.getElementById("comision_forma").value;
	var cambio = $('input:radio[name=cambio_nueva_forma]:checked').val(); 
	var status = document.getElementById("estatus").value;
	var codigo_sat_n = document.getElementById("codigo_sat_n").value;
	var porcentaje = $('input:radio[name=porcentaje_nueva_forma]:checked').val(); 
	

	if (nombre == ""){
		alert("Inserta un nombre para esta forma de pago.");
	}else{

		$.ajax({
			url: '/guardarformadepago',
			type: 'POST',
			dataType: 'html',
			data:{nombre_pago : nombre,	cambio_pago : cambio, comision_pago:comision, estatus : status, codigo_sat_n:codigo_sat_n, porcentaje :porcentaje },
			success : function(data){

				if (data  == "Insertado"){
					alert("Forma de pago dada de alta.");
					location.reload("/formadepago")
				}else{
					alert("Ocurrio un error");
				}
			}
		});
		
	}
	
}


function GetForma(){
	var meta = $("#s_forma_pago option:selected").val();

	if (meta == "Seleccione forma de pago.."){

		$('#contenedor1').empty();
		$('#contenedorguardar').empty();

	}else{	
		
		$.ajax({
			url: '/editarformapago',
			type: 'POST',
			dataType: 'html',
			data:{ id : meta},
			success : function(data){
				$('#contenedorguardar').empty();
				$('#contenedor1').html(data);
				$('#contenedorguardar').append('<div class="input-group input-group-md"><span class="input-group-addon"><input type="button" class="btn btn-lg btn-success" onClick="GuardarFormaEditada();" value="Guardar Forma de Pago" name="guardareditar" id="guardareditar"></span></div>');

			}
		});
	}
}


function GuardarFormaEditada(){


	var id = document.getElementById("id_").value;
	var nombre = document.getElementById("nombre_forma_pago").value;
	var comision = document.getElementById("comision_").value;
	var cambio = $('input:radio[name=cambio_]:checked').val();
	var estatus = $("#estatus_edit option:selected").val();
	var codigo_sat = document.getElementById("codigo_sat").value;
	var porcentaje_e = document.getElementById("porcentaje_").value;

	$.ajax({
			url: '/guardareditarformapago',
			type: 'POST',
			dataType: 'html',
			data:{id:id, nombre_pago : nombre,	cambio_pago : cambio, comision_pago:comision, estatus : estatus, codigo_sat:codigo_sat , porcentaje:porcentaje_e},
			success : function(data){
				if (data  == "Insertado"){
					alert("Forma de pago modificada.");
					location.reload("/formadepago")
				}else{
					alert("Ocurrio un error");
				}
			}
		});


}

function EliminarForma(){
	var formadepago = $("#s_forma_pago option:selected").val();

	if (formadepago =="Seleccione forma de pago.."){
		alert("Debes seleccionar una forma de pago primero.");
	}else{
		var nombre = document.getElementById("nombre_forma_pago").value;
		var r = confirm("Deseas eliminar "+nombre+" del inventario:");
		if (r == true) {

			$.ajax({
					url: '/eliminarformapago',
					type: 'POST',
					dataType: 'html',
					data:{id:formadepago},
					success : function(data){
						if (data  == "Eliminado"){
							alert("Forma de pago eliminada.");
							location.reload("/formadepago_ifame")
						}else{
							alert("Ocurrio un error");
						}
					}
				});
		    
		} else {
		    location.reload("/formadepago_ifame")
		}
	} 

}