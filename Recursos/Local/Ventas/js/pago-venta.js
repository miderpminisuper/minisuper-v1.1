$(document).ready(function(){

$('#id_respuesta').hide();

   $('#formcliente').submit(function(e){
	  e.preventDefault();
		var datos = $("#refusuario").val();
    BuscaClientes(datos);
  });

	$("#pay-end-venta").click(function(){
		$('#paraBoton').empty();

		var cotizacion = $('#id_cotizacion_unico').val();
		if(cotizacion != ''){

		$("#formas-de-pago-all").empty();
		$("#historial").empty();
		$("#total-pago").empty();
		$('#historial-resta').empty();
		$("#faltante-cambio").empty();



		$.ajax({
			url: '/getformasdepago', 
			type: 'POST',
			dataType: 'json',
			data : {cotizacion : cotizacion},
			success : function(data) {
				var subtotal = '$' + data["Subtotal"].toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");

				$('#lgFormGroupInput2').text(subtotal);
				$('#lgFormGroupInput').val(data["Subtotal"]);
				$('#forma-pago-s').html(data["FormasDePago"]);
				$('#forma-pago-s').selectpicker('render');
				$('#forma-pago-s').selectpicker('refresh');
				$("#pay-end-venta-modal").trigger("click");
			},
		});
		}else{
			alert("Debe Tener Artículos en el carrito o no se ha creado el identificador de cotización adecuadamente, favor de intentar de nuevo o vuelva a comenzar la venta");
		}

	});


	$('#forma-pago-s').on('changed.bs.select', function (e) {
	 	var array_select = $(this).selectpicker('val');
		var array_divs = [];

		$( "#formas-de-pago-all div.formapago" ).each(function( index ) {
			var id_div = $(this).attr("id");
			array_divs.push(id_div);
		});

		$.each(array_divs,function(key,value){
			var ex = array_select.indexOf(value);
			if(ex < 0){
				var deleteid = "#"+value;
				$( "#formas-de-pago-all div.formapago" ).remove( deleteid);
			}
		});

		//codigo para cargar tipo de pago
		$( "#formas-de-pago-all div.formapago" ).each(function( index ) {
			var id_div = $(this).attr("id");
			$.each( array_select, function( key, value ) {
				if (value == id_div){
					delete array_select[key];
				}
			});
		});

		$.ajax({
			url: '/loadformapagodiv', //funcion RealizarPagoConFormaDePago
			type: 'POST',
			dataType: 'html',
			data : {array_select: array_select},
			success : function(data) {
				$("#formas-de-pago-all").append(data);

				var subtotal = $('#lgFormGroupInput').val();
				var arraySuma  = [];

				arraySuma.push(subtotal);

					var arrayEnte = [];
					var arraySub = [];
					var tamanio = 0;
				$( ".formas-de-pago-all div.formapago fieldset.form-group div input.add-value" ).each(function( index ) {
					tamanio += 1;
					var value_add = $(this).val();
					var porcentaje = $(this).attr("porcentaje");
					var nombre = $(this).attr("nombre");
					var value_add2 = Number(value_add);
					var aux = [];
					aux.push(value_add2);
					aux.push(porcentaje);
					aux.push(nombre);
					arrayEnte.push(aux);

					arraySuma.push(value_add2);
				});

				sumarArray(subtotal, arraySuma, arrayEnte,tamanio);

			},
		});



	});

});

function sumarArray(subtotal, array, ente, tamanio){
	$.ajax({
		url: '/addvaluetotal',
		type: 'POST',
		dataType: 'html',
		data : {arrayAdd: array,
				subtotal: subtotal,
				ente : ente,
				length: tamanio
				},
		success : function(data) {
			$("#total-pago").html(data);
			blurRestarValor();
		},
	});
}

function Cargarhistorial(){
	$("#historial").empty();
	$( "#formas-de-pago-all div.formapago fieldset.form-group div input.add-value" ).each(function( index ) {
		var value_add = $(this).val();
		var comname = $(this).attr("nombre");
		var porcentaje = $(this).attr("porcentaje");
		var total = $('#pago_total').val();
		if (porcentaje =="true"){
			$('#historial').append('<h4 class="display-7">+'+ comname +':'+ value_add +'%</h4>');
		}else{
			$('#historial').append('<h4 class="display-7">+'+ comname +':$'+ Number(value_add).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") +'</h4>');
		}
	});
}

function blurRestarValor() {
	var arrayEnte = [];
	var arraySub = [];
	var total = $('#lgFormGroupInput').val();
	var tamanio = 0;

	//$("#historial-resta").empty();
	$( "#formas-de-pago-all div.formapago fieldset.form-group div input.subtract-value" ).each(function( index ) {
		tamanio += 1;
		var value_sub = $(this).val();
		validaPago($(this)[0],$('#error_pago')[0]);
		var comname = $(this).attr("nombre");
		var cambio = $(this).attr("cambio");
		var value_sub2 = Number(value_sub);
		var aux = [];
		aux.push(value_sub2);
		aux.push(cambio);
		aux.push(comname);
		arrayEnte.push(aux);
		arraySub.push(value_sub2);
	});

					var arrayEntex = [];
					var tamaniomonto = 0;

				$( ".formas-de-pago-all div.formapago fieldset.form-group div input.add-value" ).each(function( index ) {
					tamaniomonto += 1;
					var value_add = $(this).val();
					var porcentaje = $(this).attr("porcentaje");
					var nombre = $(this).attr("nombre");
					var value_add2 = Number(value_add);
					var aux = [];
					aux.push(value_add2);
					aux.push(porcentaje);
					aux.push(nombre);
					arrayEntex.push(aux);
				});

	var subtotal = $('#lgFormGroupInput').val();

	$.ajax({
		url: '/subvaluetotal',
		type: 'POST',
		dataType: 'json',
		data : {
				 arraySub: arraySub,
				 total: total,
				 ente : arrayEnte,
				 length : tamanio,
				 lengthmonto : tamaniomonto,
				 ent : arrayEntex,
				 subtotal: subtotal
		},
		success : function(data) {
			if(data != null){
			$("#faltante-cambio").html(data["Resto"]);
			$("#total-pago").html(data["TotalaPagar"]);
			$('#historial-resta').html(data["HistorialMenos"]);
			$('#historial').html(data["HistorialMas"]);
			$('#paraBoton').html(data["Boton"]);
			}
		},
	});

}


function cerrarPago(){

	var arrayEnte = [];
	var arraySub = [];
	var total = $('#lgFormGroupInput').val();
	var tamanio = 0;

	$( "#formas-de-pago-all div.formapago fieldset.form-group div input.subtract-value" ).each(function( index ) {
		tamanio += 1;
		var value_sub = $(this).val();
		validaPago($(this)[0],$('#error_pago')[0]);
		var comname = $(this).attr("nombre");
		var cambio = $(this).attr("cambio");
		var ide = $(this).attr("identificador"); 
		var value_sub2 = Number(value_sub);
		var aux = [];
		aux.push(value_sub2);
		aux.push(cambio);
		aux.push(comname);
		aux.push(ide);
		arrayEnte.push(aux);
		arraySub.push(value_sub2);

	});

					var arrayEntex = [];
					var tamaniomonto = 0;

				$( ".formas-de-pago-all div.formapago fieldset.form-group div input.add-value" ).each(function( index ) {
					tamaniomonto += 1;
					var value_add = $(this).val();
					var porcentaje = $(this).attr("porcentaje");
					var nombre = $(this).attr("nombre");
					
					var value_add2 = Number(value_add);
					var aux = [];
					aux.push(value_add2);
					aux.push(porcentaje);
					aux.push(nombre);

					arrayEntex.push(aux);
				});

	var subtotal = $('#lgFormGroupInput').val();
	var cotizacion = $('#id_cotizacion_unico').val();

	$.ajax({
		url: '/inserrtaVenta',
		type: 'POST',
		dataType: 'json',
		data : {
				 arraySub: arraySub,
				 total: total,
				 ente : arrayEnte,
				 length : tamanio,
				 lengthmonto : tamaniomonto,
				 ent : arrayEntex,
				 subtotal: subtotal,
				 cotizacion:cotizacion
		},
		success : function(data) {
			if(data != null){
				console.log(data);
				if (data["Estatus"]==="false"){
				$("#cancelarModalCalculadora").trigger("click");
				console.log("Ahi no vamo");
				alert(data["Mensaje"]);
				}else{
				$("#cancelarModalCalculadora").trigger("click");
				$("#btnPagos").trigger("click");
				console.log("Ahi vamo");
				$('#ident').text(data["ID"]);
				$('#identhide').val(data["ID"]);
				}
			}
		},
	});
}

 function validaPago(elemento,tag_error){
	var estado = false;
	if(elemento.value === ""){ //Si el campo esta vacio
	elemento.parentNode.setAttribute("class","input-group has-error");
	tag_error.parentNode.setAttribute("class","form-group text-center has-error");
	tag_error.innerText ="El campo debe tener numeros positivos";
	$('#btn_cerrarPago').attr("disabled",true); //Desabilitar boton de Cerrar Venta
	}else{
	var valorNum = elemento.value;
	if (/^([0-9])*$/.test(valorNum) || /^\d*\.?\d*$/.test(valorNum)){ //Si es un valor numerico
		elemento.parentNode.removeAttribute("class","input-group has-error");
		elemento.parentNode.setAttribute("class","input-group has-success");
		tag_error.parentNode.removeAttribute("class","form-group has-error");
		tag_error.innerText ="";
		estado = true;
		$('#btn_cerrarPago').attr("disabled",false); //Habilita boton de Cerrar Venta
	}else{
		elemento.parentNode.setAttribute("class","input-group has-error");
		tag_error.parentNode.setAttribute("class","form-group text-center has-error");
		tag_error.innerText ="Debe ser un valor numerico";
		$('#btn_cerrarPago').attr("disabled",true); //Desabilitar boton de Cerrar Venta
	}
	}
	return estado;
}
function valida(){
	var inputs = $('.form-control.subtract-value');
	var valido = true;
	for(var i=0;i<inputs.length;i++){
		if(!validaPago(inputs[0],$('#error_pago')[0])){
			valido = false;
		}	
	}
	return valido;
}

function consultaCliente(){
	$('#identc').text($('#identhide').val());
	$('#identhidec').val($('#identhide').val());
	$("#btnPagos").trigger("click");
	$("#btnCliente").trigger("click");
}

function generaTicket(){
	var venta = $('#identhide').val();
	$("#btnPagos").trigger("click");
	
	$.ajax({
		url: '/GeneraTicket',
		type: 'POST',
		dataType: 'json',
		data : {
			venta: venta
		},
		success : function(data) {
			if(data != null){
				console.log(data);
				if (data["Estatus"]==="false"){
					console.log("Ahi no vamo con el ticket");
					alert(data["Mensaje"]);
				}else{
									
					var doc = new jsPDF();
					var specialElementHandlers = {
						'#editor': function (element, renderer) {
							return true;
						}
					};

					doc.fromHTML(data["Mensaje"], 15, 15, {
							'width': 170,
								'elementHandlers': specialElementHandlers
						});
					// doc.text(data["Mensaje"], 10, 10);
					doc.save('Ticket_'+data["ID"]+'.pdf');
					console.log("Ahi va el ticket");
					location.reload();
				}
			}
		},
	});
}

function generaFactura(ide){
	var venta = $('#identhidec').val();
	$("#btnCliente").trigger("click");
	
	$.ajax({
		url: '/GeneraFactura',
		type: 'POST',
		dataType: 'json',
		data : {
			venta: venta,
			cliente : ide
		},
		success : function(data) {
			if(data != null){
				console.log(data);
				if (data["Estatus"]==="false"){
					console.log("Ahi no vamo con el ticket");
					alert(data["Mensaje"]);
				}else{
									
					var doc = new jsPDF();
					var specialElementHandlers = {
						'#editor': function (element, renderer) {
							return true;
						}
					};

					doc.setFontSize(8);
					doc.text(data["Mensaje"], 20, 20);
					doc.save('Factura_'+data["ID"]+'.pdf');
					console.log("Ahi va el ticket");
					location.reload();
				}
			}
		},
	});


}

  function BuscaClientes(dato) {
              $('#error_buscar').empty();

              if (dato != "") {
                //Busca
                $.ajax({
                  url: '/BuscaClientesV',
                  type: 'POST',
                  dataType: 'json',
                  data: { dato: dato },
                  success: function (data) {
                    if (data != '') {
                      if (data != null) {
                          $('#id_respuesta').hide();
                          $('#pagination-ventas').empty();
                          $('#id_elemento').empty();
                          var tabla = generarTabla(data)
                          $('#id_elemento').html(tabla);
                          console.log(data);
                        }
                          $.ajax({
                            url: '/getPaginationResultSearchV',
                            type: 'POST',
                            dataType: 'html',
                            data: { getPage: 1 },
                            success: function (data) {
                              $('#pagination-ventas').html(data);
                            },
                          });
                        } else {
                          $('#error_codigo').append(data);
                        }
                      },
                      error: function (xhr) {
                        console.log("error: " + xhr.status);
                        $('#pagination-ventas').empty();
                        $('#id_elemento').empty();
                        $('#id_respuesta').show();
                      }
                  });

              } else {
                $('#error_buscar').append("Debe escribir algo que consultar");
              }
            }


            function getPagination(page) {
              $.ajax({
                url: '/nexpaginationV',
                type: 'POST',
                dataType: 'json',
                data: { num_page: page },
                success: function (data) {
                  var tabla = generarTabla(data);
                  $('#id_elemento').html(tabla);
                  $.ajax({
                    url: '/getPaginationResultSearchV',

                    type: 'POST',
                    dataType: 'html',
                    data: { getPage: page },
                    success: function (data) {
                      $('#pagination-ventas').html(data);
                    },
                  });
                },
              });
            }

            function generarTabla(datos) {
              if (datos != null) {

                var tabla = document.createElement("table");
                tabla.id = "TablaConsulta"
                var tblBody = document.createElement("tbody");
                var cabecera = document.createElement("thead");
                cabecera.className = "thead-inverse";
                var hilera = document.createElement("tr");

                var celda = document.createElement("th");
                celda.appendChild(document.createTextNode("RFC"));
                hilera.appendChild(celda);
                var celda = document.createElement("th");
                celda.appendChild(document.createTextNode("NOMBRE"));
                hilera.appendChild(celda);
                var celda = document.createElement("th");
                celda.appendChild(document.createTextNode("DIRECCION"));
                hilera.appendChild(celda);
                var celda = document.createElement("th");
                celda.appendChild(document.createTextNode("E-MAIL"));
                hilera.appendChild(celda);

                cabecera.appendChild(hilera);

                tabla.appendChild(cabecera);

                for (var i = 0; i < datos.length; i++) {

                  var IDe = datos[i].Id;
                  var Rfc = datos[i].Rfc;
                  var Nombre = datos[i].Nombre;
                  var Direccion = datos[i].Direccion.Calle + ' ' + datos[i].Direccion.NumExterior + ', ' + datos[i].Direccion.Colonia + ', '+  datos[i].Direccion.Municipio+', '+datos[i].Direccion.Estado;
                  var Correo = datos[i].Contacto.Email;

                  //Crea primer renglón
                  hilera = document.createElement("tr");

                  var celda = document.createElement("td");
                  celda.appendChild(document.createTextNode(Rfc));
                  hilera.appendChild(celda);

                  var celda = document.createElement("td");
                  celda.appendChild(document.createTextNode(Nombre));
                  hilera.appendChild(celda);

                  var celda = document.createElement("td");
                  celda.appendChild(document.createTextNode(Direccion));
                  hilera.appendChild(celda);

                  var celda = document.createElement("td");
                  celda.appendChild(document.createTextNode(Correo));
                  hilera.appendChild(celda);

                  //Atributos del renglón
                  hilera.setAttribute("id", IDe);
                  hilera.onmouseover = function () { document.getElementById(this.id).style.backgroundColor = "#e3e3e3"; };
                  hilera.onmouseout = function () { document.getElementById(this.id).style.backgroundColor = "#FAFAFA"; };
                  hilera.ondblclick = function () {
					generaFactura(this.id)  
                  };
                  tblBody.appendChild(hilera);
                }

                tabla.appendChild(tblBody);
                tabla.className = "table table-hover text-center col-md-8";
                tabla.setAttribute("id", "sample_1")

                return tabla;
              }
            }

