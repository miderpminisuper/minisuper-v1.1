$(document).ready(function () {
    cantidadInicial = 0;
    descuentoInicial = 0;
    $("#impresion").hide();

    $('#scanner').val("");
    $("#scanner").focus();

    $("#scanner").on('keyup', function (e) {
        if (e.keyCode == 13) {
            codigoBarra = $("#scanner").val();
            $('#scanner').val("");
            $("#scanner").focus();
            $.ajax({
                async: false,
                cache: false,
                method: "POST",
                url: "/venta/fn2",
                dataType: 'HTML',
                data: { cb: codigoBarra },
                success: function (data) {
                    idProd = data;
                    if (idProd === "") {
                        console.log(codigoBarra + " No en BD -- /venta/fn2");
                        $("#scanner").focus();
                        $('#scanner').val("");
                    } else {
                        console.log(codigoBarra + " En BD-- /venta/fn2");
                        id_cot_exist = $("#id_cotizacion_unico").val();

                        if (!enLista(idProd)) {
                            if (id_cot_exist == "") {
                                console.log(" Aqui se debe instanciar la cotizacion:");
                                $.ajax({
                                    method: "POST",
                                    async: false,
                                    cache: false,
                                    url: "/venta/crearcotizacion",
                                    dataType: 'HTML',
                                    data: { idProducto: idProd },
                                    success: function (data) {
                                        $("#load-id-cotizacion").html(data);
                                    }
                                });
                                //alert("Se crea la cotizacion: " + $("#id_cotizacion_unico").val());
                            }
                            console.log("Finaliza la sesion");
                            console.log("Llamar insertar Nuevo registro");
                            insertarRegistro(idProd);
                        } else {
                            console.log("Ya existe una cotizacion " + $("#id_cotizacion_unico").val());
                            console.log("Llamar modificar registro");
                            modificarPorCodogoDeBarras(data);
                        }

                    }
                }
            });

        }
    });

    $("#CancelarCotizacion").click(function () {
        if ($("#id_cotizacion_unico").val() != "") {
            $.ajax({
                method: "POST",
                url: "/venta/eliminaCot",
                async: false,
                cache: false,
                dataType: 'html',
                data: {
                    ID: $("#id_cotizacion_unico").val()
                },
                success: function (data) {
                    if (data == "") {
                        $("#articulos-lista").html("");
                        $("#id_cotizacion_unico").val("");
                        $("#scanner").focus();
                        $('#scanner').val("");
                        alert("Cotizacion eliminada...");
                    } else {
                        alert(data);
                    }
                }
            });
        }
    });

    $("#GuardarCotizacion").click(function () {
        if ($("#id_cotizacion_unico").val() != "") {
            pagebackup = $("body").html();
            $("#labelCot").text($("#id_cotizacion_unico").val());
            cotizacion = $("#impresion").html();
            $("body").html("");
            $("body").html(cotizacion)
            $("#impresion").show();
            window.print();
            $("body").html(pagebackup);

            location.reload();

        }
    });

});

function enLista(idProducto) {
    id = $('#' + idProducto).val();
    if (id != undefined) {
        console.log("Existe en lista, modificar -- ventas.js enLista(id)");
        return true;
    } else {
        console.log("No existe en lista, agregar nuevo -- ventas.js enLista(id)");
        return false;
    }

}
function insertarRegistro(idProducto) {
    $.ajax({
        method: "POST",
        url: "/venta/fn1",
        async: false,
        cache: false,
        dataType: 'json',
        data: { cb: codigoBarra, almOrigen: "589e3e24eaf33c45e47c5278" },
        success: function (data) {
            Registro = data.Registro;
            if (data.ID != "") {
                id = data.ID;
                $("#scanner").focus();
                $('#scanner').val("");
                destino = $("#id_cotizacion_unico").val();
                $.ajax({
                    method: "POST",
                    url: "/venta/transaccionV",
                    async: false,
                    cache: false,
                    dataType: 'json',
                    data: {
                        ID: id,
                        valorprevio: 0,
                        valornuevo: 1,
                        almOrigen: "589e3e24eaf33c45e47c5278",
                        almdestino: "589e4b00eaf33c23dc28304b",
                        idusr: "5895142917ce77e76d8e7bcd",
                        idcli: "589e4b00eaf33c23dc28304b",
                        id_cot: destino,
                        desc: 0
                    },
                    success: function (data) {
                        if (data.Exito) {
                            $("#articulos-lista").append(Registro);
                        }
                        else {
                            alert(data.Valor);
                        }

                    }
                });
            }

        }
    });
}

function modificarPorCodogoDeBarras(idProducto) {
    prev = $("#cantidad" + idProducto).val();
    nuev = parseFloat(prev) + 1;
    destino = $("#id_cotizacion_unico").val();
    dcto = $("#descuento" + idProducto).val();
    $.ajax({
        method: "POST",
        url: "/venta/transaccionV",
        async: false,
        cache: false,
        dataType: 'json',
        data: {
            ID: idProducto,
            valorprevio: prev,
            valornuevo: nuev,
            almOrigen: "589e3e24eaf33c45e47c5278",
            almdestino: "589e4b00eaf33c23dc28304b",
            idusr: "5895142917ce77e76d8e7bcd",
            idcli: "589e4b00eaf33c23dc28304b",
            id_cot: destino,
            desc: dcto
        },
        success: function (data) {
            if (data.Exito == true) {
                $("#cantidad" + idProducto).val(data.Valor);
                calcularFila(idProducto);
            }
            else {
                $("#cantidad" + idProducto).val(data.Valor);
                alert(data.Descripcion);
            }
        }
    });

}

function calcularFila(idProducto) {
    var cantidad = parseFloat($("#cantidad" + idProducto).val());
    var descuento = parseFloat($("#descuento" + idProducto).val());
    var precio = parseFloat($("#precio" + idProducto).val());

    var total = cantidad * precio * (1 - descuento / 100);

    $("#importe" + idProducto).val(total);
}

function obtenerCantidadInicial(valor) {
    this.cantidadInicial = $("#cantidad" + valor).val();
    console.log("Se obtiene el dato de el campo " + this.cantidadInicial);
}

function modificacionCantidad(valor) {
    x = $("#cantidad" + valor).val();
    if (x == this.cantidadInicial) {
        console.log("son lo mismo")
    }
    else {
        if (x < 0) {
            console.log("modificacionCantidad: Movimiento no admitido. Solo se validan valores mayores o iguales que cero")
            alert("modificacionCantidad: Movimiento no admitido. Solo se validan valores mayores o iguales que cero")
            $("#cantidad" + valor).val(this.cantidadInicial)
        } else {
            console.log("sucedió un cambio, llamar transaccion");
            destino = $("#id_cotizacion_unico").val();
            dcto = $("#descuento" + valor).val();
            $.ajax({
                method: "POST",
                url: "/venta/transaccionV",
                async: false,
                cache: false,
                dataType: 'json',
                data: {
                    ID: valor,
                    valorprevio: this.cantidadInicial,
                    valornuevo: x,
                    almOrigen: "589e3e24eaf33c45e47c5278",
                    almdestino: "589e4b00eaf33c23dc28304b",
                    idusr: "5895142917ce77e76d8e7bcd",
                    idcli: "589e4b00eaf33c23dc28304b",
                    id_cot: destino,
                    desc: dcto
                },
                success: function (data) {
                    if (data.Exito == true) {
                        $("#cantidad" + valor).val(data.Valor);
                        this.cantidadInicial = x;
                        console.log("Deberia cambiar a "+ this.cantidadInicial);
                        calcularFila(valor);
                    }
                    else {
                        $("#cantidad" + valor).val(data.Valor);
                        alert(data.Descripcion);
                    }
                }
            });
            if (x == 0) {
                alert("eliminar elemento");
                $("#cantidad" + valor).parent().parent().remove();
            } else {
                console.log("modificar en transaccion");
            }
        }
    }
    $('#scanner').val("");
    $("#scanner").focus();
}

function obtenerDescuentoInicial(valor) {
    this.descuentoInicial = $("#descuento" + valor).val();
    console.log("Se obtiene el dato de el campo " + this.descuentoInicial);
}

function modificacionDescuento(valor) {
    x = $("#descuento" + valor).val();
    if (x < 0 || x > 100) {
        alert("Debe introducir valores entre 0 y 100. No se realizará la modificación");
        $("#descuento" + valor).val(this.descuentoInicial);

    } else {
        if (x != this.descuentoInicial) {
            destino = $("#id_cotizacion_unico").val();
            num = $("#cantidad" + valor).val();
            imp = $("#precio" + valor).val();
            $.ajax({
                method: "POST",
                url: "/venta/modifdcto",
                async: false,
                cache: false,
                dataType: 'html',
                data: {
                    ID: valor,
                    id_cot: destino,
                    cantidad: num,
                    almOrigen: "589e3e24eaf33c45e47c5278",
                    almdestino: "589e4b00eaf33c23dc28304b",
                    idusr: "5895142917ce77e76d8e7bcd",
                    idcli: "589e4b00eaf33c23dc28304b",
                    desc: x,
                    precio: imp
                },
                success: function (data) {
                    alert("cambio realizado");
                    calcularFila(valor);
                }
            });
        }
        else {
            console.log("No sucedió un cambio");
            $("#descuento" + valor).val(this.descuentoInicial);
        }
    }

    $('#scanner').val("");
    $("#scanner").focus();
}
function restauraCantidad(idProducto) {
    $("#cantidad" + idProducto).val(this.cantidadInicial);
}
function restauraDescuento(idProducto) {
    $("#descuento" + idProducto).val(this.descuentoInicial);
}