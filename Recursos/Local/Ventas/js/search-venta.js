$(document).ready(function(){
	
	$("#scanner_search").on('keyup', function (e) {
        if (e.keyCode == 13) {
			BuscarArticuloParaVenta();
		}
	});
	
	$("#search-mongo-venta").click(function(){
		BuscarArticuloParaVenta();
	});
	
	$("#buscar-click").click(function(){
		
	});
	
	$("#dismiss-empty").click(function(){
		document.getElementById("result-mongo-range-search").innerHTML="";
		document.getElementById("search-pagination").innerHTML="";
	});
	
	$("#modal-search").on('shown.bs.modal',function(e){
		$("#scanner_search").val("");
		$("#scanner_search").focus();
	});
	
	/*
 *@melchor
 *Funcion para la busqueda de SKU
 */
$("#scanner_search2").on('keyup', function (e) {
        if (e.keyCode == 13) {
			BuscarArticuloParaKardex();
		}
	});
$("#search-mongo-venta2").click(function(){
		BuscarArticuloParaKardex();
	});
//Melchor
//Para traslado
$("#scanner_search3").on('keyup', function (e) {
        if (e.keyCode == 13) {
			BuscarArticuloParaTraslado();
		}
	});
$("#search-mongo-venta3").click(function(){
		BuscarArticuloParaTraslado();
	});
	
});

//se ejecuta al realizar la busqueda, esta funcion se llama en la parte superior
function BuscarArticuloParaVenta(){
	var texto = $("#scanner_search").val();
	$.ajax({
		url: '/searchAjaxArtiElastic',
		type: 'POST',
		dataType: 'html',
		data : {searchtext:texto},
		success : function(data) {
			//$('#content-list').html(data);
			$.ajax({
				url: '/valuePaginatioElasticResult', //funcion GetProductsIdTo_Info_Paginatio
				type: 'POST',
				dataType: 'html',
				data : {},
				success : function(data) {
					$('#result-mongo-range-search').html(data);
				},
			});
			
			$.ajax({
				url: '/placepagination',
				type: 'POST',
				dataType: 'html',
				data : {},
				success : function(data) {
					$('#search-pagination').html(data);
					
				},
			});
			
			
		},
	});
	
}

//se recibe un numero de página para redirecionar informacion al div
function ReloadPage(p){
//	alert(p);
	$.ajax({
		url: '/valuePaginatioElasticResult', //funcion GetProductsIdTo_Info_Paginatio
		type: 'POST',
		dataType: 'html',
		data : {page:p},
		success : function(data) {
			$('#result-mongo-range-search').html(data);
		},
	});
	$.ajax({
		url: '/placepagination',
		type: 'POST',
		dataType: 'html',
		data : {page:p},
		success : function(data) {
			$('#search-pagination').html(data);
			
		},
	});
}

function SetAddRegistro(codigo){
	arregloId = document.getElementsByName("elemento");
    arregloCantidad = document.getElementsByName("cantidadv");
    descuento = document.getElementsByName("descuento");
    total = document.getElementsByName("total");
    precio = document.getElementsByName("precio");
    console.log("Elemento buscado: " + $('#scanner').val());
    $.ajax({
        method: "POST",
        url: "/venta/fn2",
        dataType: 'HTML',
        data: { cb: codigo},
        success: function (data) {

            if (data === "") {
                console.log("No se encuentra el objeto en BD");
               
            } else {
                if (!enArreglo(data, arregloId, arregloCantidad)) {
                    console.log("No se encuentra en lista");
                    console.log("Agregar...");
                    $.ajax({
                        method: "POST",
                        url: "/venta/fn1",
                        dataType: 'HTML',
                        data: { cb: codigo },
                        success: function (data) {
                            $("#articulos-lista").append(data);
                            CalculaImportes(arregloCantidad, precio, descuento, total);
                        }
                    });
                    console.log("Agregado...");
                    CalculaImportes(arregloCantidad, precio, descuento, total);
                } else {
                    
                     CalculaImportes(arregloCantidad, precio, descuento, total);
                }

            }
        }
    });
    CalculaImportes(arregloCantidad, precio, descuento, total);
}

function CalculaImportes(arregloCantidad, precio, descuento, total) {
    arregloId = document.getElementsByName("elemento");
                                    arregloCantidad = document.getElementsByName("cantidadv");
                                    descuento = document.getElementsByName("descuento");
                                    total = document.getElementsByName("total");
                                    precio = document.getElementsByName("precio");
                                    console.log("------Cantidades-------")
                                    console.log("Cantidades: "+arregloCantidad)
                                    console.log("----------------------")
    var elem = precio.length;
    console.log(elem)
    if (elem > 0) {
        for (i = 0; i < elem; i++) {
            total[i].value = parseFloat(arregloCantidad[i].value) * precio[i].value;
        }
    } else {
        console.log("No hay nada que calcular");
    }
}

/*
 *@melchor
 *Metodo usado para agregar un articulo al campo de texto de entrada para consulta de kardex
 */
function SetAddCodeArt(nombre,id){
$("#nombreArticulo").attr('value',nombre);
$("#idArticulo").attr('value',id);
}
/*
 *@melchor
 *Funcion para busqueda de articulo en modal para generar Kardex
 */
function BuscarArticuloParaKardex(){
	var texto = $("#scanner_search2").val();
	$.ajax({
		url: '/searchAjaxArtiElastic2',
		type: 'POST',
		dataType: 'html',
		data : {searchtext:texto},
		success : function(data) {
			//$('#content-list').html(data);
			$.ajax({
				url: '/valuePaginatioElasticResult2', //funcion GetProductsIdTo_Info_Paginatio
				type: 'POST',
				dataType: 'html',
				data : {},
				success : function(data) {
					$('#result-mongo-range-search').html(data);
				},
			});
			
			$.ajax({
				url: '/placepagination',
				type: 'POST',
				dataType: 'html',
				data : {},
				success : function(data) {
					$('#search-pagination').html(data);
					
				},
			});
			
			
		},
	});
	
}
/*
 *@melchor
 *Metodo usado para agregar un articulo al campo de texto de entrada para consulta de kardex
 */
function SetCodigoTraslado(nombre,codigo){
//$("#nombreArticulo").attr('value',nombre);
$("#elarticulo").attr('value',codigo);
}
/*
 *@melchor
 *Funcion para busqueda de articulo en modal para generar Kardex
 */
function BuscarArticuloParaTraslado(){
	var texto = $("#scanner_search3").val();
	$.ajax({
		url: '/searchAjaxArtiElastic3',
		type: 'POST',
		dataType: 'html',
		data : {searchtext:texto},
		success : function(data) {
			//$('#content-list').html(data);
			$.ajax({
				url: '/valuePaginatioElasticResult3', //funcion GetProductsIdTo_Info_Paginatio
				type: 'POST',
				dataType: 'html',
				data : {},
				success : function(data) {
					$('#result-mongo-range-search').html(data);
				},
			});
			
			$.ajax({
				url: '/placepagination',
				type: 'POST',
				dataType: 'html',
				data : {},
				success : function(data) {
					$('#search-pagination').html(data);
					
				},
			});
			
			
		},
	});
	
}