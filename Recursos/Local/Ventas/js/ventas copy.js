$(document).ready(function () {
    var anterior;
    var nuevo;

    $('input[name="cantidadv"]').focus();

    $("#scanner").focus();
    $("#scanner").on('keyup', function (e) {
        if (e.keyCode == 13) {
            id_cot_exist = $("#id_cotizacion_unico").val();
            arregloId = document.getElementsByName("elemento");
            arregloCantidad = document.getElementsByName("cantidadv");
            descuento = document.getElementsByName("descuento");
            total = document.getElementsByName("total");
            precio = document.getElementsByName("precio");
            $.ajax({
                method: "POST",
                url: "/venta/fn2",
                dataType: 'HTML',
                data: { cb: $('#scanner').val() },
                success: function (data) {
                    if (data === "") {
                        $("#scanner").focus();
                        $('#scanner').val("");
                    } else {
                        if (id_cot_exist == "") {
                            $.ajax({
                                method: "POST",
                                url: "/venta/crearcotizacion",
                                dataType: 'HTML',
                                data: { idProducto: data },
                                success: function (data) {
                                    $("#load-id-cotizacion").html(data);
                                }
                            });
                        }

                        if (!enArreglo(data, arregloId, arregloCantidad)) {
                            $.ajax({
                                method: "POST",
                                url: "/venta/fn1",
                                async: false,
                                cache: false,
                                dataType: 'json',
                                data: { cb: $('#scanner').val(), id_cotizacion: id_cot_exist },
                                success: function (data) {
                                    Registro = data.Registro;
                                    if (data.ID != "") {
                                        id = data.ID;
                                        $("#scanner").focus();
                                        $('#scanner').val("");
                                        destino = $("#id_cotizacion_unico").val();
                                        $.ajax({
                                            method: "POST",
                                            url: "/venta/transaccionV",
                                            async: false,
                                            cache: false,
                                            dataType: 'json',
                                            data: {
                                                ID: id, valorprevio: 0,
                                                valornuevo: 1, almOrigen: "Almacen",
                                                almdestino: destino, idusr: "586e8db2e5932c512e3fc888",
                                                idcli: "Usuario General"
                                            },
                                            success: function (data) {
                                                if (data.Exito) {
                                                    $("#articulos-lista").append(Registro);
                                                }
                                                else {
                                                    alert(data.Descripcion);
                                                }

                                            }
                                        });
                                        CalculaImportes();

                                    }
                                    else {
                                        anterior = 0;
                                        nuevo = 0;
                                        $("#scanner").focus();
                                        $('#scanner').val("");
                                        CalculaImportes();
                                        alert("No se encuentra producto en la BD");
                                    }

                                }
                            });

                            CalculaImportes();
                        } else {
                            $("#scanner").focus();
                            $('#scanner').val("");
                            CalculaImportes();
                        }

                    }
                }
            });
            CalculaImportes();
        }
    });
});

function vacia() {
    $("#scanner").focus();
};
function enArreglo(id, arregloId, arregloCantidad) {
    pos = 0;
    arregloId = document.getElementsByName("elemento");
    arregloCantidad = document.getElementsByName("cantidadv");
    elem = arregloId.length;
    i = 0;

    cambio = false;
    encontrado = false;
    for (i = 0; i < elem; i++) {
        if (arregloId[i].value == id) {
            anterior = parseFloat(arregloCantidad[i].value);
            nuevo = parseFloat(arregloCantidad[i].value) + 1.0;


            $.ajax({
                method: "POST",
                url: "/venta/transaccionV",
                async: false,
                cache: false,
                dataType: 'json',
                data: {
                    ID: id, valorprevio: parseFloat(anterior),
                    valornuevo: (parseFloat(anterior) + 1), almOrigen: "Almacen",
                    almdestino: destino, idusr: "586e8db2e5932c512e3fc888",
                    idcli: "Usuario General"
                },
                success: function (data) {
                    if (data.Exito == true) {
                        //arregloCantidad[i].value = parseFloat(arregloCantidad[i].value) + 1.0;
                        cambio = true;

                    }
                    else {
                        alert(data.Descripcion);
                    }
                    espera = false;
                }
            });
            //
            if (cambio == true)
                arregloCantidad[i].value = parseFloat(arregloCantidad[i].value) + 1.0;
            encontrado = true;
        }

    }
    CalculaImportes();

    return encontrado;

}
function CalculaImportes() {
    arregloId = document.getElementsByName("elemento");
    arregloCantidad = $("input[name='cantidadv']");
    descuento = document.getElementsByName("descuento");
    total = document.getElementsByName("total");
    precio = document.getElementsByName("precio");
    arregloEsFraccion = $("input[name='fraccionario']");


    var elem = precio.length;
    if (elem > 0) {
        for (i = 0; i < elem; i++) {
            if($(arregloEsFraccion[i]).val() == "false")
                $(arregloCantidad[i]).val(Math.floor($(arregloCantidad[i]).val()));
            console.log($(arregloCantidad[i]).val());
            total[i].value = parseFloat(arregloCantidad[i].value) * (precio[i].value - (precio[i].value * (descuento[i].value / 100)));
            calculaDescuento(descuento[i]);
        }

    } else {
        console.log("No hay nada que calcular");
    }
}
function calculaInsercion(x) {
    anterior = $(x).val();
}

function calculaPrecio(x) {
    nuevo = $(x).val();
    if (nuevo >= 0) {
        if (nuevo != anterior) {
            arregloId = document.getElementsByName("elemento");
            arregloCantidad = document.getElementsByName("cantidadv");
            descuento = document.getElementsByName("descuento");
            total = document.getElementsByName("total");
            precio = document.getElementsByName("precio");
            cantidad = $.inArray(x, arregloCantidad);
            id = arregloId[parseFloat(cantidad)].value;
            pre = precio[parseFloat(cantidad)].value;
            des = descuento[parseFloat(cantidad)].value;
            $.ajax({
                method: "POST",
                url: "/venta/transaccionV",
                async: false,
                cache: false,
                dataType: 'json',
                data: {
                    ID: id, valorprevio: parseFloat(anterior),
                    valornuevo: parseFloat(nuevo), almOrigen: "Almacen",
                    almdestino: destino, idusr: "586e8db2e5932c512e3fc888",
                    idcli: "Usuario General"
                },
                success: function (data) {
                    if (data.Exito == true) {
                        cambio = true;
                        arregloCantidad[cantidad] = parseFloat(nuevo);

                    }
                    else {
                        alert(data.Descripcion);
                        arregloCantidad[cantidad] = parseFloat(anterior);
                    }
                }
            });
            CalculaImportes();

        }
        else {
            console.log("Ahorita no joven, no esta haciendo cambios->")
        }
    } else {
        $(x).val(anterior);
        alert("Los n&uacute;meros negativos no son v&aacute;lidos.")
    }

}
function calculaDescuento(x) {
    menorQueCeroCero(x);
    mayorQueCienCien(x);
    arregloId = document.getElementsByName("elemento");
    arregloCantidad = document.getElementsByName("cantidadv");
    descuento = document.getElementsByName("descuento");
    total = document.getElementsByName("total");
    precio = document.getElementsByName("precio");
    elementos = arregloId.length;
    for (i = 0; i < elementos; i++)
        total[i].value = (precio[i].value - (precio[i].value * (descuento[i].value / 100))) * arregloCantidad[i].value;
}

function menorQueCeroCero(x) {
    if ($(x).val() < 0)
        $(x).val(0);
}
function mayorQueCienCien(x) {
    if ($(x).val() > 100)
        $(x).val(100);
}