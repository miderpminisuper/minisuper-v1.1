$(document).ready(function(){

	var numcampos = "";
	var contenedor = $("#contenedor1");
    
    
    $("body").on("click","#agregarcampo_e", function(e){
    	var campostotales= $("#contenedor1 div").length;
    	numcampos = campostotales-5;
   		 console.log(numcampos);
    	$(contenedor).append('<div class="input-group input-group-md claves" id="contenedorclave_'+numcampos+'"><span class="input-group-addon">Clave: </span><input type="text" name="clave_'+numcampos+'" id="clave_'+numcampos+'" value="" class="form-control clave"><span class="input-group-addon">Valor: </span><input type="text" name="valor_'+numcampos+'" id="valor_'+numcampos+'" value="" class="form-control valor"><span class="input-group-addon btn btn-danger eliminar">-</span></div>');
    });



	$("body").on("click",".eliminar", function(e){
		var campostotales= $("#contenedor1 div").length;
		numcampos = campostotales-5;
		console.log(numcampos);

       if( numcampos > 0 ) {
           $(this).parent('div').remove();
           numcampos--;
       }
       return false;
   });




	
	$("#nuevo_almacen").click(function(){

		$.ajax({
			url: '/crearalmacen',
			type: 'POST',
			dataType: 'html',
			data:{},
			success : function(data){
				$('#contenedor2').html(data);
			}
		});
	});

});


function GuardarAlmacen(){

	var nombre = document.getElementById("nombre_almacen").value;
	var tipo = $('input:radio[name=tipo_almacen]:checked').val(); 
	var defecto = $('input:radio[name=almacen_defecto]:checked').val();

	var campos_extras = $("#contenedor3 div").length;
	var claves = []
	var valores = []



	if (campos_extras > 0){
		for (i = 1; i <= campos_extras;i++){
			var clave = document.getElementById("clave"+i).value;
			var valor = document.getElementById("valor"+i).value;
			claves.push(clave);
			valores.push(valor);

		}		
	}

	var contenedor_x =  $("#contenedor3");

	if (nombre == ""){
		alert("Inserta un nombre para este almacén.");
	}else{

		$.ajax({
			url: '/guardaralmacen',
			type: 'POST',
			dataType: 'html',
			data:{nombre_almacen : nombre,	tipo_almacen : tipo, defecto_almacen:defecto, claves_k : claves , valores_k : valores},
			success : function(data){
				if (data  == "Insertado"){
					alert("Operación exitosa.");
					location.reload("/almacenes");
				}else{
					alert("Ocurrio un error");
				}
			}
		});
		
	}
	
}

function Agregarcampo(){
	var contenedor = $("#contenedor3");
	var numcampos = $("#contenedor3 div").length+1;
	$(contenedor).append('<div class="input-group input-group-md"><span class="input-group-addon">Clave: </span><input type="text" name="clave'+numcampos+'" id="clave'+numcampos+'" value="" class="form-control clave"><span class="input-group-addon">Valor: </span><input type="text" name="valor'+numcampos+'" id="valor'+numcampos+'" value="" class="form-control valor"></div>');
 }

function Eliminarcampo(){
	$("#contenedor3 div:last-child").remove();
}

function GetAlmacenes(){
	var meta = $("#s_almacen option:selected").val();
	
	if (meta == "Seleccione Almacen.."){
		$('#contenedor1').empty();
		$('#contenedorguardar').empty();
		
	}else{	
		
		$.ajax({
			url: '/editaralmacen',
			type: 'POST',
			dataType: 'html',
			data:{ id : meta},
			success : function(data){
				$('#contenedorguardar').empty();
				$('#contenedor1').html(data);
				$('#contenedorguardar').append('<div class="input-group input-group-md"><span class="input-group-addon"><input type="button" class="btn btn-lg btn-success" onClick="GuardarAlmacenEditado();" value="Guardar Almacén" name="guardareditar" id="guardareditar"></span></div>');
				
			}
		});
	}
}

function GuardarAlmacenEditado(){


	var id = document.getElementById("id_").value;
	var nombre = document.getElementById("nombre_").value;
	var tipo = $('input:radio[name=tipo_]:checked').val();
	var defecto = $('input:radio[name=almacen_d]:checked').val();

	var claves = []
	var valores = []

	$("#contenedor1 div.claves").each(function(){
			// $(this).each(function(){

		var cl = $(this).find(".clave").val();
		var vl = $(this).find(".valor").val();

		claves.push(cl);
	 	valores.push(vl);

        	  
    });

	$.ajax({
			url: '/guardareditaralmacen',
			type: 'POST',
			dataType: 'html',
			data:{id:id, nombre_almacen : nombre,	tipo_almacen : tipo, defecto_almacen:defecto, claves_k : claves , valores_k : valores},
			success : function(data){
				if (data  == "Insertado"){
					alert("Operación exitosa");
					location.reload("/almacenes");
				}else{
					alert("Ocurrio un error");
				}
			}
		});

}

function EliminarAlmacen(){
	var almacen = $("#s_almacen option:selected").val();

	if (almacen =="Seleccione Almacen.."){
		alert("Debes seleccionar un Almacen primero.");
	}else{
		var nombre = document.getElementById("nombre_").value;
		var r = confirm("Deseas eliminar "+nombre+" del inventario:");
		if (r == true) {

			$.ajax({
					url: '/eliminaralmacen',
					type: 'POST',
					dataType: 'html',
					data:{id:almacen},
					success : function(data){
						if (data  == "Eliminado"){
							alert("Almacén eliminado.");
							location.reload("/almacenes_frame")
						}else{
							alert("Ocurrio un error");
						}
					}
				});
		    
		} else {
		    location.reload("/almacenes_frame")
		}
	} 

}