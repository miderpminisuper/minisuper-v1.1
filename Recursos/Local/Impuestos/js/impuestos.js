$(document).ready(function(){

	$("body").on("click",".eliminar", function(e){
		var numcampos = $("#contenedor1 .claves").length;
		if( numcampos > 0 ) {
        	$(this).parent('div').remove();
        }
        return false;
   	});
	
});

function GetImpuesto(){
	var meta = $("#s_impuesto option:selected").val();

	if (meta == "Seleccione Impuesto.."){
		$('#contenedor1').empty();
		$('#contenedorguardar').empty();
	}else{	
		
		$.ajax({
			url: '/editarimpuesto',
			type: 'POST',
			dataType: 'html',
			data:{ id : meta},
			success : function(data){
				$('#contenedorguardar').empty();
				$('#contenedor1').html(data);
				$('#contenedorguardar').append('<div class="input-group input-group-md"><span class="input-group-addon"><input type="button" class="btn btn-lg btn-success" onClick="GuardarImpuesto();" value="Guardar Impuesto" name="guardarimpuesto" id="guardarimpuesto"></span></div>');
			}
		});
	}
}


function Agregarcampoimpuesto(id){
	var contenedor = $("#contenedor1");
	var numcampos = $("#contenedor1 .claves").length;

	$(contenedor).append('<div class="input-group input-group-md claves clearfix" id="valores_'+numcampos+'"><span class="input-group-addon">Nombre:</span><input type="text" name="nombre_'+numcampos+'" id="nombre_'+numcampos+'" value="" class="form-control nombre"><span class="input-group-addon">Tipo:&nbsp;<label class="radio-inline"><input type="radio" name="impuesto_tipo_'+numcampos+'" id="impuesto_tipo_'+numcampos+'" value ="porcentaje"checked>%&nbsp;</label><label class="radio-inline"><input type="radio" name="impuesto_tipo_'+numcampos+'" id="impuesto_tipo_'+numcampos+'" value ="moneda">$</label></span><span class="input-group-addon">Valor:</span><input type="text" name="valor_'+numcampos+'" id="valor_'+numcampos+'" value="" class="form-control valor"><span class="input-group-addon">Descripcion: </span><input type="text" name="desc_'+numcampos+'" id="desc_'+numcampos+'" value="" class="form-control descripcion"><span class="input-group-addon btn btn-danger eliminar">-</span></div>');

}

function GuardarImpuesto(){
	var id = $("#id_").val();
	var meta_impuesto = $("#grupo_").val();
	var prioridad = $("#prioridad_ option:selected").val();
	var numcampos_extras = $("#contenedor1 .claves").length;
	var nombres = []
	var valores = []
	var descripcion = []
	var tipos = []

	if (numcampos_extras > 0){
		i = 0;
		$("#contenedor1 div.claves").each(function(){
			var nombre = document.getElementById("nombre_"+i).value;
			var valor = document.getElementById("valor_"+i).value;
			var desc = document.getElementById("desc_"+i).value;
			var tipo = $('input:radio[name=impuesto_tipo_'+i+']:checked').val();
			nombres.push(nombre);
			valores.push(valor);
			descripcion.push(desc);
			tipos.push(tipo);
			i++;
		});		
	}

		$.ajax({
			url: '/guardarimpuesto',
			type: 'POST',
			dataType: 'html',
			data:{ id :id, meta_impuesto : meta_impuesto, prioridad : prioridad, nombres : nombres, valores : valores, descripcion : descripcion, tipos : tipos},
			success : function(data){
					if (data  == "Insertado"){
						alert("Operación Exitosa");
						location.reload("/impuestos_frame");
					}else{
						alert("Ocurrio un error");
					}
				
			}
		});

}

function Agregarmeta(){

	var contenedor = $("#contenedornewmeta");
	$('#contenedornewmeta').empty();

	$.ajax({
		url: '/newmetaimpuesto',
		type: 'POST',
		dataType: 'html',
		data:{},
		success : function(data){
			$(contenedor).append(data);
		}
	});

	

}

function GuardarImpuestoNuevo(){
	var meta_impuesto = $("#nombre_grupo").val();
	var prioridad = $("#prioridad_n option:selected").val();

	console.log(meta_impuesto);
	console.log(prioridad);


	if (meta_impuesto == ""){
		alert("Debes introducir un nombre para el nuevo meta-impuesto");

	}else{

		$.ajax({
		url: '/newermetaimpuesto',
		type: 'POST',
		dataType: 'html',
		data:{meta_impuesto : meta_impuesto, prioridad:prioridad },
		success : function(data){
			if (data  == "Insertado"){
				alert("Operación Exitosa");
				location.reload("/impuestos_frame");
			}else{
				alert("Ocurrio un error");
			}
			
		}
	});

	}
}

function EliminarMeta(){
	var impuesto = $("#s_impuesto option:selected").val();

	if (impuesto =="Seleccione Impuesto.."){
		alert("Ningún IMPUESTO seleccionado");
	}else{
		var nombre = document.getElementById("grupo_").value;
		var r = confirm("Deseas eliminar "+nombre+" del inventario:");
		if (r == true) {

			$.ajax({
					url: '/eliminarimpuesto',
					type: 'POST',
					dataType: 'html',
					data:{id:impuesto},
					success : function(data){
						if (data  == "Eliminado"){
							alert("Impuesto eliminado.");
							location.reload("/impuestos_frame")
						}else{
							alert("Ocurrio un error");
						}
					}
				});
		    
		} else {
		    location.reload("/impuestos_frame")
		}
	} 

}