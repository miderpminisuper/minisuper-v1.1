$(document).ready(function(){

	
	$('#cabeceraajuste').hide();
	$('#cabeceratraslado').hide();

	$('#contenedortabla').hide();
	$('#contenedorbotonguardar').hide();
	$('#contenedornoencontrado').hide();

		$("body").on("click",".eliminar", function(e){
   			e.preventDefault();
    		$(this).closest('tr').remove(); 
    		var articulos_en_cuestion = $('#contenedordeproductos tr').length;
    		if (articulos_en_cuestion == 0){
    				$('#contenedortabla').hide();
					$('#contenedorbotonguardar').hide();
					$('#contenedornoencontrado').hide();

    		}      	      
	       // return false;
	   });
});

function AjusteTraslado(){


	var origen = $("#s_origen option:selected").val();
	var destino = $("#s_destino option:selected").val();

	
	
	$('#contenedortabla').hide();
	$('#contenedornoencontrado').empty();
	$('#contenedordeproductos').empty();		
	$('#contenedorbotonguardar').hide();

	if (origen == "Seleccione Origen.." || destino == "Seleccione Destino.."){
		$('#contenedorajustes').empty();
	}

	if (origen != "Seleccione Origen.." && destino != "Seleccione Destino.."){
		
		if (origen == destino){
			
			$.ajax({
			url: '/ajusteotraslado',
			type: 'POST',
			dataType: 'html',
			data:{origen:origen,destino:destino,tipo:"ajuste"},
			success : function(data){
				$('#cabeceraajuste').show();
				$('#cabeceratraslado').hide();				
				$('#contenedorajustes').html(data);
				$('#contenedorajustes').focus();
				}
			});

		}else{

			$.ajax({
			url: '/ajusteotraslado',
			type: 'POST',
			dataType: 'html',
			data:{origen:origen,destino:destino,tipo:"traslado"},
			success : function(data){
				$('#cabeceraajuste').hide();
				$('#cabeceratraslado').show();
				$('#contenedorajustes').html(data);
				$('#contenedorajustes').focus();

				}
			});		

		}
	}

}

function GetArticulo(){
	
	var cod_b_art =	$('#elarticulo').val();
	var origen = $("#s_origen option:selected").val();
	var destino = $("#s_destino option:selected").val();

	if (cod_b_art != ""){
		var carrito = $('#'+cod_b_art).val();
	
		var articulos_en_cuestion = $('#contenedordeproductos tr').length;
	

	if (carrito != undefined){
		var input_carrito = $('#'+cod_b_art);
		$('#contenedornoencontrado').empty();
		alert("Ya has agregado este articulo");
		input_carrito.focus();
	}else{		
			$.ajax({
				url: '/ajustearticulo',
				type: 'POST',
				dataType: 'html',
				data:{cod_b_art:cod_b_art,origen:origen,destino:destino,articulos_agregados:articulos_en_cuestion},
				success : function(data){
			
					switch (data) {

					    case '<h3 class="text-center">Origen: Desactivado     Destino: Activo</h3>':
					    	$('#contenedornoencontrado').show();
					    	$('#contenedornoencontrado').empty();
					        $('#contenedornoencontrado').append(data);
					       	break;
					    case '<h3 class="text-center">Origen: Bloqueado      Destino: Activo</h3>':
					    	$('#contenedornoencontrado').show();
					    	$('#contenedornoencontrado').empty();
					        $('#contenedornoencontrado').append(data);
					       	break;
					    case '<h3 class="text-center">Origen: No Existe     Destino: Activo</h3>':
					    	$('#contenedornoencontrado').show();
					    	$('#contenedornoencontrado').empty();
					        $('#contenedornoencontrado').append(data);
					       	break;
					    case '<h3 class="text-center">Origen: Activo      Destino: Desactivado</h3>':
					    	$('#contenedornoencontrado').show();
					    	$('#contenedornoencontrado').empty();
					        $('#contenedornoencontrado').append(data);
					       	break;
					    case '<h3 class="text-center">Origen: Activo      Destino: Bloqueado</h3>':
					    	$('#contenedornoencontrado').show();
					    	$('#contenedornoencontrado').empty();
					       	$('#contenedornoencontrado').append(data);
					       	break;
					    case '<h3 class="text-center">Origen: Activo      Destino: No Existe</h3>':
					    	$('#contenedornoencontrado').show();
					    	$('#contenedornoencontrado').empty();
					     	$('#contenedornoencontrado').append(data);
					       	break;
					    case '<h3 class="text-center">Articulo no encontrado</h3>':
					    	$('#contenedornoencontrado').show();
					    	$('#contenedornoencontrado').empty();
					        $('#contenedornoencontrado').append(data);
					        break;							
					    case '<h3 class="text-center">Origen: No Existe      Destino: No Existe</h3>':
					       	$('#contenedornoencontrado').show();
					       	$('#contenedornoencontrado').empty();
					        $('#contenedornoencontrado').append(data);
					    	break;	
					    default:
					    	$('#contenedornoencontrado').empty();
					       	$('#contenedorbotonguardar').show();
							$('#contenedortabla').show();
							$('#contenedordeproductos').append(data);
					}
				}
			});						
	}
	
	}else{
		$('#contenedornoencontrado').empty();
		alert("Introduce un código de barra");
	}
}


function Ejecutar(){

	var i = 0;
	var codigos = []
	var nombres = []
	var precios = []
	var existencias = []
	var operaciones = []
	var cantidades = []
	var existenciasd = []
	var origen = $("#s_origen option:selected").val();
	var destino = $("#s_destino option:selected").val();

	$("#contenedordeproductos tr.renglon").each(function(){
	
		var cds = $('#codigo_b'+i).html()
		var nms = $('#desc_b'+i).html()
		var prc = $('#precio_b'+i).html()
		var exs = $('#origen_b'+i).html()

		if (origen == destino){
			var ope = $('input:radio[name=operacion'+cds+']:checked').val(); 
		}else{
			var exsd = $('#destino_b'+i).html()
			existenciasd.push(exsd)
		}


		var cant = $('#'+cds).val()

		codigos.push(cds)
		nombres.push(nms)
		precios.push(prc)
		existencias.push(exs)
		operaciones.push(ope)
		cantidades.push(cant)
		i++;        	 
    });

    $.ajax({
		url: '/realizarmovimiento',
		type: 'POST',
		dataType: 'html',
		data:{ codigos:codigos, nombres:nombres,precios:precios, existencias:existencias, operaciones:operaciones, cantidades:cantidades,origen:origen,destino:destino,existenciasd:existenciasd},
		success : function(data){
			if (data == "no"){
				alert("No se puede surtir desde el almacen de origen.")
				location.reload("/ajustetraslado");
			}else{
				alert("Operacion Existosa")
				location.reload("/ajustetraslado");
			}
			}
		});

}


function BuscarArticulo(){
	alert("Ir a buscar articulo");	
}


