$(document).ready(function(){
	console.log("Movimientos listos");


});

function GetMovimientos(){

	var tipo_movimiento = $("#s_movimientos option:selected").val();

	if (tipo_movimiento != "Seleccione Movimiento.."){
		
		$.ajax({
			url: '/obtenermovimientos',
			type: 'POST',
			dataType: 'html',
			data:{tipo_movimiento:tipo_movimiento},
			success : function(data){							
				$('#contenedormovimientos').html(data);
				}
			});

	}else{

		$('#contenedormovimientos').empty();

	}

}

function GetProductosUnicos(idmovimiento){

	var tipo_movimiento = $("#s_movimientos option:selected").val();

	$.ajax({
		url: '/productosdemovimiento',
		type: 'POST',
		dataType: 'html',
		data:{idmovimiento:idmovimiento,tipo_movimiento:tipo_movimiento},
		success : function(data){							
			$('#contenedorproductosunicos').html(data);
		}
	});

}