
            $(document).ready(function () {
                    //Tooltip
                    $('[data-toggle="tooltip"]').tooltip(); 


                    //Oculta divs
                    $('#id_decimales').prop('disabled', 'disabled');


                    //Si cambia el radio físico muestra div anterior
                    $('#radioFisico').change(function () {
                      if ($(this).is(':checked')) {
                        $('#id_venta_fisico').show('fast');
                      } else {
                        $('#id_venta_fisico').hide('fast');
                      }
                    });


                    //si cambia a radio lógico debe ocultarse el div anterior
                    $('#radioLogico').change(function () {
                      $('#id_venta_fisico').hide('fast');
                    });


                   
                    $('#id_precio').blur(function () {
                      var precio = $('#id_precio').val();
                      var costo = $('#id_costo').val();                      
                      if ( !isNaN(precio) && !isNaN(costo) && precio <= costo){
                        $('#error_precio').addClass("alert alert-warning");
                        $('#error_precio').append("Te sugiero que el precio no sea menor o igual que el costo para que no tengas pérdidas, pero es tu pedo si la quieres cagar.");
                      }
                    });

                    $('#id_precio').focus(function () {
                      $('#error_precio').empty();
                      $('#error_precio').removeClass("alert alert-warning");
                    });


                    //al perder el foco el codigo se valida
                    $('#codigobarra').blur(function () {
                      var code = $('#codigobarra').val();
                      $.ajax({
                        url: '/ValidaCodigo',
                        type: 'POST',
                        dataType: 'html',
                        data: { code: code },
                        success: function (data, status) {
                          if (data != '') {
                            $('#error_codigo').append(data);
                            console.log(status);
                          } else {
                            console.log(status);
                          }
                        },
                      });
                    });

                    //al recibir el foco se debe vaciar el div de error
                    $('#codigobarra').focus(function () {
                      $('#error_codigo').empty();
                    });
                    $('#idecampo,#idevalor').focus(function () {
                      $('#error_campo_nuevo').empty();
                    });

                    //Si cambia la unidad cambia el label de Cantidad
                    $('#id_unidades').change(function () {
                      var unidad = $('#id_unidades').val();
                      if (unidad != 0) {
                        $('#cant').text(unidad + 's');
                      } else {
                        $('#cant').text('');
                      }
                    });

                    //validacion de combo venta fraccionaria
                    $('#id_venta_fraccionaria').change(function () {
                      if ($("#id_venta_fraccionaria").is(":checked")) {
                        $('#id_decimales').prop('disabled', false);
                        $('#id_decimales').val('');
                        $('#div_decimales').addClass("form-group");
                      } else {
                        $('#id_decimales').val('0');
                        $('#id_decimales').prop('disabled', 'disabled');
                        $('#div_decimales').removeClass("form-group");
                      }
                    });


                   var MaxInputs = 6; //Número Maximo de Campos
                    var AddButton = $("#AgregaCampo"); //ID del Botón Agregar
                    var CancelButton = $("#cancelarAdd");
                    var saveButton = $("#guardar_attribs")



                    $(AddButton).click(function () {
                      var x = document.getElementById("tbody_etiquetas").children.length;
                      var FieldCount = x + 1; //para el seguimiento de los campos
                      console.log(FieldCount);
                      $("#error_campo_nuevo").empty();

                      if (/^\s*$/.test($("#idecampo").val()) || /^\s*$/.test($("#idevalor").val())) {
                        $("#error_campo_nuevo").append("El nombre del campo o valor no puede estar vacío");
                      } else {
                        if ($("#div_tabla").children.length > 0) {

                        }
                        if (x < MaxInputs) {
                          $("#tbody_etiquetas").append(
                            '<tr>\n\
                            <td><input type="text" class="form-control" name="idecampo" value="' + $("#idecampo").val() + '" readonly></td>\n\
                            <td><input type="text" class="form-control" name="idevalor" value="' + $("#idevalor").val() + '" readonly></td>\n\
                            <td><button type="button" class="btn btn-danger deleteButton">\n\
                            <span class="glyphicon glyphicon-trash btn-xs"> </span></button></td>\n\
                         </tr>');
                          FieldCount++;
                          $("#idevalor").val('');
                          $("#idecampo").val('');
                        }
                      }
                    });

                    $(CancelButton).click(function () {
                      $("#tbody_etiquetas").empty();
                    });

                    $(saveButton).click(function () {
                      $("#tbody_tab_general").append($("#tbody_etiquetas")[0].children);
                    });


                    $(document).on('click', '.deleteButton', function () {
                      $(this).parent().parent().remove();
                    });
                   

                    $('#Myform').submit(function(event){
                    var code = $('#codigobarra').val();
                    if (code === ''){
                      GeneraCodigo();                    
                    }
                    });


                  });
                  ///////////////////////////////////////////////////////////////////////////////////   
                                   

                  function GeneraCodigo() {
                    $('#codigobarra').val('');
                    $.ajax({
                      url: '/GeneraCodigo',
                      type: 'POST',
                      dataType: 'html',
                      data: {},
                      success: function (data, status) {
                        if (data != '') {
                          $('#codigobarra').focus();
                          $('#codigobarra').val(data);
                          $('#codigobarra').blur();
                          console.log(status);

                        } else {
                          console.log(status);
                        }
                      },
                    });
                  }

                  function MuestradivImagen() {
                    if ($('#divimagen').is(':visible')) {
                      $('#divimagen').hide();
                    } else {
                      $('#divimagen').show();
                    }
                  }




