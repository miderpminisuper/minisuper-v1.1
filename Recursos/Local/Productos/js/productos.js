$(document).ready(function () {
	//window.onload = CargaTabla();
  	$('#FormBusqueda').submit(function(e){
      e.preventDefault();
      BuscaArticulos(true);
  	});

  	$('#FormBusqueda_frame').submit(function(e){
      e.preventDefault();
      BuscaArticulos(false);
  	});
		
	});

    function BuscaArticulos(frame) 
	{
        $('#id_mensaje').empty();
        $('#id_mensaje').empty();
        var dato = $('#id_Buscar').val();

        if (dato != "") {
          $.ajax({
            url: '/BuscaArticulos',
            type: 'POST',
            dataType: 'json',
            data: { dato: dato },
            success: function (data) {
               if (data != '') {
                 if (data != null) {
                     $('#id_elemento').empty();
                     var tabla = generarTabla(data, frame)
                     $('#id_elemento').html(tabla);
                     console.log(data);
                   }
                     $.ajax({
                       url: '/getPaginationResultSearchA',
                       type: 'POST',
                       dataType: 'html',
                       data: { getPage: 1 },
                       success: function (data) {
                         $('#pagination-ventas').html(data);
                       },
                     });
                   } else {
                     $('#id_mensaje').append(data);
                   }
                 },
                      error: function (xhr) {
                        $('#id_mensaje').empty();
                        $('#id_elemento').empty();
                        $('#id_mensaje').append("No se encontraron registros");
                      }
                  });

              } else {
                $('#id_mensaje').append("Debe escribir algo que consultar");
              }
            }

	function CargaTabla(frame) {
      $('#id_mensaje').empty();
          $.ajax({
            url: '/CargaArticulos',
            type: 'POST',
            dataType: 'json',
            data: {},
            success: function (data) {
              if (data != '') {
                if (data != null) {
                  $('#pagination-ventas').empty();
                  $('#id_elemento').empty();
                  var tabla = generarTabla(data,frame)
                  $('#id_elemento').html(tabla);
                  console.log(data);
                }
                  $.ajax({
                    url: '/getPaginationResultSearchA',
                    type: 'POST',
                    dataType: 'html',
                    data: { getPage: 1 },
                    success: function (data) {
                      $('#pagination-ventas').html(data);
                    },
                  });
              }else{
                $('#id_mensaje').append(data);
              }
            },
            error: function (xhr) {
              $('#pagination-ventas').empty();
              $('#id_elemento').empty();
            }
          });
      }

    function getPagination(page, frame) {
      $('#id_mensaje').empty();
      $.ajax({
        url: '/nexpaginationA',
        type: 'POST',
        dataType: 'json',
        data: { num_page: page },
        success: function (data) {
          var tabla = generarTabla(data, frame);
          $('#id_elemento').html(tabla);
          $.ajax({
            url: '/getPaginationResultSearchA',
            type: 'POST',
            dataType: 'html',
            data: { getPage: page },
            success: function (data) {
              $('#pagination-ventas').html(data);
            },
          });
        },
      });
    }

    function generarTabla(datos, frame) {	
      if (datos != null) {

        var tabla = document.createElement("table");
        tabla.id = "TablaIndex"
        var tblBody = document.createElement("tbody");
        var cabecera = document.createElement("thead");
        cabecera.className = "thead-inverse";
        var hilera = document.createElement("tr");

        var celda = document.createElement("th");
        celda.appendChild(document.createTextNode("IMAGEN"));
        hilera.appendChild(celda);
        var celda = document.createElement("th");
        celda.appendChild(document.createTextNode("CÓDIGO"));
        hilera.appendChild(celda);
        var celda = document.createElement("th");
        celda.appendChild(document.createTextNode("DESCRIPCIÓN"));
        hilera.appendChild(celda);
        var celda = document.createElement("th");
        celda.appendChild(document.createTextNode("TIPO"));
        hilera.appendChild(celda);
        var celda = document.createElement("th");
        celda.appendChild(document.createTextNode("UNIDAD"));
        hilera.appendChild(celda);
        var celda = document.createElement("th");
        celda.appendChild(document.createTextNode("PRECIO"));
        hilera.appendChild(celda);
        cabecera.appendChild(hilera);

        tabla.appendChild(cabecera);
        propiedades_hilera = new Array();

        for (var i = 0; i < datos.length; i++) {

          var IDe = datos[i].ID;
          var Codigo = datos[i].CodBarra;
          var descripcion = datos[i].Descripcion;
          var tipo = datos[i].Tipo;
          var unidad = datos[i].Unidad;
          var precio = datos[i].PreVenta;
          var imagen = datos[i].TagImg;

          //Crea primer renglón
          hilera = document.createElement("tr");

          //Crea primer Celda Imagen
          var celda = document.createElement("td");
          var img = document.createElement('img');
          img.src = imagen;
          img.setAttribute("id", Codigo);
          img.setAttribute('width', '50px');
          img.setAttribute('height', '50px');
          //  img.onclick = function () {

          //  };
          celda.appendChild(img);
          hilera.appendChild(celda);

          //Segunda celda Codigo
          var celda = document.createElement("td");
          celda.appendChild(document.createTextNode(Codigo));
          hilera.appendChild(celda);

          //Tercer celda Descripcion
          var celda = document.createElement("td");
          var a = document.createElement('a');
          a.appendChild(document.createTextNode(descripcion));
          a.title = Codigo;

          a.setAttribute("data-toggle", "modal");
          celda.appendChild(a);
          hilera.appendChild(celda);

          //Tipo
          var celda = document.createElement("td");
          celda.appendChild(document.createTextNode(tipo));
          hilera.appendChild(celda);

          //Unidad
          var celda = document.createElement("td");
          celda.appendChild(document.createTextNode(unidad));
          hilera.appendChild(celda);

          //Precio
          var celda = document.createElement("td");
          celda.appendChild(document.createTextNode(precio));
          hilera.appendChild(celda);

          //Atributos del renglón
          hilera.setAttribute("id", Codigo);
          hilera.onmouseover = function () { document.getElementById(this.id).style.backgroundColor = "#e3e3e3"; };
          hilera.onmouseout = function () { document.getElementById(this.id).style.backgroundColor = "#FAFAFA"; };
          hilera.ondblclick = function () {			
			if(frame)
			{
				window.location.replace("/articulos/" + this.id);
			}
			else
			{
				window.location.replace("/articulos_frame/" + this.id);
			}
          };
          tblBody.appendChild(hilera);
        }

        tabla.appendChild(tblBody);
        //tabla.setAttribute("border", "1");
        tabla.className = "table table-hover text-center col-md-12";
        tabla.setAttribute("id", "tablaArticulos")

        return tabla;
      }
    }
