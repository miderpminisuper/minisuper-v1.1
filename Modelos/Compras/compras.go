package ModeloCompras

import (
	"bytes"
	"context"
	"database/sql"
	"encoding/base64"
	"fmt"
	"image/jpeg"
	"image/png"
	"log"
	"path/filepath"
	strconv "strconv"
	"time"

	"../General"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	elastic "gopkg.in/olivere/elastic.v5"
)

//const (
//	SERVIDOR_MONGO              = "192.168.1.110"
//	BASE_MONGO                  = "minisuper"
//	COLECCION_PRODUCTOS_MONGO   = "productos"
//	COLECCION_ALMACENES_MONGO   = "almacenes"
//	COLECCION_PROVEEDORES_MONGO = "proveedores"
//	COLECCION_MOVIMIENTOS_MONGO = "movimientos"
//	COLECCION_IMPUESTOS_MONGO   = "impuestos"
//	COLECCION_IMAGENES_MONGO    = "ImagenesArticulos"
//	COLECCION_USUARIOS_MONGO    = "usuarios"
//	COLECCION_UNIDADES_MONGO    = "medidas"
//	BASE_POSTGRESQL             = "minisuper"
//	TABLA_POSTGRESQL            = "Almacen"
//	SERVIDOR_POSTGRESQL         = "192.168.1.110"
//	USER_POSTGRESQL             = "postgres"
//	PORT_POSTGRESQL             = "5432"
//	PASS_POSGRESQL              = "12345"

//	INDICE_ELASTICSEARCH       = "minisuper"
//	SERVIDOR_ELASTICSEARCH     = "192.168.1.110:9200"
//	COLECCION_ELASTICSEARCH    = "productos"
//	TOTAL_RESULTADOS_DEVUELTOS = 10000
//)

//Producto Estructura a escribir en template
type Producto struct {
	ID            bson.ObjectId     `bson:"_id,omitempty"`
	Cantidad      float64           `bson:"cantidad"`
	Costo         float64           `bson:"costo"`
	Impuestos     map[string]string `bson:"impuestos"`
	TotalImpuesto float64           `bson:"total_impuesto"`
	Importe       float64           `bson:"importe"`
	Ganancia      string            `bson:"ganancia"`
}

//IDE Estructura a escribir en template
type IDE struct {
	ID bson.ObjectId `bson:"_id,omitempty"`
}

//Articulo estructura de productos de mongo
type Articulo struct {
	ID          bson.ObjectId     `bson:"_id,omitempty"`
	CodBarra    string            `bson:"codigobarra"`
	Descripcion string            `bson:"descripcion"`
	Imagen      bson.ObjectId     `bson:"imagen,omitempty"`
	Fotos       []bson.ObjectId   `bson:"fotos,omitempty"`
	Tipo        string            `bson:"tipo"`
	Unidad      string            `bson:"unidad"`
	Entero      bool              `bson:"usa_fraccion"`
	NumDec      int               `bson:"num_dec"`
	Etiquetas   map[string]string `bson:"etiquetas"`
	PreCompra   float64           `bson:"costo"`
	PreVenta    float64           `bson:"precio"`
	CostoTotal  string            `bson:"costo_total,omitempty"`
	Impuestos   map[string]string `bson:"impuestos,omitempty"`
	TagImg      string
	Estatus     string
}

//ArticuloElastic estructura de datos de artículo para insertar en elastic
type ArticuloElastic struct {
	ID          bson.ObjectId     `bson:"_id,omitempty" 	json:"_id,omitempty"`
	CodBarra    string            `bson:"codigobarra" 		json:"codigobarra"`
	Descripcion string            `bson:"descripcion" 		json:"descripcion"`
	Imagen      bson.ObjectId     `bson:"imagen,omitempty" 	json:"imagen,omitempty"`
	Fotos       []bson.ObjectId   `bson:"fotos,omitempty" 	json:"fotos,omitempty"`
	Tipo        string            `bson:"tipo" 				json:"tipo"`
	Unidad      string            `bson:"unidad" 			json:"unidad"`
	Entero      bool              `bson:"usa_fraccion" 		json:"usa_fraccion"`
	NumDec      int               `bson:"num_dec" 			json:"num_dec"`
	Etiquetas   map[string]string `bson:"etiquetas" 		json:"etiquetas"`
	PreCompra   float64           `bson:"costo" 			json:"costo"`
	PreVenta    float64           `bson:"precio" 			json:"precio"`
}

//DatosArticulo estructura de datos de producto en PostgreSQL
type DatosArticulo struct {
	ID       string
	Cantidad float64
	Estatus  string
}

//Almacen estructura de almacenes en mongo
type Almacen struct {
	ID      bson.ObjectId     `bson:"_id,omitempty"`
	Nombre  string            `bson:"nombre"`
	Tipo    string            `bson:"tipo"`
	Defecto string            `bson:"defecto"`
	Datos   map[string]string `bson:"datos"`
}

//Provedores estructura de almacenes en mongo
type Provedores struct {
	ID     bson.ObjectId `bson:"_id,omitempty"`
	Nombre string        `bson:"nombre_completo"`
}

//Provedores estructura de almacenes en mongo
type Usuarios struct {
	ID     bson.ObjectId `bson:"_id,omitempty"`
	Nombre string        `bson:"nombre_completo"`
}

//Impuestos estructura de almacenes en mongo
type Impuestos struct {
	ID            bson.ObjectId    `bson:"_id,omitempty"`
	Grupo         string           `bson:"grupo_impuesto"`
	Prioridad     int              `bson:"prioridad"`
	DatosImpuesto []DatosImpuestos `bson:"impuestos"`
}

//DatosImpuestos estructura de datos de cada tipo de impuestos
type DatosImpuestos struct {
	Nombre      string  `bson:"nombre"`
	Tipo        string  `bson:"tipo"`
	Valor       float64 `bson:"valor"`
	Descripción string  `bson:"descripcion"`
}

//Unidades estructura de medidas en mongo
type Unidades struct {
	ID           bson.ObjectId  `bson:"_id,omitempty"`
	Grupo        string         `bson:"grupo_medida"`
	DatosMedidas []DatosMedidas `bson:"medidas"`
}

//DatosMedidas estructura de datos de cada tipo de medida
type DatosMedidas struct {
	Nombre      string `bson:"nombre"`
	Abreviatura string `bson:"abreviatura"`
}

//Movimiento estructura de movimientos en mongo
type Movimiento struct {
	ID             bson.ObjectId `bson:"_id,omitempty"`
	Tipo           string        `bson:"tipo_movimiento"`
	Date           time.Time     `bson:"fecha_hora"`
	AlmacenOrigen  bson.ObjectId `bson:"almacen_origen,omitempty"`
	AlmacenDestino bson.ObjectId `bson:"almacen_destino,omitempty"`
	UsuarioOrigen  bson.ObjectId `bson:"usuario_origen,omitempty"`
	UsuarioDestino bson.ObjectId `bson:"usuario_destino,omitempty"`
	Productos      []Producto    `bson:"productos"`
	Actualiza      []Editores    `bson:"edita"`
}

//Editores estructura de editores de compras
type Editores struct {
	ID   bson.ObjectId `bson:"_id,omitempty"`
	Tipo string        `bson:"tipo_movimiento"`
	Date time.Time     `bson:"fecha_hora"`
}

//Movimiento estructura de movimientos en mongo
type MovimientoIndex struct {
	ID             bson.ObjectId
	Date           time.Time
	AlmacenOrigen  Provedores
	AlmacenDestino Provedores
	UsuarioOrigen  Provedores
	UsuarioDestino Provedores
	Productos      []Producto
}

type Cliente struct {
	ID          bson.ObjectId     `bson:"_id,omitempty"`
	Rfc         string            `bson:"rfc"`
	Nombre      string            `bson:"nombre_completo"`
	Activo      bool              `bson:"activo"`
	Direccion   Direccion         `bson:"direccion"`
	Contacto    Contacto          `bson:"contacto"`
	Adicionales map[string]string `bson:"campos_adicionales"`
}

type Contacto struct {
	Id       bson.ObjectId `bson:"_id,omitempty"`
	Email    string        `bson:"email"`
	Telefono string        `bson:"telefono"`
	Celular  string        `bson:"celular"`
}

type Direccion struct {
	Id           bson.ObjectId `bson:"_id,omitempty"`
	Calle        string        `bson:"calle"`
	NumExterior  int           `bson:"num_ext"`
	NumInterior  int           `bson:"num_int"`
	Colonia      string        `bson:"colonia"`
	Localidad    string        `bson:"localidad"`
	Municipio    string        `bson:"municipio"`
	Estado       string        `bson:"estado"`
	CodigoPostal int           `bson:"cod_post"`
}

//ConsultaAlmacenesMgo regresa los almacenes de mongo
func ConsultaAlmacenesMgo() []Almacen {
	almacenes := []Almacen{}
	productos, sesion := ConectaMongoDB(models.NOMBRE_BD_MONGO, models.COLECCION_ALMACENES_MONGO, models.SERVIDOR_MONGO)
	err := productos.Find(nil).All(&almacenes)
	check(err, "Problema al consultar almacenes en mongo")
	sesion.Close()
	return almacenes
}

//ConsultaProveedorMgo consulta los proveedores de mgo
func ConsultaProveedorMgo() []Provedores {
	provedores := []Provedores{}
	productos, sesion := ConectaMongoDB(models.NOMBRE_BD_MONGO, models.COLECCION_PROVEEDORES, models.SERVIDOR_MONGO)
	err := productos.Find(bson.M{"activo": true}).All(&provedores)
	check(err, "Problema al consultar almacenes en mongo")
	sesion.Close()
	return provedores
}

//ConectarElastic crea conexion a elasticsearch
func ConectarElastic() *elastic.Client {
	client, err := elastic.NewClient(elastic.SetURL(models.SERVIDOR_ELASTICSEARCH))
	check(err, "No se pudo conectar con Elastic")
	return client
}

//BuscarEnElastic busca un texto específico en elástic
func BuscarEnElastic(texto string, client *elastic.Client) *elastic.SearchResult {
	boolquery := elastic.NewBoolQuery() //var boolquery *elastic.NewMatchQuery

	if texto != "" {
		boolquery = boolquery.Must(elastic.NewMatchQuery("_all", texto)).Should(elastic.NewMatchQuery("descripcion", texto))
	}

	docs, err := client.Search().Index(models.INDICE_ELASTICSEARCH).Type(models.COLECCION_ELASTICSEARCH).
		Query(boolquery).
		From(0).Size(models.TOTAL_RESULTADOS_DEVUELTOS).
		Do(context.TODO())
	check(err, "Error al realizar búsqueda en Elastic")
	return docs
}

//FlushElastic hace flush a la consulta realizada
func FlushElastic(client *elastic.Client) {
	_, err := client.Flush().Index(models.INDICE_ELASTICSEARCH).Do(context.TODO())
	check(err, "Error al limpiar Objeto de consulta de elastic")
}

//GetArticles regresa una coleccion de artículos de Mongo
func GetArticles(datos []bson.ObjectId) []Articulo {
	var result []Articulo
	var res Articulo
	c, _ := ConectaMongoDB(models.NOMBRE_BD_MONGO, models.COLECCION_PRODUCTOS, models.SERVIDOR_MONGO)

	for _, value := range datos {
		res = Articulo{}
		c.Find(bson.M{"_id": value}).One(&res)
		result = append(result, res)
	}
	return result
}

//GetArticle regresa una coleccion de artículos de Mongo
func GetArticle(datos bson.ObjectId) Articulo {
	var result Articulo

	c, s := ConectaMongoDB(models.NOMBRE_BD_MONGO, models.COLECCION_PRODUCTOS, models.SERVIDOR_MONGO)
	c.Find(bson.M{"_id": datos}).One(&result)
	s.Close()

	return result
}

//GetArticleForElastic regresa una coleccion de artículos de Mongo
func GetArticleForElastic(datos bson.ObjectId) ArticuloElastic {
	var result ArticuloElastic
	c, s := ConectaMongoDB(models.NOMBRE_BD_MONGO, models.COLECCION_PRODUCTOS, models.SERVIDOR_MONGO)
	c.Find(bson.M{"_id": datos}).One(&result)
	s.Close()
	return result
}

//GetAllCompras regresa los movimientos
func GetAllCompras() []MovimientoIndex {
	var result []Movimiento
	var res []MovimientoIndex
	var rex MovimientoIndex

	c, s := ConectaMongoDB(models.NOMBRE_BD_MONGO, models.COLECCION_MOVIMIENTOS_MONGO, models.SERVIDOR_MONGO)
	c.Find(bson.M{"tipo_movimiento": "compra"}).Sort("-fecha_hora").All(&result)
	s.Close()

	for _, v := range result {
		rex = MovimientoIndex{}
		rex.ID = v.ID
		rex.AlmacenDestino.ID = v.AlmacenDestino
		rex.AlmacenOrigen.ID = v.AlmacenOrigen
		rex.UsuarioOrigen.ID = v.UsuarioOrigen
		rex.UsuarioDestino.ID = v.UsuarioDestino
		rex.Date = v.Date
		rex.Productos = v.Productos
		res = append(res, rex)
	}
	return res
}

//GetCompras regresa movimientos específicos
func GetCompras(datos []bson.ObjectId) []MovimientoIndex {
	var result []Movimiento
	var res Movimiento
	var rets []MovimientoIndex
	var rex MovimientoIndex

	c, s := ConectaMongoDB(models.NOMBRE_BD_MONGO, models.COLECCION_MOVIMIENTOS_MONGO, models.SERVIDOR_MONGO)
	for _, value := range datos {
		res = Movimiento{}
		c.Find(bson.M{"_id": value}).One(&res)
		result = append(result, res)
	}

	for _, v := range result {
		rex = MovimientoIndex{}
		rex.ID = v.ID
		rex.AlmacenDestino.ID = v.AlmacenDestino
		rex.AlmacenOrigen.ID = v.AlmacenOrigen
		rex.UsuarioOrigen.ID = v.UsuarioOrigen
		rex.UsuarioDestino.ID = v.UsuarioDestino
		rex.Date = v.Date
		rex.Productos = v.Productos
		rets = append(rets, rex)
	}

	s.Close()
	return rets
}

//GetCompra regresa un movimiento específico por id
func GetCompraById(ide string) Movimiento {
	var res Movimiento
	ids := bson.ObjectIdHex(ide)

	c, s := ConectaMongoDB(models.NOMBRE_BD_MONGO, models.COLECCION_MOVIMIENTOS_MONGO, models.SERVIDOR_MONGO)
	c.Find(bson.M{"_id": ids}).One(&res)
	s.Close()
	return res
}

//ConsultaCompra verifica si existe la compra asociada a un id dado
func ConsultaCompra(id string) int {
	moves, sesion := ConectaMongoDB(models.NOMBRE_BD_MONGO, models.COLECCION_MOVIMIENTOS_MONGO, models.SERVIDOR_MONGO)
	n, err := moves.Find(bson.M{"_id": bson.ObjectIdHex(id)}).Count()
	check(err, "Error al consultar codigo de producto en MONGO")
	sesion.Close()
	return n
}

func GetNameProvedor(id bson.ObjectId) string {
	var provedor Provedores
	c, s := ConectaMongoDB(models.NOMBRE_BD_MONGO, models.COLECCION_PROVEEDORES, models.SERVIDOR_MONGO)
	c.Find(bson.M{"_id": id}).One(&provedor)
	s.Close()
	return provedor.Nombre
}

func GetNameAlmacen(id bson.ObjectId) string {
	var almacen Almacen
	c, s := ConectaMongoDB(models.NOMBRE_BD_MONGO, models.COLECCION_ALMACENES_MONGO, models.SERVIDOR_MONGO)
	c.Find(bson.M{"_id": id}).One(&almacen)
	s.Close()
	return almacen.Nombre
}

func GetNameUsuario(id bson.ObjectId) string {
	var user Usuarios
	c, s := ConectaMongoDB(models.NOMBRE_BD_MONGO, models.COLECCION_ALMACENES_MONGO, models.SERVIDOR_MONGO)
	c.Find(bson.M{"_id": id}).One(&user)
	s.Close()
	return user.Nombre
}

//RegresaTagImagen regresa el tag de la imagen con ID correspondiente
func RegresaTagImagen(ID string) (string, string) {

	Base, err := models.ConectarMgoRegresaBase(models.NOMBRE_BD_MONGO, models.SERVIDOR_MONGO)
	check(err, "Error al conectar con almacenes de MongoDB")
	objid := bson.ObjectIdHex(ID)
	img, err1 := Base.GridFS(models.COLECCION_IMAGENES_MONGO).OpenId(objid)
	check(err1, "Error al leer Imagen de MONGO")

	b := make([]byte, img.Size())
	img.Read(b)

	imagen, _ := jpeg.Decode(bytes.NewReader(b))

	buffer := new(bytes.Buffer)

	if err2 := jpeg.Encode(buffer, imagen, nil); err2 != nil {
		log.Println("unable to encode image.")
	}
	str := base64.StdEncoding.EncodeToString(buffer.Bytes())
	var tmp = `data:image/jpg;base64,` + str

	return tmp, img.Name()
}

//AbreImagenes regresa el tag de la imagen de un conjunto de Articulos
func AbreImagenes(enc []Articulo, gridfs *mgo.GridFS) []Articulo {

	for i, v := range enc {
		if v.Imagen.Hex() != "" {
			img, err1 := gridfs.OpenId(v.Imagen)
			defer img.Close()
			check(err1, "Error al leer Imagen de MONGO: "+v.Imagen.Hex())

			b := make([]byte, img.Size())
			n, err := img.Read(b)
			check(err, "Error al crear mapa de bytes")

			fmt.Println("N -> ", n)
			imagen, err := jpeg.Decode(bytes.NewReader(b))
			check(err, "Error al decodificar")
			buffer := new(bytes.Buffer)

			err2 := jpeg.Encode(buffer, imagen, nil)
			check(err2, "Error al codificar.")

			str := base64.StdEncoding.EncodeToString(buffer.Bytes())
			var tmp = `data:image/jpg;base64,` + str

			enc[i].TagImg = tmp
		}
	}
	return enc
}

//RegresaSrcImagenes regresa el tag de la imagen de un conjunto de Articulos
func RegresaSrcImagenes(enc []Articulo) []Articulo {
	Base, err := models.ConectarMgoRegresaBase(models.NOMBRE_BD_MONGO, models.SERVIDOR_MONGO)
	check(err, "Error al conectar con almacenes de MongoDB")

	imagenes := Base.C(models.COLECCION_IMAGENES_FILES_MONGO)
	tmp := ``
	for i, v := range enc {
		if v.Imagen.Hex() != "" {

			n, err := imagenes.Find(bson.M{"_id": v.Imagen}).Count()
			check(err, "Error al consultar id de imagen de producto en MONGO")

			if n > 0 {
				img, err1 := Base.GridFS(models.COLECCION_IMAGENES_MONGO).OpenId(v.Imagen)

				check(err1, "Error al leer Imagen de MONGO: "+v.Imagen.Hex())

				b := make([]byte, img.Size())
				_, err := img.Read(b)
				check(err, "Error al crear mapa de bytes")

				switch extension := filepath.Ext(img.Name()); extension {
				case ".jpg", ".jpeg":
					imagen, _ := jpeg.Decode(bytes.NewReader(b))
					buffer := new(bytes.Buffer)
					if err := jpeg.Encode(buffer, imagen, nil); err != nil {
						log.Println("unable to encode image.")
					}
					str := base64.StdEncoding.EncodeToString(buffer.Bytes())
					tmp = `data:image/jpg;base64,` + str

				case ".png":

					imagen, _ := png.Decode(bytes.NewReader(b))
					buffer := new(bytes.Buffer)
					if err := png.Encode(buffer, imagen); err != nil {
						log.Println("unable to encode image.")
					}
					str := base64.StdEncoding.EncodeToString(buffer.Bytes())
					tmp = `data:image/png;base64,` + str

				}

				enc[i].TagImg = tmp

				defer img.Close()
			}
		}
	}
	return enc
}

//RegresaSrcImagene regresa el tag de la imagen de un conjunto de Articulos
func RegresaSrcImagen(enc Articulo) Articulo {
	Base, err := models.ConectarMgoRegresaBase(models.NOMBRE_BD_MONGO, models.SERVIDOR_MONGO)
	check(err, "Error al conectar con almacenes de MongoDB")

	if enc.Imagen.Hex() != "" {
		img, err1 := Base.GridFS(models.COLECCION_IMAGENES_MONGO).OpenId(enc.Imagen)

		check(err1, "Error al leer Imagen de MONGO: "+enc.Imagen.Hex())

		b := make([]byte, img.Size())
		_, err := img.Read(b)
		check(err, "Error al crear mapa de bytes")

		var tmp = ``
		///////////////////////////////////////////////////////////////////////////
		switch extension := filepath.Ext(img.Name()); extension {

		case ".jpg", ".jpeg":
			imagen, _ := jpeg.Decode(bytes.NewReader(b))
			buffer := new(bytes.Buffer)
			if err := jpeg.Encode(buffer, imagen, nil); err != nil {
				log.Println("unable to encode image.")
			}
			str := base64.StdEncoding.EncodeToString(buffer.Bytes())
			tmp = `data:image/jpg;base64,` + str

		case ".png":

			imagen, _ := png.Decode(bytes.NewReader(b))
			buffer := new(bytes.Buffer)
			if err := png.Encode(buffer, imagen); err != nil {
				log.Println("unable to encode image.")
			}
			str := base64.StdEncoding.EncodeToString(buffer.Bytes())
			tmp = `data:image/png;base64,` + str

		}
		enc.TagImg = tmp
		defer img.Close()
	}
	return enc
}

//RegresaEstatusDeAlmacen regresa el tag de la imagen de un conjunto de Articulos
func RegresaEstatusDeAlmacen(enc Articulo, almacen string, ID string) Articulo {

	BasePosGres := models.ConectarPostgres(models.USER_POSTGRESQL, models.PASS_POSGRESQL, models.BASE_POSTGRESQL, models.SERVIDOR_POSTGRESQL, models.PORT_POSTGRESQL, "disable")
	query := `select "estatus" from "` + almacen + `" where 'id_producto' = '` + ID + `'`
	row, err := BasePosGres.Query(query)
	check(err, "Error al Consultar ID en Postgresql")
	var estatus string
	for row.Next() {
		err := row.Scan(&estatus)
		check(err, "Error al scanear estatus de postgres")
	}
	row.Close()
	BasePosGres.Close()

	enc.Estatus = estatus

	//////////agregar combo ya hecho en html con e estatus seleccionado para mandarlo y plancharlo en la vista

	return enc
}
func RegresaTagImagenes(enc Articulo) Articulo {
	Base, err := models.ConectarMgoRegresaBase(models.NOMBRE_BD_MONGO, models.SERVIDOR_MONGO)

	check(err, "Error al conectar con almacenes de MongoDB")
	tagimg := ``

	if enc.Imagen.Hex() != "" {
		img, err1 := Base.GridFS(models.COLECCION_IMAGENES_MONGO).OpenId(enc.Imagen)

		check(err1, "Error al leer Imagen de MONGO: "+enc.Imagen.Hex())

		b := make([]byte, img.Size())
		_, err := img.Read(b)
		check(err, "Error al crear mapa de bytes")

		imagen, err := jpeg.Decode(bytes.NewReader(b))
		check(err, "Error al decodificar")
		buffer := new(bytes.Buffer)

		err2 := jpeg.Encode(buffer, imagen, nil)
		check(err2, "Error al codificar.")

		str := base64.StdEncoding.EncodeToString(buffer.Bytes())
		var tmp = `data:image/jpg;base64,` + str

		tagimg = `<img id="` + img.Name() + `" alt="Responsive image" name="ImagenProducto" width="50px" height="50px";" src="` + tmp + `">`
		enc.TagImg = tagimg
		defer img.Close()

	}
	return enc
}

func GeneraSelectImpuestos() string {
	impuestos := ConsultaImpuestosMgo()
	templ := ``
	opc := ``
	for _, v := range impuestos {
		templ += `<div class="col-lg-6 col-md-12">
                          <div class="form-group">
                            <div id="div_impuestos" class="input-group input-group-md" data-toggle="tooltip" data-placement="top" title="Porcentaje Correspondiente a Impuesto">
                              <label for="id_impuestos" class="input-group-addon"><span>Decimales:</span></label>
                                <select class="form-control" id="id_impuestos" name="impuestos">
                                  <option value="">--SELECCIONE--</option>`
		opc = ``
		for _, val := range v.DatosImpuesto {
			opc += `<option value="` + strconv.FormatFloat(val.Valor, 'f', 2, 64) + `">` + val.Nombre + `</option>`
		}

		templ = templ + opc + `</select></div><small class="help-block" id="error_decimales"></small></div></div>`
	}
	return templ
}

func GeneraSelectImpuestosModalEdit() string {
	impuestos := ConsultaImpuestosMgo()
	templ := ``
	opc := ``
	for _, v := range impuestos {
		templ += ` <div class="col-lg-6 col-md-12">
                          <div class="form-group">
                            <div id="div_valimpuestos_edit" class="input-group input-group-md" data-toggle="tooltip" data-placement="top" title="Porcentaje Correspondiente a Impuesto">
                              <label for="id_valimpuestos_edit" class="input-group-addon"><span>` + v.Grupo + `:</span></label>
                                <select class="form-control" id="id_valimpuestos_edit" name="impuestos_edit_cb" nombre="` + v.Grupo + `">
                                  <option value="">--SELECCIONE--</option>`
		opc = ``
		for _, val := range v.DatosImpuesto {
			opc += `<option value="` + strconv.FormatFloat(val.Valor, 'f', 2, 64) + `">` + val.Nombre + `</option>`
		}

		templ = templ + opc + `</select></div><small class="help-block" id="error_impuestos"></small></div></div>`
	}
	return templ
}

func GeneraSelectImpuestosModalAlta() string {
	impuestos := ConsultaImpuestosMgo()
	templ := ``
	opc := ``

	for _, v := range impuestos {

		templ += ` <div class="col-lg-6 col-md-12">
                          <div class="form-group">
                            <div id="div_valimpuestos_alta" class="input-group input-group-md" data-toggle="tooltip" data-placement="top" title="Porcentaje Correspondiente a Impuesto">
                              <label for="id_valimpuestos_alta" class="input-group-addon"><span>` + v.Grupo + `:</span></label>
                                <select class="form-control" id="id_valimpuestos_alta" name="impuestos_alta_cb" nombre="` + v.Grupo + `">
                                  <option value="">--SELECCIONE--</option>`
		opc = ``
		for _, val := range v.DatosImpuesto {
			opc += `<option value="` + strconv.FormatFloat(val.Valor, 'f', 2, 64) + `">` + val.Nombre + `</option>`
		}

		templ = templ + opc + `</select></div><small class="help-block" id="error_impuestos"></small></div></div>`
	}
	return templ
}

//ConsultaImpuestosMgo consulta los proveedores de mgo
func ConsultaImpuestosMgo() []Impuestos {
	impuestos := []Impuestos{}
	datos, sesion := ConectaMongoDB(models.NOMBRE_BD_MONGO, models.COLECCION_IMPUESTOS_MONGO, models.SERVIDOR_MONGO)
	err := datos.Find(nil).All(&impuestos)
	check(err, "Problema al consultar impuestos en mongo")
	sesion.Close()
	return impuestos
}

func GeneraSelectUnidades() string {
	unidades := ConsultaUnidadesMgo()
	templ := `<option value="" selected>--SELECCIONE--</option>`
	opc := ``

	for _, v := range unidades {
		templ += `<optgroup label="` + v.Grupo + `">`
		opc = ``
		for _, val := range v.DatosMedidas {
			opc += `<option value="` + val.Nombre + `">` + val.Abreviatura + `</option>`
		}

		templ = templ + opc + `</optgroup>`
	}
	return templ
}

//ConsultaUnidadesMgo consulta los proveedores de mgo
func ConsultaUnidadesMgo() []Unidades {
	unidades := []Unidades{}
	datos, sesion := ConectaMongoDB(models.NOMBRE_BD_MONGO, models.COLECCION_IMPUESTOS_MONGO, models.SERVIDOR_MONGO)
	err := datos.Find(nil).All(&unidades)
	check(err, "Problema al consultar unidades en mongo")
	sesion.Close()
	return unidades
}

func ConsultaIDUsuario(nombre string) IDE {
	var result IDE
	c, s := ConectaMongoDB(models.NOMBRE_BD_MONGO, models.COLECCION_USUARIOS_MONGO, models.SERVIDOR_MONGO)
	c.Find(bson.M{"usuario": nombre}).Select(bson.M{"_id": 1}).One(&result)
	s.Close()
	return result
}

//ActualizaProductoDeComprasEnMongo inserta un producto en mongo
func ActualizaProductoDeComprasEnMongo(id string, costo string, precio string, costoT string, impuestos map[string]string) {
	colec, sesion := ConectaMongoDB(models.NOMBRE_BD_MONGO, models.COLECCION_PRODUCTOS, models.SERVIDOR_MONGO)
	// sesion, err := mgo.Dial(SERVIDOR_MONGO)
	// check(err, "Error al conectar con minisuper de MongoDB")
	// colec := sesion.DB(BASE_MONGO).C(COLECCION_PRODUCTOS_MONGO)
	cost, _ := strconv.ParseFloat(costo, 64)
	price, _ := strconv.ParseFloat(precio, 64)
	change := bson.M{"$set": bson.M{"costo": cost, "precio": price, "costo_total": costoT, "impuestos": impuestos}}
	err := colec.Update(bson.M{"_id": bson.ObjectIdHex(id)}, change)
	check(err, "Error al Actualizar producto a MONGO")

	sesion.Close()
}

//ActualizaProductoDeComprasEnPostgreSQL inserta datos de producto de mongo asociado a un ID
func ActualizaProductoDeComprasEnPostgreSQL(cantidad string, id string) {
	BasePosGres := models.ConectarPostgres(models.USER_POSTGRESQL, models.PASS_POSGRESQL, models.BASE_POSTGRESQL, models.SERVIDOR_POSTGRESQL, models.PORT_POSTGRESQL, "disable")
	query := ``
	n := ConsultaIDPostgres(id)

	if n > 0 {
		query = `UPDATE "` + models.TABLA_POSTGRESQL + `"  SET  "cantidad" = "cantidad" + ` + cantidad + ` WHERE id_producto = '` + id + `'`
		con, err := BasePosGres.Query(query)
		check(err, "Error al Actualizar producto en Postgresql")
		con.Close()
	} else {
		query := `INSERT INTO "` + models.TABLA_POSTGRESQL + `" ("id_producto", "cantidad", "estatus") VALUES('` + id + `',` + cantidad + `,'ACTIVO')`
		con, err := BasePosGres.Query(query)
		check(err, "Error al Insertar producto en Postgresql desde actualización")
		con.Close()
	}

	BasePosGres.Close()
}

//ActualizaProductoDeComprasEnPostgreSQL2 realiza un una actualización en el almacen donde se ha realizado la compra por id de producto y nueva cantidad
func ActualizaProductoDeComprasEnPostgreSQL2(almacenDestino string, cantidad string, producto string) (bool, string) {
	dbinfo := fmt.Sprintf("host=%s user=%s password=%s dbname=%s sslmode=disable", models.SERVIDOR_POSTGRESQL, models.USER_POSTGRESQL, models.PASS_POSGRESQL, models.BASE_POSTGRESQL)
	db, err := sql.Open("postgres", dbinfo)
	check(err, "Ocurrió un error al crear sesión en Postgres")
	defer db.Close()

	tx, err := db.Begin()
	check(err, "Problema al iniciar la seción")

	queryx := `select count(*) from  public."` + almacenDestino + `" where  "id_producto" = '` + producto + `'`
	fmt.Println(queryx)
	row, err := db.Query(queryx)
	check(err, "Error al Consultar ID de producto en Postgresql en el almacen: "+almacenDestino)
	var datos int
	for row.Next() {
		err := row.Scan(&datos)
		check(err, "Error al scanear ID de postgres")
	}
	row.Close()

	if datos > 0 {
		db.Exec("set transaction isolation level serializable")
		quer := fmt.Sprintf(`SELECT "cantidad" FROM public."%v" WHERE 'id_producto' = '%v' FOR UPDATE`, almacenDestino, producto)
		stmt, err := tx.Prepare(quer)
		check(err, "Ocurrió un error al preparar query en Postgres")

		resultSet, err := stmt.Query()
		check(err, "Ocurrió un error al ejecutar query en Postgres")

		var e float64
		e = 0
		estatus := ""
		for resultSet.Next() {
			resultSet.Scan(&e, &estatus)
		}

		fmt.Println("Existencia de Almacen: ", e, ", de producto: ", producto, " con Estatus ", estatus)

		querry := fmt.Sprintf(`UPDATE public."%v"  SET  "cantidad" = "cantidad" + %v, "estatus" = 'ACTIVO' WHERE id_producto='%v'`, almacenDestino, cantidad, producto)
		tx.Exec(querry)
		tx.Commit()
		resultSet.Close()
		stmt.Close()
		db.Close()
		return true, "Actualizado"
	} else {
		queryi := `INSERT INTO public."` + almacenDestino + `" ("id_producto", "cantidad", "estatus") VALUES('` + producto + `',` + cantidad + `,'ACTIVO')`
		con, err := db.Query(queryi)
		check(err, "Error al Insertar producto en Postgresql desde actualización")
		con.Close()
		db.Close()
		return true, "Creado"
	}

}

//RollbackPostgres inserta datos de producto de mongo asociado a un ID
func RollbackPostgres(cantidad string, id bson.ObjectId) {
	BasePosGres := models.ConectarPostgres(models.USER_POSTGRESQL, models.PASS_POSGRESQL, models.BASE_POSTGRESQL, models.SERVIDOR_POSTGRESQL, models.PORT_POSTGRESQL, "disable")
	query := ``
	n := ConsultaIDPostgres(id.Hex())
	if n > 0 {
		query = `UPDATE "` + models.TABLA_POSTGRESQL + `"  SET  "cantidad" = "cantidad" - ` + cantidad + ` WHERE id_producto = '` + id.Hex() + `'`
		con, err := BasePosGres.Query(query)
		check(err, "Error al Actualizar producto en Postgresql")
		con.Close()
	} else {
		query := `INSERT INTO "` + models.TABLA_POSTGRESQL + `" ("id_producto", "cantidad", "estatus") VALUES('` + id.Hex() + `',` + cantidad + `,'ACTIVO')`
		con, err := BasePosGres.Query(query)
		check(err, "Error al Insertar producto en Postgresql desde actualización")
		con.Close()
	}

	BasePosGres.Close()
}

//ConsultaIDPostgres busca el código de un producto y regresa el número de registros encontrados
func ConsultaIDPostgres(ID string) int {
	BasePosGres := models.ConectarPostgres(models.USER_POSTGRESQL, models.PASS_POSGRESQL, models.BASE_POSTGRESQL, models.SERVIDOR_POSTGRESQL, models.PORT_POSTGRESQL, "disable")
	query := `select count(*) from "` + models.TABLA_POSTGRESQL + `" where "id_producto" = '` + ID + `'`
	row, err := BasePosGres.Query(query)
	check(err, "Error al Consultar ID en Postgresql")
	var datos int
	for row.Next() {
		err := row.Scan(&datos)
		check(err, "Error al scanear ID de postgres")
	}
	row.Close()
	BasePosGres.Close()

	return datos
}

func InsertaMovimiento(move Movimiento) {
	productos, sesion := ConectaMongoDB(models.NOMBRE_BD_MONGO, models.COLECCION_MOVIMIENTOS_MONGO, models.SERVIDOR_MONGO)
	err := productos.Insert(bson.M{"tipo_movimiento": move.Tipo, "fecha_hora": move.Date, "almacen_origen": move.AlmacenOrigen, "almacen_destino": move.AlmacenDestino, "usuario_origen": move.UsuarioOrigen, "usuario_destino": move.UsuarioDestino, "productos": move.Productos})
	check(err, "Error al Insertar producto a MONGO")
	sesion.Close()
}

func ActualizaMovimiento(move Movimiento) {

	colec, sesion := ConectaMongoDB(models.NOMBRE_BD_MONGO, models.COLECCION_MOVIMIENTOS_MONGO, models.SERVIDOR_MONGO)

	change := bson.M{"$set": bson.M{"almacen_origen": move.AlmacenOrigen, "almacen_destino": move.AlmacenDestino, "productos": move.Productos}}
	err := colec.Update(bson.M{"_id": move.ID}, change)
	check(err, "Error al Actualizar almacen y proveedor de movimiento en MONGO")

	err = colec.Update(bson.M{"_id": move.ID}, bson.M{"$push": bson.M{"edita": move.Actualiza}})
	check(err, "Error al Agregar editor de movimiento en MONGO")

	sesion.Close()
}

func EliminaMovimiento(id string) {
	colec, sesion := ConectaMongoDB(models.NOMBRE_BD_MONGO, models.COLECCION_MOVIMIENTOS_MONGO, models.SERVIDOR_MONGO)
	err := colec.Remove(bson.M{"_id": bson.ObjectIdHex(id)})
	check(err, "Error al eliminar movimiento en MONGO")

	sesion.Close()
}

//ConsultaUnidadesDistintas funcion que selecciona de Mongo todas
//las unidades distintas de los productos
func ConsultaUnidadesDistintas() []string {
	var unidades []string
	productos, sesion := ConectaMongoDB(models.NOMBRE_BD_MONGO, models.COLECCION_MOVIMIENTOS_MONGO, models.SERVIDOR_MONGO)
	err := productos.Find(nil).Distinct("unidad", &unidades)
	check(err, "Problema al consultar articulos distintos")
	sesion.Close()
	return unidades
}

//ConsultaCodigo busca el código de un producto y regresa el número de registros encontrados
func ConsultaCodigo(codigo string) int {
	productos, sesion := ConectaMongoDB(models.NOMBRE_BD_MONGO, models.COLECCION_PRODUCTOS, models.SERVIDOR_MONGO)
	n, err := productos.Find(bson.M{"codigobarra": codigo}).Count()
	check(err, "Error al consultar codigo de producto en MONGO")
	sesion.Close()
	return n
}

//GeneraCodigo funcion que genera codigo de barras a partir de un objectid aleatorio
func GeneraCodigo() string {
	OBJ := bson.NewObjectIdWithTime(time.Now())
	return OBJ.Hex()[:10]
}

//InsertaProductoEnMongo inserta un producto en mongo
func InsertaProductoEnMongo(producto Articulo) {
	productos, sesion := ConectaMongoDB(models.NOMBRE_BD_MONGO, models.COLECCION_PRODUCTOS, models.SERVIDOR_MONGO)
	if producto.Imagen.Hex() != "" {
		err := productos.Insert(bson.M{"_id": producto.ID, "codigobarra": producto.CodBarra, "descripcion": producto.Descripcion, "tipo": producto.Tipo, "unidad": producto.Unidad, "usa_fraccion": producto.Entero, "num_dec": producto.NumDec, "etiquetas": producto.Etiquetas, "costo": producto.PreCompra, "precio": producto.PreVenta, "imagen": producto.Imagen})
		check(err, "Error al Insertar producto a MONGO")
	} else {
		err := productos.Insert(bson.M{"_id": producto.ID, "codigobarra": producto.CodBarra, "descripcion": producto.Descripcion, "tipo": producto.Tipo, "unidad": producto.Unidad, "usa_fraccion": producto.Entero, "num_dec": producto.NumDec, "etiquetas": producto.Etiquetas, "costo": producto.PreCompra, "precio": producto.PreVenta})
		check(err, "Error al Insertar producto a MONGO")
	}
	sesion.Close()
}

//InsertaProductoEnPostgreSQL inserta datos de producto de mongo asociado a un ID
func InsertaProductoEnPostgreSQL(datos DatosArticulo, dec int) {
	BasePosGres := models.ConectarPostgres(models.USER_POSTGRESQL, models.PASS_POSGRESQL, models.BASE_POSTGRESQL, models.SERVIDOR_POSTGRESQL, models.PORT_POSTGRESQL, "disable")
	///////
	///////  Agregar validacion de transaccion única y que el artículo exista o no
	///////

	query := `INSERT INTO "` + models.TABLA_POSTGRESQL + `" ("id_producto", "cantidad", "cstatus") VALUES('` + datos.ID + `',` + strconv.FormatFloat(datos.Cantidad, 'f', dec, 32) + `,'` + datos.Estatus + `')`
	con, err := BasePosGres.Query(query)
	check(err, "Error al Insertar producto en Postgresql")
	con.Close()
	BasePosGres.Close()

	////////////////
	////////////////
}

func InsertaAlmacenDesdeComprasMGO(almacen Almacen) {
	almacenes, sesion := ConectaMongoDB(models.NOMBRE_BD_MONGO, models.COLECCION_ALMACENES_MONGO, models.SERVIDOR_MONGO)
	err := almacenes.Insert(bson.M{"_id": almacen.ID, "nombre": almacen.Nombre, "tipo": almacen.Tipo, "defecto": almacen.Defecto, "datos": almacen.Datos})
	check(err, "Error al Insertar almacen a MONGO")
	sesion.Close()
}

func InsertaAlmacenDesdeComprasSQL(id string) {
	BasePosGres := models.ConectarPostgres(models.USER_POSTGRESQL, models.PASS_POSGRESQL, models.BASE_POSTGRESQL, models.SERVIDOR_POSTGRESQL, models.PORT_POSTGRESQL, "disable")
	query := `CREATE TABLE ` + `"` + id + `"` + `()`

	id_producto := `ALTER TABLE ` + `"` + id + `"` + `ADD COLUMN id_producto VARCHAR(25)`
	cantidad := `ALTER TABLE ` + `"` + id + `"` + `ADD COLUMN cantidad NUMERIC`
	estatus := `ALTER TABLE ` + `"` + id + `"` + `ADD COLUMN estatus VARCHAR(30)`

	_, _ = BasePosGres.Exec(query)
	_, _ = BasePosGres.Exec(id_producto)
	_, _ = BasePosGres.Exec(cantidad)
	_, _ = BasePosGres.Exec(estatus)
	BasePosGres.Close()

}

func ActualizaUnidadDesdeCompras(unidad Unidades) {
	unidades, sesion := ConectaMongoDB(models.NOMBRE_BD_MONGO, models.COLECCION_UNIDADES_MONGO, models.SERVIDOR_MONGO)
	change := bson.M{"$set": bson.M{"medidas": unidad.DatosMedidas}}
	err := unidades.Update(bson.M{"_id": unidad.ID}, change)
	check(err, "Error al Actualizar unidad a MONGO")
	sesion.Close()
}

func InsertaUnidadDesdeCompras(unidad Unidades) {
	unidades, sesion := ConectaMongoDB(models.NOMBRE_BD_MONGO, models.COLECCION_UNIDADES_MONGO, models.SERVIDOR_MONGO)
	err := unidades.Insert(bson.M{"_id": unidad.ID, "grupo_medida": unidad.Grupo, "medidas": unidad.DatosMedidas})
	check(err, "Error al Insertar almacen a MONGO")
	sesion.Close()
}

/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////

///// DatosCliente Para fatcturacion
func DatosCliente(id bson.ObjectId) Cliente {
	cliente := Cliente{}	
	datos, sesion := ConectaMongoDB(models.NOMBRE_BD_MONGO, models.COLECCION_CLIENTES, models.SERVIDOR_MONGO)
	err := datos.Find(bson.M{"_id": id}).One(&cliente)
	check(err, "Problema al consultar clientes en mongo")
	sesion.Close()
	return cliente
}

// FormasPago la forma en que se pago la venta
func DatosAlmacen(id bson.ObjectId) Almacen {
	almacen := Almacen{}
datos, sesion := ConectaMongoDB(models.NOMBRE_BD_MONGO, models.COLECCION_ALMACENES_MONGO, models.SERVIDOR_MONGO)
	err := datos.Find(bson.M{"_id": id}).One(&almacen)
	check(err, "Problema al consultar Almacen en mongo")
	sesion.Close()
	return almacen
}
