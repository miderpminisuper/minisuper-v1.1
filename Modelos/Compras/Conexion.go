// Conexion
package ModeloCompras

import (
	"database/sql"
	"fmt"

	_ "github.com/lib/pq"
	mgo "gopkg.in/mgo.v2"
)

func check(err error, mensaje string) {
	if err != nil {
		fmt.Println("____")
		fmt.Println(mensaje)
		fmt.Println("____")
		panic(err)
	}
}

func ConexionPostgres(host string, nombreBD string, usuarioDb string, passwordDb string) *sql.DB {
	db, err := sql.Open("postgres", "host="+host+" user="+usuarioDb+" dbname="+nombreBD+" password="+passwordDb+" sslmode=disable")
	check(err, "Error al conectar con Postgres")
	return db
}

func ConectaMongoDB(Base string, Coleccion string, Ip string) (*mgo.Collection, *mgo.Session) {
	session, err := mgo.Dial(Ip)
	check(err, "Error al conectar con almacenes de MongoDB")
	//session.SetMode(mgo.Monotonic, true)
	colec := session.DB(Base).C(Coleccion)
	return colec, session
}

func AbrirSesionMongo(ip_servidor string) *mgo.Session {
	session, err := mgo.Dial(ip_servidor)
	check(err, "NO SE PUDO CONECTAR A LA BASE DE DATOS MONGO")
	return session
}

func CerrarSesionMongo(conexion *mgo.Session) {
	_, err := conexion.BuildInfo()
	check(err, "Ha ocurrido un error al cerrar la conexión")
	conexion.Close()
}

func ObtieneBaseDeSesion(nombrebd string, conexion *mgo.Session) *mgo.Database {
	conexion.SetMode(mgo.Monotonic, true)
	return conexion.DB(nombrebd)
}

func ObtieneGidfs(Base string, Coleccion string, Ip string) (*mgo.GridFS, *mgo.Session) {
	session, err := mgo.Dial(Ip)
	check(err, "Error al conectar con almacenes de MongoDB")
	colec := session.DB(Base).GridFS(Coleccion)
	return colec, session
}

func ObtieneColeccionDeBase(colec string, db *mgo.Database) *mgo.Collection {
	return db.C(colec)
}
