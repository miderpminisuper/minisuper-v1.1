package ModeloCompras

import (
	_ "fmt"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

func GetDataBase() (*mgo.Database, *mgo.Session) {
	session, err := mgo.Dial("127.0.0.1")
	if err != nil {
		panic(err)
	}
	db := session.DB("MiderpTest")
	return db, session
}

func RetornaImagen(id bson.ObjectId) (*mgo.GridFile, *mgo.Session) {

	db, sesion := GetDataBase()
	img, err := db.GridFS("ImagenesArticulos").OpenId(id)
	if err != nil {
		panic(err)
	}
	return img, sesion
}

func ReturnAllImage() (gfs *mgo.GridFS, sesion *mgo.Session) {
	db, sesion := GetDataBase()
	gfs = db.GridFS("ImagenesArticulos")
	return
}
