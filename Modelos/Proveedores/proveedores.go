package provedors

import (
	"../General"
	"gopkg.in/mgo.v2/bson"
)

func GetAllProveedor() ([]models.Proveedor, error) {
	var result []models.Proveedor
	c, _, err := models.ConectarMongo(models.SERVIDOR_MONGO, models.NOMBRE_BD_MONGO, models.COLECCION_PROVEEDORES)
	if err != nil {
		return nil, err
	}
	err = c.Find(bson.M{}).Select(bson.M{}).Limit(50).All(&result)
	if err != nil {
		return nil, err
	}
	return result, err
}

func NewProveedor(data interface{}) error {
	c, _, err := models.ConectarMongo(models.SERVIDOR_MONGO, models.NOMBRE_BD_MONGO, models.COLECCION_PROVEEDORES)
	if err != nil {
		return err
	}
	err = c.Insert(data)
	if err != nil {
		return err
	}
	return err
}

func GetProveedorId(id bson.ObjectId) (models.Proveedor, error) {
	var result models.Proveedor
	c, _, err := models.ConectarMongo(models.SERVIDOR_MONGO, models.NOMBRE_BD_MONGO, models.COLECCION_PROVEEDORES)
	if err != nil {
		return result, err
	}
	err = c.Find(bson.M{"_id": id}).One(&result)
	if err != nil {
		return result, err
	}
	return result, err
}

func EditProveedorId(id bson.ObjectId, rfc string, nombre string, activo bool, contact interface{}, direccion interface{}, m interface{}) error {
	c, _, err := models.ConectarMongo(models.SERVIDOR_MONGO, models.NOMBRE_BD_MONGO, models.COLECCION_PROVEEDORES)
	if err != nil {
		return err
	}
	change := bson.M{"$set": bson.M{"rfc": rfc, "nombre_completo": nombre, "activo": activo, "contacto": contact, "direccion": direccion, "campos_adicionales": m}}
	err = c.Update(bson.M{"_id": id}, change)
	if err != nil {
		return err
	}
	return err
}

func EliminaProveedor(id string) error {
	colec, _, err := models.ConectarMongo(models.SERVIDOR_MONGO, models.NOMBRE_BD_MONGO, models.COLECCION_PROVEEDORES)
	if err != nil {
		return err
	}
	err = colec.Remove(bson.M{"_id": bson.ObjectIdHex(id)})
	if err != nil {
		return err
	}
	return err
}
