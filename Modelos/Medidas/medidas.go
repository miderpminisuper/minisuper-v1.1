package medidas_m

import (
	"fmt"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type Metamedida struct {
	Id     bson.ObjectId `bson:"_id,omitempty"`
	Grupo  string        `bson:"grupo_medida"`
	Medida []Medidas     `bson:"medidas"`
}

type Medidas struct {
	Nombre      string `bson:"nombre"`
	Abreviatura string `bson:"abreviatura"`
}

func conectMgoMedidas() (*mgo.Collection, *mgo.Session) {
	session, err := mgo.Dial("192.168.1.110")
	if err != nil {
		panic(err)
	}
	c := session.DB("minisuper").C("medidas")
	return c, session
}

func GetMedidas() []Metamedida {
	var result []Metamedida
	c, s := conectMgoMedidas()
	defer s.Close()
	c.Find(nil).All(&result)
	return result

}

func GetMedida(id string) Metamedida {
	var impuestesillo Metamedida
	ids := bson.ObjectIdHex(id)
	c, s := conectMgoMedidas()
	c.Find(bson.M{"_id": ids}).One(&impuestesillo)
	defer s.Close()
	return impuestesillo
}

func EditarMedida(data interface{}, id bson.ObjectId) bool {
	var insertado bool
	c, s := conectMgoMedidas()
	err := c.Update(bson.M{"_id": id}, data)
	if err != nil {
		panic(err)
		insertado = false
	} else {
		fmt.Println("Updated Meta Unidad :)")
		insertado = true
	}
	defer s.Close()
	return insertado

}

func InsertarMedida(data interface{}) bool {
	c, s := conectMgoMedidas()
	err := c.Insert(data)
	var insertado bool
	if err != nil {
		fmt.Println("Error : ", err)
		insertado = false
	} else {
		fmt.Println("Add new Meta-Unidad :)")
		insertado = true
	}
	defer s.Close()
	return insertado
}

func EliminarMedida(id bson.ObjectId) bool {
	var eliminado bool
	c, s := conectMgoMedidas()
	err := c.Remove(bson.M{"_id": id})
	if err != nil {
		fmt.Println("Fallo la eliminacion", err)
		eliminado = false
	} else {
		fmt.Println("Medida Eliminada :)")
		eliminado = true
	}
	defer s.Close()
	return eliminado

}
