package kardex_modelo

import (
	"fmt"
	"time"

	"database/sql"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

/*
@melchormendoza
Manejo de error de conexion
@params
	err: Error
	mensaje: mensaje
@return
	err: Error generado
*/
func check(err error, mensaje string) {
	if err != nil {
		fmt.Println("____")
		fmt.Println(mensaje)
		fmt.Println("____")
		panic(err)
	}
}

/*
@melchormendoza
Estructura movimientos
@params
	null
@return
	null
*/
type Movimientos struct {
	Id             bson.ObjectId  `bson:"_id,omitempty"`
	TipoMovimiento string         `bson:"tipo_movimiento"`
	FechaHora      time.Time      `bson:"fecha_hora"`
	Origen         bson.ObjectId  `bson:"almacen_origen,omitempty"`
	Destino        bson.ObjectId  `bson:"almacen_destino,omitempty"`
	UsOrigen       bson.ObjectId  `bson:"usuario_origen,omitempty"`
	UsDestino      bson.ObjectId  `bson:"usuario_destino,omitempty"`
	Productos      []ProductosAll `bson:"productos"`
}

/*
@melchormendoza
Estrctura de almacen
@params
	null
@return
	null
*/
type AlmOrigen struct {
	Id      bson.ObjectId     `bson:"_id,omitempty"`
	Nombre  string            `bson:"nombre"`
	Tipo    string            `bson:"tipo"`
	Defecto string            `bson:"defecto"`
	Datos   map[string]string `bson:"datos"`
}

/*
@melchormendoza
Estrucctura de articulos
@params
	null
@return
	null
*/
type Articulo struct {
	Id          bson.ObjectId `bson:"_id,omitempty"`
	CodBarra    string        `bson:"codigobarra"`
	Descripcion string        `bson:"descripcion"`
	Costo       float64       `bson:"costo"`
}

/*
@melchormendoza
Estrucura productos
@params
	null
@return
	null
*/
type ProductosAll struct {
	Id       bson.ObjectId `bson:"_id,omitempty"`
	Cantidad float64       `bson:"cantidad"`
}

/*
*@melchormendoza
Estructura de usuarios
@params
	null
@return
	null
*/
type UsOrigen struct {
	Id     bson.ObjectId `bson:"_id,omitempty"`
	Nombre string        `bson:"nombre"`
}

/*
@melchormendoza
Estrucuctura de inventario
@params
	null
@return
	null
*/
type Inventario struct {
	Id       string
	Cantidad float64
	Estatus  string
}

/*
func conectMgoPagos() (*mgo.Collection, *mgo.Session) {
	session, err := mgo.Dial("192.168.1.110")
	if err != nil {
		panic(err)
	}
	c := session.DB("minisuper").C("forma_pago")
	return c, session
}
*/
/*
@melchormendoza
Conexion a MongoDB para la coleccion movimientos
@params
	null
@return
	session: Conexion
*/
func conectMgoMovimientos() (*mgo.Collection, *mgo.Session) {
	session, err := mgo.Dial("192.168.1.110")
	if err != nil {
		panic(err)
	}
	c := session.DB("minisuper").C("movimientos")
	return c, session
}

/*
@melchormendoza
Conexion a MongoDB
@params
	coleccion: Coleccion a conectar
@return
	session: conexion
*/
func conectaMongo(coleccion string) (*mgo.Collection, *mgo.Session) {
	session, err := mgo.Dial("192.168.1.110")
	if err != nil {
		panic(err)
	}
	c := session.DB("minisuper").C(coleccion)
	return c, session
}

/*
@melchormendoza
Conexion a PostgreSQL
@params
	host: IP del servidor de BD
	nombreBD: Nombre de la base de dato
	usuarioBD: Usuario de la base de datos
	passwordBD: Contraseña de la base de datos
@return
	db: Conexion
*/
func conectaPostgresql(host string, nombreBD string, usuarioDb string, passwordDb string) *sql.DB {
	db, err := sql.Open("postgres", "host="+host+" user="+usuarioDb+" dbname="+nombreBD+" password="+passwordDb+" sslmode=disable")
	check(err, "Error al conectar con Postgres")
	return db
}

/*
@melchormendoza
Devuelve todos los movimientos
@params
	idAlmacen : Id del ammacen a buscar
	finicial : fecha de inicio
	ffinal: fecha final
@return
	kardex: Movimientos
*/
func GetMovimientos(idAlmacen string, finicial string, ffinal string) []Movimientos {
	var kardex []Movimientos
	//	ids := bson.ObjectIdHex(id)
	//	fmt.Println(ids)
	c, s := conectMgoMovimientos()
	var orContent []bson.M
	//var andDate []bson.M
	//from := time.Date(2017, 01, 1, 0, 0, 0, 0, time.UTC)
	//to := time.Date(2017, 02, 3, 0, 0, 0, 0, time.UTC)
	//fmt.Println(from)
	//fmt.Println(to)

	orContent = append(orContent, bson.M{"almacen_origen": bson.ObjectIdHex(idAlmacen)})
	orContent = append(orContent, bson.M{"almacen_destino": bson.ObjectIdHex(idAlmacen)})
	//andDate = append(andDate, bson.M{"fecha_hora": bson.M{"$gte": from, "$lte": to}})
	fmt.Println(orContent)
	c.Find(bson.M{"$or": orContent}).All(&kardex)
	//c.Find(bson.M{"almacen_destino": bson.ObjectIdHex(idAlmacen)}).All(&kardex)
	//c.Find(bson.M{"fecha_hora": bson.M{"$gte": from, "$lte": to}}).All(&kardex)
	defer s.Close()
	return kardex

}

/*
func GetFormasPago() []FormaPago {
	var result []FormaPago
	c, s := conectMgoPagos()
	defer s.Close()
	c.Find(nil).All(&result)
	return result

}
*/ /*
func InsertarPago(data interface{}) bool {
	c, s := conectMgoPagos()
	err := c.Insert(data)
	var insertado bool
	if err != nil {
		fmt.Println("Error : ", err)
		insertado = false
	} else {
		fmt.Println("Add new forma de pago :)")
		insertado = true
	}
	defer s.Close()
	return insertado
}
*/
/*
func GetFormadepago(id string) FormaPago {
	var forma FormaPago
	ids := bson.ObjectIdHex(id)
	c, s := conectMgoPagos()
	c.Find(bson.M{"_id": ids}).One(&forma)
	defer s.Close()
	return forma

}
*/
/*
func EditarFormadepago(data interface{}, id bson.ObjectId) bool {
	var insertado bool
	c, s := conectMgoPagos()
	err := c.Update(bson.M{"_id": id}, data)
	if err != nil {
		panic(err)
		insertado = false
	} else {
		fmt.Println("Updated Forma de Pago :)")
		insertado = true
	}
	defer s.Close()
	return insertado

}
*/
/*
@melchormendoza
Devuelve el nombre de un almacen dado
@param
	id: ObjectId de un almacen
@return
	nombre: Nombre del almacen
*/
func GetAlmacenById(id bson.ObjectId) string {
	var alm AlmOrigen
	var nombre string
	//ids := bson.ObjectIdHex(id)
	c, s := conectaMongo("almacenes")
	c.Find(bson.M{"_id": id}).One(&alm)
	defer s.Close()
	nombre = alm.Nombre
	return nombre
}

/*
@melchor
Metodo para traer el nombre de un usuario dado
@params
	id: ObkectId del usuario a devolver
@return
	nombre: Nombre del usuario
*/
func GetUsuarioById(id bson.ObjectId) string {
	var us UsOrigen
	var nombre string
	c, s := conectaMongo("usuarios")
	c.Find(bson.M{"_id": id}).One(&us)
	defer s.Close()
	nombre = us.Nombre
	return nombre
}

/*
@melchormendoza
Metodo para traer los movimiento dado un arreglo de cocumentos
@params
	ids: arreglo de objetos de id de documentos
@return
	Estructura de documentos
*/
func GetArrayMovimientos(ids []bson.ObjectId) []Movimientos {
	var kardex []Movimientos
	c, s := conectMgoMovimientos()
	c.Find(bson.M{"_id": bson.M{"$in": ids}}).All(&kardex)
	defer s.Close()
	return kardex

}

/*
@melchormendoza
Metodo para traer el costo de un producto dado.
@params
	id: SKU
@return
	costo
*/
func GetProductoById(id string) float64 {
	var art Articulo
	var costo float64
	ids := bson.ObjectIdHex(id)
	c, s := conectaMongo("productos")
	c.Find(bson.M{"_id": ids}).One(&art)
	defer s.Close()
	costo = art.Costo
	return costo
}

/*
@melchormendoza
Metodo para trae el inventario actual de un SKU desde postgres
@Params:
	idAlmacen: almacen de consulta
	id : SKU de consulta
@return
	inventario
*/
func GetInventario(idAlmacen string, id string) float64 {
	BasePosGres := conectaPostgresql("192.168.1.110", "minisuper", "postgres", "12345")
	query := `SELECT "cantidad" FROM "` + idAlmacen + `" where "id_producto" = '` + id + `'`
	con, err := BasePosGres.Query(query)
	check(err, "Error al consultar producto en Postgresql")
	var inventario float64
	for con.Next() {
		err := con.Scan(&inventario)
		check(err, "Error al scanear producto de postgres")
	}
	con.Close()
	BasePosGres.Close()
	return inventario
}

/*
@melchormendoza
Trae los datos de un movimiento dado
*/
func GetMovimientoById(id string) []Movimientos {
	var mov []Movimientos
	ids := bson.ObjectIdHex(id)
	c, s := conectaMongo("movimientos")
	c.Find(bson.M{"_id": ids}).All(&mov)
	defer s.Close()
	return mov
}
func GetProductoNombreById(id string) string {
	var art Articulo
	var nombre string
	ids := bson.ObjectIdHex(id)
	c, s := conectaMongo("productos")
	c.Find(bson.M{"_id": ids}).One(&art)
	defer s.Close()
	nombre = art.Descripcion
	return nombre
}
