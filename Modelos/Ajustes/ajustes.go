package ajustes_m

import (
	"database/sql"
	"fmt"
	"strconv"
	"time"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type Articulo struct {
	ID          bson.ObjectId     `bson:"_id,omitempty" 	json:"_id,omitempty"`
	CodBarra    string            `bson:"codigobarra" 		json:"codigobarra"`
	Descripcion string            `bson:"descripcion" 		json:"descripcion"`
	Imagen      bson.ObjectId     `bson:"imagen,omitempty" 	json:"imagen,omitempty"`
	Fotos       []bson.ObjectId   `bson:"fotos,omitempty" 	json:"fotos,omitempty"`
	Tipo        string            `bson:"tipo" 				json:"tipo"`
	Unidad      string            `bson:"unidad" 			json:"unidad"`
	Entero      bool              `bson:"usa_fraccion" 		json:"usa_fraccion"`
	NumDec      int               `bson:"num_dec" 			json:"num_dec"`
	Etiquetas   map[string]string `bson:"etiquetas" 		json:"etiquetas"`
	PreCompra   float64           `bson:"costo" 			json:"costo"`
	PreVenta    float64           `bson:"precio" 			json:"precio"`
}

type Almacen struct {
	Id      bson.ObjectId     `bson:"_id,omitempty"`
	Nombre  string            `bson:"nombre"`
	Tipo    string            `bson:"tipo"`
	Defecto string            `bson:"defecto"`
	Campos  map[string]string `bson:"datos"`
}

type ArticuloPostgres struct {
	Id       string
	Cantidad float64
	Estatus  string
}
type User struct {
	Id bson.ObjectId `bson:"_id,omitempty"`
}

type Movimiento struct {
	ID             bson.ObjectId `bson:"_id,omitempty"`
	Tipo           string        `bson:"tipo_movimiento"`
	Date           time.Time     `bson:"fecha_hora"`
	AlmacenOrigen  bson.ObjectId `bson:"almacen_origen,omitempty"`
	AlmacenDestino bson.ObjectId `bson:"almacen_destino,omitempty"`
	Usuario        bson.ObjectId `bson:"usuario,omitempty"`
	Productos      []Producto    `bson:"productos"`
}

type Producto struct {
	ID     bson.ObjectId `bson:"_id,omitempty"`
	Nombre string        `bson:"nombre"`
}

func conectardb(usuario string, password string, basededatos string, host string, port string, ssl string) *sql.DB {
	db, err := sql.Open(""+usuario+"", "user="+usuario+" password="+password+" dbname="+basededatos+" host="+host+" port="+port+" sslmode="+ssl+"")
	if err != nil {
		fmt.Println(err)
	}
	return db
}

func conectMgoMovimientos() (*mgo.Collection, *mgo.Session) {
	session, err := mgo.Dial("192.168.1.110")
	if err != nil {
		panic(err)
	}
	c := session.DB("minisuper").C("movimientos")
	return c, session
}

func conectMgoAlmacenes() (*mgo.Collection, *mgo.Session) {
	session, err := mgo.Dial("192.168.1.110")
	if err != nil {
		panic(err)
	}
	c := session.DB("minisuper").C("almacenes")
	return c, session
}

func conectMgoUsuarios() (*mgo.Collection, *mgo.Session) {
	session, err := mgo.Dial("192.168.1.110")
	if err != nil {
		panic(err)
	}
	c := session.DB("minisuper").C("usuarios")
	return c, session
}

func conectMgoProductos() (*mgo.Collection, *mgo.Session) {
	session, err := mgo.Dial("192.168.1.110")
	if err != nil {
		panic(err)
	}
	c := session.DB("minisuper").C("productos")
	return c, session
}

func GetAlmacenes() []Almacen {
	var result []Almacen
	c, s := conectMgoAlmacenes()
	defer s.Close()
	c.Find(nil).Select(bson.M{"_id": 1, "nombre": 1}).All(&result)
	return result

}

func GetProductoMongo(cod_barra string) Articulo {
	var result Articulo
	c, s := conectMgoProductos()
	defer s.Close()
	c.Find(bson.M{"codigobarra": cod_barra}).One(&result)
	return result
}

func GetProductoMongoID(cod_barra string) string {
	var result Articulo
	c, s := conectMgoProductos()
	defer s.Close()
	c.Find(bson.M{"codigobarra": cod_barra}).Select(bson.M{"_id": 1}).One(&result)
	return result.ID.Hex()
}

func GetProductoPostgres(id_prod, alm_origen, alm_destino string) (ArticuloPostgres, ArticuloPostgres) {
	dbminisuper := conectardb("postgres", "12345", "minisuper", "192.168.1.110", "5432", "disable")
	defer dbminisuper.Close()

	var articulo_origen ArticuloPostgres
	var articulo_destino ArticuloPostgres

	query_alm_origen := `SELECT "id_producto","cantidad","estatus" FROM public."` + alm_origen + `" WHERE id_producto='` + id_prod + `'`
	query_alm_destino := `SELECT "id_producto","cantidad","estatus" FROM public."` + alm_destino + `" WHERE id_producto='` + id_prod + `'`

	resultado, _ := dbminisuper.Query(query_alm_origen)

	for resultado.Next() {
		err := resultado.Scan(&articulo_origen.Id, &articulo_origen.Cantidad, &articulo_origen.Estatus)
		if err != nil {
			fmt.Println(err)
		}
	}

	resultado2, _ := dbminisuper.Query(query_alm_destino)

	for resultado2.Next() {
		err := resultado2.Scan(&articulo_destino.Id, &articulo_destino.Cantidad, &articulo_destino.Estatus)
		if err != nil {
			fmt.Println(err)
		}
	}

	return articulo_origen, articulo_destino
}

func GetUsuario(user string) User {
	var usuario User
	c, s := conectMgoUsuarios()
	defer s.Close()
	c.Find(bson.M{"usuario": user}).Select(bson.M{"_id": 1}).One(&usuario)
	return usuario
}

func RealizarAjuste(alm_origen, id_producto string, cantidad float64) {
	dbminisuper := conectardb("postgres", "12345", "minisuper", "192.168.1.110", "5432", "disable")
	cantidad_a_string := strconv.FormatFloat(cantidad, 'f', -1, 64)
	query := `UPDATE public."` + alm_origen + `" SET cantidad='` + cantidad_a_string + `' WHERE id_producto='` + id_producto + `'`
	_, _ = dbminisuper.Exec(query)
	dbminisuper.Close()
}

func RealizarTraslado(alm_origen, alm_destino, id_prod string, cant_origen, cant_destino float64) {
	dbminisuper := conectardb("postgres", "12345", "minisuper", "192.168.1.110", "5432", "disable")
	var articulo_destino ArticuloPostgres

	cant_origen_s := strconv.FormatFloat(cant_origen, 'f', -1, 64)
	cant_destino_s := strconv.FormatFloat(cant_destino, 'f', -1, 64)

	//verificar si el producto id_prod existe en el almacen destino, si no existe crearlo
	// como verificamos eso
	query_veriry := `SELECT id_producto FROM public."` + alm_destino + `" WHERE id_producto='` + id_prod + `'`

	resultado2, _ := dbminisuper.Query(query_veriry)

	for resultado2.Next() {
		err := resultado2.Scan(&articulo_destino.Id)
		if err != nil {
			fmt.Println(err)
		}
	}

	if articulo_destino.Id != "" {
		query := `UPDATE public."` + alm_origen + `" SET cantidad='` + cant_origen_s + `' WHERE id_producto='` + id_prod + `'`
		query2 := `UPDATE public."` + alm_destino + `" SET cantidad='` + cant_destino_s + `' WHERE id_producto='` + id_prod + `'`
		_, _ = dbminisuper.Exec(query)
		_, _ = dbminisuper.Exec(query2)

	} else {
		query := `UPDATE public."` + alm_origen + `" SET cantidad='` + cant_origen_s + `' WHERE id_producto='` + id_prod + `'`
		query2 := `INSERT INTO public."` + alm_destino + `" (id_producto,cantidad,estatus) VALUES ('` + id_prod + `','` + cant_destino_s + `','ACTIVO')`
		_, _ = dbminisuper.Exec(query)
		_, _ = dbminisuper.Exec(query2)
	}

	dbminisuper.Close()
}

func GuardarMovimiento(data interface{}) bool {
	c, s := conectMgoMovimientos()
	err := c.Insert(data)
	var insertado bool
	if err != nil {
		fmt.Println("Error : ", err)
		insertado = false
	} else {
		fmt.Println("Add new Movimiento :)")
		insertado = true
	}
	defer s.Close()
	return insertado

}
