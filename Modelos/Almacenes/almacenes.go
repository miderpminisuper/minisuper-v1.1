package almacenes_m

import (
	"fmt"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type Almacen struct {
	Id      bson.ObjectId     `bson:"_id,omitempty"`
	Nombre  string            `bson:"nombre"`
	Tipo    string            `bson:"tipo"`
	Defecto string            `bson:"defecto"`
	Campos  map[string]string `bson:"datos"`
}

func GetAlmacenes() []Almacen {
	var result []Almacen
	c, s := conectMgoAlmacenes()
	defer s.Close()
	c.Find(nil).All(&result)
	return result

}

func conectMgoAlmacenes() (*mgo.Collection, *mgo.Session) {
	session, err := mgo.Dial("192.168.1.110")
	if err != nil {
		panic(err)
	}
	c := session.DB("minisuper").C("almacenes")
	return c, session
}

func InsertarAlmacen(data interface{}) bool {
	c, s := conectMgoAlmacenes()
	err := c.Insert(data)
	var insertado bool
	if err != nil {
		fmt.Println("Error : ", err)
		insertado = false
	} else {
		fmt.Println("Add new Almacen :)")
		insertado = true
	}
	defer s.Close()
	return insertado
}

func GetAlmacen(id string) Almacen {
	var almacencillo Almacen
	ids := bson.ObjectIdHex(id)
	c, s := conectMgoAlmacenes()
	c.Find(bson.M{"_id": ids}).One(&almacencillo)
	defer s.Close()
	return almacencillo

}

func EditarAlmacen(data interface{}, id bson.ObjectId) bool {
	var insertado bool
	c, s := conectMgoAlmacenes()
	err := c.Update(bson.M{"_id": id}, data)
	if err != nil {
		panic(err)
		insertado = false
	} else {
		fmt.Println("Updated almacen :)")
		insertado = true
	}
	defer s.Close()
	return insertado

}

func EliminarAlmacen(id bson.ObjectId) bool {
	var eliminado bool
	c, s := conectMgoAlmacenes()
	err := c.Remove(bson.M{"_id": id})
	if err != nil {
		fmt.Println("Fallo la eliminacion", err)
		eliminado = false
	} else {
		fmt.Println("Almacen Eliminado :)")
		eliminado = true
	}
	defer s.Close()
	return eliminado

}
