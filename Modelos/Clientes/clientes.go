package clients

import (
	"../General"
	"gopkg.in/mgo.v2/bson"
)

func GetAllClient() []models.Cliente {
	var result []models.Cliente
	c, s, _ := models.ConectarMongo(models.SERVIDOR_MONGO, models.NOMBRE_BD_MONGO, models.COLECCION_CLIENTES)
	c.Find(bson.M{}).Select(bson.M{}).Limit(50).All(&result)
	s.Close()
	return result
}

func NewClient(data interface{}) error {

	c, _, err := models.ConectarMongo(models.SERVIDOR_MONGO, models.NOMBRE_BD_MONGO, models.COLECCION_CLIENTES)
	if err != nil {
		return err
	}
	err = c.Insert(data)
	if err != nil {
		return err
	}

	return err
}

func GetClientId(id bson.ObjectId) models.Cliente {
	var result models.Cliente
	c, s, _ := models.ConectarMongo(models.SERVIDOR_MONGO, models.NOMBRE_BD_MONGO, models.COLECCION_CLIENTES)
	c.Find(bson.M{"_id": id}).One(&result)
	s.Close()
	return result
}

func EditClientId(id bson.ObjectId, rfc string, nombre string, activo bool, contact interface{}, direccion interface{}, aditional interface{}) error {
	c, _, err := models.ConectarMongo(models.SERVIDOR_MONGO, models.NOMBRE_BD_MONGO, models.COLECCION_CLIENTES)
	if err != nil {
		return err
	}
	change := bson.M{"$set": bson.M{"rfc": rfc, "nombre_completo": nombre, "activo": activo, "contacto": contact, "direccion": direccion, "campos_adicionales": aditional}}
	err = c.Update(bson.M{"_id": id}, change)
	if err != nil {
		return err
	}
	return err
}

func EliminaCliente(id string) error {
	colec, _, err := models.ConectarMongo(models.SERVIDOR_MONGO, models.NOMBRE_BD_MONGO, models.COLECCION_CLIENTES)
	if err != nil {
		return err
	}
	err = colec.Remove(bson.M{"_id": bson.ObjectIdHex(id)})
	if err != nil {
		return err
	}
	return err
}
