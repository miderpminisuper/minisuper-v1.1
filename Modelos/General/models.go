package models

import (
	"gopkg.in/mgo.v2/bson"
)

type Usuarios struct {
	Id          bson.ObjectId     `bson:"_id,omitempty"`
	Nombre      string            `bson:"nombre_completo"`
	Usuario     string            `bson:"usuario"`
	Password    string            `bson:"password"`
	Activo      bool              `bson:"activo"`
	Direccion   Direccion         `bson:"direccion"`
	Contacto    Contacto          `bson:"contacto"`
	Adicionales map[string]string `bson:"campos_adicionales"`
}

type Cliente struct {
	Id          bson.ObjectId     `bson:"_id,omitempty"`
	Rfc         string            `bson:"rfc"`
	Nombre      string            `bson:"nombre_completo"`
	Activo      bool              `bson:"activo"`
	Direccion   Direccion         `bson:"direccion"`
	Contacto    Contacto          `bson:"contacto"`
	Adicionales map[string]string `bson:"campos_adicionales"`
}

type Proveedor struct {
	Id          bson.ObjectId     `bson:"_id,omitempty"`
	Rfc         string            `bson:"rfc"`
	Nombre      string            `bson:"nombre_completo"`
	Activo      bool              `bson:"activo"`
	Direccion   Direccion         `bson:"direccion"`
	Contacto    Contacto          `bson:"contacto"`
	Adicionales map[string]string `bson:"campos_adicionales"`
}

type DatosFiscales struct {
	Id          bson.ObjectId     `bson:"_id,omitempty"`
	Rfc         string            `bson:"rfc"`
	Nombre      string            `bson:"nombre_completo"`
	Direccion   Direccion         `bson:"direccion"`
	Contacto    Contacto          `bson:"contacto"`
	Adicionales map[string]string `bson:"campos_adicionales"`
}

type Contacto struct {
	Id       bson.ObjectId `bson:"_id,omitempty"`
	Email    string        `bson:"email"`
	Telefono string        `bson:"telefono"`
	Celular  string        `bson:"celular"`
}

type Direccion struct {
	Id           bson.ObjectId `bson:"_id,omitempty"`
	Calle        string        `bson:"calle"`
	NumExterior  int           `bson:"num_ext"`
	NumInterior  int           `bson:"num_int"`
	Colonia      string        `bson:"colonia"`
	Localidad    string        `bson:"localidad"`
	Municipio    string        `bson:"municipio"`
	Estado       string        `bson:"estado"`
	CodigoPostal int           `bson:"cod_post"`
}

type Person struct {
	Id       bson.ObjectId `bson:"_id,omitempty"`
	Usuario  string        `bson:"usuario"`
	Password string        `bson:"password"`
}
