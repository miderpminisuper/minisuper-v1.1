package models

//Paquete modelo: pertenece al paquete modelo debido a que maneja los parametros de conexion
import (
	"database/sql"
	"fmt"

	"github.com/robfig/config"
	"gopkg.in/mgo.v2"
)

/*
 * Generales
 * Configuracion de parametros de conexion a base de datos.
 */
const (
	ARCHIVO_CONFIGURACION          = "configuracion_general.cfg"
	COLECCION_PRODUCTOS            = "productos"
	COLECCION_IMAGENES_MONGO       = "ImagenesArticulos"
	COLECCION_IMAGENES_FILES_MONGO = "ImagenesArticulos.files"
	TABLA_POSTGRESQL               = "Almacen"
	COLECCION_USUARIOS             = "usuarios"
	COLECCION_DATOS_FISCALES       = "datosfiscales"
	COLECCION_CLIENTES             = "clientes"
	COLECCION_PROVEEDORES          = "proveedores"
	COLECCION_ALMACENES_MONGO      = "almacenes"
	COLECCION_MOVIMIENTOS_MONGO    = "movimientos"
	COLECCION_USUARIOS_MONGO       = "usuarios"
	COLECCION_IMPUESTOS_MONGO      = "impuestos"
	COLECCION_UNIDADES_MONGO       = "medidas"

	INDICE_ELASTICSEARCH       = "minisuper"
	SERVIDOR_ELASTICSEARCH     = "192.168.1.110:9200"
	COLECCION_ELASTICSEARCH    = "productos"
	TOTAL_RESULTADOS_DEVUELTOS = 100
)

//Se lee el archivo de configuracion general
var arch_conf, err_archivo = config.ReadDefault(ARCHIVO_CONFIGURACION)

//se extra el la direccion del servidor en donde se aloja el mongo
var SERVIDOR_MONGO, err_ip = arch_conf.String("CONFIG_DB_MONGO", "ip")

//se extrae el nombre de la base de datos de mongo
var NOMBRE_BD_MONGO, err_bd_postgres = arch_conf.String("CONFIG_DB_MONGO", "nombre_bd")

//se extrae el nombre de base de datos de postgres
var BASE_POSTGRESQL, err_nombre_postgres = arch_conf.String("CONFIG_DB_POSTGRES", "nombre_pg_bd")

//se extrae la direccion ip de la base de datos de postgres
var SERVIDOR_POSTGRESQL, err_ip_postgres = arch_conf.String("CONFIG_DB_POSTGRES", "ip_pg_bd")

//se extrae el usuario de base de datos de postgres
var USER_POSTGRESQL, err_usuario_postgres = arch_conf.String("CONFIG_DB_POSTGRES", "usuario_pg_bd")

//se extrae el puerto de la base de datos de postgres
var PORT_POSTGRESQL, err_puerto_postgres = arch_conf.String("CONFIG_DB_POSTGRES", "puerto_pg_bd")

//se extrae la contraseña de base de datos de postgres
var PASS_POSGRESQL, err_psw_postgres = arch_conf.String("CONFIG_DB_POSTGRES", "password_pg_bd")

func ConectarMongo(ip string, baseDatos string, coleccion string) (*mgo.Collection, *mgo.Session, error) {
	session, err := mgo.Dial(ip)
	if err != nil {
		//fmt.Println("Error en la conexion con el servidor: ", err)
		//fmt.Println("Parametros de acceso: ", "IP:", ip, "BD:", baseDatos, "COLLECCION:", coleccion)
		return nil, session, err
	}
	c := session.DB(baseDatos).C(coleccion)
	return c, session, nil
}

func ConexionMongo(ip string) (*mgo.Session, error) {
	session, err := mgo.Dial(ip)
	if err != nil {
		return session, err
	}
	return session, nil
}

func ConectarMgoRegresaBase(nombrebd string, ip string) (*mgo.Database, error) {
	session, err := mgo.Dial(ip)
	if err != nil {
		return nil, err
	}
	Base := session.DB(nombrebd)
	return Base, err
}

func CerrarSesionMongo(session_mgo *mgo.Session) {
	session_mgo.Close()
}

func ConectarPostgres(usuario string, password string, basededatos string, host string, port string, ssl string) *sql.DB {
	db, err := sql.Open(""+usuario+"", "user="+usuario+" password="+password+" dbname="+basededatos+" host="+host+" port="+port+" sslmode="+ssl+"")
	if err != nil {
		fmt.Println(err)
	}
	return db
}
