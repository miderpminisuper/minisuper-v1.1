package modelo_acceso

import (
	"fmt"
	//"log"
	"crypto/md5"
	"math/rand"

	"../General"
	//mgo "gopkg.in/mgo.v2"
	"encoding/hex"

	"gopkg.in/mgo.v2/bson"
)

type Usuario struct {
	Id       bson.ObjectId `bson:"_id,omitempty"`
	Username string        `bson:"usuario"`
	Userpass string        `bson:"password"`
}

func ComprobarUsuario(usuario string, password string) (*Usuario, error) {
	//	ingreso, err := mgo.Dial("192.168.1.110:27017")
	//	if err != nil { //connecttion failed
	//		log.Println(err)
	//		return nil
	//	}
	//	defer ingreso.Close()

	//	c := ingreso.DB("minisuper").C("usuarios")

	c, session, err_s := models.ConectarMongo(models.SERVIDOR_MONGO, models.NOMBRE_BD_MONGO, models.COLECCION_USUARIOS)
	if err_s != nil {
		return nil, err_s
	}
	var result Usuario
	bisonDocSearch := bson.M{"usuario": usuario, "password": password}

	fmt.Println(bisonDocSearch)

	err_f := c.Find(bisonDocSearch).Select(bson.M{"_id": "true", "usuario": "true", "password": "true"}).One(&result)

	models.CerrarSesionMongo(session)
	//catch exception
	if err_f != nil {
		fmt.Println("Usuario no encontrado...")
		return nil, err_f
	} else {
		fmt.Println("Usuario verificado...")
		return &result, err_f
	}
}

func ExistMailandUpdate(mail string) (string, error) {

	c, _, err := models.ConectarMongo(models.SERVIDOR_MONGO, models.NOMBRE_BD_MONGO, models.COLECCION_USUARIOS)
	if err != nil {
		return "", err
	}
	var result models.Person
	bisonDocSearch := bson.M{"email": mail}
	err = c.Find(bisonDocSearch).Select(bson.M{"_id": "true", "usuario": "true", "password": "true"}).One(&result)

	if err == nil {
		secreto := RandStringRunes(6)
		hasher := md5.New()
		hasher.Write([]byte(secreto))
		secretomd5 := hex.EncodeToString(hasher.Sum(nil))
		bisonDocFilter := bson.M{"$set": bson.M{"password": secretomd5}}
		bisonDocSearch = bson.M{"_id": result.Id}
		_, err = c.UpdateAll(bisonDocSearch, bisonDocFilter)
		fmt.Println("ENTRANDO A: ", secreto, bisonDocSearch)
		return secreto, err

	}
	return "", err
}

func RandStringRunes(n int) string {

	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}

var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
