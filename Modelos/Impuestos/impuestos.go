package impuestos_m

import (
	"fmt"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type MetaImpuesto struct {
	Id        bson.ObjectId `bson:"_id,omitempty"`
	Grupo     string        `bson:"grupo_impuesto"`
	Prioridad int           `bson:"prioridad"`
	Impuestos []Impuesto    `bson:"impuestos"`
}

type Impuesto struct {
	Nombre      string  `bson:"nombre"`
	Tipo        string  `bson:"tipo"`
	Valor       float64 `bson:"valor"`
	Descripcion string  `bson:"descripcion"`
}

func conectMgoImpuestos() (*mgo.Collection, *mgo.Session) {
	session, err := mgo.Dial("192.168.1.110")
	if err != nil {
		panic(err)
	}
	c := session.DB("minisuper").C("impuestos")
	return c, session
}

func GetImpuestos() []MetaImpuesto {
	var result []MetaImpuesto
	c, s := conectMgoImpuestos()
	defer s.Close()
	c.Find(nil).All(&result)
	return result

}

func GetImpuesto(id string) MetaImpuesto {
	var impuestesillo MetaImpuesto
	ids := bson.ObjectIdHex(id)
	c, s := conectMgoImpuestos()
	c.Find(bson.M{"_id": ids}).One(&impuestesillo)
	defer s.Close()
	return impuestesillo
}

func EditarImpuesto(data interface{}, id bson.ObjectId) bool {
	var insertado bool
	c, s := conectMgoImpuestos()
	err := c.Update(bson.M{"_id": id}, data)
	if err != nil {
		panic(err)
		insertado = false
	} else {
		fmt.Println("Updated Meta Impuesto :)")
		insertado = true
	}
	defer s.Close()
	return insertado

}

func InsertarImpuesto(data interface{}) bool {
	c, s := conectMgoImpuestos()
	err := c.Insert(data)
	var insertado bool
	if err != nil {
		fmt.Println("Error : ", err)
		insertado = false
	} else {
		fmt.Println("Add new Meta-Impuesto :)")
		insertado = true
	}
	defer s.Close()
	return insertado
}

func EliminarImpuesto(id bson.ObjectId) bool {
	var eliminado bool
	c, s := conectMgoImpuestos()
	err := c.Remove(bson.M{"_id": id})
	if err != nil {
		fmt.Println("Fallo la eliminacion", err)
		eliminado = false
	} else {
		fmt.Println("Impuesto Eliminado :)")
		eliminado = true
	}
	defer s.Close()
	return eliminado

}
