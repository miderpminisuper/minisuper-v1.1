package fiscal_m

import (
	"fmt"

	"../General"
	//"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

func InsertFiscalData(data interface{}) bool {
	var b bool = false
	col, ses, err := models.ConectarMongo(models.SERVIDOR_MONGO, models.NOMBRE_BD_MONGO, models.COLECCION_DATOS_FISCALES)
	if err != nil {
		fmt.Println("Error en InsertFiscalData|Models", err)
	}
	err2 := col.Insert(data)
	if err2 != nil {
		fmt.Println(err)
	} else {
		b = true
	}
	ses.Close()
	return b
}

func GetFiscalDataOne() models.DatosFiscales {
	var result models.DatosFiscales
	col, ses, err := models.ConectarMongo(models.SERVIDOR_MONGO, models.NOMBRE_BD_MONGO, models.COLECCION_DATOS_FISCALES)
	if err != nil {
		fmt.Println("Error en GetFiscalDataOne|Modelos|datosfiscales.go", err)
	}
	col.Find(bson.M{}).Select(bson.M{}).One(&result)
	ses.Close()
	return result
}

func EditFiscalDataId(id bson.ObjectId, rfc string, nombre string, direccion interface{}, contact interface{}, aditional interface{}) bool {
	var b bool = false
	col, ses, err := models.ConectarMongo(models.SERVIDOR_MONGO, models.NOMBRE_BD_MONGO, models.COLECCION_DATOS_FISCALES)
	if err != nil {
		fmt.Println("Error en GetFiscalDataOne|Modelos|datosfiscales.go", err)
	}
	change := bson.M{"$set": bson.M{"rfc": rfc, "nombre_completo": nombre, "direccion": direccion, "contacto": contact, "campos_adicionales": aditional}}
	err2 := col.Update(bson.M{"_id": id}, change)
	if err2 != nil {
		b = false
	} else {
		b = true
	}
	ses.Close()
	return b
}

func EliminarDatosFiscales() error {
	col, _, err := models.ConectarMongo(models.SERVIDOR_MONGO, models.NOMBRE_BD_MONGO, models.COLECCION_DATOS_FISCALES)
	status := col.Remove(nil)
	fmt.Println("Se ha eliminado, ", status)
	return err
}
