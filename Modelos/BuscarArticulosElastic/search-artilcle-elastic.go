package search_elastic

import (
	"bytes"
	"context"
	"encoding/base64"
	"fmt"
	"image/jpeg"
	"image/png"
	"log"
	"path/filepath"

	"../Articulos"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	elastic "gopkg.in/olivere/elastic.v5"
)

const (
	SERVIDOR_MONGO           = "192.168.1.110"
	BASE_MONGO               = "minisuper"
	COLECCION_MONGO          = "productos"
	COLECCION_IMAGENES_MONGO = "ImagenesArticulos"
	BASE_POSTGRESQL          = "minisuper"
	TABLA_POSTGRESQL         = "Almacen"
	SERVIDOR_POSTGRESQL      = "192.168.1.110"
	USER_POSTGRESQL          = "postgres"
	PORT_POSTGRESQL          = "5432"
	PASS_POSGRESQL           = "12345"

	//const INDICE_ELASTICSEARCH = "miderp_desarrollo"
	INDICE_ELASTICSEARCH       = "minisuper"
	SERVIDOR_ELASTICSEARCH     = "192.168.1.110:9200"
	COLECCION_ELASTICSEARCH    = "productos"
	TOTAL_RESULTADOS_DEVUELTOS = 10000
)

func ConectarElastic() *elastic.Client {
	client, err := elastic.NewClient(elastic.SetURL(SERVIDOR_ELASTICSEARCH))
	check(err, "No se pudo conectar con Elastic")
	return client
}

//BuscarEnElastic busca un texto específico en elástic
func BuscarEnElastic(texto string, client *elastic.Client) *elastic.SearchResult {
	boolquery := elastic.NewBoolQuery() //var boolquery *elastic.NewMatchQuery

	if texto != "" {
		//		boolquery = boolquery.Must(elastic.NewMatchQuery("_all", texto)).Should(elastic.NewMatchQuery("descripcion", texto))
		boolquery = boolquery.Should(elastic.NewMatchPhraseQuery("descripcion", texto)).Must(elastic.NewMatchQuery("descripcion", texto))
	}

	docs, err := client.Search().Index(INDICE_ELASTICSEARCH).Type(COLECCION_ELASTICSEARCH).
		Query(boolquery).
		From(0).Size(TOTAL_RESULTADOS_DEVUELTOS).
		Do(context.TODO())
	check(err, "Error al realizar búsqueda en Elastic")
	return docs
}

func check(err error, mensaje string) {
	if err != nil {
		fmt.Println(mensaje)
		panic(err)
	}
}

func ConectaMongoDB(bd string, col string, server string) (*mgo.Collection, *mgo.Session) {
	session, err := mgo.Dial(server)
	if err != nil {
		panic(err)
	}
	c := session.DB(bd).C(col)
	return c, session
}

func GetArticleMongoToRangePagination(datos []string) []ModeloArticulos.Articulo {

	var result []ModeloArticulos.Articulo
	var valores = []bson.M{}

	for _, value := range datos {
		valores = append(valores, bson.M{"_id": bson.ObjectIdHex(value)})
	}

	c, s := ConectaMongoDB(BASE_MONGO, COLECCION_MONGO, SERVIDOR_MONGO)
	c.Find(bson.M{"$or": valores}).All(&result)
	s.Close()

	return result
}

//conectar solo a base de datos
func GetDataBase() (*mgo.Database, *mgo.Session) {
	session, err := mgo.Dial(SERVIDOR_MONGO)
	if err != nil {
		panic(err)
	}
	db := session.DB(BASE_MONGO)
	return db, session
}

//traer imagen de mongodb

func GetImagenIdSearch(id bson.ObjectId) string {

	fmt.Println(id)
	var tmp = ``
	if id != "" {
		//		img, sesiondb := images_m.RetornaImagen(id)

		db, sesiondb := GetDataBase()
		img, err := db.GridFS("ImagenesArticulos").OpenId(id)
		if err != nil {
			panic(err)
		}
		//return img, sesion

		fmt.Println(img.Name())
		b := make([]byte, img.Size())
		n, _ := img.Read(b)
		// fmt.Println("Size ->", img.Size())
		fmt.Println("N -> ", n)

		//				var imagen image.Image

		switch extension := filepath.Ext(img.Name()); extension {
		case ".jpg", ".jpeg":
			imagen, _ := jpeg.Decode(bytes.NewReader(b))
			//			var tmp = `<img src="data:image/jpg;base64,%s">`

			buffer := new(bytes.Buffer)

			if err := jpeg.Encode(buffer, imagen, nil); err != nil {
				log.Println("unable to encode image.")
			}
			str := base64.StdEncoding.EncodeToString(buffer.Bytes())
			//			fmt.Fprintf(w, tmp, str)
			tmp = `<img src="data:image/jpg;base64,` + str + `" style="width:100px">`
			defer sesiondb.Close()

		case ".png":

			imagen, _ := png.Decode(bytes.NewReader(b))
			//			var tmp = `<img src="data:image/png;base64,%s">`

			buffer := new(bytes.Buffer)

			if err := png.Encode(buffer, imagen); err != nil {
				log.Println("unable to encode image.")
			}

			str := base64.StdEncoding.EncodeToString(buffer.Bytes())
			//fmt.Fprintf(w, tmp, str)
			tmp = `<img src="data:image/png;base64,` + str + `" style="width:100px: height:100px">`

			defer sesiondb.Close()
		}

	} else {
		tmp = `<p>No se encontro el ID de la imagen</p>`
		//fmt.Fprintf(w, tmp)

	}
	return tmp
}
