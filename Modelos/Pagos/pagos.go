package pagos_m

import (
	"fmt"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type FormaPago struct {
	Id         bson.ObjectId `bson:"_id,omitempty"`
	Nombre     string        `bson:"nombre"`
	Cambio     bool          `bson:"devuelve_cambio"`
	Comision   float64       `bson:"comision"`
	Estatus    string        `bson:"status"`
	Sat        string        `bson:"sat"`
	Porcentaje bool          `bson:"porcentaje"`
}

func conectMgoPagos() (*mgo.Collection, *mgo.Session) {
	session, err := mgo.Dial("192.168.1.110")
	if err != nil {
		panic(err)
	}
	c := session.DB("minisuper").C("forma_pago")
	return c, session
}

func GetFormasPago() []FormaPago {
	var result []FormaPago
	c, s := conectMgoPagos()
	defer s.Close()
	c.Find(nil).All(&result)
	return result

}

func InsertarPago(data interface{}) bool {
	c, s := conectMgoPagos()
	err := c.Insert(data)
	var insertado bool
	if err != nil {
		fmt.Println("Error : ", err)
		insertado = false
	} else {
		fmt.Println("Add new forma de pago :)")
		insertado = true
	}
	defer s.Close()
	return insertado
}

func GetFormadepago(id string) FormaPago {
	var forma FormaPago
	ids := bson.ObjectIdHex(id)
	c, s := conectMgoPagos()
	c.Find(bson.M{"_id": ids}).One(&forma)
	defer s.Close()
	return forma

}

func EditarFormadepago(data interface{}, id bson.ObjectId) bool {
	var insertado bool
	c, s := conectMgoPagos()
	err := c.Update(bson.M{"_id": id}, data)
	if err != nil {
		panic(err)
		insertado = false
	} else {
		fmt.Println("Updated Forma de Pago :)")
		insertado = true
	}
	defer s.Close()
	return insertado

}

func EliminarFormadepago(id bson.ObjectId) bool {
	var eliminado bool
	c, s := conectMgoPagos()
	err := c.Remove(bson.M{"_id": id})
	if err != nil {
		fmt.Println("Fallo la eliminacion", err)
		eliminado = false
	} else {
		fmt.Println("Forma de Pago Eliminada :)")
		eliminado = true
	}
	defer s.Close()
	return eliminado

}
