package movimientos_m

import (
	_ "fmt"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"time"
)

func ConectMgoMovimientos() (*mgo.Collection, *mgo.Session) {
	session, err := mgo.Dial("192.168.1.110")
	if err != nil {
		panic(err)
	}
	c := session.DB("minisuper").C("movimientos")
	return c, session
}

func ConectMgoSession(servidor string) *mgo.Session {
	session, err := mgo.Dial(servidor)
	if err != nil {
		panic(err)
	}
	return session
}

func Obtenertipodemovimientos(tipo_movimiento string) interface{} {

	c, s := ConectMgoMovimientos()
	defer s.Close()

	switch {
	case tipo_movimiento == "venta":
		var movimientos []MovimientosVenta
		c.Find(bson.M{"tipo_movimiento": tipo_movimiento}).Select(bson.M{"_id": 1, "tipo_movimiento": 1, "fecha_hora": 1, "almacen_origen": 1, "almacen_destino": 1, "usuario_origen": 1, "usuario_destino": 1, "total_venta": 1, "productos": 1}).All(&movimientos)
		return movimientos
		break
	case tipo_movimiento == "compra":
		var movimientos []MovimientoCompras
		c.Find(bson.M{"tipo_movimiento": tipo_movimiento}).All(&movimientos)
		return movimientos
		break
	case tipo_movimiento == "devolucion":
		var movimientos []MovimientosDevolucion
		c.Find(bson.M{"tipo_movimiento": tipo_movimiento}).All(&movimientos)
		return movimientos
		break
	case tipo_movimiento == "garantia":
		var movimientos []MovimientosGarantia
		c.Find(bson.M{"tipo_movimiento": tipo_movimiento}).All(&movimientos)
		return movimientos
		break
	case tipo_movimiento == "ajuste":
		var movimientos []MovimientoAjuste
		c.Find(bson.M{"tipo_movimiento": tipo_movimiento}).Select(bson.M{"_id": 1, "tipo_movimiento": 1, "fecha_hora": 1, "almacen_origen": 1, "usuario": 1, "productos": 1}).All(&movimientos)
		return movimientos
		break
	case tipo_movimiento == "traslado":
		var movimientos []MovimientoTraslado
		c.Find(bson.M{"tipo_movimiento": tipo_movimiento}).Select(bson.M{"_id": 1, "tipo_movimiento": 1, "fecha_hora": 1, "almacen_origen": 1, "usuario": 1, "productos": 1, "almacen_destino": 1}).All(&movimientos)
		return movimientos
		break
	case tipo_movimiento == "cotizacion":
		var movimientos []MovimientoCotizacion
		c.Find(bson.M{"tipo_movimiento": tipo_movimiento}).All(&movimientos)
		return movimientos
		break
	}
	return false

}

func GetMovimientoUnico(idmov, tipo_movimiento string) interface{} {

	c, s := ConectMgoMovimientos()
	defer s.Close()

	id := bson.ObjectIdHex(idmov)

	switch {
	case tipo_movimiento == "venta":
		var movimiento MovimientosVenta
		c.Find(bson.M{"_id": id}).One(&movimiento)
		return movimiento
		break
	case tipo_movimiento == "compra":
		var movimiento MovimientoCompras
		c.Find(bson.M{"_id": id}).One(&movimiento)
		return movimiento
		break
	case tipo_movimiento == "devolucion":
		var movimiento MovimientosDevolucion
		c.Find(bson.M{"_id": id}).One(&movimiento)
		return movimiento
		break
	case tipo_movimiento == "garantia":
		var movimiento MovimientosGarantia
		c.Find(bson.M{"_id": id}).One(&movimiento)
		return movimiento
		break
	case tipo_movimiento == "ajuste":
		var movimiento MovimientoAjuste
		c.Find(bson.M{"_id": id}).One(&movimiento)
		return movimiento
		break
	case tipo_movimiento == "traslado":
		var movimiento MovimientoTraslado
		c.Find(bson.M{"_id": id}).One(&movimiento)
		return movimiento
		break
	case tipo_movimiento == "cotizacion":
		var movimiento MovimientoCotizacion
		c.Find(bson.M{"_id": id}).One(&movimiento)
		return movimiento
		break
	}
	return false

}

// ++++++++++++++ E S T R U C T U R A S +++++++++++++++++++++++++++++++++++++++++++++

// -------------------- Movimiento de Ajuste  --------------------
type MovimientoAjuste struct {
	ID             bson.ObjectId    `bson:"_id,omitempty"`
	Tipo           string           `bson:"tipo_movimiento"`
	Date           time.Time        `bson:"fecha_hora"`
	AlmacenOrigen  bson.ObjectId    `bson:"almacen_origen,omitempty"`
	AlmacenDestino bson.ObjectId    `bson:"almacen_destino,omitempty"`
	Usuario        bson.ObjectId    `bson:"usuario,omitempty"`
	Productos      []ProductoAjuste `bson:"productos"`
}

type ProductoAjuste struct {
	ID            bson.ObjectId `bson:"_id,omitempty"`
	CodigoDeBarra string        `bson:"codigobarra"`
	Nombre        string        `bson:"nombre"`
	CantidadA     float64       `bson:"cantidad_anterior"`
	CantidadB     float64       `bson:"cantidad_solicitada"`
	CantidadC     float64       `bson:"cantidad_final"`
	Operacion     string        `bson:"operacion"`
}

// -------------------- Movimiento de Traslado  --------------------
type MovimientoTraslado struct {
	ID             bson.ObjectId      `bson:"_id,omitempty"`
	Tipo           string             `bson:"tipo_movimiento"`
	Date           time.Time          `bson:"fecha_hora"`
	AlmacenOrigen  bson.ObjectId      `bson:"almacen_origen,omitempty"`
	AlmacenDestino bson.ObjectId      `bson:"almacen_destino,omitempty"`
	Usuario        bson.ObjectId      `bson:"usuario,omitempty"`
	Productos      []ProductoTraslado `bson:"productos"`
}

type ProductoTraslado struct {
	ID            bson.ObjectId `bson:"_id,omitempty"`
	CodigoDeBarra string        `bson:"codigobarra"`
	Nombre        string        `bson:"nombre"`
	CantidadA     float64       `bson:"existencia_origen"`
	CantidadB     float64       `bson:"existencia_destino"`
	CantidadC     float64       `bson:"cantidad_solicitada"`
	CantidadD     float64       `bson:"cantidad_origen"`
	CantidadE     float64       `bson:"cantidad_destino"`
}

// -------------------- Movimiento de Compra  --------------------
type MovimientoCompras struct {
	ID             bson.ObjectId     `bson:"_id,omitempty"`
	Tipo           string            `bson:"tipo_movimiento"`
	Date           time.Time         `bson:"fecha_hora"`
	AlmacenOrigen  bson.ObjectId     `bson:"almacen_origen"`
	AlmacenDestino bson.ObjectId     `bson:"almacen_destino"`
	UsuarioOrigen  bson.ObjectId     `bson:"usuario_origen"`
	UsuarioDestino bson.ObjectId     `bson:"usuario_destino"`
	Productos      []ProductoCompras `bson:"productos"`
}

type ProductoCompras struct {
	ID            bson.ObjectId     `bson:"_id,omitempty"`
	Cantidad      float64           `bson:"cantidad"`
	Costo         float64           `bson:"costo"`
	Impuestos     map[string]string `bson:"impuestos"`
	TotalImpuesto float64           `bson:"total_impuesto"`
	Importe       float64           `bson:"importe"`
	Ganancia      string            `bson:"ganancia"`
}

// -------------------- Movimiento de Cotizacion  --------------------

type MovimientoCotizacion struct {
	ID             bson.ObjectId        `bson:"_id,omitempty"`
	Tipo           string               `bson:"tipo_movimiento"`
	FechaHora      time.Time            `bson:"fecha_hora"`
	IdCliente      bson.ObjectId        `bson:"usuario_destino,omitempty"`
	IdUsuario      bson.ObjectId        `bson:"usuario_origen,omitempty"`
	AlmacenOrigen  bson.ObjectId        `bson:"almacen_origen,omitempty"`
	AlmacenDestino bson.ObjectId        `bson:"almacen_destino,omitempty"`
	Productos      []ProductoCotizacion `bson:"productos"`
}

type ProductoCotizacion struct {
	ID             bson.ObjectId `bson:"_id,omitempty"`
	Cantidad       float64       `bson:"cantidad"`
	Costo          float64       `bson:"costo"`
	Importe        float64       `bson:"precio"`
	Descuento      float64       `bson:"descuento"`
	DescuentoTotal float64       `bson:"descuento_total"`
	ImporteTotal   float64       `bson:"importe"`
}

// -------------------- Movimiento de Ventas  --------------------
type MovimientosVenta struct {
	ID             bson.ObjectId   `bson:"_id,omitempty"`
	Tipo           string          `bson:"tipo_movimiento"`
	Date           time.Time       `bson:"fecha_hora"`
	Cotizacion     bson.ObjectId   `bson:"cotizacion"`
	AlmacenOrigen  bson.ObjectId   `bson:"almacen_origen,omitempty"`
	AlmacenDestino bson.ObjectId   `bson:"almacen_destino,omitempty"`
	UsuarioOrigen  bson.ObjectId   `bson:"usuario_origen,omitempty"`
	UsuarioDestino bson.ObjectId   `bson:"usuario_destino,omitempty"`
	Productos      []ProductoVenta `bson:"productos"`
	FormasPago     FormasDePago    `bson:"formas_pago"`
	Comprobante    string          `bson:"comprobante"`
	TotalVenta     float64         `bson:"total_venta"`
	TotalCompra    float64         `bson:"total_compra"`
	TotalDescuento float64         `bson:"total_descuento"`
	Subtotal       float64         `bson:"subtotal"`
	Recibe         float64         `bson:"recibe"`
	Cambio         float64         `bson:"cambio"`
}

type ProductoVenta struct {
	ID               bson.ObjectId     `bson:"_id,omitempty"`
	Cantidad         float64           `bson:"cantidad"`
	Descuento        float64           `bson:"descuento"`
	Precio           float64           `bson:"precio"`
	CostoTotal       float64           `bson:"costo_total"`
	ImpuestoAplicado float64           `bson:"total_impuesto"`
	Impuestos        map[string]string `bson:"impuestos"`
	Importe          float64           `bson:"importe"`
	Subtotal         float64           `bson:"subtotal"`
}

type FormasDePago struct {
	Tformas int     `bson:"total_formas"`
	Formas  []Forma `bson:"formas"`
}

type Forma struct {
	ID         bson.ObjectId `bson:"_id,omitempty"`
	Clave      string        `bson:"clavesat"`
	Nombre     string        `bson:"nombre"`
	Importe    float64       `bson:"importe"`
	Comision   float64       `bson:"comision"`
	Porcentaje bool          `bson:"porcentaje"`
}

// -------------------- Movimiento de Garantia  --------------------

type MovimientosGarantia struct {
	Id             bson.ObjectId      `bson:"_id,omitempty"`
	TipoMovimiento string             `bson:"tipo_movimiento"`
	FechaHora      time.Time          `bson:"fecha_hora"`
	Origen         bson.ObjectId      `bson:"almacen_origen,omitempty"`
	Destino        bson.ObjectId      `bson:"almacen_destino,omitempty"`
	UsOrigen       bson.ObjectId      `bson:"usuario_origen,omitempty"`
	UsDestino      bson.ObjectId      `bson:"usuario_destino,omitempty"`
	Productos      []ProductoGarantia `bson:"productos"`
	Referencia     bson.ObjectId      `bson:"referencia,omitempty"`
}
type ProductoGarantia struct {
	Id            bson.ObjectId `bson:"_id,omitempty"`
	Cantidad      float64       `bson:"cantidad"`
	Costo         float64       `bson:"costo"`
	Precio        float64       `bson:"precio"`
	Importe       float64       `bson:"importe"`
	Ganancia      float64       `bson:"ganancia"`
	totalImpuesto float64       `bson:"total_impuesto"`
}

// -------------------- Movimiento de Devolucion  --------------------
type MovimientosDevolucion struct {
	Id             bson.ObjectId        `bson:"_id,omitempty"`
	TipoMovimiento string               `bson:"tipo_movimiento"`
	FechaHora      time.Time            `bson:"fecha_hora"`
	Origen         bson.ObjectId        `bson:"almacen_origen,omitempty"`
	Destino        bson.ObjectId        `bson:"almacen_destino,omitempty"`
	UsOrigen       bson.ObjectId        `bson:"usuario_origen,omitempty"`
	UsDestino      bson.ObjectId        `bson:"usuario_destino,omitempty"`
	Productos      []ProductoDevolucion `bson:"productos"`
	Referencia     bson.ObjectId        `bson:"referencia,omitempty"`
}
type ProductoDevolucion struct {
	Id            bson.ObjectId `bson:"_id,omitempty"`
	Cantidad      float64       `bson:"cantidad"`
	Costo         float64       `bson:"costo"`
	Precio        float64       `bson:"precio"`
	Importe       float64       `bson:"importe"`
	Ganancia      float64       `bson:"ganancia"`
	totalImpuesto float64       `bson:"total_impuesto"`
}
