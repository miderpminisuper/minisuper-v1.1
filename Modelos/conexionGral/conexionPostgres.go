package conex_gral

import (
	"database/sql"
	//"fmt"
	"log"

	//"github.com/lib/pq"
)

func ConexionPostgres(host string, nombreBD string, usuarioDb string, passwordDb string) *sql.DB {
	//fmt.Println(host + " " + nombreBD + " " + usuarioDb + " " + passwordDb)
	db, err := sql.Open("postgres", "host="+host+" user="+usuarioDb+" dbname="+nombreBD+" password="+passwordDb+" sslmode=disable")
	//db, err := sql.Open("postgres", "host=localhost user=postgres dbname=almacenes_development password=pgBd2016 sslmode=disable")
	//db, err := sql.Open("postgres", "postgres://postgres:pgBd2016@localhost/pruebas?sslmode=verify-full")
	if err != nil {
		log.Fatal(err)
	}
	return db
}
