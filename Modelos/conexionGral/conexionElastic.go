package conex_gral

import (
	"context"
	"fmt"

	//"../articulos"
	//"gopkg.in/mgo.v2/bson"
	elastic "gopkg.in/olivere/elastic.v5"
)

//const INDICE_ELASTICSEARCH = "miderp_desarrollo"
const INDICE_ELASTICSEARCH = "miderp_consulta"
const SERVIDOR_ELASTICSEARCH = "192.168.1.110:9200"
const COLECCION_ELASTICSEARCH = "Articulos"
const TOTAL_RESULTADOS_DEVUELTOS = 10000

func ConectarElastic() *elastic.Client {
	//conexion a elastic
	client, err := elastic.NewClient(elastic.SetURL(SERVIDOR_ELASTICSEARCH))
	if err != nil {
		fmt.Println("No se pudo conectar con ElasticSearch")
		panic(err)
	}
	return client
}

func BuscarEnElastic(texto string, client *elastic.Client) *elastic.SearchResult {
	boolquery := elastic.NewBoolQuery() //var boolquery *elastic.NewMatchQuery

	if texto != "" {
		fmt.Println(texto, boolquery)
		boolquery = boolquery.Must(elastic.NewMatchQuery("_all", texto)).Should(elastic.NewMatchQuery("descripcion", texto))
	}

	docs, err := client.Search().Index(INDICE_ELASTICSEARCH).Type(COLECCION_ELASTICSEARCH).
		Query(boolquery).
		From(0).Size(TOTAL_RESULTADOS_DEVUELTOS).
		Do(context.TODO())
	if err != nil {
		fmt.Println("Error al realizar la búsqueda")
		panic(err)
	}
	//	fmt.Println(docs.Hits)
	return docs
}
