package conex_gral

import (
	"fmt"

	"gopkg.in/mgo.v2"
)

const HOST_MONGO = "192.168.1.110"
const BD_MONGO = "MidErp_Desarrollo"
const BD_MONGO_PEDIDOS = "pedidos"
const BD_MONGO_SOLICITUDES = "solicitudes"
const COLECCION_MONGO_ARTICULOS = "Articulos"
const COLECCION_MONGO_METAARTICULOS = "MetaArticulos"
const COLECCION_MONGO_CRT_COMUNES = "CrtComunes"
const COLECCIONALMACENES = "EstructuraAlmacenes"
const CLIENTE_DEFECTO = "SOL_cliente12345_20161228"

func ConectMgoArticle() (*mgo.Collection, *mgo.Session) {
	//session, err := mgo.Dial("192.168.1.108")
	session, err := mgo.Dial(HOST_MONGO)
	if err != nil {
		fmt.Println("Error al conectar MongoDb: ", BD_MONGO)
		panic(err)
	}
	//c := session.DB("MiderpTest").C("ArticulosAleatorio")
	c := session.DB(BD_MONGO).C(COLECCION_MONGO_ARTICULOS)
	return c, session
}

func ConectMgoAlmacenes() (*mgo.Collection, *mgo.Session) {
	//session, err := mgo.Dial("192.168.1.108")
	session, err := mgo.Dial(HOST_MONGO)
	if err != nil {
		fmt.Println("Error al conectar MongoDb: ", BD_MONGO)
		panic(err)
	}
	c := session.DB(BD_MONGO).C(COLECCIONALMACENES)
	return c, session
}

func ConexionMongo(bd string, coleccion string) *mgo.Collection {
	session, err := mgo.Dial(HOST_MONGO)
	if err != nil {
		fmt.Println("NO SE PUDO CONECTAR")
		panic(err)
	}
	// Optional. Switch the session to a monotonic behavior.
	session.SetMode(mgo.Monotonic, true)
	//c := session.DB("mibasedatos").C("almacenes_totals")
	c := session.DB(bd).C(coleccion)
	return c
}
