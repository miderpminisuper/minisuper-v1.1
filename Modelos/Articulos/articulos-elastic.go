package ModeloArticulos

import (
	"fmt"

	"golang.org/x/net/context"
	elastic "gopkg.in/olivere/elastic.v5"
)

//InsertElasticValue inserta un articulo de minisuper en elastic
func InsertElasticValue(index string, types string, id string, data interface{}) {
	// Create a client and connect to http://192.168.2.10:9201
	//	client, err := elastic.NewClient(elastic.SetURL("http://192.168.1.111:9200"))
	client, err := elastic.NewClient(elastic.SetURL("http://192.168.1.110:9200"))
	if err != nil {
		fmt.Println("Error al crear cliente de elastic para borrar")
		panic(err)
	}

	exists, err := client.IndexExists(index).Do(context.TODO())
	if err != nil {
		fmt.Println("Falló llamada a índice de Elastic:", index)
	}
	if !exists {
		// Index does not exist yet.
		fmt.Println("el indice no existe ", index)

	} else {
		put1, err := client.Index().Index(index).Type(types).Id(id).BodyJson(data).Do(context.TODO())
		if err != nil {
			fmt.Println("error al indexar 1")
			panic(err)
		} else {
			fmt.Printf("Indexed to index %s, type %s\n", put1.Index, put1.Type)
		}

	}
	client.Stop()
}

//DeleteElasticValue elimina un docuemnto de elastic por ID
func DeleteElasticValue(index string, types string, id string) {

	fmt.Println(index, types, id)

	client, err := elastic.NewClient(elastic.SetURL("http://192.168.1.110:9200"))
	if err != nil {
		fmt.Println("Error al crear cliente de elastic para borrar")
		panic(err)
	}

	_, err = client.Get().Index(index).Type(types).Id(id).Do(context.TODO())
	if err != nil {
		fmt.Println("error al consultar producto por ID, pero pasa a alta")
	} else {
		_, err1 := client.Delete().Index(index).Type(types).Id(id).Do(context.TODO())
		check(err1, "Error al intentar borrar--->>>")
	}

	client.Stop()
}
