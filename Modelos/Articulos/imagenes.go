package ModeloArticulos

import (
	"fmt"
	"os"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

func UploadImageToMongodb(path string, namefile string) bson.ObjectId {

	db, err := mgo.Dial("192.168.1.110")
	check(err, "Error al conectar con mongo")
	base := db.DB("minisuper")

	file2, err := os.Open(path + "/" + namefile)
	check(err, "Error al abrir el archivo o el archivo no existe")
	defer file2.Close()
	stat, err := file2.Stat()
	check(err, "Error al leer el archivo")
	bs := make([]byte, stat.Size()) // read the file
	_, err = file2.Read(bs)
	check(err, "Error al crear objeto que contendrá el archivo")

	img, err := base.GridFS("ImagenesArticulos").Create(namefile)
	ids_img := img.Id()

	check(err, "error al crear archivo en mongo")
	_, err = img.Write(bs)

	check(err, "error al escribir archivo en mongo")
	fmt.Println("File uploaded successfully to mongo ")
	err = img.Close()
	check(err, "error al cerrar img de mongo")
	db.Close()

	idimg := getObjectIdToInterface(ids_img)

	return idimg

}

func getObjectIdToInterface(i interface{}) bson.ObjectId {
	var v = i.(bson.ObjectId)
	return v
}
