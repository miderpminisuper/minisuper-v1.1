package ModeloArticulos

// import (
// 	"fmt"

// 	"golang.org/x/net/context"
// 	"gopkg.in/mgo.v2/bson"
// 	"gopkg.in/olivere/elastic.v5"
// )

// type ContectElastic struct {
// 	Index  string      `json:"_index"`
// 	Type   string      `json:"_type"`
// 	Source SourceValue `json:"_source"`
// }

// type Roles struct {
// 	Id     bson.ObjectId `json:"_id"`
// 	Name   string        `json:"name"`
// 	Campos []string      `json:"campos"`
// }

// type Persona struct {
// 	Id     bson.ObjectId `json:"_id"`
// 	Name   string        `json:"name"`
// 	Campos []string      `json:"campos"`
// }

// type SourceValue struct {
// 	Name          string          `json:"name"`
// 	Persona       bson.ObjectId   `json:"personaid"`
// 	PersonaName   string          `json:"tipopersona"`
// 	CamposPersona []CamposValor   `json:"camposvalor"`
// 	Rol           []RolFields     `json:"camposrol"`
// 	CamposPropios []CamposPropios `json:"campospropios"`
// 	Padre         bson.ObjectId   `json:"parent"`
// 	Username      string          `json:"username"`
// 	Userpass      string          `json:"userpass"`
// 	Useremail     string          `json:"useremail"`
// }

// type CamposValor struct {
// 	Id    bson.ObjectId `json:"_id"`
// 	Clave string        `json:"clave"`
// 	Valor interface{}   `json:"valor"`
// }

// type RolFields struct {
// 	Id     bson.ObjectId `json:"_id"`
// 	Name   string        `json:"name"`
// 	Campos []CamposValor `json:"campos"`
// }

// type CamposPropios struct {
// 	Id    bson.ObjectId `json:"_id"`
// 	Clave string        `json:"clave"`
// 	Valor interface{}   `json:"valor"`
// }

// func InsertElasticValue(index string, id string, data interface{}) {
// 	// Create a client and connect to http://192.168.2.10:9201
// 	//	client, err := elastic.NewClient(elastic.SetURL("http://192.168.1.111:9200"))
// 	client, err := elastic.NewClient(elastic.SetURL("http://192.168.1.110:9200"))
// 	if err != nil {
// 		// Handle error
// 	}

// 	fmt.Println(client)
// 	exists, err := client.IndexExists(index).Do(context.TODO())
// 	if err != nil {
// 	}
// 	if !exists {
// 		fmt.Println("el indice no existe")

// 	} else {
// 		put1, err := client.Index().Index(index).Type("Users").Id(id).BodyJson(data).Do(context.TODO())
// 		if err != nil {
// 			fmt.Println("error al indexar 1")
// 			panic(err)
// 		} else {
// 			fmt.Printf("Indexed to index %s, type %s\n", put1.Index, put1.Type)
// 		}

// 	}
// }

// func DeleteElasticValue(index string, types string, id string) {
// 	client, err := elastic.NewClient(elastic.SetURL("http://192.168.1.110:9200"))
// 	if err != nil {
// 		// Handle error
// 	}
// 	res, err := client.Delete().Index(index).Type(types).Id(id).Do(context.TODO())
// 	if err != nil {
// 		fmt.Println(err)
// 	}
// 	if res.Found != true {
// 		fmt.Println("expected Found = true; got %v", res.Found)
// 	}
// }
