package ModeloArticulos

import (
	"bytes"
	"context"
	"encoding/base64"
	"fmt"
	"image/jpeg"
	"image/png"
	"log"
	"path/filepath"
	"strconv"
	"time"

	"../General"
	elastic "gopkg.in/olivere/elastic.v5"

	"gopkg.in/mgo.v2/bson"
)

//Producto Estructura a escribir en template
type Producto struct {
	Articulo   Articulo
	Datos      DatosArticulo
	Etiquetas  string
	TipoFisico string
	TipoLogico string
	TipoKit    string
	ErrorTipo  string
	NomImg     string
	Entero     string
}

//Articulo estructura de productos de mongo
type Articulo struct {
	ID          bson.ObjectId     `bson:"_id,omitempty" 	json:"_id,omitempty"`
	CodBarra    string            `bson:"codigobarra" 		json:"codigobarra"`
	Descripcion string            `bson:"descripcion" 		json:"descripcion"`
	Imagen      bson.ObjectId     `bson:"imagen,omitempty" 	json:"imagen,omitempty"`
	Fotos       []bson.ObjectId   `bson:"fotos,omitempty" 	json:"fotos,omitempty"`
	Tipo        string            `bson:"tipo" 				json:"tipo"`
	Unidad      string            `bson:"unidad" 			json:"unidad"`
	Entero      bool              `bson:"usa_fraccion" 		json:"usa_fraccion"`
	NumDec      int               `bson:"num_dec" 			json:"num_dec"`
	Etiquetas   map[string]string `bson:"etiquetas" 		json:"etiquetas"`
	PreCompra   float64           `bson:"costo" 			json:"costo"`
	PreVenta    float64           `bson:"precio" 			json:"precio"`
	TagImg      string
	Estatus     string
	Cantidad    string
}

//DatosArticulo estructura de datos de producto en PostgreSQL
type DatosArticulo struct {
	ID       string
	Cantidad float64
	Estatus  string
}

//Unidades estructura de medidas en mongo
type Unidades struct {
	ID           bson.ObjectId  `bson:"_id,omitempty"`
	Grupo        string         `bson:"grupo_medida"`
	DatosMedidas []DatosMedidas `bson:"medidas"`
}

//DatosMedidas estructura de datos de cada tipo de medida
type DatosMedidas struct {
	Nombre      string `bson:"nombre"`
	Abreviatura string `bson:"abreviatura"`
}

//InsertaProductoEnMongo inserta un producto en mongo
func InsertaProductoEnMongo(producto Articulo) {
	productos, sesion := ConectaMongoDB(models.NOMBRE_BD_MONGO, models.COLECCION_PRODUCTOS, models.SERVIDOR_MONGO)
	if producto.Imagen.Hex() != "" {
		err := productos.Insert(bson.M{"_id": producto.ID, "codigobarra": producto.CodBarra, "descripcion": producto.Descripcion, "tipo": producto.Tipo, "unidad": producto.Unidad, "usa_fraccion": producto.Entero, "num_dec": producto.NumDec, "etiquetas": producto.Etiquetas, "costo": producto.PreCompra, "precio": producto.PreVenta, "imagen": producto.Imagen})
		check(err, "Error al Insertar producto a MONGO")
	} else {
		err := productos.Insert(bson.M{"_id": producto.ID, "codigobarra": producto.CodBarra, "descripcion": producto.Descripcion, "tipo": producto.Tipo, "unidad": producto.Unidad, "usa_fraccion": producto.Entero, "num_dec": producto.NumDec, "etiquetas": producto.Etiquetas, "costo": producto.PreCompra, "precio": producto.PreVenta})
		check(err, "Error al Insertar producto a MONGO")
	}

	sesion.Close()
}

//InsertaProductoEnPostgreSQL inserta datos de producto de mongo asociado a un ID
func InsertaProductoEnPostgreSQL(datos DatosArticulo, dec int) {
	BasePosGres := ConexionPostgres(models.SERVIDOR_POSTGRESQL, models.BASE_POSTGRESQL, models.USER_POSTGRESQL, models.PASS_POSGRESQL)
	query := `INSERT INTO "` + models.TABLA_POSTGRESQL + `" ("id_producto", "Cantidad", "Estatus") VALUES('` + datos.ID + `',` + strconv.FormatFloat(datos.Cantidad, 'f', dec, 32) + `,'` + datos.Estatus + `')`
	con, err := BasePosGres.Query(query)
	check(err, "Error al Insertar producto en Postgresql")
	con.Close()
	BasePosGres.Close()
}

//ConsultaCodigo busca el código de un producto y regresa el número de registros encontrados
func ConsultaCodigo(codigo string) int {
	productos, sesion := ConectaMongoDB(models.NOMBRE_BD_MONGO, models.COLECCION_PRODUCTOS, models.SERVIDOR_MONGO)
	n, err := productos.Find(bson.M{"codigobarra": codigo}).Count()
	check(err, "Error al consultar codigo de producto en MONGO")
	sesion.Close()
	return n
}

//ActualizaProductoEnMongo inserta un producto en mongo
func ActualizaProductoEnMongo(producto Articulo) {

	//productos, sesion := ConectaMongoDB(BASE_MONGO, COLECCION_MONGO, SERVIDOR_MONGO)
	colec, sesion := ConectaMongoDB(models.NOMBRE_BD_MONGO, models.COLECCION_PRODUCTOS, models.SERVIDOR_MONGO)
	//sesion, err := mgo.Dial(SERVIDOR_MONGO)
	//check(err, "Error al conectar con minisuper de MongoDB")
	//colec := sesion.DB(BASE_MONGO).C(COLECCION_MONGO)
	change := bson.M{}

	if producto.Imagen.Hex() != "" {
		change = bson.M{"$set": bson.M{"codigobarra": producto.CodBarra, "descripcion": producto.Descripcion, "tipo": producto.Tipo, "unidad": producto.Unidad, "usa_fraccion": producto.Entero, "num_dec": producto.NumDec, "etiquetas": producto.Etiquetas, "costo": producto.PreCompra, "precio": producto.PreVenta, "imagen": producto.Imagen}}
	} else {
		change = bson.M{"$set": bson.M{"codigobarra": producto.CodBarra, "descripcion": producto.Descripcion, "tipo": producto.Tipo, "unidad": producto.Unidad, "usa_fraccion": producto.Entero, "num_dec": producto.NumDec, "etiquetas": producto.Etiquetas, "costo": producto.PreCompra, "precio": producto.PreVenta}}
	}

	err := colec.Update(bson.M{"_id": producto.ID}, change)
	check(err, "Error al Actualizar producto a MONGO")
	// fmt.Println("este es e erro", err)

	sesion.Close()
}

//ActualizaProductoEnPostgreSQL inserta datos de producto de mongo asociado a un ID
func ActualizaProductoEnPostgreSQL(datos DatosArticulo, dec int) {
	BasePosGres := ConexionPostgres(models.SERVIDOR_POSTGRESQL, models.BASE_POSTGRESQL, models.USER_POSTGRESQL, models.PASS_POSGRESQL)
	query := ``
	n := ConsultaIDPostgres(datos.ID)

	if n > 0 {
		query = `UPDATE "` + models.TABLA_POSTGRESQL + `"  SET  "Cantidad" = ` + strconv.FormatFloat(datos.Cantidad, 'f', dec, 32) + `,"Estatus"='` + datos.Estatus + `' WHERE id_producto='` + datos.ID + `'`
		con, err := BasePosGres.Query(query)
		check(err, "Error al Actualizar producto en Postgresql")
		con.Close()
	} else {
		query := `INSERT INTO "` + models.TABLA_POSTGRESQL + `" ("id_producto", "Cantidad", "Estatus") VALUES('` + datos.ID + `',` + strconv.FormatFloat(datos.Cantidad, 'f', dec, 32) + `,'` + datos.Estatus + `')`
		con, err := BasePosGres.Query(query)
		check(err, "Error al Insertar producto en Postgresql desde actualización")
		con.Close()
	}

	BasePosGres.Close()
}

//ConsultaIDPostgres busca el código de un producto y regresa el número de registros encontrados
func ConsultaIDPostgres(ID string) int {
	BasePosGres := ConexionPostgres(models.SERVIDOR_POSTGRESQL, models.BASE_POSTGRESQL, models.USER_POSTGRESQL, models.PASS_POSGRESQL)
	query := `select count(*) from "` + models.TABLA_POSTGRESQL + `" where "id_producto" = '` + ID + `'`
	row, err := BasePosGres.Query(query)
	check(err, "Error al Consultar ID en Postgresql")
	var datos int
	for row.Next() {
		err := row.Scan(&datos)
		check(err, "Error al scanear ID de postgres")
	}
	row.Close()
	BasePosGres.Close()

	return datos
}

//ConsultaCodigoYRegresaEstructuras rastrea en mongo y postgres los datos de un producto asociado a un código
func ConsultaCodigoYRegresaEstructuras(codigo string) (Articulo, DatosArticulo) {
	productos, sesion := ConectaMongoDB(models.NOMBRE_BD_MONGO, models.COLECCION_PRODUCTOS, models.SERVIDOR_MONGO)
	var producto Articulo
	err := productos.Find(bson.M{"codigobarra": codigo}).One(&producto)
	check(err, "Error al consultar codigo de producto en MONGO y regresar estructuras")
	sesion.Close()

	BasePosGres := ConexionPostgres(models.SERVIDOR_POSTGRESQL, models.BASE_POSTGRESQL, models.USER_POSTGRESQL, models.PASS_POSGRESQL)
	query := `select "id_producto", "Cantidad", "Estatus" from "` + models.TABLA_POSTGRESQL + `" where "id_producto" = '` + producto.ID.Hex() + `'`
	row, err := BasePosGres.Query(query)
	check(err, "Error al Consultar producto en Postgresql")
	var datos DatosArticulo
	for row.Next() {
		err := row.Scan(&datos.ID, &datos.Cantidad, &datos.Estatus)
		check(err, "Error al scanear producto de postgres")
	}
	row.Close()
	BasePosGres.Close()
	return producto, datos
}

func ConsultaCodigoYRegresaEstructuras2(codigo string) Articulo {

	productos, sesion := ConectaMongoDB(models.NOMBRE_BD_MONGO, models.COLECCION_PRODUCTOS, models.SERVIDOR_MONGO)
	var producto Articulo
	err := productos.Find(bson.M{"codigobarra": codigo}).One(&producto)
	check(err, "Error al consultar codigo de producto en MONGO")
	sesion.Close()
	return producto
}

func ConsultaCodigoYRegresaEstructuras3(codigo string, tabla_destino string) (Articulo, DatosArticulo) {
	productos, sesion := ConectaMongoDB(models.NOMBRE_BD_MONGO, models.COLECCION_PRODUCTOS, models.SERVIDOR_MONGO)
	var producto Articulo
	err := productos.Find(bson.M{"codigobarra": codigo}).One(&producto)
	check(err, "Error al consultar codigo de producto en MONGO y regresar estructuras")
	sesion.Close()
	BasePosGres := ConexionPostgres(models.SERVIDOR_POSTGRESQL, models.BASE_POSTGRESQL, models.USER_POSTGRESQL, models.PASS_POSGRESQL)
	query := `select "id_producto","cantidad","estatus" from  public."` + tabla_destino + `" where "id_producto" = '` + producto.ID.Hex() + `'`
	row, err := BasePosGres.Query(query)
	fmt.Println(query)
	check(err, "Error al Consultar producto en Postgresql")
	var datos DatosArticulo
	for row.Next() {
		err := row.Scan(&datos.ID, &datos.Cantidad, &datos.Estatus)
		check(err, "Error al scanear producto de postgres")
	}
	row.Close()
	BasePosGres.Close()
	return producto, datos
}

//RegresaTagImagen regresa el tag de la imagen con ID correspondiente
func RegresaTagImagen(ID string) (string, string) {
	objid := bson.ObjectIdHex(ID)
	img, err := ObtieneBaseDeSesion(models.NOMBRE_BD_MONGO, AbrirSesionMongo(models.SERVIDOR_MONGO)).GridFS(models.COLECCION_IMAGENES_MONGO).OpenId(objid)
	check(err, "Error al leer Imagen de MONGO")
	b := make([]byte, img.Size())
	n, _ := img.Read(b)
	fmt.Println("N -> ", n)

	var tmp = ``
	///////////////////////////////////////////////////////////////////////////
	switch extension := filepath.Ext(img.Name()); extension {
	case ".jpg", ".jpeg":
		imagen, _ := jpeg.Decode(bytes.NewReader(b))
		buffer := new(bytes.Buffer)
		if err := jpeg.Encode(buffer, imagen, nil); err != nil {
			log.Println("unable to encode image.")
		}
		str := base64.StdEncoding.EncodeToString(buffer.Bytes())
		tmp = `data:image/jpg;base64,` + str

	case ".png":

		imagen, _ := png.Decode(bytes.NewReader(b))
		buffer := new(bytes.Buffer)
		if err := png.Encode(buffer, imagen); err != nil {
			log.Println("unable to encode image.")
		}
		str := base64.StdEncoding.EncodeToString(buffer.Bytes())
		tmp = `data:image/png;base64,` + str

	}
	return tmp, img.Name()
	///////////////////////////////////////////////////////////////////////////

}

//RegresaSrcImagenes regresa el tag de la imagen de un conjunto de Articulos
func RegresaSrcImagenes(enc []Articulo) []Articulo {
	session, err := models.ConexionMongo(models.SERVIDOR_MONGO)
	check(err, "Error al conectar con almacenes de MongoDB")
	Base := session.DB(models.NOMBRE_BD_MONGO)
	imagenes := Base.C("ImagenesArticulos.files")
	tmp := ``
	for i, v := range enc {
		if v.Imagen.Hex() != "" {

			n, err := imagenes.Find(bson.M{"_id": v.Imagen}).Count()
			check(err, "Error al consultar id de imagen de producto en MONGO")
			fmt.Println(n)
			if n > 0 {
				img, err1 := Base.GridFS(models.COLECCION_IMAGENES_MONGO).OpenId(v.Imagen)

				check(err1, "Error al leer Imagen de MONGO: "+v.Imagen.Hex())

				b := make([]byte, img.Size())
				n, err := img.Read(b)
				check(err, "Error al crear mapa de bytes")
				fmt.Println("N -> ", n)

				switch extension := filepath.Ext(img.Name()); extension {
				case ".jpg", ".jpeg":
					imagen, _ := jpeg.Decode(bytes.NewReader(b))
					buffer := new(bytes.Buffer)
					if err := jpeg.Encode(buffer, imagen, nil); err != nil {
						log.Println("unable to encode image.")
					}
					str := base64.StdEncoding.EncodeToString(buffer.Bytes())
					tmp = `data:image/jpg;base64,` + str

				case ".png":

					imagen, _ := png.Decode(bytes.NewReader(b))
					buffer := new(bytes.Buffer)
					if err := png.Encode(buffer, imagen); err != nil {
						log.Println("unable to encode image.")
					}
					str := base64.StdEncoding.EncodeToString(buffer.Bytes())
					tmp = `data:image/png;base64,` + str

				}

				enc[i].TagImg = tmp

				defer img.Close()
			}
		}
	}

	defer session.Close()
	return enc
}

//ConsultaUnidadesDistintas funcion que selecciona de Mongo todas
//las unidades distintas de los productos
func ConsultaUnidadesDistintas() []string {
	var unidades []string
	productos, sesion := ConectaMongoDB(models.NOMBRE_BD_MONGO, models.COLECCION_PRODUCTOS, models.SERVIDOR_MONGO)
	err := productos.Find(nil).Distinct("unidad", &unidades)
	check(err, "Problema al consultar articulos distintos")
	sesion.Close()
	return unidades
}

//ConsultaEstatusDistintos es una funcion que recupera los estatus distintos de Postgresql
func ConsultaEstatusDistintos() []string {
	var estatus []string
	BasePosGres := ConexionPostgres(models.SERVIDOR_POSTGRESQL, models.BASE_POSTGRESQL, models.USER_POSTGRESQL, models.PASS_POSGRESQL)
	query := `select distinct "Estatus" from "` + models.TABLA_POSTGRESQL + `"`
	row, err := BasePosGres.Query(query)
	check(err, "Error al Consultar estatus en Postgresql")
	str := ""
	for row.Next() {
		str = ""
		err := row.Scan(&str)
		check(err, "Error al scanear producto de postgres")
		estatus = append(estatus, str)
	}
	row.Close()
	BasePosGres.Close()
	return estatus
}

//GeneraCodigo funcion que genera codigo de barras a partir de un objectid aleatorio
func GeneraCodigo() string {
	OBJ := bson.NewObjectIdWithTime(time.Now())
	return OBJ.Hex()[:10]
}

//GetAllArticles regresa una coleccion de artículos de Mongo
func GetAllArticles() []Articulo {
	var result []Articulo
	productos, sesion := ConectaMongoDB(models.NOMBRE_BD_MONGO, models.COLECCION_PRODUCTOS, models.SERVIDOR_MONGO)
	productos.Find(nil).All(&result)
	sesion.Close()
	return result
}

//GetArticles regresa una coleccion de artículos de Mongo
func GetArticles(datos []bson.ObjectId) []Articulo {
	var result []Articulo
	var res Articulo
	productos, sesion := ConectaMongoDB(models.NOMBRE_BD_MONGO, models.COLECCION_PRODUCTOS, models.SERVIDOR_MONGO)

	for _, value := range datos {
		res = Articulo{}
		productos.Find(bson.M{"_id": value}).One(&res)
		result = append(result, res)
	}

	sesion.Close()
	return result
}

func GeneraSelectUnidades() string {
	unidades := ConsultaUnidadesMgo()
	templ := `<option value="" selected>--SELECCIONE--</option>`
	opc := ``

	for _, v := range unidades {
		templ += `<optgroup label="` + v.Grupo + `">`
		opc = ``
		for _, val := range v.DatosMedidas {
			opc += `<option value="` + val.Nombre + `">` + val.Abreviatura + `</option>`
		}

		templ = templ + opc + `</optgroup>`
	}
	return templ
}

func GeneraSelectUnidadesfijo(abr string) string {
	unidades := ConsultaUnidadesMgo()
	templ := `<option value="">--SELECCIONE--</option>`
	opc := ``

	for _, v := range unidades {
		templ += `<optgroup label="` + v.Grupo + `">`
		opc = ``
		for _, val := range v.DatosMedidas {
			if abr == val.Nombre {
				opc += `<option value="` + val.Nombre + `" selected>` + val.Abreviatura + `</option>`
			} else {
				opc += `<option value="` + val.Nombre + `">` + val.Abreviatura + `</option>`
			}

		}

		templ = templ + opc + `</optgroup>`
	}
	return templ
}

//ConsultaUnidadesMgo consulta los proveedores de mgo
func ConsultaUnidadesMgo() []Unidades {
	unidades := []Unidades{}
	datos, sesion := ConectaMongoDB(models.NOMBRE_BD_MONGO, "medidas", models.SERVIDOR_MONGO)
	err := datos.Find(nil).All(&unidades)
	check(err, "Problema al consultar unidades en mongo")
	sesion.Close()
	return unidades
}

//BuscarEnElastic busca un texto específico en elástic
func BuscarEnElastic(texto string, client *elastic.Client) *elastic.SearchResult {
	boolquery := elastic.NewBoolQuery()

	if texto != "" {
		boolquery = boolquery.Must(elastic.NewMatchQuery("_all", texto)).Should(elastic.NewMatchQuery("descripcion", texto))
	}

	docs, err := client.Search().Index(models.INDICE_ELASTICSEARCH).Type(models.COLECCION_ELASTICSEARCH).
		Query(boolquery).
		From(0).Size(models.TOTAL_RESULTADOS_DEVUELTOS).
		Do(context.TODO())
	check(err, "Error al realizar búsqueda en Elastic")
	return docs
}

//ConectarElastic crea conexion a elasticsearch
func ConectarElastic() *elastic.Client {
	client, err := elastic.NewClient(elastic.SetURL(models.SERVIDOR_ELASTICSEARCH))
	check(err, "No se pudo conectar con Elastic")
	return client
}

//FlushElastic hace flush a la consulta realizada
func FlushElastic(client *elastic.Client) {
	_, err := client.Flush().Index(models.INDICE_ELASTICSEARCH).Do(context.TODO())
	check(err, "Error al limpiar Objeto de consulta de elastic")
}
