package users

import (
	"../../Controladores"
	"../General"
	"gopkg.in/mgo.v2/bson"
)

/*
func conectMgoUser() (*mgo.Collection, *mgo.Session) {
	session, err := mgo.Dial("192.168.1.110")
	if err != nil {
		panic(err)
	}
	c := session.DB("minisuper").C("usuarios")
	return c, session
}
*/
func GetAllUser() ([]models.Usuarios, error) {
	var result []models.Usuarios
	/*c, s := conectMgoUser()*/
	c, s, err_conex := models.ConectarMongo(models.SERVIDOR_MONGO, models.NOMBRE_BD_MONGO, models.COLECCION_USUARIOS)
	if err_conex != nil {
		return nil, err_conex
	}
	c.Find(bson.M{}).Select(bson.M{}).Limit(50).All(&result)
	s.Close()
	return result, err_conex
}

func NewUser(data interface{}) error {
	c, _, err := models.ConectarMongo(models.SERVIDOR_MONGO, models.NOMBRE_BD_MONGO, models.COLECCION_USUARIOS)
	if err != nil {
		return err
	}
	err = c.Insert(data)
	if err != nil {
		return err
	}
	return err
}
func SetUsuarioDefault() string {
	c, _, err := models.ConectarMongo(models.SERVIDOR_MONGO, models.NOMBRE_BD_MONGO, models.COLECCION_USUARIOS)
	if err != nil {
		return "Upps! No pude conectarme, lo arreglare."
	}
	id := bson.NewObjectId()
	user := models.Usuarios{Id: id, Nombre: "Juan Pérez", Usuario: "admin", Password: controllers.EncriptaTexto("admin"), Activo: true}
	err = c.Insert(user)
	if err != nil {
		return "Upps! algo hicimos mal, lo arreglare."
	}
	return id.Hex()
}
func GetUserId(id bson.ObjectId) (models.Usuarios, error) {
	var result models.Usuarios
	/*c, s := conectMgoUser()*/
	c, s, err_conex := models.ConectarMongo(models.SERVIDOR_MONGO, models.NOMBRE_BD_MONGO, models.COLECCION_USUARIOS)
	if err_conex != nil {
		return result, err_conex
	}
	c.Find(bson.M{"_id": id}).One(&result)
	s.Close()
	return result, err_conex
}

func EditUserId(id bson.ObjectId, name string, username string, password string, activo bool, contact interface{}, direccion interface{}, adicional interface{}) error {
	c, _, err := models.ConectarMongo(models.SERVIDOR_MONGO, models.NOMBRE_BD_MONGO, models.COLECCION_USUARIOS)
	if err != nil {
		return err
	}
	change := bson.M{"$set": bson.M{"nombre_completo": name, "usuario": username, "password": password, "activo": activo, "contacto": contact, "direccion": direccion, "campos_adicionales": adicional}}
	err = c.Update(bson.M{"_id": id}, change)
	if err != nil {
		return err
	}
	return err
}

func EliminaUsuario(id string) error {
	colec, _, err := models.ConectarMongo(models.SERVIDOR_MONGO, models.NOMBRE_BD_MONGO, models.COLECCION_USUARIOS)
	if err != nil {
		return err
	}
	err = colec.Remove(bson.M{"_id": bson.ObjectIdHex(id)})
	if err != nil {
		return err
	}
	return err
}
