package venta_m

import (
	"fmt"
	"time"

	"../General"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type FormaPago struct {
	Id         bson.ObjectId `bson:"_id,omitempty"`
	Nombre     string        `bson:"nombre"`
	Cambio     bool          `bson:"devuelve_cambio"`
	Comision   float64       `bson:"comision"`
	Estatus    string        `bson:"status"`
	Porcentaje bool          `bson:"porcentaje"`
	Clave      string        `bson:"sat"`
}

type CotizacionC struct {
	Id             bson.ObjectId `bson:"_id,omitempty"         `
	FechaHora      time.Time     `bson:"fecha_hora"            `
	IdCliente      bson.ObjectId `bson:"usuario_destino,omitempty"  `
	IdUsuario      bson.ObjectId `bson:"usuario_origen,omitempty"  `
	AlmacenOrigen  bson.ObjectId `bson:"almacen_origen,omitempty"`
	AlmacenDestino bson.ObjectId `bson:"almacen_destino,omitempty"`
	Productos      []ProductoCVC `bson:"productos,omitempty"`
}

type ProductoCVC struct {
	ID             bson.ObjectId `bson:"_id,omitempty"`
	Cantidad       float64       `bson:"cantidad,omitempty"`
	Costo          float64       `bson:"costo,omitempty"`
	Precio         float64       `bson:"precio,omitempty"`
	Descuento      float64       `bson:"descuento,omitempty"`
	DescuentoTotal float64       `bson:"descuento_total,omitempty"`
	Importe        float64       `bson:"importe,omitempty"`
}

func conMongoFormaPago() (*mgo.Collection, *mgo.Session) {
	session, err := mgo.Dial("192.168.1.110")
	if err != nil {
		panic(err)
	}
	c := session.DB("minisuper").C("forma_pago")
	return c, session
}

func GetAllFormaPago() []FormaPago {
	var result []FormaPago
	c, s := conMongoFormaPago()
	c.Find(bson.M{"status": "on"}).Select(bson.M{}).All(&result)
	s.Close()
	return result
}

func GetDatosFiscales() models.DatosFiscales {
	var result models.DatosFiscales
	session, err := mgo.Dial("192.168.1.110")
	if err != nil {
		fmt.Println("----------------ERROR AL CONECTAR CON MONGO EN DATOS FISCALES---------------------")
		panic(err)
	}
	c := session.DB("minisuper").C("datosfiscales")

	c.Find(nil).Select(bson.M{}).One(&result)
	session.Close()
	return result
}

func GetFormaPagoId(id bson.ObjectId) FormaPago {
	var result FormaPago
	c, s := conMongoFormaPago()
	c.Find(bson.M{"_id": id}).One(&result)
	s.Close()
	return result
}

func GetCotizacionById(Id string) CotizacionC {
	var result CotizacionC
	session, err := mgo.Dial("192.168.1.110")
	if err != nil {
		fmt.Println("----------------ERROR AL CONECTAR CON MONGO---------------------")
		panic(err)
	}
	c := session.DB("minisuper").C("movimientos")
	ID := bson.ObjectIdHex(Id)
	err = c.Find(bson.M{"_id": ID}).One(&result)
	if err != nil {
		fmt.Println("----------------ERROR AL BUSCAR COTIZACION EN MONGO---------------------")
		panic(err)
	}

	return result
}

func GetFormaByID(Id string) FormaPago {
	var result FormaPago
	session, err := mgo.Dial("192.168.1.110")
	if err != nil {
		fmt.Println("----------------ERROR AL CONECTAR CON MONGO POR FORMA DE PAGO---------------------")
		panic(err)
	}
	c := session.DB("minisuper").C("forma_pago")
	ID := bson.ObjectIdHex(Id)
	err = c.Find(bson.M{"_id": ID}).One(&result)
	if err != nil {
		fmt.Println("----------------ERROR AL BUSCAR FORMA DE PAGO EN MONGO---------------------")
		panic(err)
	}

	return result
}

func ConectarMongoVenta() (*mgo.Collection, *mgo.Session) {
	session, err := mgo.Dial("192.168.1.110")
	if err != nil {
		fmt.Println("----------------ERROR AL CONECTAR PARA VENTA EN MONGO---------------------")
		panic(err)
	}
	c := session.DB("minisuper").C("movimientos")
	return c, session
}

func CrearVenta(data interface{}) bool {
	var b bool = false
	c, s := ConectarMongoVenta()
	err := c.Insert(data)
	if err != nil {
		fmt.Println(err)
	} else {
		b = true
	}
	s.Close()
	return b
}

func GetAllClientes(dato string) []models.Cliente {
	var result []models.Cliente
	session, err := mgo.Dial("192.168.1.110")
	if err != nil {
		fmt.Println("----------------ERROR AL CONECTAR PARA CLIENTE EN MONGO---------------------")
		panic(err)
	}
	c := session.DB("minisuper").C("clientes")
	var todos []bson.M
	rfc := bson.M{"rfc": bson.M{"$regex": dato, "$options": "i"}}
	nombre := bson.M{"nombre_completo": bson.M{"$regex": dato, "$options": "i"}}
	todos = append(todos, rfc)
	todos = append(todos, nombre)

	c.Find(bson.M{"$or": todos}).All(&result)
	session.Close()
	return result
}

//GetClientes regresa una coleccion de artículos de Mongo
func GetClientes(datos []bson.ObjectId) []models.Cliente {
	var result []models.Cliente
	var res models.Cliente

	s, err := mgo.Dial("192.168.1.110")
	if err != nil {
		fmt.Println("----------------ERROR AL CONECTAR PARA CLIENTE EN MONGO---------------------")
		panic(err)
	}
	c := s.DB("minisuper").C("clientes")

	for _, value := range datos {
		res = models.Cliente{}
		c.Find(bson.M{"_id": value}).One(&res)
		result = append(result, res)
	}

	s.Close()
	return result
}
