package venta_m

import (
	"fmt"
	"time"

	"../General"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

//Movimiento estructura de movimientos en mongo
type MovimientoIndex struct {
	ID             bson.ObjectId
	Date           time.Time
	AlmacenOrigen  Provedores
	AlmacenDestino Provedores
	UsuarioOrigen  Provedores
	UsuarioDestino Provedores
	Productos      []Producto
}

//Almacen estructura de almacenes en mongo
type Almacen struct {
	ID      bson.ObjectId     `bson:"_id,omitempty"`
	Nombre  string            `bson:"nombre"`
	Tipo    string            `bson:"tipo"`
	Defecto string            `bson:"defecto"`
	Datos   map[string]string `bson:"datos"`
}

//Editores estructura de editores de compras
type Editores struct {
	ID   bson.ObjectId `bson:"_id,omitempty"`
	Tipo string        `bson:"tipo_movimiento"`
	Date time.Time     `bson:"fecha_hora"`
}

//Movimiento estructura de movimientos en mongo
//Esta Estructura está incluida en el archivo de Ismael
type Movimiento struct {
	ID             bson.ObjectId `bson:"_id,omitempty"`
	Tipo           string        `bson:"tipo_movimiento"`
	Date           time.Time     `bson:"fecha_hora"`
	AlmacenOrigen  bson.ObjectId `bson:"almacen_origen,omitempty"`
	AlmacenDestino bson.ObjectId `bson:"almacen_destino,omitempty"`
	UsuarioOrigen  bson.ObjectId `bson:"usuario_origen,omitempty"`
	UsuarioDestino bson.ObjectId `bson:"usuario_destino,omitempty"`
	Productos      []Producto    `bson:"productos"`
	Actualiza      []Editores    `bson:"edita"`
}

//Provedores estructura de almacenes en mongo
type Provedores struct {
	ID     bson.ObjectId `bson:"_id,omitempty"`
	Nombre string        `bson:"nombre_completo"`
}

type Producto struct {
	ID            bson.ObjectId     `bson:"_id,omitempty"`
	Cantidad      float64           `bson:"cantidad"`
	Costo         float64           `bson:"costo"`
	Impuestos     map[string]string `bson:"impuestos"`
	TotalImpuesto float64           `bson:"total_impuesto"`
	Importe       float64           `bson:"importe"`
	Ganancia      string            `bson:"ganancia"`
}

func ConectarMongoCot() (*mgo.Collection, *mgo.Session) {
	session, err := mgo.Dial("192.168.1.110")
	if err != nil {
		panic(err)
	}
	c := session.DB("minisuper").C("cotizaciones_venta")
	return c, session
}

func CrearCotizacion(data interface{}) bool {
	var b bool = false
	c, s := ConectarMongoCot()
	err := c.Insert(data)
	if err != nil {
		fmt.Println(err)
	} else {
		b = true
	}
	s.Close()
	return b
}

func GetAllVentas() []MovimientoIndex {
	var result []Movimiento
	var res []MovimientoIndex
	var rex MovimientoIndex

	c, s, _ := models.ConectarMongo(models.SERVIDOR_MONGO, models.NOMBRE_BD_MONGO, models.COLECCION_MOVIMIENTOS_MONGO)
	c.Find(bson.M{"tipo_movimiento": "venta"}).Sort("-fecha_hora").All(&result)
	s.Close()

	for _, v := range result {
		rex = MovimientoIndex{}
		rex.ID = v.ID
		rex.AlmacenDestino.ID = v.AlmacenDestino
		rex.AlmacenOrigen.ID = v.AlmacenOrigen
		rex.UsuarioOrigen.ID = v.UsuarioOrigen
		rex.UsuarioDestino.ID = v.UsuarioDestino
		rex.Date = v.Date
		rex.Productos = v.Productos
		res = append(res, rex)
	}
	return res
}

//GetCompras regresa movimientos específicos
func GetVentas(datos []bson.ObjectId) []MovimientoIndex {
	var result []Movimiento
	var res Movimiento
	var rets []MovimientoIndex
	var rex MovimientoIndex

	c, s, _ := models.ConectarMongo(models.SERVIDOR_MONGO, models.NOMBRE_BD_MONGO, models.COLECCION_MOVIMIENTOS_MONGO)
	for _, value := range datos {
		res = Movimiento{}
		c.Find(bson.M{"_id": value}).One(&res)
		result = append(result, res)
	}

	for _, v := range result {
		rex = MovimientoIndex{}
		rex.ID = v.ID
		fmt.Println("ObjectId. ", rex.ID)
		rex.AlmacenDestino.ID = v.AlmacenDestino
		rex.AlmacenOrigen.ID = v.AlmacenOrigen
		rex.UsuarioOrigen.ID = v.UsuarioOrigen
		rex.UsuarioDestino.ID = v.UsuarioDestino
		rex.Date = v.Date
		rex.Productos = v.Productos
		rets = append(rets, rex)
	}

	s.Close()
	return rets
}

func ObtenerNombreUsuario(id bson.ObjectId) string {
	var nombre bson.M
	c, _, err := models.ConectarMongo(models.SERVIDOR_MONGO, models.NOMBRE_BD_MONGO, models.COLECCION_USUARIOS_MONGO)
	if err != nil {
		fmt.Println("Errores: ", err)
	}
	err = c.Find(bson.M{"_id": id}).One(&nombre)
	if err != nil {
		fmt.Println("Errores: ", err)
	}
	if nombre["usuario"] == nil {
		return "NO EXISTE"
	}
	return nombre["usuario"].(string)
}

func GetVentaById(ide string) Movimiento {
	var res Movimiento
	ids := bson.ObjectIdHex(ide)
	c, _, err := models.ConectarMongo(models.SERVIDOR_MONGO, models.NOMBRE_BD_MONGO, models.COLECCION_MOVIMIENTOS_MONGO)
	if err != nil {
		fmt.Println("Errores: ", err)
	}
	c.Find(bson.M{"_id": ids}).One(&res)
	return res
}

//ConsultaAlmacenesMgo regresa los almacenes de mongo
func ConsultaAlmacenesMgo() []Almacen {
	almacenes := []Almacen{}
	productos, _, err := models.ConectarMongo(models.SERVIDOR_MONGO, models.NOMBRE_BD_MONGO, models.COLECCION_ALMACENES_MONGO)
	err = productos.Find(nil).All(&almacenes)
	if err != nil {
		fmt.Println("Errores: ", err)
	}
	return almacenes
}

//ConsultaProveedorMgo consulta los proveedores de mgo
func ConsultaProveedorMgo() []Provedores {
	provedores := []Provedores{}
	productos, _, err := models.ConectarMongo(models.SERVIDOR_MONGO, models.NOMBRE_BD_MONGO, models.COLECCION_PROVEEDORES)
	err = productos.Find(bson.M{"activo": true}).All(&provedores)
	if err != nil {
		fmt.Println("Errores: ", err)
	}
	return provedores
}
