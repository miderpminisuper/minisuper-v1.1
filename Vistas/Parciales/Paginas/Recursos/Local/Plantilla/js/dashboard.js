$( document ).ready(function() {
    console.log("¡ready!");
    message('¡Wow! ¡lo has hecho estupendo!',1); 
    //message('¡Ups! ¡Hicimos algo mal! ¡No se lo digas a nadie!¡Lo arreglaremos!',0);
    message('¡Wow! ¡el proceso fue de pelos!',1);

	/*
	 *Estilo para los menus principales.
	 */
    $("[rel='tooltip']").tooltip();    
    $('.thumbnail').hover(
        function(){
            $(this).find('.caption').fadeIn(250)
        },
        function(){
            $(this).find('.caption').fadeOut(205)
        }
    ); 

    /*
     * Funcion salir
     */
    $( "#logout" ).click(function() {
        bootbox.confirm({
        title: "Mensaje",
        message: "<strong>¡Hola!</strong>, ¿Te vas tan pronto?",
        buttons: {
            confirm: {
                label: 'Si',
                className: 'btn-info'
            },
            cancel: {
                label: 'Cancelar',
                className: 'btn-danger'
            }
        },
        callback: function (result) {
            console.log('¿Te vas tan pronto?: ' + result);
            if(result == true){
                $.ajax({
                /*la URL para la petición*/
                /*url : '/users',*/
                url : '/logout',
                /*la información a enviar(también es posible utilizar una cadena de datos)*/
                data : { id : 123 },    
                /*especifica si será una petición POST o GET*/
                type : 'GET',    
                /*el tipo de información que se espera de respuesta*/
                dataType : 'html',    
                /*código a ejecutar si la petición es satisfactoria;la respuesta es pasada como argumento a la función*/
                success : function(result) {
                    console.log('success OK');
                    location.reload();
                },
                /*código a ejecutar si la petición falla; son pasados como argumentos a la función el objeto de la petición en crudo y código de estatus de la petición*/
                error : function(xhr, status) {
                    console.log('Disculpe, existió un problema');
                },    
                /*código a ejecutar sin importar si la petición falló o no*/
                complete : function(xhr, status) {
                    console.log('Proceso terminado');
                }
                });
            }else{
                //alert('Gracias.');
            }

        }
        });
    });
    /**
     * 
     */
    $( "#menuventas" ).click(function() {
        $.ajax({
        /*la URL para la petición*/
        /*url : '/users',*/
        url : '',
        /*la información a enviar(también es posible utilizar una cadena de datos)*/
        data : { id : 123 },    
        /*especifica si será una petición POST o GET*/
        type : 'GET',    
        /*el tipo de información que se espera de respuesta*/
        dataType : 'html',    
        /*código a ejecutar si la petición es satisfactoria;la respuesta es pasada como argumento a la función*/
        success : function(result) {
            console.log('success OK');
            $("#contenedor").html('<div class="alert alert-info" role="alert">Aquí el módulo de VENTAS</div>')
        },
        /*código a ejecutar si la petición falla; son pasados como argumentos a la función el objeto de la petición en crudo y código de estatus de la petición*/
        error : function(xhr, status) {
            console.log('Disculpe, existió un problema');
        },    
        /*código a ejecutar sin importar si la petición falló o no*/
        complete : function(xhr, status) {
            console.log('Proceso terminado');
        }
        });
    });
    /**
 * @melchormendoza
 * {menu compras}
 * Funcion que trae la vista de compras a la pagina principal.
 * @params: 
 * @return:Vista HTML
 */
$( "#menucompras" ).click(function() {
    $.ajax({
    /*la URL para la petición*/
    url : '',
    /*la información a enviar(también es posible utilizar una cadena de datos)*/
    data : { id : 123 },    
    /*especifica si será una petición POST o GET*/
    type : 'post',    
    /*el tipo de información que se espera de respuesta*/
    dataType : 'html',    
    /*código a ejecutar si la petición es satisfactoria;la respuesta es pasada como argumento a la función*/
    success : function(result) {
        console.log('success OK');
        $("#contenedor").html('<div class="alert alert-info" role="alert">Aquí el módulo de COMPRAS</div>')
    },
    /*código a ejecutar si la petición falla; son pasados como argumentos a la función el objeto de la petición en crudo y código de estatus de la petición*/
    error : function(xhr, status) {
        console.log('Disculpe, existió un problema');
    },    
    /*código a ejecutar sin importar si la petición falló o no*/
    complete : function(xhr, status) {
        console.log('Proceso terminado');
    }
    });
});
/**
 * @melchormendoza
 * {menu compras}
 * Funcion que trae la vista de inventarios a la pagina principal.
 * @params: 
 * @return:Vista HTML
 */
$( "#menuinventario" ).click(function() {
    $.ajax({
    /*la URL para la petición*/
    url : '',
    /*la información a enviar(también es posible utilizar una cadena de datos)*/
    data : { id : 123 },    
    /*especifica si será una petición POST o GET*/
    type : 'post',    
    /*el tipo de información que se espera de respuesta*/
    dataType : 'html',    
    /*código a ejecutar si la petición es satisfactoria;la respuesta es pasada como argumento a la función*/
    success : function(result) {
        console.log('success OK');
        $("#contenedor").html('<div class="alert alert-info" role="alert">Aquí el mÓdulo de INVENTARIOS</div>')
    },
    /*código a ejecutar si la petición falla; son pasados como argumentos a la función el objeto de la petición en crudo y código de estatus de la petición*/
    error : function(xhr, status) {
        console.log('Disculpe, existió un problema');
    },    
    /*código a ejecutar sin importar si la petición falló o no*/
    complete : function(xhr, status) {
        console.log('Proceso terminado');
    }
    });
});
/**
 * @melchormendoza
 * {menu compras}
 * Funcion que trae la vista de inventarios a la pagina principal.
 * @params: 
 * @return:Vista HTML
 */
$( "#menuadministracion" ).click(function() {
    $.ajax({
    /*la URL para la petición*/
    url : '',
    /*la información a enviar(también es posible utilizar una cadena de datos)*/
    data : { id : 123 },    
    /*especifica si será una petición POST o GET*/
    type : 'post',    
    /*el tipo de información que se espera de respuesta*/
    dataType : 'html',    
    /*código a ejecutar si la petición es satisfactoria;la respuesta es pasada como argumento a la función*/
    success : function(result) {
        console.log('success OK');
        $("#contenedor").html('<div class="alert alert-info" role="alert">Aquí el módulo de ADMINISTRACIÓN</div>')
    },
    /*código a ejecutar si la petición falla; son pasados como argumentos a la función el objeto de la petición en crudo y código de estatus de la petición*/
    error : function(xhr, status) {
        console.log('Disculpe, existió un problema');
    },    
    /*código a ejecutar sin importar si la petición falló o no*/
    complete : function(xhr, status) {
        console.log('Proceso terminado');
    }
    });
});
/**
 * @melchormendoza
 * Trae el layout de usuarios
 */
$( "#menuusuarios" ).click(function() {
    alert("aqui estoy");
    $.ajax({
    /*la URL para la petición*/
    url : '/users',
    /*la información a enviar(también es posible utilizar una cadena de datos)*/
     
    /*especifica si será una petición POST o GET*/
    type : 'post',    
    /*el tipo de información que se espera de respuesta*/
    dataType : 'html',    
    /*código a ejecutar si la petición es satisfactoria;la respuesta es pasada como argumento a la función*/
    success : function(result) {
        console.log('success OK');
        $("#contenedor").html('<div class="alert alert-info" role="alert">Aquí el módulo de USUARIOS</div>')
    },
    /*código a ejecutar si la petición falla; son pasados como argumentos a la función el objeto de la petición en crudo y código de estatus de la petición*/
    error : function(xhr, status) {
        console.log('Disculpe, existió un problema');
    },    
    /*código a ejecutar sin importar si la petición falló o no*/
    complete : function(xhr, status) {
        console.log('Proceso terminado');
    }
    });
});
$( "#menudescuentos" ).click(function() {
    $.ajax({
    /*la URL para la petición*/
    url : '',
    /*la información a enviar(también es posible utilizar una cadena de datos)*/
    data : { id : 123 },    
    /*especifica si será una petición POST o GET*/
    type : 'post',    
    /*el tipo de información que se espera de respuesta*/
    dataType : 'html',    
    /*código a ejecutar si la petición es satisfactoria;la respuesta es pasada como argumento a la función*/
    success : function(result) {
        console.log('success OK');
        $("#contenedor").html('<div class="alert alert-info" role="alert">Aquí el módulo de DESCUENTOS</div>')
    },
    /*código a ejecutar si la petición falla; son pasados como argumentos a la función el objeto de la petición en crudo y código de estatus de la petición*/
    error : function(xhr, status) {
        console.log('Disculpe, existió un problema');
    },    
    /*código a ejecutar sin importar si la petición falló o no*/
    complete : function(xhr, status) {
        console.log('Proceso terminado');
    }
    });
});
/**
 * @melchormendoza
 * Trae el layout de facturacion.
 * params:
 * return: view
 */
$( "#menufacturacion" ).click(function() {
    $.ajax({
    /*la URL para la petición*/
    url : '',
    /*la información a enviar(también es posible utilizar una cadena de datos)*/
    data : { id : 123 },    
    /*especifica si será una petición POST o GET*/
    type : 'post',    
    /*el tipo de información que se espera de respuesta*/
    dataType : 'html',    
    /*código a ejecutar si la petición es satisfactoria;la respuesta es pasada como argumento a la función*/
    success : function(result) {
        console.log('success OK');
        $("#contenedor").html('<div class="alert alert-info" role="alert">Aquí el módulo de FACTURACIÓN</div>')
    },
    /*código a ejecutar si la petición falla; son pasados como argumentos a la función el objeto de la petición en crudo y código de estatus de la petición*/
    error : function(xhr, status) {
        console.log('Disculpe, existió un problema');
    },    
    /*código a ejecutar sin importar si la petición falló o no*/
    complete : function(xhr, status) {
        console.log('Proceso terminado');
    }
    });
});

    });
    
            /*
            * @melchor
            * Función que maneja los mensajes de error
            * @params msg:texto del mensaje,st:estado del mensaje
            * @return mensaje jGrowl
             */
            function message(msg,st) 
            {
                if(st=='0'){
                    var tema = 'jGrowl-error';
                    var stk = true;
                }else if(st=='1'){
                    var tema = 'jGrowl-succes';
                    var stk = false;
                }
            $.jGrowl(msg, {
                    header: 'Mensaje:',
                    theme: tema,
                    sticky:stk,
                    life:'2000'
                });
            
            }
            function reload(){
                location.reload();
            }