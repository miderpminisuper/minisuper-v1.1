package Compras

import (
	"encoding/json"
	"fmt"
	"html/template"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/gorilla/mux"

	"../../Controladores/Session"
	"../../Modelos/Compras"
	"../../Modelos/General"
	"gopkg.in/mgo.v2/bson"
)

var tf = template.FuncMap{"add": add}

var tmpl = template.Must(template.New("").Funcs(tf).ParseFiles(
	"Vistas/index.html",
	"Vistas/Parciales/Plantilla/header.html",
	"Vistas/Parciales/Plantilla/footer.html",
	"Vistas/Parciales/Plantilla/styles.html",
	"Vistas/Parciales/Plantilla/scripts.html",
	"Vistas/Parciales/Paginas/Compras/scripts.html",
	"Vistas/Parciales/Paginas/Compras/styles.html",
	"Vistas/Parciales/Paginas/Compras/content.html",
))

var tmpl2 = template.Must(template.New("").Funcs(tf).ParseFiles(
	"Vistas/index.html",
	"Vistas/Parciales/Plantilla/header.html",
	"Vistas/Parciales/Plantilla/footer.html",
	"Vistas/Parciales/Plantilla/styles.html",
	"Vistas/Parciales/Plantilla/scripts.html",
	"Vistas/Parciales/Paginas/Compras/scripts.html",
	"Vistas/Parciales/Paginas/Compras/styles.html",
	"Vistas/Parciales/Paginas/Compras/edit.html",
))

var tmpl2_frame = template.Must(template.New("").Funcs(tf).ParseFiles(
	"Vistas/index.html",
	"Vistas/Parciales/Plantilla/header_blank.html",
	"Vistas/Parciales/Plantilla/footer_blank.html",
	"Vistas/Parciales/Plantilla/styles.html",
	"Vistas/Parciales/Plantilla/scripts.html",
	"Vistas/Parciales/Paginas/Compras/scripts.html",
	"Vistas/Parciales/Paginas/Compras/styles.html",
	"Vistas/Parciales/Paginas/Compras/edit.html",
))

var tmpl3 = template.Must(template.New("").Funcs(tf).ParseFiles(
	"Vistas/index.html",
	"Vistas/Parciales/Plantilla/header.html",
	"Vistas/Parciales/Plantilla/footer.html",
	"Vistas/Parciales/Plantilla/styles.html",
	"Vistas/Parciales/Plantilla/scripts.html",
	"Vistas/Parciales/Paginas/Compras/scripts.html",
	"Vistas/Parciales/Paginas/Compras/styles.html",
	"Vistas/Parciales/Paginas/Compras/index.html",
))

var tmpl3_frame = template.Must(template.New("").Funcs(tf).ParseFiles(
	"Vistas/index.html",
	"Vistas/Parciales/Plantilla/header_blank.html",
	"Vistas/Parciales/Plantilla/footer_blank.html",
	"Vistas/Parciales/Plantilla/styles.html",
	"Vistas/Parciales/Plantilla/scripts.html",
	"Vistas/Parciales/Paginas/Compras/scripts.html",
	"Vistas/Parciales/Paginas/Compras/styles.html",
	"Vistas/Parciales/Paginas/Compras/index.html",
))

var tmpl_frame = template.Must(template.New("").Funcs(tf).ParseFiles(
	"Vistas/index.html",
	"Vistas/Parciales/Plantilla/header_blank.html",
	"Vistas/Parciales/Plantilla/footer_blank.html",
	"Vistas/Parciales/Plantilla/styles.html",
	"Vistas/Parciales/Plantilla/scripts.html",
	"Vistas/Parciales/Paginas/Compras/scripts.html",
	"Vistas/Parciales/Paginas/Compras/styles.html",
	"Vistas/Parciales/Paginas/Compras/content.html",
))

//ArticuloElastic estructura de datos de artículo para insertar en elastic
type ArticuloElastic struct {
	CodBarra    string            `json:"codigobarra"`
	Descripcion string            `json:"descripcion"`
	Imagen      bson.ObjectId     `json:"imagen,omitempty"`
	Fotos       []bson.ObjectId   `json:"fotos,omitempty"`
	Tipo        string            `json:"tipo"`
	Unidad      string            `json:"unidad"`
	Entero      bool              `json:"usa_fraccion"`
	NumDec      int               `json:"num_dec"`
	Etiquetas   map[string]string `json:"etiquetas"`
	PreCompra   float64           `json:"costo"`
	PreVenta    float64           `json:"precio"`
}

//HoraMovimiento
var HoraMovimiento = time.Now()

//CargaPantalla es una funcion que redirige
//al usuario a la pantalla de alta sin pasar por validaciones
func CargaPantalla(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		userName := Session.GetUserName(r)
		if userName != "" {
			//Carga Combo de almacenes
			alma := ModeloCompras.ConsultaAlmacenesMgo()
			templ := `<option value="">--SELECCIONE--</option>`
			for _, v := range alma {
				if v.Nombre == "ALMACEN_DEFAULT" {
					templ += `<option value="` + v.ID.Hex() + `" selected>` + v.Nombre + `</option>`
				} else {
					templ += `<option value="` + v.ID.Hex() + `">` + v.Nombre + `</option>`
				}
			}
			almacenes := template.HTML(templ)
			//Carga Combo de provedores
			prove := ModeloCompras.ConsultaProveedorMgo()

			for len(prove) == 0 {
				prove = ModeloCompras.ConsultaProveedorMgo()
			}

			pro := `<option value="">--SELECCIONA--</option>`
			//<option value="1">PROVEEDOR 1</option><option value="2">PROVEEDOR 2</option>`
			for _, v := range prove {
				if v.Nombre == "PROVEEDOR_DEFAULT" {
					pro += `<option value=` + v.ID.Hex() + ` selected>` + v.Nombre + `</option>`
				} else {
					pro += `<option value=` + v.ID.Hex() + `>` + v.Nombre + `</option>`
				}
			}
			proveedores := template.HTML(pro)

			ImPuestoEdit := ModeloCompras.GeneraSelectImpuestosModalEdit()
			impuestosE := template.HTML(ImPuestoEdit)
			ImPuestoAlta := ModeloCompras.GeneraSelectImpuestosModalAlta()
			impuestosA := template.HTML(ImPuestoAlta)

			uni := ModeloCompras.GeneraSelectUnidades()

			unidad := template.HTML(uni)

			HoraMovimiento = time.Now()

			tmpl.ExecuteTemplate(w, "index_layout", map[string]interface{}{
				"almacenes":   almacenes,
				"proveedores": proveedores,
				"unidades":    unidad,
				"impuestosE":  impuestosE,
				"impuestosA":  impuestosA,
			})
		} else {
			http.Redirect(w, r, "/", 302)
		}
	} else if r.Method == "POST" {
		fmt.Println("POST")
	}
}

//CargaPantalla es una funcion que redirige
//al usuario a la pantalla de alta sin pasar por validaciones
func CargaPantalla_frame(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		userName := Session.GetUserName(r)
		if userName != "" {
			//Carga Combo de almacenes
			alma := ModeloCompras.ConsultaAlmacenesMgo()
			templ := `<option value="">--SELECCIONE--</option>`
			for _, v := range alma {
				if v.Nombre == "ALMACEN_DEFAULT" {
					templ += `<option value="` + v.ID.Hex() + `" selected>` + v.Nombre + `</option>`
				} else {
					templ += `<option value="` + v.ID.Hex() + `">` + v.Nombre + `</option>`
				}
			}

			almacenes := template.HTML(templ)

			//Carga Combo de provedores
			prove := ModeloCompras.ConsultaProveedorMgo()

			for len(prove) == 0 {
				prove = ModeloCompras.ConsultaProveedorMgo()
			}

			pro := `<option value="">--SELECCIONA--</option>`
			//<option value="1">PROVEEDOR 1</option><option value="2">PROVEEDOR 2</option>`
			for _, v := range prove {
				if v.Nombre == "PROVEEDOR_DEFAULT" {
					pro += `<option value=` + v.ID.Hex() + ` selected>` + v.Nombre + `</option>`
				} else {
					pro += `<option value=` + v.ID.Hex() + `>` + v.Nombre + `</option>`
				}
			}
			proveedores := template.HTML(pro)

			ImPuestoEdit := ModeloCompras.GeneraSelectImpuestosModalEdit()
			impuestosE := template.HTML(ImPuestoEdit)
			ImPuestoAlta := ModeloCompras.GeneraSelectImpuestosModalAlta()
			impuestosA := template.HTML(ImPuestoAlta)

			uni := ModeloCompras.GeneraSelectUnidades()
			unidad := template.HTML(uni)

			HoraMovimiento = time.Now()

			tmpl_frame.ExecuteTemplate(w, "index_layout", map[string]interface{}{
				"almacenes":   almacenes,
				"proveedores": proveedores,
				"unidades":    unidad,
				"impuestosE":  impuestosE,
				"impuestosA":  impuestosA,
			})
		} else {
			http.Redirect(w, r, "/", 302)
		}
	} else if r.Method == "POST" {
		fmt.Println("POST")
	}
}

//EditaCompra es una funcion que redirige
//al usuario a la pantalla de alta sin pasar por validaciones
func EditaCompra(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		userName := Session.GetUserName(r)
		if userName != "" {
			var id string
			vars := mux.Vars(r)
			id = vars["id"]

			if ModeloCompras.ConsultaCompra(id) > 0 {
				compra := ModeloCompras.GetCompraById(id)

				//Carga Combo de almacenes
				alma := ModeloCompras.ConsultaAlmacenesMgo()

				templ := `<option value="" selected>--SELECCIONE--</option>`

				for _, v := range alma {
					if v.ID == compra.AlmacenDestino {
						templ += `<option value="` + v.ID.Hex() + `" selected>` + v.Nombre + `</option>`
					} else {
						templ += `<option value="` + v.ID.Hex() + `">` + v.Nombre + `</option>`
					}
				}

				almacenes := template.HTML(templ)

				//Carga Combo de provedores
				prove := ModeloCompras.ConsultaProveedorMgo()

				for len(prove) == 0 {
					prove = ModeloCompras.ConsultaProveedorMgo()
				}

				pro := `<option value="" selected>--SELECCIONA--</option>`
				//<option value="1">PROVEEDOR 1</option><option value="2">PROVEEDOR 2</option>`
				for _, v := range prove {
					if v.ID == compra.AlmacenOrigen {
						pro += `<option value=` + v.ID.Hex() + ` selected>` + v.Nombre + `</option>`
					} else {
						pro += `<option value=` + v.ID.Hex() + `>` + v.Nombre + `</option>`
					}
				}

				proveedores := template.HTML(pro)

				var elem string
				for _, v := range compra.Productos {
					articulo := ModeloCompras.GetArticle(v.ID)
					articulo = ModeloCompras.RegresaSrcImagen(articulo)
					impst := ""

					for key, val := range v.Impuestos {
						impst += key + ":" + val + ","
					}

					impst = strings.Trim(impst, ",")

					var cantidad string

					if articulo.Entero == true {
						cantidad = strconv.FormatFloat(v.Cantidad, 'f', 3, 64)
					} else {
						cantidad = strconv.FormatFloat(v.Cantidad, 'f', 0, 64)
					}

					elem += `<tr>
									<td><img widtn="50px" height="50px" src="` + articulo.TagImg + `"></td>
									<td><input type="text" class="form-control" name="c_descripcion" value="` + articulo.Descripcion + `" readonly><input type="hidden" name="c_idex" value="` + v.ID.Hex() + `"></td>
									<td><input style="width:120px;" type="text" class="form-control" name="c_costo" value="` + strconv.FormatFloat(v.Costo, 'f', 2, 32) + `" readonly></td>
									<td><input style="width:120px;" type="text" class="form-control" name="c_cantidad" value="` + cantidad + `" readonly></td>
									<td><input style="width:120px;" type="text" class="form-control" name="c_unidad" value="` + articulo.Unidad + `" readonly></td>
									<td><input style="width:120px;" type="text" class="form-control" name="c_impuesto" value="` + strconv.FormatFloat(v.TotalImpuesto, 'f', 2, 32) + `" readonly><input type="hidden" name="c_nom_impuesto" value="` + impst + `"></td>
									<td><input style="width:120px;" type="text" class="form-control" name="c_ganancia" value="` + v.Ganancia + `" readonly></td>
									<td><input style="width:120px;" type="text" class="form-control" name="c_precio" value="` + strconv.FormatFloat(articulo.PreVenta, 'f', 2, 32) + `" readonly></td>
									<td><input style="width:120px;" type="text" class="form-control" name="c_importe" value="` + strconv.FormatFloat(v.Importe, 'f', 2, 32) + `" readonly></td>
								</tr>`
					//	<td><button type="button" class="btn btn-danger deleteButton">
					// <span class="glyphicon glyphicon-trash btn-xs"> </span></button></td>
				}

				ide := template.HTML(id)
				filas := template.HTML(elem)
				tmpl2.ExecuteTemplate(w, "index_layout", map[string]interface{}{
					"almacenes":   almacenes,
					"proveedores": proveedores,
					"move":        ide,
					"productos":   filas,
				})
			}
			http.Redirect(w, r, "/", 302)
		} else {
			http.Redirect(w, r, "/", 302)
		}
	} else if r.Method == "POST" {
		fmt.Println("POST")
	}
}

//EditaCompra es una funcion que redirige
//al usuario a la pantalla de alta sin pasar por validaciones
func EditaCompra_frame(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		userName := Session.GetUserName(r)
		if userName != "" {
			var id string
			vars := mux.Vars(r)
			id = vars["id"]

			compra := ModeloCompras.GetCompraById(id)

			//Carga Combo de almacenes
			alma := ModeloCompras.ConsultaAlmacenesMgo()

			templ := `<option value="" selected>--SELECCIONE--</option>`

			for _, v := range alma {
				if v.ID == compra.AlmacenDestino {
					templ += `<option value="` + v.ID.Hex() + `" selected>` + v.Nombre + `</option>`
				} else {
					templ += `<option value="` + v.ID.Hex() + `">` + v.Nombre + `</option>`
				}
			}

			almacenes := template.HTML(templ)

			//Carga Combo de provedores
			prove := ModeloCompras.ConsultaProveedorMgo()

			for len(prove) == 0 {
				prove = ModeloCompras.ConsultaProveedorMgo()
			}

			pro := `<option value="" selected>--SELECCIONA--</option>`
			//<option value="1">PROVEEDOR 1</option><option value="2">PROVEEDOR 2</option>`
			for _, v := range prove {
				if v.ID == compra.AlmacenOrigen {
					pro += `<option value=` + v.ID.Hex() + ` selected>` + v.Nombre + `</option>`
				} else {
					pro += `<option value=` + v.ID.Hex() + `>` + v.Nombre + `</option>`
				}
			}

			proveedores := template.HTML(pro)

			var elem string
			for _, v := range compra.Productos {
				articulo := ModeloCompras.GetArticle(v.ID)
				articulo = ModeloCompras.RegresaSrcImagen(articulo)
				impst := ""

				for key, val := range v.Impuestos {
					impst += key + ":" + val + ","
				}

				impst = strings.Trim(impst, ",")

				var cantidad string
				if articulo.Entero == true {
					cantidad = strconv.FormatFloat(v.Cantidad, 'f', 3, 64)
				} else {
					cantidad = strconv.FormatFloat(v.Cantidad, 'f', 0, 64)
				}

				elem += `<tr>
							<td><img widtn="50px" height="50px" src="` + articulo.TagImg + `"></td>
							<td><input type="text" class="form-control" name="c_descripcion" value="` + articulo.Descripcion + `" readonly><input type="hidden" name="c_idex" value="` + v.ID.Hex() + `"></td>
							<td><input style="width:120px;" type="text" class="form-control" name="c_costo" value="` + strconv.FormatFloat(v.Costo, 'f', 2, 32) + `" readonly></td>
							<td><input style="width:120px;" type="text" class="form-control" name="c_cantidad" value="` + cantidad + `" readonly></td>
							<td><input style="width:120px;" type="text" class="form-control" name="c_unidad" value="` + articulo.Unidad + `" readonly></td>
							<td><input style="width:120px;" type="text" class="form-control" name="c_impuesto" value="` + strconv.FormatFloat(v.TotalImpuesto, 'f', 2, 32) + `" readonly><input type="hidden" name="c_nom_impuesto" value="` + impst + `"></td>
							<td><input style="width:120px;" type="text" class="form-control" name="c_ganancia" value="` + v.Ganancia + `" readonly></td>
							<td><input style="width:120px;" type="text" class="form-control" name="c_precio" value="` + strconv.FormatFloat(articulo.PreVenta, 'f', 2, 32) + `" readonly></td>
                            <td><input style="width:120px;" type="text" class="form-control" name="c_importe" value="` + strconv.FormatFloat(v.Importe, 'f', 2, 32) + `" readonly></td>
                         </tr>`
				// <td><button type="button" class="btn btn-danger deleteButton">
				// <span class="glyphicon glyphicon-trash btn-xs"> </span></button></td>
			}

			ide := template.HTML(id)
			filas := template.HTML(elem)

			tmpl2_frame.ExecuteTemplate(w, "index_layout", map[string]interface{}{
				"almacenes":   almacenes,
				"proveedores": proveedores,
				"move":        ide,
				"productos":   filas,
			})
		} else {
			http.Redirect(w, r, "/", 302)
		}
	} else if r.Method == "POST" {
		fmt.Println("POST")
	}
}

//CargaPantallaIndex es una funcion que redirige
//al usuario a la pantalla de alta sin pasar por validaciones
func CargaPantallaIndex(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		userName := Session.GetUserName(r)
		if userName != "" {
			tmpl3.ExecuteTemplate(w, "index_layout", nil)
		} else {
			http.Redirect(w, r, "/", 302)
		}
	} else if r.Method == "POST" {

	}
}

//CargaPantallaIndex es una funcion que redirige
//al usuario a la pantalla de alta sin pasar por validaciones
func CargaPantallaIndex_frame(w http.ResponseWriter, r *http.Request) {
	userName := Session.GetUserName(r)
	if userName != "" {
		tmpl3_frame.ExecuteTemplate(w, "index_layout", nil)
	} else {
		http.Redirect(w, r, "/", 302)
	}
}

//Variables globales para búsqueda y paginación
var Ids []string
var numero_registros int64
var tammuestra int = 5
var ImPuesto = ModeloCompras.GeneraSelectImpuestos()

//BuscaArticulos funcion que busca artoculos en elastic
func BuscaArticulos(w http.ResponseWriter, r *http.Request) {
	Ids = nil
	if r.Method == "POST" {
		r.ParseForm()
		dato := r.FormValue("dato")
		fmt.Println(dato)

		if dato != "" {
			conexionElastic := ModeloCompras.ConectarElastic()
			docs := ModeloCompras.BuscarEnElastic(dato, conexionElastic)

			if docs.Hits.TotalHits > 0 {
				numero_registros = docs.Hits.TotalHits
				fmt.Printf("\n\t %d registros encontradas en %d  milisegundos\n", docs.Hits.TotalHits, docs.TookInMillis)
				for _, item := range docs.Hits.Hits {
					iid := item.Id
					Ids = append(Ids, iid)
				}
				var arrayids []bson.ObjectId

				reg := len(Ids)
				if reg < tammuestra {
					for _, valor := range Ids {
						hexa := bson.ObjectIdHex(valor)
						arrayids = append(arrayids, hexa)
					}
				} else {
					for _, valor := range Ids[0:tammuestra] {
						hexa := bson.ObjectIdHex(valor)
						arrayids = append(arrayids, hexa)
					}
				}

				result := ModeloCompras.GetArticles(arrayids)
				result = ModeloCompras.RegresaSrcImagenes(result)

				ModeloCompras.FlushElastic(conexionElastic)

				jData, _ := json.Marshal(result)

				w.Header().Set("Content-Type", "application/json")

				w.Write(jData)

			} else {
				fmt.Println("No se encontró algún registro")
			}

		} else {
			fmt.Println("Sin datos que consultar")
		}
	} else if r.Method == "GET" {
	}
}

//add funcion para template
func add(x, y int) int {
	return x + y
}

//GetSearchPaginationGeneral funcion que regresa artículos de correspondiente paginación
func GetSearchPaginationGeneral(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {

		pageS := r.FormValue("num_page")
		page, _ := strconv.Atoi(pageS)

		lim := tammuestra
		skip := (page * lim) - lim
		limite := skip + lim
		fmt.Println("Solicita la página: ", page, " del número: ", skip, " al: ", limite)
		result, _ := GetValuesDataMongodbPagination(limite, skip, page)
		jData, err := json.Marshal(result)
		if err != nil {
			panic(err)
			return
		}
		w.Header().Set("Content-Type", "application/json")
		w.Write(jData)
	}
}

//GetValuesDataMongodbPagination funcion obtiene de un conjunto de Ids
func GetValuesDataMongodbPagination(limit int, skip int, page int) ([]ModeloCompras.Articulo, int64) {
	var arrayids []bson.ObjectId
	//limit es el rango final de la segunda página
	//skip desde donde va a comenzar
	//Si es una página final entonces debe ser el ´límite del tamaño del arreglo de Ids

	numpags := float32(numero_registros) / float32(tammuestra)
	numpags2 := int(numpags)
	if numpags > float32(numpags2) {
		numpags2++
	}

	if page == numpags2 {
		final := len(Ids) % tammuestra
		fmt.Println(final)
		if final == 0 {
			for _, valor := range Ids[skip:limit] {
				hexa := bson.ObjectIdHex(valor)
				arrayids = append(arrayids, hexa)
			}
			fmt.Println("Se consultó última página: ", page, "del: ", skip, " al ", limit)
		} else {
			for _, valor := range Ids[skip : skip+final] {
				hexa := bson.ObjectIdHex(valor)
				arrayids = append(arrayids, hexa)
			}
			fmt.Println("Se consultó última página: ", page, "del: ", skip, " al ", skip+final)
		}

	} else {
		for _, valor := range Ids[skip:limit] {
			hexa := bson.ObjectIdHex(valor)
			arrayids = append(arrayids, hexa)
		}

	}

	result := ModeloCompras.GetArticles(arrayids)
	result = ModeloCompras.RegresaSrcImagenes(result)

	return result, numero_registros
}

//GetAllResultSearchElastic function for know pagination number
func GetAllResultSearchElastic(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {

		page, _ := strconv.Atoi(r.FormValue("getPage"))

		fmt.Println("valores de IDS->", len(Ids))

		var tmpl_pag = ` <ul class="pagination pagination-lg" > `

		var numTotal int
		numTotal = len(Ids)

		NumPagina := float32(numTotal) / float32(tammuestra)
		NumPagina2 := int(NumPagina)
		if NumPagina > float32(NumPagina2) {
			NumPagina2++
		}

		if page == 1 {
			tmpl_pag = tmpl_pag + ` <li><a><i class="fa fa-angle-left"></i></a></li>`
		} else if page > 1 {
			tmpl_pag = tmpl_pag + ` <li>
                                <a onClick="getPagination(` + strconv.Itoa(page-1) + `);">
                                    <i class="fa fa-angle-left"></i>
                                </a>
                            </li>`
		}

		var i int = page - 2
		var y int = i + 4
		if NumPagina2 > 5 {
			if i >= 1 {
				if y <= NumPagina2-2 {
					for i <= y {
						if i == page {
							tmpl_pag = tmpl_pag + `<li class="page-active"><a onClick="getPagination(` + strconv.Itoa(i) + `);"> ` + strconv.Itoa(i) + ` </a></li>`
						} else {
							tmpl_pag = tmpl_pag + `<li><a onClick="getPagination(` + strconv.Itoa(i) + `);"> ` + strconv.Itoa(i) + ` </a></li>`
						}
						i++

					}
					tmpl_pag = tmpl_pag + `<li><a> ... </a></li>`
					tmpl_pag = tmpl_pag + `<li><a onClick="getPagination(` + strconv.Itoa(NumPagina2) + `);"> ` + strconv.Itoa(NumPagina2) + ` </a></li>`
				} else {
					var z int = NumPagina2 - 4

					tmpl_pag = tmpl_pag + `<li><a onClick="getPagination(` + strconv.Itoa(1) + `);"> ` + strconv.Itoa(1) + ` </a></li>`
					tmpl_pag = tmpl_pag + `<li><a> ... </a></li>`
					for z <= NumPagina2 {
						if z == page {
							tmpl_pag = tmpl_pag + `<li class="page-active"><a onClick="getPagination(` + strconv.Itoa(z) + `);"> ` + strconv.Itoa(z) + ` </a></li>`
						} else {
							tmpl_pag = tmpl_pag + `<li><a onClick="getPagination(` + strconv.Itoa(z) + `);"> ` + strconv.Itoa(z) + ` </a></li>`
						}
						z++
					}
				}
			} else if i <= 0 {
				var x int = 1
				for x <= 5 {
					if x == page {
						tmpl_pag = tmpl_pag + `<li class="page-active"><a onClick="getPagination(` + strconv.Itoa(x) + `);"> ` + strconv.Itoa(x) + ` </a></li>`
					} else {
						tmpl_pag = tmpl_pag + `<li><a onClick="getPagination(` + strconv.Itoa(x) + `);"> ` + strconv.Itoa(x) + ` </a></li>`
					}
					x++
				}
				tmpl_pag = tmpl_pag + `<li><a> ... </a></li>`
				tmpl_pag = tmpl_pag + `<li><a onClick="getPagination(` + strconv.Itoa(NumPagina2) + `);"> ` + strconv.Itoa(NumPagina2) + ` </a></li>`
			}
		} else {
			var x int = 1
			for x <= NumPagina2 {
				if i == page {
					tmpl_pag = tmpl_pag + `<li class="page-active"><a onClick="getPagination(` + strconv.Itoa(x) + `);"> ` + strconv.Itoa(x) + ` </a></li>`
				} else {
					tmpl_pag = tmpl_pag + `<li><a onClick="getPagination(` + strconv.Itoa(x) + `);"> ` + strconv.Itoa(x) + ` </a></li>`
				}
				x++
			}
			tmpl_pag = tmpl_pag + `<li><a> ... </a></li>`
			tmpl_pag = tmpl_pag + `<li><a onClick="getPagination(` + strconv.Itoa(NumPagina2) + `);"> ` + strconv.Itoa(NumPagina2) + ` </a></li>`
		}

		if page == NumPagina2 {
			tmpl_pag = tmpl_pag + `<li><a><i class="fa fa-angle-right"></i></a></li>`
		} else if page < NumPagina2 {
			tmpl_pag = tmpl_pag + `<li>
                                <a onClick="getPagination(` + strconv.Itoa(page+1) + `);">
                                    <i class="fa fa-angle-right"></i>
                                </a>
                            </li>`
		}

		tmpl_pag = tmpl_pag + ` </ul> `

		fmt.Fprintf(w, tmpl_pag)

	}

}

//InsertaCompra es una funcion que inserta a mongo, postgres y elastic los datos de la nueva compra
func InsertaCompra(w http.ResponseWriter, r *http.Request) {

	if r.Method == "GET" {

		http.Redirect(w, r, "/", 302)

	} else if r.Method == "POST" {

		r.ParseMultipartForm(0)

		idproducto := r.Form["c_idex"]
		costo := r.Form["c_costo"]
		costoT := r.Form["c_costo_total"]
		impuesto := r.Form["c_impuesto"]
		nomimpuesto := r.Form["c_nom_impuesto"]
		importe := r.Form["c_importe"]
		ganancia := r.Form["c_ganancia"]
		cantidad := r.Form["c_cantidad"]
		precio := r.Form["c_precio"]
		almacen := r.FormValue("almacenes")
		provedor := r.FormValue("proveedor")
		tipomovimiento := "compra"

		usuario := Session.GetUserName(r)
		fmt.Println("Usuario: ", usuario)
		IDUsuario := ModeloCompras.ConsultaIDUsuario(usuario)
		fmt.Println("IDUsuario: ", IDUsuario.ID)
		var Move ModeloCompras.Movimiento
		var Producto ModeloCompras.Producto
		var Productos []ModeloCompras.Producto

		//Actualización de artículos de compra
		t := len(idproducto)
		fmt.Println("Productos a ingresar a compras: ", t)
		imps2 := make(map[string]string)

		for i := 0; i < t; i++ {

			imps2 = make(map[string]string)
			for _, val := range strings.Split(nomimpuesto[i], ",") {
				isko := strings.Split(val, ":")
				if isko[1] != "" {
					imps2[isko[0]] = isko[1]
				}
			}

			ModeloCompras.ActualizaProductoDeComprasEnMongo(idproducto[i], costo[i], precio[i], costoT[i], imps2)

			ModeloCompras.ActualizaProductoDeComprasEnPostgreSQL2(almacen, cantidad[i], idproducto[i])

			Product := ModeloCompras.GetArticle(bson.ObjectIdHex(idproducto[i]))
			ModeloCompras.DeleteElasticValue(models.INDICE_ELASTICSEARCH, models.COLECCION_ELASTICSEARCH, idproducto[i])

			articulo := ArticuloElastic{CodBarra: Product.CodBarra, Descripcion: Product.Descripcion, Imagen: Product.Imagen, Fotos: Product.Fotos, Tipo: Product.Tipo, Unidad: Product.Unidad, Entero: Product.Entero, NumDec: Product.NumDec, Etiquetas: Product.Etiquetas, PreCompra: Product.PreCompra, PreVenta: Product.PreVenta}
			ModeloCompras.InsertElasticValue(models.INDICE_ELASTICSEARCH, models.COLECCION_ELASTICSEARCH, idproducto[i], articulo)

			Producto = ModeloCompras.Producto{}
			Producto.ID = bson.ObjectIdHex(idproducto[i])

			cant, _ := strconv.ParseFloat(cantidad[i], 32)
			Producto.Cantidad = cant
			cost, _ := strconv.ParseFloat(costo[i], 32)
			Producto.Costo = cost
			importt, _ := strconv.ParseFloat(importe[i], 32)
			Producto.Importe = importt

			Producto.Ganancia = ganancia[i]
			impust, _ := strconv.ParseFloat(impuesto[i], 32)
			Producto.TotalImpuesto = impust

			Productos = append(Productos, Producto)

		}

		imps := make(map[string]string)
		for l, v := range nomimpuesto {
			imps = make(map[string]string)
			if v != "" {
				for _, val := range strings.Split(v, ",") {
					isko := strings.Split(val, ":")
					if isko[1] != "" {
						imps[isko[0]] = isko[1]
					}
				}
			}
			Productos[l].Impuestos = imps
		}

		Move.Tipo = tipomovimiento
		Move.Date = HoraMovimiento
		Move.AlmacenOrigen = bson.ObjectIdHex(provedor)
		Move.AlmacenDestino = bson.ObjectIdHex(almacen)
		Move.UsuarioOrigen = bson.ObjectIdHex(provedor)
		Move.UsuarioDestino = IDUsuario.ID
		Move.Productos = Productos

		//Alta de Movimiento en mongo
		fmt.Println("Por insertar movimiento en mongo")
		ModeloCompras.InsertaMovimiento(Move)

		// fmt.Println("Producto insertado con ID: ", Data.ID, " e imagen con ID: ", Product.ID)
		// http.Redirect(w, r, "/del/"+Product.CodBarra, 302)
		http.Redirect(w, r, "/compras_frame", 302)

	}
}

//EditaCompras edita la compra
func EditaCompras(w http.ResponseWriter, r *http.Request) {
	//	if r.Method == "GET" {

	//		http.Redirect(w, r, "/", 302)

	//	} else if r.Method == "POST" {

	//		r.ParseMultipartForm(0)

	//		idmovimiento := r.FormValue("id")
	//		//Se obtiene el movimiento para echar atrás los productos
	//		//se echarán atrás los productos de la edición uno por uno restando cantidades a los inventarios
	//		compra := ModeloCompras.GetCompraById(idmovimiento)

	//		for _, v := range compra.Productos {
	//			ModeloCompras.RollbackPostgres(strconv.FormatFloat(v.Cantidad, 'f', 3, 32), v.ID)
	//		}

	//		//Se reinsertan los productos y las características del movimiento
	//		// Se añade quien edita el movimiento
	//		idproducto := r.Form["c_idex"]
	//		costo := r.Form["c_costo"]
	//		impuesto := r.Form["c_impuesto"]
	//		nomimpuesto := r.Form["c_nom_impuesto"]
	//		importe := r.Form["c_importe"]
	//		ganancia := r.Form["c_ganancia"]
	//		cantidad := r.Form["c_cantidad"]
	//		precio := r.Form["c_precio"]
	//		almacen := r.FormValue("almacenes")
	//		provedor := r.FormValue("proveedor")

	//		tipomovimiento := "compra"
	//		usuario := Session.GetUserName(r)
	//		fmt.Println("Usuario: ", usuario)
	//		IDUsuario := ModeloCompras.ConsultaIDUsuario(usuario)
	//		fmt.Println("IDUsuario: ", IDUsuario.ID)

	//		var Move ModeloCompras.Movimiento
	//		var Producto ModeloCompras.Producto
	//		var Productos []ModeloCompras.Producto

	//		//Actualización de artículos de compra
	//		t := len(idproducto)
	//		for i := 0; i < t; i++ {
	//			fmt.Println("Por actualizar producto en mongo con ID: ", idproducto[i], " con un costo de: ", costo[i], " y un precio de: ", precio[i])
	//			ModeloCompras.ActualizaProductoDeComprasEnMongo(idproducto[i], costo[i], precio[i])
	//			fmt.Println("Por actualizar producto en psql con ID: ", idproducto[i], " con una cantidad adicional de: ", cantidad[i])
	//			ModeloCompras.ActualizaProductoDeComprasEnPostgreSQL(cantidad[i], idproducto[i])

	//			Product := ModeloCompras.GetArticle(bson.ObjectIdHex(idproducto[i]))

	//			ModeloCompras.DeleteElasticValue(ModeloCompras.INDICE_ELASTICSEARCH, ModeloCompras.COLECCION_ELASTICSEARCH, idproducto[i])
	//			fmt.Println("Producto ELIMINADO DE ELASTIC")
	//			articulo := ArticuloElastic{CodBarra: Product.CodBarra, Descripcion: Product.Descripcion, Imagen: Product.Imagen, Fotos: Product.Fotos, Tipo: Product.Tipo, Unidad: Product.Unidad, Entero: Product.Entero, NumDec: Product.NumDec, Etiquetas: Product.Etiquetas, PreCompra: Product.PreCompra, PreVenta: Product.PreVenta}
	//			ModeloCompras.InsertElasticValue(ModeloCompras.INDICE_ELASTICSEARCH, ModeloCompras.COLECCION_ELASTICSEARCH, idproducto[i], articulo)
	//			fmt.Println("Producto insertado EN ELASTIC")

	//			Producto = ModeloCompras.Producto{}
	//			Producto.ID = bson.ObjectIdHex(idproducto[i])
	//			cant, _ := strconv.ParseFloat(cantidad[i], 32)
	//			Producto.Cantidad = cant
	//			cost, _ := strconv.ParseFloat(costo[i], 32)
	//			Producto.Costo = cost
	//			importt, _ := strconv.ParseFloat(importe[i], 32)
	//			Producto.Importe = importt
	//			Producto.Ganancia = ganancia[i]
	//			impust, _ := strconv.ParseFloat(impuesto[i], 32)
	//			Producto.TotalImpuesto = impust

	//			Productos = append(Productos, Producto)
	//		}

	//		imps := make(map[string]string)

	//		for l, v := range nomimpuesto {
	//			imps = make(map[string]string)
	//			if v != "" {
	//				for _, val := range strings.Split(v, ",") {
	//					isko := strings.Split(val, ":")
	//					imps[isko[0]] = isko[1]
	//				}
	//				fmt.Println(3)
	//			}
	//			fmt.Println(imps)
	//			Productos[l].Impuestos = imps
	//		}

	//		Move.ID = bson.ObjectIdHex(idmovimiento)
	//		Move.AlmacenOrigen = bson.ObjectIdHex(provedor)
	//		Move.AlmacenDestino = bson.ObjectIdHex(almacen)
	//		Move.Productos = Productos

	//		var Editor ModeloCompras.Editores
	//		Editor.ID = IDUsuario.ID
	//		Editor.Tipo = tipomovimiento
	//		Editor.Date = HoraMovimiento

	//		Move.Actualiza = append(Move.Actualiza, Editor)

	//		//Alta de Movimiento en mongo
	//		fmt.Println("Por actualizar movimiento en mongo con movimiento: ", Move)
	//		ModeloCompras.ActualizaMovimiento(Move)

	//		//fmt.Println("Producto insertado con ID: ", Data.ID, " e imagen con ID: ", Product.ID)
	//		//http.Redirect(w, r, "/del/"+Product.CodBarra, 302)
	//		//http.Redirect(w, r, "/indexcompras", 302)
	//		http.Redirect(w, r, "/indexcompras_frame", 302)

	//	}
}

//EliminaMovimiento elimina el movimiento echando atrás sus productos
func EliminaMovimiento(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {

		http.Redirect(w, r, "/", 302)

	} else if r.Method == "POST" {

		r.ParseMultipartForm(0)
		fmt.Println(r.PostForm)

		idmovimiento := r.FormValue("id")
		compra := ModeloCompras.GetCompraById(idmovimiento)

		for _, v := range compra.Productos {
			ModeloCompras.RollbackPostgres(strconv.FormatFloat(v.Cantidad, 'f', 3, 32), v.ID)
		}

		ModeloCompras.EliminaMovimiento(idmovimiento)
		http.Redirect(w, r, "/indexcompras", 302)
	}

}

//ActualizaCompra actualiza el artículo
func ActualizaCompra(w http.ResponseWriter, r *http.Request) {

	if r.Method == "GET" {
		http.Redirect(w, r, "/", 302)
	} else if r.Method == "POST" {
		r.ParseMultipartForm(0)
		fmt.Println(r.PostForm)

	}
}

//AgregaCompra regresa un elemento de una tabla a partir de un Id al cliente
func AgregaCompra(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		r.ParseForm()
		Ide := r.FormValue("id")
		//Almacen := r.FormValue("almacen")

		if bson.IsObjectIdHex(Ide) {

			ID := bson.ObjectIdHex(Ide)

			arti := ModeloCompras.GetArticle(ID)
			arti = ModeloCompras.RegresaSrcImagen(arti)

			// fmt.Println("Consultando el estatus del artículo: ", Ide, " en el almacen: ", Almacen)
			// arti = ModeloCompras.RegresaEstatusDeAlmacen(arti, Almacen, Ide)

			jData, _ := json.Marshal(arti)
			w.Header().Set("Content-Type", "application/json")
			w.Write(jData)
		}

	}
}

//AltaArticuloDeCompra regresa un elemento de una tabla a partir de un Id al cliente
func AltaArticuloDeCompra(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {

		if ImPuesto == "" {
			ImPuesto = ModeloCompras.GeneraSelectImpuestos()
		}

		uni := ModeloCompras.ConsultaUnidadesDistintas()
		Unidades := `<option value="" selected>--SELECCIONE--</option>`
		for _, v := range uni {
			Unidades += `<option value=` + v + `>` + v + `</option>`
		}

		elem := `<fieldset>
      				<div class="row text-center well">
          				<div class="input-group input-group-md">
           					<div class="input-group">
            					<label for="codigobarra" class="input-group-addon"><span>Código*:</span></label>
            						<input type="text" class="form-control" id="codigobarra" name="codigobarra" placeholder="Código">
            					<a onclick="GeneraCodigo()" class="btn btn-info btn-lg input-group-addon" style="background-color: #337ab7;color: white;border-bottom-color: #337AB6;">
              						<span class="glyphicon glyphicon-barcode"></span> Generar
            					</a>
          					</div>
        				</div>
          				<div id="error_codigo" class="help-block with-errors"></div>
      				</div>

    				<div class="row well">

      					<div class="col-md-6 col-sm-12 text-center">
            				<div class="col-md-6 form-group has-error has-danger">
								<label class="custom-control custom-checkbox mb-2 mr-sm-2 mb-sm-0" data-toggle="tooltip" data-placement="top" title="Es un producto Físico!">
								<input type="radio" id="radioFisico" value="fisico" name="tipos" class="custom-control-input" checked>
								Físico*
								</label>
								<label class="custom-control custom-checkbox mb-2 mr-sm-2 mb-sm-0" data-toggle="tooltip" data-placement="top" title="Es un producto Lógico!">
								<input type="radio" id="radioLogico" value="logico" name="tipos" class="custom-control-input">
								Lógico*
								</label>
            				</div>

      					</div>

						<div class="col-md-6 col-sm-12 text-center form-group">
							<div class="input-group input-group-md" data-toggle="tooltip" data-placement="top" title="Unidad de medida del producto">
							<label for="id_unidades" class="input-group-addon"><span>Unidad*:</span></label>
							<select id="id_unidades" name="unidades" class="form-control" required>` + Unidades + `
							</select>
							</div>
						</div>

    			</div>

			<div id="id_venta_fisico" class="row well text-center">

					<div class="col-lg-6 col-md-12">
							<label>
							<input type="checkbox" id="id_venta_fraccionaria" name="fraccion" value="fraccion">
							Para venta Fraccionaria
						</label>
					</div>

					<div class="col-lg-6 col-md-12">
						<div id="div_decimales" class="input-group input-group-md" data-toggle="tooltip" data-placement="top" title="Numero maximo de decimales">
							<label for="id_decimales" class="input-group-addon"><span>Decimales:</span></label>
								<select class="form-control" id="id_decimales" name="decimales">
									<option value="">--SELECCIONE--</option>
									<option value="0">0</option>
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
								</select>
						</div>
					</div>

			</div>

			<div class="row well">
				<div class="col-md-12">
					<div class="col-md-12 well">
						<label for="id_descripcion" class="col-md-12 text-left"><span>Descripción* :</span></label>
						<div class="col-md-12">
							<textarea class="form-control" rows="2" id="id_descripcion" name="descripcion" placeholder="Descripción del producto..." 
								style="margin: 0px; width: 460px; height: 62px;" data-toggle="tooltip" data-placement="top" title="Realiza una descripción de este producto"></textarea>
						</div>
					</div>
				</div>


			<div class="col-md-12">

						<div class="col-md-12 input-group input-group-md form-group well">
							<div class="input-group col-md-12">
								<label for="id_costo" class="input-group-addon"><span>Costo*    :</span></label>
								<label for="id_costo" class="input-group-addon"><span class="glyphicon glyphicon glyphicon-usd"></span></label>
								<input type="text" class="form-control" id="id_costo" placeholder="Monto" name="costo" data-toggle="tooltip" data-placement="top" title="Precio de compra" value="">
								<label id="cosotss" for="id_costo" class="input-group-addon"></label>
							</div>
						</div>

						<div class="col-md-12 input-group input-group-md form-group well">
							<div class="input-group col-md-12">
								<label for="id_cantidad" class="input-group-addon"><span>Cantidad*:</span></label>
								<label for="id_cantidad" class="input-group-addon"><span class="glyphicon glyphicon-hand-right"></span></label>
								<input type="text" class="form-control" id="id_cantidad" placeholder="Cantidad" name="cantidad" data-toggle="tooltip" data-placement="top" title="Numero de productos">
								<label id="cant" for="id_cantidad" class="input-group-addon"></label>
							</div>
						</div>

						<div class="col-md-12 input-group input-group-md form-group well">
							<div class="input-group col-md-12">
								<label for="id_ganancia" class="input-group-addon"><span>Ganancia*:</span></label>
								<label for="id_ganancia" class="input-group-addon"><span class="glyphicon glyphicon-hand-right"></span></label>
								<input type="text" class="form-control" id="id_ganancia" placeholder="Ganancia" name="ganancia" data-toggle="tooltip" data-placement="top" title="Ganancia del producto">
								<label id="ganan" for="id_ganancia" class="input-group-addon"></label>
							</div>
						</div>

						<div class="col-md-12 input-group input-group-md form-group well">
							<label for="id_impuesto" class="col-md-12 text-left"><span>Aplica Impuesto*:</span></label>
							<div class="col-md-12 text-center">
									` + ImPuesto + `
							</div>
						</div>

						<div class="col-md-12 input-group input-group-md well">
								<div class="input-group col-md-12">
									<label for="id_precio" class="input-group-addon"><span>Precio*  :</span></label>
									<label for="id_precio" class="input-group-addon"><span class="glyphicon glyphicon glyphicon-usd"></span></label>
									<input type="text" class="form-control" id="id_precio" placeholder="Monto" name="precio" data-toggle="tooltip" data-placement="top" title="Precio de venta" value="">
									<label id="precioss" for="id_precio" class="input-group-addon"></label>
								</div>      
						</div>

						<div class="col-md-12 input-group input-group-md well">
								<div class="input-group col-md-12">
									<label for="id_importe" class="input-group-addon"><span>Importe* :</span></label>
									<label for="id_importe" class="input-group-addon"><span class="glyphicon glyphicon glyphicon-usd"></span></label>
									<input type="text" class="form-control" id="id_importe" placeholder="Importe" name="importe" data-toggle="tooltip" data-placement="top" title="Importe de Compra">
									<label id="importe" for="id_importe" class="input-group-addon"></label>
								</div>      
						</div>
			</div>

		</div>

					<div class="col-md-12 well text-center">
						<div id="contenedor">
							<div class="input-group input-group-md well">
							<input type="text" class="form-control" id="idecampo" name="idecampo" placeholder="Campo">
							<input type="text" class="form-control" id="idevalor" name="idevalor" placeholder="Valor">
							<label id="AgregaCampo" value="Agregar" class="btn btn-primary input-group-addon" style="background-color: #337ab7;color: white;border-bottom-color: #337AB6;"><span class="glyphicon glyphicon-plus"></span>Agregar</label>
							</div>
						</div>
						<div id="error_campo_nuevo" class="help-block with-errors"></div>
					</div>

					<div class="col-md-12 well" id="div_tabla" style="display:none;">
						<table class="table">
							<thead class="thead-inverse">
							<tr>
								<th>Etiqueta</th>
								<th>Valor</th>
							</tr>
							</thead>

							<tbody id="tbody_tab_general">
							</tbody>
						</table>
					</div>


					<div id="divimagen" class="row">
						<div class="col-md-12 well">
							<input type="file" name="imagen" id="filer_input" multiple="multiple">			
							<div id="error_imagen" class="help-block with-errors"></div>
						</div>
					</div>

</fieldset>`

		// el := elem1 + "|" + elem

		fmt.Fprintf(w, elem)
	}

}

func AltaProductoDesdeCompras(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		r.ParseForm()

		var Product ModeloCompras.Articulo
		//get value form
		codigo := r.FormValue("codigobarra")
		descripcion := r.FormValue("descripcion")
		tipos := r.FormValue("tipos")
		unidades := r.FormValue("unidades")
		fraccion := r.FormValue("fraccion")
		idecampo := r.Form["idecampo[]"]
		idevalor := r.Form["idevalor[]"]
		costo := r.FormValue("costo")
		precio := r.FormValue("precio")

		etiquetas := make(map[string]string)

		t := len(idecampo)
		for i := 0; i < t-1; i++ {
			etiquetas[idecampo[i]] = idevalor[i]
		}

		Product.Etiquetas = etiquetas
		Product.ID = bson.NewObjectId()

		if codigo != "" {
			n := ModeloCompras.ConsultaCodigo(codigo)
			if n > 0 {
				Product.CodBarra = ModeloCompras.GeneraCodigo()
			} else {
				Product.CodBarra = codigo
			}
		} else {
			Product.CodBarra = ModeloCompras.GeneraCodigo()
		}

		Product.Descripcion = descripcion
		Product.Tipo = tipos
		Product.Unidad = unidades
		fmt.Println(fraccion)
		if fraccion == "fraccion" {
			Product.Entero = true
			Product.NumDec = 3
		} else {
			Product.Entero = false
			Product.NumDec = 0
		}

		Product.PreCompra, _ = strconv.ParseFloat(costo, 64)
		Product.PreVenta, _ = strconv.ParseFloat(precio, 64)

		ModeloCompras.InsertaProductoEnMongo(Product)
		fmt.Println("Producto insertado EN MONGO")

		articulo := ArticuloElastic{CodBarra: Product.CodBarra, Descripcion: Product.Descripcion, Imagen: Product.Imagen, Fotos: Product.Fotos, Tipo: Product.Tipo, Unidad: Product.Unidad, Entero: Product.Entero, NumDec: Product.NumDec, Etiquetas: Product.Etiquetas, PreCompra: Product.PreCompra, PreVenta: Product.PreVenta}
		ModeloCompras.InsertElasticValue(models.INDICE_ELASTICSEARCH, models.COLECCION_ELASTICSEARCH, Product.ID.Hex(), articulo)
		fmt.Println("Producto insertado EN ELASTIC")
		fmt.Println("Producto insertado con ID: ", Product.ID)

		result := ModeloCompras.GetArticle(Product.ID)
		result = ModeloCompras.RegresaSrcImagen(result)

		jData, _ := json.Marshal(result)
		w.Header().Set("Content-Type", "application/json")
		w.Write(jData)
	}
}

//Variables globales para búsqueda y paginación
var Ides_index []string
var numero_registros_index int
var tammuestra_index int = 8

//CargaCompras funcion que regresa los primeros artoculos de la base y recupoera todos en una variable global
func CargaCompras(w http.ResponseWriter, r *http.Request) {
	result := ModeloCompras.GetAllCompras()
	if len(result) > 0 {
		Ides_index = nil
		for _, item := range result {
			iid := item.ID.Hex()
			Ides_index = append(Ides_index, iid)
		}
		numero_registros_index = len(Ides_index)
		var arrayids []bson.ObjectId
		reg := len(Ides_index)
		if reg < tammuestra_index {
			for _, valor := range Ides_index {
				hexa := bson.ObjectIdHex(valor)
				arrayids = append(arrayids, hexa)
			}
		} else {
			for _, valor := range Ides_index[0:tammuestra_index] {
				hexa := bson.ObjectIdHex(valor)
				arrayids = append(arrayids, hexa)
			}
		}
		result = ModeloCompras.GetCompras(arrayids)
		for i := range result {
			result[i].AlmacenOrigen.Nombre = ModeloCompras.GetNameProvedor(result[i].AlmacenOrigen.ID)
			result[i].AlmacenDestino.Nombre = ModeloCompras.GetNameAlmacen(result[i].AlmacenDestino.ID)
			result[i].UsuarioOrigen.Nombre = ModeloCompras.GetNameProvedor(result[i].UsuarioOrigen.ID)
			result[i].UsuarioDestino.Nombre = ModeloCompras.GetNameUsuario(result[i].UsuarioDestino.ID)
		}
		jData, _ := json.Marshal(result)
		w.Header().Set("Content-Type", "application/json")
		w.Write(jData)
	} else {
		fmt.Println("No se encontraron Datos en la Base de Datos")
	}
}

//GetSearchPaginationGeneral funcion que regresa artículos de correspondiente paginación
func GetSearchPaginationGeneral_index(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {

		pageS := r.FormValue("num_page")
		page, _ := strconv.Atoi(pageS)

		lim := tammuestra_index
		skip := (page * lim) - lim
		limite := skip + lim
		fmt.Println("Solicita la página: ", page, " del número: ", skip, " al: ", limite)
		result, _ := GetValuesDataMongodbPagination_index(limite, skip, page)
		jData, _ := json.Marshal(result)
		w.Header().Set("Content-Type", "application/json")
		w.Write(jData)
	}
}

//GetValuesDataMongodbPagination_index funcion obtiene de un conjunto de Ids
func GetValuesDataMongodbPagination_index(limit int, skip int, page int) ([]ModeloCompras.MovimientoIndex, int) {
	var arrayids []bson.ObjectId
	//limit es el rango final de la segunda página
	//skip desde donde va a comenzar
	//Si es una página final entonces debe ser el ´límite del tamaño del arreglo de Ids

	numpags := float32(numero_registros_index) / float32(tammuestra_index)
	numpags2 := int(numpags)
	if numpags > float32(numpags2) {
		numpags2++
		fmt.Println("se aumentó uno el total")
	}

	if page == numpags2 {
		final := len(Ides_index) % tammuestra_index
		fmt.Println(final)
		if final == 0 {
			for _, valor := range Ides_index[skip:limit] {
				hexa := bson.ObjectIdHex(valor)
				arrayids = append(arrayids, hexa)
			}
			fmt.Println("Se consultó última página: ", page, "del: ", skip, " al ", limit)
		} else {
			for _, valor := range Ides_index[skip : skip+final] {
				hexa := bson.ObjectIdHex(valor)
				arrayids = append(arrayids, hexa)
			}
			fmt.Println("Se consultó última página: ", page, "del: ", skip, " al ", skip+final)
		}

	} else {
		for _, valor := range Ides_index[skip:limit] {
			hexa := bson.ObjectIdHex(valor)
			arrayids = append(arrayids, hexa)
		}

	}

	result := ModeloCompras.GetCompras(arrayids)

	for i := range result {
		result[i].AlmacenOrigen.Nombre = ModeloCompras.GetNameProvedor(result[i].AlmacenOrigen.ID)
		result[i].AlmacenDestino.Nombre = ModeloCompras.GetNameAlmacen(result[i].AlmacenDestino.ID)
		result[i].UsuarioOrigen.Nombre = ModeloCompras.GetNameProvedor(result[i].UsuarioOrigen.ID)
		result[i].UsuarioDestino.Nombre = ModeloCompras.GetNameUsuario(result[i].UsuarioDestino.ID)
	}

	return result, numero_registros_index
}

//GetAllResultSearchElastic_index function for know pagination number
func GetAllResultSearchElastic_index(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		page, _ := strconv.Atoi(r.FormValue("getPage"))
		var tmpl_pag = ` <ul class="pagination pagination-lg" > `
		var numTotal int
		numTotal = len(Ides_index)
		NumPagina := float32(numTotal) / float32(tammuestra_index)
		NumPagina2 := int(NumPagina)
		if NumPagina > float32(NumPagina2) {
			NumPagina2++
		}
		if page == 1 {
			tmpl_pag = tmpl_pag + ` <li><a><i class="fa fa-angle-left"></i></a></li>`
		} else if page > 1 {
			tmpl_pag = tmpl_pag + ` <li>
                                <a onClick="getPagination(` + strconv.Itoa(page-1) + `);">
                                    <i class="fa fa-angle-left"></i>
                                </a>
                            </li>`
		}
		var i int = page - 2
		var y int = i + 4
		if NumPagina2 > 5 {
			if i >= 1 {
				if y <= NumPagina2-2 {
					for i <= y {
						if i == page {
							tmpl_pag = tmpl_pag + `<li class="page-active"><a onClick="getPagination(` + strconv.Itoa(i) + `);"> ` + strconv.Itoa(i) + ` </a></li>`
						} else {
							tmpl_pag = tmpl_pag + `<li><a onClick="getPagination(` + strconv.Itoa(i) + `);"> ` + strconv.Itoa(i) + ` </a></li>`
						}
						i++
					}
					tmpl_pag = tmpl_pag + `<li><a> ... </a></li>`
					tmpl_pag = tmpl_pag + `<li><a onClick="getPagination(` + strconv.Itoa(NumPagina2) + `);"> ` + strconv.Itoa(NumPagina2) + ` </a></li>`
				} else {
					var z int = NumPagina2 - 4
					tmpl_pag = tmpl_pag + `<li><a onClick="getPagination(` + strconv.Itoa(1) + `);"> ` + strconv.Itoa(1) + ` </a></li>`
					tmpl_pag = tmpl_pag + `<li><a> ... </a></li>`
					for z <= NumPagina2 {
						if z == page {
							tmpl_pag = tmpl_pag + `<li class="page-active"><a onClick="getPagination(` + strconv.Itoa(z) + `);"> ` + strconv.Itoa(z) + ` </a></li>`
						} else {
							tmpl_pag = tmpl_pag + `<li><a onClick="getPagination(` + strconv.Itoa(z) + `);"> ` + strconv.Itoa(z) + ` </a></li>`
						}
						z++
					}
				}
			} else if i <= 0 {
				var x int = 1
				for x <= 5 {
					if x == page {
						tmpl_pag = tmpl_pag + `<li class="page-active"><a onClick="getPagination(` + strconv.Itoa(x) + `);"> ` + strconv.Itoa(x) + ` </a></li>`
					} else {
						tmpl_pag = tmpl_pag + `<li><a onClick="getPagination(` + strconv.Itoa(x) + `);"> ` + strconv.Itoa(x) + ` </a></li>`
					}
					x++
				}
				tmpl_pag = tmpl_pag + `<li><a> ... </a></li>`
				tmpl_pag = tmpl_pag + `<li><a onClick="getPagination(` + strconv.Itoa(NumPagina2) + `);"> ` + strconv.Itoa(NumPagina2) + ` </a></li>`
			}
		} else {
			var x int = 1
			for x <= NumPagina2 {
				if i == page {
					tmpl_pag = tmpl_pag + `<li class="page-active"><a onClick="getPagination(` + strconv.Itoa(x) + `);"> ` + strconv.Itoa(x) + ` </a></li>`
				} else {
					tmpl_pag = tmpl_pag + `<li><a onClick="getPagination(` + strconv.Itoa(x) + `);"> ` + strconv.Itoa(x) + ` </a></li>`
				}
				x++
			}
			tmpl_pag = tmpl_pag + `<li><a> ... </a></li>`
			tmpl_pag = tmpl_pag + `<li><a onClick="getPagination(` + strconv.Itoa(NumPagina2) + `);"> ` + strconv.Itoa(NumPagina2) + ` </a></li>`
		}
		if page == NumPagina2 {
			tmpl_pag = tmpl_pag + `<li><a><i class="fa fa-angle-right"></i></a></li>`
		} else if page < NumPagina2 {
			tmpl_pag = tmpl_pag + `<li>
                                <a onClick="getPagination(` + strconv.Itoa(page+1) + `);">
                                    <i class="fa fa-angle-right"></i>
                                </a>
                            </li>`
		}

		tmpl_pag = tmpl_pag + ` </ul> `
	}
}

func AltaAlmacenDesdeCompras(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		r.ParseForm()

		nombre := r.FormValue("nombre")
		tipo := r.FormValue("tipo")
		campos := r.Form["campos[]"]
		valores := r.Form["valores[]"]

		c := len(campos)
		d := len(valores)

		var almacen ModeloCompras.Almacen

		almacen.ID = bson.NewObjectId()
		almacen.Nombre = nombre
		almacen.Tipo = tipo
		almacen.Defecto = "no"

		datos := make(map[string]string)
		if c == d {
			for i := 0; i < c; i++ {
				datos[campos[i]] = valores[i]
			}
		}

		almacen.Datos = datos

		ModeloCompras.InsertaAlmacenDesdeComprasMGO(almacen)
		ModeloCompras.InsertaAlmacenDesdeComprasSQL(almacen.ID.Hex())
		templ := ``

		alma := ModeloCompras.ConsultaAlmacenesMgo()
		templ = `<option value="">--SELECCIONE--</option>`
		for _, v := range alma {
			if v.Nombre == "ALMACEN_DEFAULT" {
				templ += `<option value="` + v.ID.Hex() + `" selected>` + v.Nombre + `</option>`
			} else {
				templ += `<option value="` + v.ID.Hex() + `">` + v.Nombre + `</option>`
			}
		}
		fmt.Println("almacen añadido con id: ", almacen.ID.Hex())
		fmt.Fprintf(w, templ)
	}
}

func AltaUnidadDesdeCompras(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		r.ParseForm()
		nombre := r.FormValue("nombre")
		tipo := r.FormValue("tipo")
		abreviatura := r.FormValue("abreviatura")

		unidades := ModeloCompras.ConsultaUnidadesMgo()
		bandera := false
		ban := false
		for i, v := range unidades {
			if strings.ToUpper(v.Grupo) == strings.ToUpper(tipo) {
				//Insera una nueva abreviatura en este tipo
				for _, val := range v.DatosMedidas {
					if strings.ToUpper(val.Nombre) == strings.ToUpper(nombre) {
						ban = true
						bandera = true
					}
				}
				if ban == false {
					var datos ModeloCompras.DatosMedidas
					datos.Nombre = nombre
					datos.Abreviatura = abreviatura
					unidades[i].DatosMedidas = append(unidades[i].DatosMedidas, datos)
					ModeloCompras.ActualizaUnidadDesdeCompras(unidades[i])
					bandera = true
				}

			}
		}

		if bandera == false {
			var uni ModeloCompras.Unidades
			uni.ID = bson.NewObjectId()
			uni.Grupo = tipo
			var datas []ModeloCompras.DatosMedidas
			var data ModeloCompras.DatosMedidas
			data.Nombre = nombre
			data.Abreviatura = abreviatura
			datas = append(datas, data)
			uni.DatosMedidas = datas
			ModeloCompras.InsertaUnidadDesdeCompras(uni)
		}

		selectunidades := ModeloCompras.GeneraSelectUnidades()
		fmt.Println("unidad almacenada")
		fmt.Fprintf(w, selectunidades)

	}
}
