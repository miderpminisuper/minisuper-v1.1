package venta

import (
	"encoding/json"
	"fmt"
	_ "html/template"
	"net/http"
	"strconv"
	"time"

	"../../Controladores/Session"
	"../../Modelos/Compras"
	"../../Modelos/General"
	"../../Modelos/Venta"
	"github.com/leekchan/accounting"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type ResVenta struct {
	ID      string
	Mensaje string
	Estatus string
}

type FormaySubtotal struct {
	Subtotal     float64
	FormasDePago string
}

type TotalyResto struct {
	TotalaPagar    string
	Resto          string
	HistorialMas   string
	HistorialMenos string
	Boton          string
}

//Venta Estructura de movimiento de venta
type MovimientoVenta struct {
	ID             bson.ObjectId   `bson:"_id,omitempty"`
	Tipo           string          `bson:"tipo_movimiento"`
	Date           time.Time       `bson:"fecha_hora"`
	Cotizacion     bson.ObjectId   `bson:"cotizacion"`
	AlmacenOrigen  bson.ObjectId   `bson:"almacen_origen,omitempty"`
	AlmacenDestino bson.ObjectId   `bson:"almacen_destino,omitempty"`
	UsuarioOrigen  bson.ObjectId   `bson:"usuario_origen,omitempty"`
	UsuarioDestino bson.ObjectId   `bson:"usuario_destino,omitempty"`
	Productos      []ProductoVenta `bson:"productos"`
	FormasPago     FormasDePago    `bson:"formas_pago"`
	Comprobante    string          `bson:"comprobante"`
	TotalVenta     float64         `bson:"total_venta"`
	TotalCompra    float64         `bson:"total_compra"`
	TotalDescuento float64         `bson:"total_descuento"`
	Subtotal       float64         `bson:"subtotal"`
	Recibe         float64         `bson:"recibe"`
	Cambio         float64         `bson:"cambio"`
	TotalImpuestos float64         `bson:"total_impuesto"`
}

//ProductoVenta Estructura de productos de venta
type ProductoVenta struct {
	ID               bson.ObjectId     `bson:"_id,omitempty"`
	Cantidad         float64           `bson:"cantidad"`
	Nombre           string            `bson:"nombre"`
	Descuento        float64           `bson:"descuento"`
	Precio           float64           `bson:"precio"`
	CostoTotal       float64           `bson:"costo_total"`
	ImpuestoAplicado float64           `bson:"total_impuesto"`
	Impuestos        map[string]string `bson:"impuestos"`
	Importe          float64           `bson:"importe"`
	Subtotal         float64           `bson:"subtotal"`
	Unidad           string            `bson:"unidad"`
}

//FormasDePago estructura de formas de pago en mongo
type FormasDePago struct {
	Tformas int     `bson:"total_formas"`
	Formas  []Forma `bson:"formas"`
}

//Forma estructura de formas de pago en mongo
type Forma struct {
	ID         bson.ObjectId `bson:"_id,omitempty"`
	Clave      string        `bson:"clavesat"`
	Nombre     string        `bson:"nombre"`
	Importe    float64       `bson:"importe"`
	Comision   float64       `bson:"comision"`
	Porcentaje bool          `bson:"porcentaje"`
}

func GetFormasDePagoMongo(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		r.ParseForm()
		cotizacion := r.FormValue("cotizacion")
		coti := venta_m.GetCotizacionById(cotizacion)
		var Suma float64
		Suma = 0
		for _, val := range coti.Productos {
			//imp := venta_m.GetImpuestoDeArticulo(v.ID)
			Suma += val.Importe
		}
		result := venta_m.GetAllFormaPago()
		tmp := ``
		for _, v := range result {
			//			fmt.Println(k, "   --->  ", v)
			tmp += `<option value="` + v.Id.Hex() + `">` + v.Nombre + `</option>`
			//fmt.Fprintf(w, `<option value="%s">%s</option>`, v.Id.Hex(), v.Nombre)
		}

		var FyT FormaySubtotal

		FyT.Subtotal = Suma
		FyT.FormasDePago = tmp

		jData, _ := json.Marshal(FyT)
		w.Header().Set("Content-Type", "application/json")
		w.Write(jData)
	}
}

type FormaPago struct {
	Id       bson.ObjectId `bson:"_id,omitempty"`
	Nombre   string        `bson:"nombre"`
	Cambio   bool          `bson:"devuelve_cambio"`
	Comision float64       `bson:"comision"`
	Estatus  string        `bson:"status"`
}

func RealizarPagoConFormaDePago(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		r.ParseForm()
		for k, v := range r.PostForm {
			if k == "array_select[]" {
				for _, value := range v {
					//					fmt.Println(value)
					if value != "" {

						result := venta_m.GetFormaPagoId(bson.ObjectIdHex(value))
						fmt.Fprintf(w, `<div id="%s" class="formapago col-md-12">`, value)
						fmt.Fprintf(w, `<fieldset class="form-group"><legend>%s</legend>`, result.Nombre)

						fmt.Fprintf(w, `<div class="input-group">`)
						fmt.Fprintf(w, `<label for="lgFormGroupInput" class="input-group-addon">Monto:</label>`)
						if result.Porcentaje == true {
							fmt.Fprintf(w, `<input type="number" onblur="blurRestarValor(this);" class="form-control subtract-value" placeholder="0" identificador="%s" porcentaje="true" nombre="%s" cambio="%s">`, result.Id.Hex(), result.Nombre, strconv.FormatBool(result.Cambio))
						} else {
							fmt.Fprintf(w, `<input type="number" onblur="blurRestarValor(this);" class="form-control subtract-value" placeholder="0" identificador="%s" porcentaje="false" nombre="%s" cambio="%s">`, result.Id.Hex(), result.Nombre, strconv.FormatBool(result.Cambio))
						}

						//fmt.Fprintf(w, `<label for="lgFormGroupInput" class="input-group-addon"><span class="label label-default"></span></label>`)
						fmt.Fprintf(w, `</div>`)

						if result.Comision != 0 {
							ac := accounting.Accounting{Symbol: "", Precision: 2}
							if result.Porcentaje == true {
								fmt.Fprintf(w, `<div class="input-group">`)
								fmt.Fprintf(w, `<label class="input-group-addon">Comisión:</label>`)
								fmt.Fprintf(w, `<input type="number" class="form-control add-value" porcentaje="true" value="%v" nombre="%s" placeholder="0" readonly>`, result.Comision, result.Nombre)
								fmt.Fprintf(w, `<label class="input-group-addon">%%</label>`)
								fmt.Fprintf(w, `</div>`)
							} else {
								fmt.Fprintf(w, `<div class="input-group">`)
								fmt.Fprintf(w, `<label class="input-group-addon">Comisión:</label>`)
								fmt.Fprintf(w, `<input type="number" class="form-control add-value" value="%v" porcentaje="false" placeholder="0" nombre="%s" readonly>`, ac.FormatMoney(result.Comision), result.Nombre)
								fmt.Fprintf(w, `<label class="input-group-addon">$</label>`)
								fmt.Fprintf(w, `</div>`)
							}
						} else {
							fmt.Fprintf(w, `<div style="display:none;" class="input-group">`)
							fmt.Fprintf(w, `<label class="input-group-addon">Comisión:</label>`)
							fmt.Fprintf(w, `<input type="number" class="form-control add-value" value="0" porcentaje="false" placeholder="0" nombre="%s" readonly>`, result.Nombre)
							fmt.Fprintf(w, `<label class="input-group-addon">$</label>`)
							fmt.Fprintf(w, `</div>`)
						}

						fmt.Fprintf(w, `</fieldset>`)
						fmt.Fprintf(w, `</div>`)
					}
				}
			}
		}

	}
}

func SumarValoresDeArrayParaTotal(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		r.ParseForm()
		tam := r.FormValue("length")
		sub := r.FormValue("subtotal")
		subtotal, _ := strconv.ParseFloat(sub, 32)
		tamanio, _ := strconv.Atoi(tam)

		////////////////////////////
		//subtotal += float64(200)
		//////////////////////////////

		var totalapagar float32 = 0.00

		for i := 0; i < tamanio; i++ {
			ente := r.Form["ente["+strconv.Itoa(i)+"][]"]
			val := ente[0]
			porcentaje := ente[1]
			valor, _ := strconv.ParseFloat(val, 32)
			if porcentaje == "true" {
				//totalapagar += float32(valor) * float32(subtotal) / float32(100)
			} else {
				totalapagar += float32(valor)
			}
		}

		totalapagar += float32(subtotal)
		ac := accounting.Accounting{Symbol: "$", Precision: 2}

		fmt.Fprintf(w, `<input type="hidden" id="pago_total" value="%v">`, totalapagar)
		fmt.Fprintf(w, `<h2 class="display-5">Total Compra: %v</h2>`, ac.FormatMoney(subtotal))
		fmt.Fprintf(w, `<h2 class="display-5">Total a Pagar: %v</h2>`, ac.FormatMoney(totalapagar))

	}
}

func RestarAtotal(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		r.ParseForm()
		ac := accounting.Accounting{Symbol: "$", Precision: 2}
		tot := r.FormValue("total")
		tota, _ := strconv.ParseFloat(tot, 32)
		total := float32(tota)

		sub := r.FormValue("subtotal")
		subtotal, _ := strconv.ParseFloat(sub, 32)

		////////////////////////////
		//subtotal += float64(200)
		total = float32(subtotal)
		//////////////////////////////

		tam := r.FormValue("length")
		tamanio, _ := strconv.Atoi(tam)
		var totalCC float32 = 0.00
		var totalSC float32 = 0.00
		var totalapagar float32 = 0.00
		hmas := ``
		hmenos := ``
		for i := 0; i < tamanio; i++ {
			ente := r.Form["ente["+strconv.Itoa(i)+"][]"]
			val := ente[0]
			cambio := ente[1]
			nombre := ente[2]
			valor, _ := strconv.ParseFloat(val, 32)

			if cambio == "true" {
				totalCC += float32(valor)
			} else {
				totalSC += float32(valor)
			}

			ent := r.Form["ent["+strconv.Itoa(i)+"][]"]
			vax := ent[0]
			porcentaje := ent[1]
			nome := ent[2]
			va, _ := strconv.ParseFloat(vax, 32)
			if porcentaje == "true" {
				totalapagar += float32(va) * float32(valor) / float32(100)
				hmas += `<h4 class="display-7">+` + nome + `: ` + vax + `%--> ` + ac.FormatMoney(float32(va)*float32(valor)/float32(100)) + `</h4>`
				hmenos += `<h4 class="display-7">-` + nombre + `: ` + ac.FormatMoney(float32(valor)+(float32(va)*float32(valor)/float32(100))) + `</h4>`
			} else {
				totalapagar += float32(va)
				hmas += `<h4 class="display-7">+` + nome + `: ` + ac.FormatMoney(va) + `</h4>`
				var x float32 = 0.00
				if valor != 0.00 {
					x = float32(valor) + float32(va)
				}
				hmenos += `<h4 class="display-7">-` + nombre + `: ` + ac.FormatMoney(x) + `</h4>`
			}

		}

		totalapagar += float32(subtotal)
		totpag := ``
		totpag += `<input type="hidden" id="pago_total" value="` + strconv.FormatFloat(float64(totalapagar), 'f', 2, 32) + `">`
		totpag += `<h1 class="display-5">Total Compra: ` + ac.FormatMoney(subtotal) + `</h1>`
		totpag += `<h1 class="display-5">Total a Pagar: ` + ac.FormatMoney(totalapagar) + `</h1>`

		suma := totalCC + totalSC

		var cambio float32
		var restante float32

		restante = total - suma
		cambio = suma - total

		var result TotalyResto
		boton := ``
		resto := ``
		//resto += `<h2 class="display-5">Se reciben: ` + ac.FormatMoney(suma) + `</h2>`

		//fmt.Fprintf(w, `<h2 class="display-5">Se reciben: `+ac.FormatMoney(suma)+`</h2>`)
		if total > suma {
			resto += `<h2 class="display-5">Faltan: ` + ac.FormatMoney(restante) + `</h2>`
			//fmt.Fprintf(w, `<h2 class="display-5">Faltan:%v</h2>`, ac.FormatMoney(restante)) //se imprime la cantidad restante a pagar
		} else if total == suma {
			resto += `<h2 class="display-5"> Sin Cambio: ` + ac.FormatMoney(0) + `</h2>`
			boton = `<button type="button" class="btn btn-primary" onclick="cerrarPago()" id="btn_cerrarPago">Cerrar Venta</button>`
			//fmt.Fprintf(w, `<h2 class="display-5"> Sin Cambio: `+ac.FormatMoney(0)+`</h2>`) //se imprime cambio de 0 pesos
		} else if total < suma {
			if cambio > totalCC {
				resto += `<h2 class="display-5"> Cambio: ` + ac.FormatMoney(cambio) + `</h2>`
				resto += `<h2 class="display-5" name="name_cambio" style="color: #ff0000;">No se devuelve Cambio</h2>`
				//fmt.Fprintf(w, `<h2 class="display-5" style="color: #ffffff; background-color: #ff0000">No es posible devolver Cambio</h2>`) //se imprime pesos para dar cambio
			} else {
				resto += `<h2 class="display-5">Cambio: ` + ac.FormatMoney(cambio) + `</h2>`
				boton = `<button type="button" class="btn btn-primary" onclick="cerrarPago()" id="btn_cerrarPago">Cerrar Venta</button>`
				//fmt.Fprintf(w, `<h2 class="display-5">Cambio:%v</h2>`, ac.FormatMoney(cambio)) //se imprime pesos para dar cambio
			}

		}

		result.Resto = resto
		result.TotalaPagar = totpag
		result.HistorialMas = hmas
		result.HistorialMenos = hmenos
		result.Boton = boton

		jData, _ := json.Marshal(result)
		w.Header().Set("Content-Type", "application/json")
		w.Write(jData)
	}
}

func InsertaVenta(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		var venta MovimientoVenta

		//var FormasP []FormasDePago
		var FormasP FormasDePago
		var Formas []Forma
		var result ResVenta
		//SE rellenan datos de venta con cotizacion
		venta.Date = time.Now()
		venta.ID = bson.NewObjectId()
		venta.Tipo = "venta"
		result.ID = venta.ID.Hex()
		r.ParseForm()
		fmt.Println(r.Form)
		ac := accounting.Accounting{Symbol: "$", Precision: 2}

		cotizacion := r.FormValue("cotizacion")

		if cotizacion != "" {
			coti := venta_m.GetCotizacionById(cotizacion)

			venta.Cotizacion = bson.ObjectIdHex(cotizacion)
			venta.AlmacenDestino = coti.IdCliente
			venta.AlmacenOrigen = coti.AlmacenOrigen
			venta.UsuarioOrigen = coti.IdUsuario

			usuario := Session.GetUserName(r)
			IDUsuario := ModeloCompras.ConsultaIDUsuario(usuario)

			venta.UsuarioDestino = IDUsuario.ID

			var Productos []ProductoVenta
			var Producto ProductoVenta
			var imptotal float64

			for _, v := range coti.Productos {
				Producto = ProductoVenta{}
				articulo := ModeloCompras.GetArticle(v.ID)
				ct, _ := strconv.ParseFloat(articulo.CostoTotal, 32)
				Producto.ID = articulo.ID
				Producto.Cantidad = v.Cantidad
				Producto.Nombre = articulo.Descripcion
				Producto.Unidad = articulo.Unidad
				Producto.CostoTotal = ct
				Producto.Descuento = v.DescuentoTotal
				Producto.Importe = v.Importe
				var imp float64
				var val float64
				for _, v := range articulo.Impuestos {
					val, _ = strconv.ParseFloat(v, 32)
					imp += (ct / (1 + (val / 100))) * (val / 100)
				}
				Producto.ImpuestoAplicado = imp
				imptotal += imp
				Producto.Impuestos = articulo.Impuestos
				Producto.Precio = v.Precio
				Producto.Subtotal = (ct * float64(v.Cantidad)) - v.DescuentoTotal

				Productos = append(Productos, Producto)
			}
			venta.TotalImpuestos = imptotal
			venta.Productos = Productos

			sub := r.FormValue("subtotal")
			subtotal, _ := strconv.ParseFloat(sub, 32)

			venta.Subtotal = subtotal
			venta.TotalCompra = subtotal
			///////////////////////////
			total := float32(subtotal)
			///////////////////////////

			tam := r.FormValue("length")
			tamanio, _ := strconv.Atoi(tam)

			FormasP.Tformas = tamanio

			var totalCC float32 = 0.00
			var totalSC float32 = 0.00
			var totalapagar float32 = 0.00

			for i := 0; i < tamanio; i++ {

				///ingresos de formas de pago////
				ente := r.Form["ente["+strconv.Itoa(i)+"][]"]
				val := ente[0]
				valor, _ := strconv.ParseFloat(val, 32)

				cambio := ente[1]

				if cambio == "true" {
					totalCC += float32(valor)
				} else {
					totalSC += float32(valor)
				}

				///comisiones y totales de formas de pago////

				ent := r.Form["ent["+strconv.Itoa(i)+"][]"]
				vax := ent[0]
				porcentaje := ent[1]
				ide := ente[3]

				var Forma Forma
				Formax := venta_m.GetFormaByID(ide)
				Forma.ID = Formax.Id
				Forma.Clave = Formax.Clave
				Forma.Comision = Formax.Comision
				Forma.Nombre = Formax.Nombre
				Forma.Porcentaje = Formax.Porcentaje

				va, _ := strconv.ParseFloat(vax, 32)

				if porcentaje == "true" {
					totalapagar += float32(va) * float32(valor) / float32(100)
					totaldeformadepago := float32(valor) + (float32(va) * float32(valor) / float32(100))
					Forma.Importe = float64(totaldeformadepago)
				} else {
					totalapagar += float32(va)

					var x float32 = 0.00
					if valor != 0.00 {
						x = float32(valor) + float32(va)
					}
					Forma.Importe = float64(x)
				}
				Formas = append(Formas, Forma)
			}

			FormasP.Formas = Formas
			venta.FormasPago = FormasP
			totalapagar += float32(subtotal)
			venta.TotalVenta = float64(totalapagar)

			suma := totalCC + totalSC

			venta.Recibe = float64(suma)

			cambio := suma - total

			if total > suma {
				// Venta inprocedente regresar error
				estante := total - suma
				result.Estatus = "false"
				result.Mensaje = "Error, falta dinero: " + ac.FormatMoney(estante)
			} else if total == suma {
				venta.Cambio = float64(cambio)
			} else if total < suma {
				if cambio > totalCC {
					result.Estatus = "false"
					result.Mensaje = "Error, no se puede regresar tanto cambio: " + ac.FormatMoney(cambio)
					// Venta inprocedente regresar error
				} else {
					venta.Cambio = float64(cambio)
				}

			}

			res := venta_m.CrearVenta(venta)

			if res {
				result.Estatus = "true"
				result.Mensaje = "Alta Correcta"
			} else {
				result.Estatus = "false"
				result.Mensaje = "Error, no se pudo insertar la venta, intente de nuevo."
			}
		} else {
			result.Estatus = "false"
			result.Mensaje = "Error, no hay cotización asociada a esta venta, intente de nuevo."
		}

		jData, _ := json.Marshal(result)
		w.Header().Set("Content-Type", "application/json")
		w.Write(jData)
	}
}

//Variables globales para búsqueda y paginación
var Ids2 []bson.ObjectId
var numero_registros2 int64
var tammuestra2 int = 5

//BuscaClientes funcion que busca clientes en elastic
func BuscaClientes(w http.ResponseWriter, r *http.Request) {
	Ids2 = nil
	if r.Method == "POST" {
		r.ParseForm()
		dato := r.FormValue("dato")
		fmt.Println(dato)

		if dato != "" {

			docs := venta_m.GetAllClientes(dato)

			if len(docs) > 0 {
				numero_registros2 = int64(len(docs))
				fmt.Println("Se encontraron: ", len(docs))

				for _, item := range docs {
					iid := item.Id
					Ids2 = append(Ids2, iid)
				}
				var arrayids []bson.ObjectId

				reg := len(Ids2)
				if reg < tammuestra2 {
					for _, valor := range Ids2 {
						arrayids = append(arrayids, valor)
					}
				} else {
					for _, valor := range Ids2[0:tammuestra2] {
						arrayids = append(arrayids, valor)
					}
				}

				result := venta_m.GetClientes(arrayids)

				jData, _ := json.Marshal(result)
				w.Header().Set("Content-Type", "application/json")
				w.Write(jData)

			} else {
				fmt.Println("No se encontró algún registro")
			}

		} else {
			fmt.Println("Sin datos que consultar")
		}
	} else if r.Method == "GET" {
	}
}

//GetSearchPaginationGeneral funcion que regresa artículos de correspondiente paginación
func GetSearchPaginationGeneralV(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {

		pageS := r.FormValue("num_page")
		page, _ := strconv.Atoi(pageS)

		lim := tammuestra2
		skip := (page * lim) - lim
		limite := skip + lim
		fmt.Println("Solicita la página: ", page, " del número: ", skip, " al: ", limite)
		result, _ := GetValuesDataMongodbPaginationV(limite, skip, page)
		jData, err := json.Marshal(result)
		if err != nil {
			panic(err)
			return
		}
		w.Header().Set("Content-Type", "application/json")
		w.Write(jData)
	}
}

//GetValuesDataMongodbPagination funcion obtiene de un conjunto de Ids
func GetValuesDataMongodbPaginationV(limit int, skip int, page int) ([]models.Cliente, int64) {
	var arrayids []bson.ObjectId
	//limit es el rango final de la segunda página
	//skip desde donde va a comenzar
	//Si es una página final entonces debe ser el ´límite del tamaño del arreglo de Ids

	numpags := float32(numero_registros2) / float32(tammuestra2)
	numpags2 := int(numpags)
	if numpags > float32(numpags2) {
		numpags2++
	}

	if page == numpags2 {
		final := len(Ids2) % tammuestra2
		fmt.Println(final)
		if final == 0 {
			for _, valor := range Ids2[skip:limit] {
				arrayids = append(arrayids, valor)
			}
			fmt.Println("Se consultó última página: ", page, "del: ", skip, " al ", limit)
		} else {
			for _, valor := range Ids2[skip : skip+final] {
				arrayids = append(arrayids, valor)
			}
			fmt.Println("Se consultó última página: ", page, "del: ", skip, " al ", skip+final)
		}
	} else {
		for _, valor := range Ids2[skip:limit] {
			arrayids = append(arrayids, valor)
		}
	}

	result := venta_m.GetClientes(arrayids)

	return result, numero_registros2
}

//GetAllResultSearchElastic function for know pagination number
func GetAllResultSearchElasticV(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {

		page, _ := strconv.Atoi(r.FormValue("getPage"))

		fmt.Println("valores de IDS->", len(Ids2))

		var tmpl_pag = ` <ul class="pagination pagination-lg" > `

		var numTotal int
		numTotal = len(Ids2)

		NumPagina := float32(numTotal) / float32(tammuestra2)
		NumPagina2 := int(NumPagina)
		if NumPagina > float32(NumPagina2) {
			NumPagina2++
		}

		if page == 1 {
			tmpl_pag = tmpl_pag + ` <li><a><i class="fa fa-angle-left"></i></a></li>`
		} else if page > 1 {
			tmpl_pag = tmpl_pag + ` <li>
                                <a onClick="getPagination(` + strconv.Itoa(page-1) + `);">
                                    <i class="fa fa-angle-left"></i>
                                </a>
                            </li>`
		}

		var i int = page - 2
		var y int = i + 4
		if NumPagina2 > 5 {
			if i >= 1 {
				if y <= NumPagina2-2 {
					for i <= y {
						if i == page {
							tmpl_pag = tmpl_pag + `<li class="page-active"><a onClick="getPagination(` + strconv.Itoa(i) + `);"> ` + strconv.Itoa(i) + ` </a></li>`
						} else {
							tmpl_pag = tmpl_pag + `<li><a onClick="getPagination(` + strconv.Itoa(i) + `);"> ` + strconv.Itoa(i) + ` </a></li>`
						}
						i++

					}
					tmpl_pag = tmpl_pag + `<li><a> ... </a></li>`
					tmpl_pag = tmpl_pag + `<li><a onClick="getPagination(` + strconv.Itoa(NumPagina2) + `);"> ` + strconv.Itoa(NumPagina2) + ` </a></li>`
				} else {
					var z int = NumPagina2 - 4

					tmpl_pag = tmpl_pag + `<li><a onClick="getPagination(` + strconv.Itoa(1) + `);"> ` + strconv.Itoa(1) + ` </a></li>`
					tmpl_pag = tmpl_pag + `<li><a> ... </a></li>`
					for z <= NumPagina2 {
						if z == page {
							tmpl_pag = tmpl_pag + `<li class="page-active"><a onClick="getPagination(` + strconv.Itoa(z) + `);"> ` + strconv.Itoa(z) + ` </a></li>`
						} else {
							tmpl_pag = tmpl_pag + `<li><a onClick="getPagination(` + strconv.Itoa(z) + `);"> ` + strconv.Itoa(z) + ` </a></li>`
						}
						z++
					}
				}
			} else if i <= 0 {
				var x int = 1
				for x <= 5 {
					if x == page {
						tmpl_pag = tmpl_pag + `<li class="page-active"><a onClick="getPagination(` + strconv.Itoa(x) + `);"> ` + strconv.Itoa(x) + ` </a></li>`
					} else {
						tmpl_pag = tmpl_pag + `<li><a onClick="getPagination(` + strconv.Itoa(x) + `);"> ` + strconv.Itoa(x) + ` </a></li>`
					}
					x++
				}
				tmpl_pag = tmpl_pag + `<li><a> ... </a></li>`
				tmpl_pag = tmpl_pag + `<li><a onClick="getPagination(` + strconv.Itoa(NumPagina2) + `);"> ` + strconv.Itoa(NumPagina2) + ` </a></li>`
			}
		} else {
			var x int = 1
			for x <= NumPagina2 {
				if i == page {
					tmpl_pag = tmpl_pag + `<li class="page-active"><a onClick="getPagination(` + strconv.Itoa(x) + `);"> ` + strconv.Itoa(x) + ` </a></li>`
				} else {
					tmpl_pag = tmpl_pag + `<li><a onClick="getPagination(` + strconv.Itoa(x) + `);"> ` + strconv.Itoa(x) + ` </a></li>`
				}
				x++
			}
			tmpl_pag = tmpl_pag + `<li><a> ... </a></li>`
			tmpl_pag = tmpl_pag + `<li><a onClick="getPagination(` + strconv.Itoa(NumPagina2) + `);"> ` + strconv.Itoa(NumPagina2) + ` </a></li>`
		}

		if page == NumPagina2 {
			tmpl_pag = tmpl_pag + `<li><a><i class="fa fa-angle-right"></i></a></li>`
		} else if page < NumPagina2 {
			tmpl_pag = tmpl_pag + `<li>
                                <a onClick="getPagination(` + strconv.Itoa(page+1) + `);">
                                    <i class="fa fa-angle-right"></i>
                                </a>
                            </li>`
		}

		tmpl_pag = tmpl_pag + ` </ul> `

		fmt.Fprintf(w, tmpl_pag)

	}

}

func GeneraTicket(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		r.ParseForm()
		var result ResVenta
		venta := r.FormValue("venta")
		ac := accounting.Accounting{Symbol: "$", Precision: 2}
		ticket := ``
		if venta != "" {
			ventana := GetVentaById(venta)
			datosfiscales := venta_m.GetDatosFiscales()

			ticket += `<div clas="col-md-8 text-center"><h5>---------` + datosfiscales.Nombre + `----------</h5>`
			ticket += `<h5> R.F.C. ` + datosfiscales.Rfc + `</h5>`
			ticket += `<h5>` + datosfiscales.Direccion.Calle + ` ` + strconv.Itoa(datosfiscales.Direccion.NumExterior) + ` ` + `</h5>`
			ticket += `<h5>` + datosfiscales.Direccion.Colonia + ` C.P. ` + strconv.Itoa(datosfiscales.Direccion.CodigoPostal) + `</h5>`
			ticket += `<h5>` + datosfiscales.Direccion.Municipio + ` ` + datosfiscales.Direccion.Estado + ` ` + `</h5>`

			ticket += `<hr></div>`

			ticket += `<div clas="col-md-8 text-center"> <p>CANT  ARTICULO        PRECIO    DESC.    TOTAL</p>`

			for _, v := range ventana.Productos {

				cant := strconv.FormatFloat(v.Cantidad, 'f', 2, 64)
				desc := ``
				if len(v.Nombre) > 15 {
					desc = v.Nombre[0:15]
				} else {
					desc = v.Nombre
				}

				precio := ac.FormatMoney(v.Precio)
				descuento := ac.FormatMoney(v.Descuento)
				importe := ac.FormatMoney(v.Importe)
				ticket += `<p>` + cant + `     ` + desc + `    ` + precio + `   ` + descuento + ` ` + importe + `</p>`
			}

			ticket += `</div><hr>`
			ticket += `<div clas="col-md-8 text-right"><h4>SUBTOTAL: ` + ac.FormatMoney(ventana.Subtotal) + ` </h4>`
			ticket += `<h4>DESCUENTO: ` + ac.FormatMoney(ventana.TotalDescuento) + ` </h4>`
			ticket += `<h4>TOTAL: ` + ac.FormatMoney(ventana.TotalCompra) + ` </h4>`
			ticket += `</div><hr>`

			ticket += `<div clas="col-md-8 text-left"><h4>FORMAS DE PAGO:</h4>`
			for _, v := range ventana.FormasPago.Formas {
				ticket += `<h5>` + v.Nombre + ` ------>  ` + ac.FormatMoney(v.Importe) + `</h5>`
				if v.Porcentaje == true {
					ticket += `<h5>Comisión + ` + strconv.FormatFloat(v.Comision, 'f', 2, 64) + `%</h5>`
				} else {
					ticket += `<h5>Comisión + ` + strconv.FormatFloat(v.Comision, 'f', 2, 64) + `$</h5>`
				}
			}
			ticket += `</div><div clas="col-md-8 text-right"><h4>TOTAL A PAGAR: ` + ac.FormatMoney(ventana.TotalVenta) + `</h4>`
			ticket += `<hr>`
			ticket += `<h4>RECIBE: ` + ac.FormatMoney(ventana.Recibe) + `</h4>`
			ticket += `<h4>CAMBIO: ` + ac.FormatMoney(ventana.Cambio) + `</h4>`
			ticket += `<hr></div>`
			ticket += `<div>FOLIO: ` + venta + `<hr></div>`
			result.Estatus = "true"
			result.Mensaje = ticket
			result.ID = venta

			jData, _ := json.Marshal(result)
			w.Header().Set("Content-Type", "application/json")
			w.Write(jData)

		} else {
			result.Estatus = "false"
			result.Mensaje = "No se capturó adecuadamente la venta, vuelva a intentarlo"

			jData, _ := json.Marshal(result)
			w.Header().Set("Content-Type", "application/json")
			w.Write(jData)
		}

	}

}

func GetVentaById(Id string) MovimientoVenta {
	var result MovimientoVenta
	session, err := mgo.Dial("192.168.1.110")
	if err != nil {
		fmt.Println("----------------ERROR AL CONECTAR CON MONGO---------------------")
		panic(err)
	}
	c := session.DB("minisuper").C("movimientos")
	ID := bson.ObjectIdHex(Id)
	err = c.Find(bson.M{"_id": ID}).One(&result)
	if err != nil {
		fmt.Println("----------------ERROR AL BUSCAR Venta EN MONGO---------------------")
		panic(err)
	}

	return result
}

/////GeneraFactura para Facturacion
func GeneraFactura(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		var result ResVenta
		r.ParseForm()
		venta := r.FormValue("venta")
		cliente := r.FormValue("cliente")
		//------------------Datos del Movimiento---------------

		//ac := accounting.Accounting{Symbol: "$", Precision: 2}

		if venta != "" {
			ventana := GetVentaById(venta)
			datosfiscales := venta_m.GetDatosFiscales()

			//Datos del Cliente
			fmt.Println("DATOS DEL CLIENTE")

			UsuarioDestino := ModeloCompras.DatosCliente(bson.ObjectIdHex(cliente))
			fmt.Println("RFC: ", UsuarioDestino.Rfc)
			fmt.Println("Nombre: ", UsuarioDestino.Nombre)
			fmt.Println("")

			////////////////////////////////////////////////////////////////////////////
			///////////////////////////////--XML--//////////////////////////////////////
			////////////////////////////////////////////////////////////////////////////

			// en este caso lo tenemos de manera estatica */

			cfd := `<?xml version="1.0" encoding="UTF-8"?>

			<cfdi:Comprobante 

			xsi:schemaLocation="http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv32.xsd" 

			xmlns:cfdi="http://www.sat.gob.mx/cfd/3" 

			xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 

			xmlns:xs="http://www.w3.org/2001/XMLSchema"

			LugarExpedicion="` + datosfiscales.Direccion.Estado + `" 

			NumCtaPago="DESCONOCIDO" 

			formaDePago="PAGO EN UNA SOLA EXHIBICION"`

			fp := ``

			for i, v := range ventana.FormasPago.Formas {
				if i == 0 {
					fp += v.Clave
				} else {
					fp += `,` + v.Clave
				}
			}

			cfd += ` metodoDePago="` + fp + `"

			subTotal="` + strconv.FormatFloat(ventana.Subtotal, 'f', 2, 64) + `" 

			total="` + strconv.FormatFloat(ventana.TotalVenta, 'f', 2, 64) + `" 

			fecha="` + ventana.Date.Format("2006-01-02 15:04:05") + `" 

            certificado="MIIEdDCCA1ygAwIBAgIUMjAwMDEwMDAwMDAxMDAwMDU4NjcwDQYJKoZIhvcNAQEFBQAwggFvMRgwFgYDVQQDDA9BLkMuIGRlIHBydWViYXMxLzAtBgNVBAoMJlNlcnZpY2lvIGRlIEFkbWluaXN0cmFjacOzbiBUcmlidXRhcmlhMTgwNgYDVQQLDC9BZG1pbmlzdHJhY2nDs24gZGUgU2VndXJpZGFkIGRlIGxhIEluZm9ybWFjacOzbjEpMCcGCSqGSIb3DQEJARYaYXNpc25ldEBwcnVlYmFzLnNhdC5nb2IubXgxJjAkBgNVBAkMHUF2LiBIaWRhbGdvIDc3LCBDb2wuIEd1ZXJyZXJvMQ4wDAYDVQQRDAUwNjMwMDELMAkGA1UEBhMCTVgxGTAXBgNVBAgMEERpc3RyaXRvIEZlZGVyYWwxEjAQBgNVBAcMCUNveW9hY8OhbjEVMBMGA1UELRMMU0FUOTcwNzAxTk4zMTIwMAYJKoZIhvcNAQkCDCNSZXNwb25zYWJsZTogSMOpY3RvciBPcm5lbGFzIEFyY2lnYTAeFw0xMjA3MjcxNzAyMDBaFw0xNjA3MjcxNzAyMDBaMIHbMSkwJwYDVQQDEyBBQ0NFTSBTRVJWSUNJT1MgRU1QUkVTQVJJQUxFUyBTQzEpMCcGA1UEKRMgQUNDRU0gU0VSVklDSU9TIEVNUFJFU0FSSUFMRVMgU0MxKTAnBgNVBAoTIEFDQ0VNIFNFUlZJQ0lPUyBFTVBSRVNBUklBTEVTIFNDMSUwIwYDVQQtExxBQUEwMTAxMDFBQUEgLyBIRUdUNzYxMDAzNFMyMR4wHAYDVQQFExUgLyBIRUdUNzYxMDAzTURGUk5OMDkxETAPBgNVBAsTCFVuaWRhZCAxMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC2TTQSPONBOVxpXv9wLYo8jezBrb34i/tLx8jGdtyy27BcesOav2c1NS/Gdv10u9SkWtwdy34uRAVe7H0a3VMRLHAkvp2qMCHaZc4T8k47Jtb9wrOEh/XFS8LgT4y5OQYo6civfXXdlvxWU/gdM/e6I2lg6FGorP8H4GPAJ/qCNwIDAQABox0wGzAMBgNVHRMBAf8EAjAAMAsGA1UdDwQEAwIGwDANBgkqhkiG9w0BAQUFAAOCAQEATxMecTpMbdhSHo6KVUg4QVF4Op2IBhiMaOrtrXBdJgzGotUFcJgdBCMjtTZXSlq1S4DG1jr8p4NzQlzxsdTxaB8nSKJ4KEMgIT7E62xRUj15jI49qFz7f2uMttZLNThipunsN/NF1XtvESMTDwQFvas/Ugig6qwEfSZc0MDxMpKLEkEePmQwtZD+zXFSMVa6hmOu4M+FzGiRXbj4YJXn9Myjd8xbL/c+9UIcrYoZskxDvMxc6/6M3rNNDY3OFhBK+V/sPMzWWGt8S1yjmtPfXgFs1t65AZ2hcTwTAuHrKwDatJ1ZPfa482ZBROAAX1waz7WwXp0gso7sDCm2/yUVww==" 

            noCertificado="20001000000100005867" 

            sello="qTzEwNUDvk/WpeFstpf/FLNmiHMxiL8pDTXGNb+DCFO0Z7SXMZJCim3JAlBw8Astvr0/jiVfo3WdWbaX88cC1l1+iipOLdedbFNHZUmsW86nC1YQHWw0S6mEiGH0ZxGa5KOl/1mrvPlDweeCiyYavNQeEuO1r0/ELobMi07w6ps=" 

            tipoDeComprobante="` + ventana.Comprobante + `"

            version="3.2" >

            <cfdi:Emisor nombre="` + datosfiscales.Nombre + `" rfc="` + datosfiscales.Rfc + `">

     		<cfdi:DomicilioFiscal codigoPostal="` + strconv.Itoa(datosfiscales.Direccion.CodigoPostal) + `" pais="MEXICO" estado="` + datosfiscales.Direccion.Estado + `" municipio="` + datosfiscales.Direccion.Municipio + ` " calle="` + datosfiscales.Direccion.Calle + `"/>

              <cfdi:RegimenFiscal Regimen="PERSONA FISCA"/>

            </cfdi:Emisor>

            <cfdi:Receptor nombre="` + UsuarioDestino.Nombre + `" rfc="` + UsuarioDestino.Rfc + `">

            <cfdi:Domicilio pais="` + UsuarioDestino.Direccion.Estado + `" calle="` + UsuarioDestino.Direccion.Calle + `"/>

            </cfdi:Receptor>

            <cfdi:Conceptos>
   `
			for _, v := range ventana.Productos {

				cfd += `<cfdi:Concepto importe="` + strconv.FormatFloat(v.Importe, 'f', 2, 64) +
					`" valorUnitario="` + strconv.FormatFloat(v.Precio, 'f', 2, 64) + `" descripcion="` + v.Nombre + `" unidad="` + v.Unidad + `" cantidad="` + strconv.FormatFloat(v.Cantidad, 'f', 2, 64) + `"/>`
			}

			cfd += `
            </cfdi:Conceptos>

            <cfdi:Impuestos>

              <cfdi:Traslados>

                <cfdi:Traslado importe="` + strconv.FormatFloat(ventana.TotalImpuestos, 'f', 2, 64) + `" tasa="????" impuesto="???"/>

              </cfdi:Traslados>

            </cfdi:Impuestos>

            <cfdi:Addenda/>

          </cfdi:Comprobante>`

			result.Estatus = "true"
			result.Mensaje = cfd
			result.ID = venta

			jData, _ := json.Marshal(result)
			w.Header().Set("Content-Type", "application/json")
			w.Write(jData)
		} else {
			result.Estatus = "false"
			result.Mensaje = "No se capturó adecuadamente el folio de venta, favor de intentar de nuevo"
			result.ID = venta

			jData, _ := json.Marshal(result)
			w.Header().Set("Content-Type", "application/json")
			w.Write(jData)

		}

	} else if r.Method == "POST" {
		fmt.Println("POST")
	}
}
