package venta

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"

	"../../Modelos/Articulos"
	"../../Modelos/Compras"
	"../../Modelos/General"
	"../../Modelos/Venta"
	"../Session"
	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2/bson"
)

//Movimiento estructura de movimientos en mongo
//Esta Estructura está incluida en el archivo de Ismael
type Movimiento struct {
	ID             bson.ObjectId `bson:"_id,omitempty"`
	Tipo           string        `bson:"tipo_movimiento"`
	Date           time.Time     `bson:"fecha_hora"`
	AlmacenOrigen  bson.ObjectId `bson:"almacen_origen,omitempty"`
	AlmacenDestino bson.ObjectId `bson:"almacen_destino,omitempty"`
	UsuarioOrigen  bson.ObjectId `bson:"usuario_origen,omitempty"`
	UsuarioDestino bson.ObjectId `bson:"usuario_destino,omitempty"`
	Productos      []Producto    `bson:"productos,omitempty"`
	Actualiza      []Editores    `bson:"edita,omitempty"`
}

//Editores estructura de editores de compras
type Editores struct {
	ID   bson.ObjectId `bson:"_id,omitempty"`
	Tipo string        `bson:"tipo_movimiento"`
	Date time.Time     `bson:"fecha_hora"`
}

//Producto Estructura a escribir en template
type Producto struct {
	ID        bson.ObjectId `bson:"_id,omitempty"`
	Cantidad  float64       `bson:"cantidad"`
	Costo     float64       `bson:"costo"`
	Impuestos float64       `bson:"total_impuesto"`
	Importe   float64       `bson:"importe"`
	Ganancia  float64       `bson:"ganancia"`
}

// fin de Estructuras de ismael

type Cotizacion struct {
	ID             bson.ObjectId `bson:"_id,omitempty"`
	FechaHora      time.Time     `bson:"fecha_hora,omitempty"`
	IdCliente      bson.ObjectId `bson:"usuario_destino,omitempty"`
	IdUsuario      bson.ObjectId `bson:"usuario_origen,omitempty"`
	AlmacenOrigen  bson.ObjectId `bson:"almacen_origen,omitempty"`
	AlmacenDestino bson.ObjectId `bson:"almacen_destino,omitempty"`
	Tipo           string        `bson:"tipo_movimiento,omitempty"`
}

type Productoregistro struct {
	IDProducto string
	Cantidad   float64
	Precio     float64
}
type ProductoDetalle struct {
	IDProducto string
	Cantidad   float64
	Estatus    string
}
type ProductoDatos struct {
	IDProducto  string
	Descripcion string
	Precio      float64
}
type ManejoErrores struct {
	Exito       bool
	Descripcion string
	Valor       float64
}
type PrimerRegistro struct {
	ID       string
	Registro string
}
type Transaccion struct {
	IDCot             string
	ID                string
	ProductoPrecio    float64
	ProductoCantidad  float64
	ProductoDescuento float64
	ValorPrevio       float64
	ValorNuevo        float64
	AlmOrigen         string
	AlmDestino        string
	IDUsr             string
	IDCli             string
}
type ProductoCV struct {
	ID             bson.ObjectId `bson:"_id,omitempty"`
	Cantidad       float64       `bson:"cantidad,omitempty"`
	Costo          float64       `bson:"costo,omitempty"`
	Importe        float64       `bson:"precio,omitempty"`
	Descuento      float64       `bson:"descuento,omitempty"`
	DescuentoTotal float64       `bson:"descuento_total,omitempty"`
	ImporteTotal   float64       `bson:"importe,omitempty"`
}
type TuplaProdID struct {
	ID        bson.ObjectId `bson:"_id,omitempty"`
	ProductoC []ProductoCV  `bson:"productos,omitempty"`
}
type PreciosCostos struct {
	ID     bson.ObjectId `bson:"_id,omitempty"`
	Precio float64       `bson:"precio,omitempty"`
	Costo  float64       `bson:"costo,omitempty"`
}

var tmpl_ventaRealizada = template.Must(template.ParseFiles(
	"Vistas/index.html",
	"Vistas/Parciales/Plantilla/header_blank.html",
	"Vistas/Parciales/Plantilla/footer_blank.html",
	"Vistas/Parciales/Plantilla/styles.html",
	"Vistas/Parciales/Plantilla/scripts.html",
	"Vistas/Parciales/Paginas/Venta/indexventas.html",
))

func EnConstruccion(w http.ResponseWriter, r *http.Request) {

	if r.Method == "GET" {
		tmpl_ventaRealizada.ExecuteTemplate(w, "index_layout", nil)
	}
	/*fmt.Fprintf(w, "<div class='alert alert-info'> hola mundo</div>")
	fmt.Println(tmpl_user)*/
}

var tmpl_venta = template.Must(template.ParseFiles(
	"Vistas/index.html",
	"Vistas/Parciales/Plantilla/header_blank.html",
	"Vistas/Parciales/Plantilla/footer_blank.html",
	"Vistas/Parciales/Plantilla/styles.html",
	"Vistas/Parciales/Plantilla/scripts.html",
	"Vistas/Parciales/Paginas/Venta/new.html",
))

var tmpl3_frame = template.Must(template.ParseFiles(
	"Vistas/index.html",
	"Vistas/Parciales/Plantilla/header_blank.html",
	"Vistas/Parciales/Plantilla/footer_blank.html",
	"Vistas/Parciales/Plantilla/styles.html",
	"Vistas/Parciales/Plantilla/scripts.html",
	"Vistas/Parciales/Paginas/Venta/venta_frame.html",
))

func IndexVentas(w http.ResponseWriter, r *http.Request) {

	if r.Method == "GET" {
		tmpl_venta.ExecuteTemplate(w, "index_layout", nil)
	}
	/*fmt.Fprintf(w, "<div class='alert alert-info'> hola mundo</div>")
	fmt.Println(tmpl_user)*/
}

var tf = template.FuncMap{"add": add}

var tmpl_ventaN = template.Must(template.ParseFiles(
	"Vistas/index.html",
	"Vistas/Parciales/Plantilla/header.html",
	"Vistas/Parciales/Plantilla/footer.html",
	"Vistas/Parciales/Plantilla/styles.html",
	"Vistas/Parciales/Plantilla/scripts.html",
	"Vistas/Parciales/Paginas/Venta/new.html",
))

var tmpl_ventaN_frame = template.Must(template.ParseFiles(
	"Vistas/index.html",
	"Vistas/Parciales/Plantilla/header_blank.html",
	"Vistas/Parciales/Plantilla/footer_blank.html",
	"Vistas/Parciales/Plantilla/styles.html",
	"Vistas/Parciales/Plantilla/scripts.html",
	"Vistas/Parciales/Paginas/Venta/new.html",
))

var tmpl2_frame = template.Must(template.New("").Funcs(tf).ParseFiles(
	"Vistas/index.html",
	"Vistas/Parciales/Plantilla/header_blank.html",
	"Vistas/Parciales/Plantilla/footer_blank.html",
	"Vistas/Parciales/Plantilla/styles.html",
	"Vistas/Parciales/Plantilla/scripts.html",
	"Vistas/Parciales/Paginas/Venta/edit_frame.html",
))

//add funcion para template
func add(x, y int) int {
	return x + y
}

func VentaNew(w http.ResponseWriter, r *http.Request) {
	userName := Session.GetUserName(r)
	if userName != "" {
		tmpl_ventaN.ExecuteTemplate(w, "index_layout", nil)
	} else {
		http.Redirect(w, r, "/login", 302)
	}
}

func VentaNew_frame(w http.ResponseWriter, r *http.Request) {
	userName := Session.GetUserName(r)
	if userName != "" {
		tmpl_ventaN_frame.ExecuteTemplate(w, "index_layout", nil)
	} else {
		http.Redirect(w, r, "/login", 302)
	}
}

func PrintFields(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		r.ParseForm()
		cb := r.FormValue("cb")
		almOrigen := r.FormValue("almOrigen")
		fmt.Println("Codigo de barras: ", cb)
		registros := ModeloArticulos.ConsultaCodigo(cb)

		campos := ""
		var result PrimerRegistro
		if registros > 0 {
			a, datos := ModeloArticulos.ConsultaCodigoYRegresaEstructuras3(cb, almOrigen)
			if datos.Cantidad > 0 && datos.Estatus == "ACTIVO" {
				campos = campos + fmt.Sprintf(`<div class="clearfix"></div>`)
				campos = campos + fmt.Sprintf(`<div>`)
				campos = campos + fmt.Sprintf(`<input type="hidden" id="fraccion%v" name="fraccionario" value='%v'>`, a.ID.Hex(), a.Entero)
				campos = campos + fmt.Sprintf(`<input type="hidden" id="compra%v" name="costo" value="%v">`, a.ID.Hex(), a.PreCompra)

				campos = campos + fmt.Sprintf(`<div class="col-md-1 col-lg-1">`)
				campos = campos + fmt.Sprintf(`	<input  type="hidden" id="%v" value="%s"  name="elemento"> %s`, a.ID.Hex(), a.ID.Hex(), a.CodBarra)
				campos = campos + fmt.Sprintf(`	<input  type="hidden" value="%s"  name="tag">`, a.CodBarra)
				campos = campos + fmt.Sprintf(`</div>`)

				campos = campos + fmt.Sprintf(`<div class="col-md-4 col-lg-4"> `)
				campos = campos + fmt.Sprintf(`%s`, a.Descripcion)
				campos = campos + fmt.Sprintf(`</div>`)

				campos = campos + fmt.Sprintf(`<div class="col-md-1 col-lg-1">`)
				campos = campos + fmt.Sprintf(`<input class="anchototal" type="number" id="cantidad%v"  value="1" onfocus="obtenerCantidadInicial('%v')" onKeydown="Javascript: if (event.keyCode==13) modificacionCantidad('%v');" onblur="restauraCantidad('%v')">`, a.ID.Hex(), a.ID.Hex(), a.ID.Hex(), a.ID.Hex())
				campos = campos + fmt.Sprintf(`</div>`)

				campos = campos + fmt.Sprintf(`<div class="col-md-1 col-lg-1">`)
				campos = campos + fmt.Sprintf(`	 %s `, a.Unidad)
				campos = campos + fmt.Sprintf(`</div>`)

				campos = campos + fmt.Sprintf(`<div class="col-md-1 col-lg-1">`)
				campos = campos + fmt.Sprintf(`	<input type="hidden" id="precio%v" id="precio%v" name="precio" value="%.2f"><div class="anchototal"> %.2f </div>`, a.ID.Hex(), a.ID.Hex(), a.PreVenta, a.PreVenta)
				campos = campos + fmt.Sprintf(`</div>`)

				campos = campos + fmt.Sprintf(`<div class="col-md-1 col-lg-1">`)
				campos = campos + fmt.Sprintf(`	<input class="anchototal" type="number" id="descuento%v" min=0 max=100 value="0"  onfocus="obtenerDescuentoInicial('%v')" onKeydown="Javascript: if (event.keyCode==13) modificacionDescuento('%v');"  onblur="restauraDescuento('%v')" >`, a.ID.Hex(), a.ID.Hex(), a.ID.Hex(), a.ID.Hex())
				campos = campos + fmt.Sprintf(`</div>`)

				campos = campos + fmt.Sprintf(`<div class="col-md-1 col-lg-1">`)
				campos = campos + fmt.Sprintf(`	<input class="anchototal" type="text" id="importe%v" placeholder="$500.00"  name="total" value ="%.2f" readonly>`, a.ID.Hex(), a.PreVenta)
				campos = campos + fmt.Sprintf(`</div>`)

				campos = campos + fmt.Sprintf(`</div>`)
				result.ID = a.ID.Hex()
				result.Registro = campos
			}
		} else {
			result.ID = ""
			result.Registro = "No encontrado"
			fmt.Println("No encontrado --- controladores.ventas.PrintFields")
		}

		jData, _ := json.Marshal(result)
		w.Header().Set("Content-Type", "application/json")
		w.Write(jData)

	}

}
func ObtenerId(w http.ResponseWriter, r *http.Request) {
	cb := r.FormValue("cb")
	registros := ModeloArticulos.ConsultaCodigo(cb)
	if registros > 0 {
		a := ModeloArticulos.ConsultaCodigoYRegresaEstructuras2(cb)
		fmt.Fprintf(w, a.ID.Hex())
	} else {
		fmt.Fprintf(w, "")
	}
}

func GeneraIdCotizacion(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		idp := r.FormValue("idProducto")
		fmt.Println("retsorno de la id->", idp)
		var idCotizacion bson.ObjectId
		idCotizacion = bson.NewObjectId()

		var contz, aux Cotizacion

		contz.ID = idCotizacion
		contz.AlmacenOrigen = bson.ObjectIdHex("589e3e24eaf33c45e47c5278")
		contz.AlmacenDestino = bson.ObjectIdHex("589e4b00eaf33c23dc28304b")
		contz.IdUsuario = bson.ObjectIdHex("5895142917ce77e76d8e7bcd")
		contz.IdCliente = bson.ObjectIdHex("589e4b00eaf33c23dc28304b")
		contz.FechaHora = time.Now()
		contz.Tipo = "cotizacion"
		// hasta aqui es lo que GenerarCotizacion debe hacer

		// id_object := bson.ObjectIdHex(idp)

		// ModificarCarrito(id_prod_bson)

		collectionCot, _, err := models.ConectarMongo(models.SERVIDOR_MONGO, models.NOMBRE_BD_MONGO, models.COLECCION_MOVIMIENTOS_MONGO)
		if err != nil {
			fmt.Println("Errores: ", err)
		}
		b := true
		err = collectionCot.Insert(contz)
		if err != nil {
			b = false
			panic(err)
		}

		// b := venta_m.CrearCotizacion(contz)

		err = collectionCot.Find(bson.M{"_id": idCotizacion}).One(&aux)
		if err != nil {
			panic(err)
		}

		fmt.Println("Objeto AUX", aux)
		fmt.Println(bson.M{"_id": idCotizacion})

		if b == true {
			fmt.Println("Se ha creado la cotizacion")
			fmt.Fprintf(w, `<input id="id_cotizacion_unico" type="hidden" value="%s">`, idCotizacion.Hex())
			fmt.Println("id cotizacion->", idCotizacion)
		} else {
			fmt.Println("No se creo la cotizacion")
		}

	}
}

func regresaExistencia(identificador string) (ProductoDatos, ProductoDetalle) {
	var articulo ModeloArticulos.Articulo
	var existencias ModeloArticulos.DatosArticulo
	articulo, existencias = ModeloArticulos.ConsultaCodigoYRegresaEstructuras(identificador)
	var art ProductoDatos
	var dt ProductoDetalle
	art.IDProducto = articulo.ID.Hex()
	art.Descripcion = articulo.Descripcion
	art.Precio = articulo.PreVenta
	dt.IDProducto = existencias.ID
	dt.Cantidad = existencias.Cantidad
	dt.Estatus = existencias.Estatus
	return art, dt
}

//ManejadorCotizacion dado los datos id(producto), y los elementos previos y nuevos define si se puede
//realizar la transaccion entre usuarios y almacenes
func ManejadorCotizacion(w http.ResponseWriter, r *http.Request) {

	if r.Method == "POST" {
		r.ParseForm()

		previo := r.FormValue("valorprevio")
		nuevo := r.FormValue("valornuevo")
		desc := r.FormValue("desc")
		id_cotizacion := r.FormValue("id_cot")
		Vprevi, err := strconv.ParseFloat(previo, 64)
		if err != nil {
			panic(err)
		}
		Vnuevo, err := strconv.ParseFloat(nuevo, 64)
		if err != nil {
			panic(err)
		}
		dcto, err := strconv.ParseFloat(desc, 64)
		if err != nil {
			panic(err)
		}
		var result Transaccion

		result.ID = r.PostFormValue("ID")
		result.IDCot = id_cotizacion
		result.ValorPrevio = Vprevi
		result.ValorNuevo = Vnuevo
		result.AlmOrigen = (r.PostFormValue("almOrigen"))
		result.AlmDestino = r.PostFormValue("almdestino")
		result.IDUsr = r.PostFormValue("idusr")
		result.IDCli = r.PostFormValue("idcli")
		result.ProductoDescuento = dcto

		fmt.Println("------------ Por insertar ------------")
		fmt.Println("Id Producto: ", result.ID)
		fmt.Println("id_cotizacion: ", result.IDCot)
		fmt.Println("Valor Prev: ", result.ValorPrevio)
		fmt.Println("Valor Nuevo: ", result.ValorNuevo)
		fmt.Println("Id origen: ", result.AlmOrigen)
		fmt.Println("Id destino: ", result.AlmDestino)
		fmt.Println("Id usr: ", result.IDUsr)
		fmt.Println("Id cli: ", result.IDCli)
		fmt.Println("Descuento: ", result.ProductoDescuento)
		fmt.Println("---------------------------------------")
		var mensaje ManejoErrores

		mensaje.Exito, mensaje.Descripcion, mensaje.Valor = TransaccionCotizacion(result)
		jData, _ := json.Marshal(mensaje)
		w.Header().Set("Content-Type", "application/json")
		w.Write(jData)
	}
}

//TransaccionCotizacion dado los datos id(producto), y los elementos previos y nuevos define si se puede
//realizar la transaccion entre usuarios y almacenes
func TransaccionCotizacion(dato Transaccion) (bool, string, float64) {

	dbinfo := fmt.Sprintf("host=%s user=%s password=%s dbname=%s sslmode=disable",
		models.SERVIDOR_POSTGRESQL, models.USER_POSTGRESQL, models.PASS_POSGRESQL, models.BASE_POSTGRESQL)
	db, err := sql.Open("postgres", dbinfo)
	if err != nil {
		panic(err)
	}
	defer db.Close()
	fmt.Printf("|| StartTime: %v\n", time.Now())
	tx, err := db.Begin()
	if err != nil {
		panic(err)
	}

	db.Exec("set transaction isolation level serializable")
	stmt, err := tx.Prepare(`SELECT cantidad, "estatus" FROM public."` + dato.AlmOrigen + `" WHERE id_producto=$1 FOR UPDATE`)
	if err != nil {
		panic(err)
	}
	resultSet, err := stmt.Query(dato.ID)
	if err != nil {
		panic(err)
	}
	var e float64
	e = 0
	estatus := ""

	for resultSet.Next() {
		resultSet.Scan(&e, &estatus)
	}
	fmt.Println("Existencia Almacen: ", e, ", Estatus", estatus, "En Carrito: ", dato.ValorPrevio, "Solicitado: ", dato.ValorNuevo)
	evaluando := e + dato.ValorPrevio - dato.ValorNuevo
	if dato.ValorNuevo > e+dato.ValorPrevio {
		Mensaje := fmt.Sprintf("No se puede surtir el producto: Cantidad solicitada excede existencia. Existencia: %.3f", e+dato.ValorPrevio)
		tx.Rollback()
		fmt.Printf("|| EndTime: %v\n", time.Now())
		fmt.Println(Mensaje)
		resultSet.Close()
		stmt.Close()
		db.Close()
		return false, Mensaje, dato.ValorPrevio
	}
	if evaluando >= 0 && estatus == "ACTIVO" {
		fmt.Println("Se puede Realizar la compra. Se surte completo: ", evaluando)
		querry := fmt.Sprintf(`UPDATE public."%v"  SET  cantidad=%v WHERE id_producto='%v'`, dato.AlmOrigen, evaluando, dato.ID)

		fmt.Println(querry)
		tx.Exec(querry)

		tx.Commit()
		fmt.Printf("|| EndTime: %v\n", time.Now())
		resultSet.Close()
		stmt.Close()
		db.Close()

		fmt.Println("Insertados ", dato.ValorNuevo-dato.ValorPrevio, " en destino ", dato.AlmDestino, ".")
		fmt.Println("-------------------------------------------------")
		fmt.Println("mgo prod: ")
		transacMGO(dato)
		fmt.Println("-------------------------------------------------")

		return true, "OK", dato.ValorNuevo
	}

	Mensaje := fmt.Sprintf("No se puede surtir el producto: [Estatus: %s ---  Existencia:  %.2f] ", estatus, e)
	tx.Rollback()
	fmt.Printf("|| EndTime: %v\n", time.Now())
	fmt.Println(Mensaje)
	resultSet.Close()
	stmt.Close()
	db.Close()
	return false, Mensaje, dato.ValorPrevio

}

func transacMGO(transaccion Transaccion) {
	collection, _, err := models.ConectarMongo(models.SERVIDOR_MONGO, models.NOMBRE_BD_MONGO, models.COLECCION_PRODUCTOS)
	if err != nil {
		fmt.Println("Errores: ", err)
	}
	collectionCot, _, err := models.ConectarMongo(models.SERVIDOR_MONGO, models.NOMBRE_BD_MONGO, models.COLECCION_MOVIMIENTOS_MONGO)
	// insertar producto a cotizacion

	var detalle PreciosCostos

	err = collection.Find(bson.M{"_id": bson.ObjectIdHex(transaccion.ID)}).Select(bson.M{"_id": 1, "precio": 1, "costo": 1}).One(&detalle)
	if err != nil {
		fmt.Println("No obtiene los datos del producto")
		log.Panic(err)
		// panic(err)
	} else {
		fmt.Println("Detalles del producto: ", detalle)
	}

	var p []ProductoCV
	var prodetalle, contador ProductoCV

	prodetalle.ID = bson.ObjectIdHex(transaccion.ID)
	prodetalle.Cantidad = transaccion.ValorNuevo
	prodetalle.Costo = detalle.Costo
	prodetalle.Descuento = transaccion.ProductoDescuento
	prodetalle.Importe = detalle.Precio
	prodetalle.DescuentoTotal = detalle.Precio / 100 * prodetalle.Cantidad * transaccion.ProductoDescuento
	prodetalle.ImporteTotal = prodetalle.Cantidad * detalle.Precio
	p = append(p, prodetalle)

	collectionCot.Find(bson.M{"_id": bson.ObjectIdHex(transaccion.IDCot), "productos._id": bson.ObjectIdHex(transaccion.ID)}).Select(bson.M{"productos.$": 1}).One(&contador)

	if contador.ID.Hex() == "" {
		fmt.Println("No existe el id del producto en la lista. Se inserta uno nuevo")
		fmt.Println("insertar nuevo producto:  ", prodetalle)
		collectionCot.Update(bson.M{"_id": bson.ObjectIdHex(transaccion.IDCot)}, bson.M{"$addToSet": bson.M{"productos": bson.M{"$each": p}}})
	} else {
		if transaccion.ValorNuevo == 0 {
			// db.getCollection('movimientos').update({"_id" : ObjectId("58961b5d17ce77e76d91053f")}, {"$pull":{"productos": {"_id":ObjectId("587937e2e757701e7cb07201")}}})
			collectionCot.Update(bson.M{"_id": bson.ObjectIdHex(transaccion.IDCot)}, bson.M{"$pull": bson.M{"productos": bson.M{"_id": bson.ObjectIdHex(transaccion.ID)}}})
		} else {
			collectionCot.Update(bson.M{"_id": bson.ObjectIdHex(transaccion.IDCot), "productos._id": bson.ObjectIdHex(transaccion.ID)}, bson.M{"$set": bson.M{"productos.$": prodetalle}})
		}
	}
}

func ModifDescuento(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		r.ParseForm()
		// ID: valor,
		// id_cot: destino,
		pr := r.FormValue("precio")
		cantidad := r.FormValue("cantidad")
		desc := r.FormValue("desc")
		id_cotizacion := r.FormValue("id_cot")
		producto := r.PostFormValue("ID")
		descuento, err := strconv.ParseFloat(desc, 64)
		if err != nil {
			panic(err)
		}
		precio, err := strconv.ParseFloat(pr, 64)
		if err != nil {
			panic(err)
		}
		num, err := strconv.ParseFloat(cantidad, 64)
		if err != nil {
			panic(err)
		}

		fmt.Println("se modificaran:   ----------------------")
		fmt.Println("id_cotizacion:    ----------------------", id_cotizacion)
		fmt.Println("en producto:      ----------------------", producto)
		fmt.Println("campo descuento:  ----------------------", descuento)
		fmt.Println("fin:              ----------------------")

		var prodetalle, contador ProductoCV
		collectionCot, _, err := models.ConectarMongo(models.SERVIDOR_MONGO, models.NOMBRE_BD_MONGO, models.COLECCION_MOVIMIENTOS_MONGO)

		collectionCot.Find(bson.M{"_id": bson.ObjectIdHex(id_cotizacion), "productos._id": bson.ObjectIdHex(producto)}).Select(bson.M{"productos.$": 1}).One(&contador)
		prodetalle.ID = contador.ID
		prodetalle.Cantidad = num
		prodetalle.Costo = contador.Costo
		prodetalle.Descuento = descuento
		prodetalle.Importe = precio
		prodetalle.DescuentoTotal = precio / 100 * num * descuento
		prodetalle.ImporteTotal = num * precio

		collectionCot.Update(bson.M{"_id": bson.ObjectIdHex(id_cotizacion), "productos._id": bson.ObjectIdHex(producto)},
			bson.M{"$set": bson.M{"productos.$.descuento": prodetalle.Descuento, "productos.$.descuento_total": prodetalle.DescuentoTotal, "productos.$.importe": prodetalle.Importe}})

	}
}

func EliminarCotizacion(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		r.ParseForm()
		idCot := r.FormValue("ID")
		if idCot == "" {
			fmt.Fprintf(w, "Se recibio un id de cotización vacío")
		} else {
			collectionCot, _, err := models.ConectarMongo(models.SERVIDOR_MONGO, models.NOMBRE_BD_MONGO, models.COLECCION_MOVIMIENTOS_MONGO)

			err = collectionCot.Remove(bson.M{"_id": bson.ObjectIdHex(idCot)})
			if err != nil {
				fmt.Fprint(w, "Error al intentar eliminar la cotizacion")
				panic(err)
			} else {
				fmt.Fprint(w, "")
			}

		}

	}
}

//CargaPantallaIndex es una funcion que redirige
//al usuario a la pantalla de alta sin pasar por validaciones
func CargaPantallaIndex_frame(w http.ResponseWriter, r *http.Request) {
	userName := Session.GetUserName(r)
	if userName != "" {
		tmpl3_frame.ExecuteTemplate(w, "index_layout", nil)
	} else {
		http.Redirect(w, r, "/", 302)
	}
}

//Variables globales para búsqueda y paginación
var Ides_index []string
var numero_registros_index int
var tammuestra_index int = 8

//CargaCompras funcion que regresa los primeros artoculos de la base y recupoera todos en una variable global
func CargaVentas(w http.ResponseWriter, r *http.Request) {
	resultx := venta_m.GetAllVentas()

	if len(resultx) > 0 {
		Ides_index = nil
		for _, item := range resultx {
			iid := item.ID.Hex()
			Ides_index = append(Ides_index, iid)
		}
		numero_registros_index = len(Ides_index)
		var arrayids []bson.ObjectId
		reg := len(Ides_index)
		if reg < tammuestra_index {
			for _, valor := range Ides_index {
				hexa := bson.ObjectIdHex(valor)
				arrayids = append(arrayids, hexa)
			}
		} else {
			for _, valor := range Ides_index[0:tammuestra_index] {
				hexa := bson.ObjectIdHex(valor)
				arrayids = append(arrayids, hexa)
			}
		}
		result := venta_m.GetVentas(arrayids)
		for i := range result {
			result[i].AlmacenOrigen.Nombre = result[i].AlmacenOrigen.ID.Hex()
			result[i].AlmacenDestino.Nombre = result[i].AlmacenDestino.ID.Hex()
			result[i].UsuarioOrigen.Nombre = venta_m.ObtenerNombreUsuario(result[i].UsuarioOrigen.ID)
			result[i].UsuarioDestino.Nombre = venta_m.ObtenerNombreUsuario(result[i].UsuarioDestino.ID)
		}
		jData, _ := json.Marshal(result)
		w.Header().Set("Content-Type", "application/json")
		w.Write(jData)
	} else {
		fmt.Println("No se encontraron Datos en la Base de Datos")
	}
}

func EditaVenta_frame(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		userName := Session.GetUserName(r)
		if userName != "" {
			var id string
			vars := mux.Vars(r)
			id = vars["id"]

			venta := venta_m.GetVentaById(id)

			//Carga Combo de almacenes
			alma := venta_m.ConsultaAlmacenesMgo()

			templ := `<option value="" selected>--SELECCIONE--</option>`

			for _, v := range alma {
				if v.ID == venta.AlmacenDestino {
					templ += `<option value="` + v.ID.Hex() + `" selected>` + v.Nombre + `</option>`
				} else {
					templ += `<option value="` + v.ID.Hex() + `">` + v.Nombre + `</option>`
				}
			}

			almacenes := template.HTML(templ)

			//Carga Combo de provedores
			prove := venta_m.ConsultaProveedorMgo()

			for len(prove) == 0 {
				prove = venta_m.ConsultaProveedorMgo()
			}

			pro := `<option value="" selected>--SELECCIONA--</option>`
			//<option value="1">PROVEEDOR 1</option><option value="2">PROVEEDOR 2</option>`
			for _, v := range prove {
				if v.ID == venta.AlmacenOrigen {
					pro += `<option value=` + v.ID.Hex() + ` selected>` + v.Nombre + `</option>`
				} else {
					pro += `<option value=` + v.ID.Hex() + `>` + v.Nombre + `</option>`
				}
			}

			proveedores := template.HTML(pro)

			var elem string
			for _, v := range venta.Productos {
				articulo := ModeloCompras.GetArticle(v.ID)
				articulo = ModeloCompras.RegresaSrcImagen(articulo)
				impst := ""

				for key, val := range v.Impuestos {
					impst += key + ":" + val + ","
				}

				impst = strings.Trim(impst, ",")

				var cantidad string
				if articulo.Entero == true {
					cantidad = strconv.FormatFloat(v.Cantidad, 'f', 3, 64)
				} else {
					cantidad = strconv.FormatFloat(v.Cantidad, 'f', 0, 64)
				}

				elem += `<tr>
							<td><img widtn="50px" height="50px" src="` + articulo.TagImg + `"></td>
							<td><input type="text" class="form-control" name="c_descripcion" value="` + articulo.Descripcion + `" readonly><input type="hidden" name="c_idex" value="` + v.ID.Hex() + `"></td>
							<td><input style="width:120px;" type="text" class="form-control" name="c_costo" value="` + strconv.FormatFloat(v.Costo, 'f', 2, 32) + `" readonly></td>
							<td><input style="width:120px;" type="text" class="form-control" name="c_cantidad" value="` + cantidad + `" readonly></td>
							<td><input style="width:120px;" type="text" class="form-control" name="c_unidad" value="` + articulo.Unidad + `" readonly></td>
							<td><input style="width:120px;" type="text" class="form-control" name="c_impuesto" value="` + strconv.FormatFloat(v.TotalImpuesto, 'f', 2, 32) + `" readonly><input type="hidden" name="c_nom_impuesto" value="` + impst + `"></td>
							<td><input style="width:120px;" type="text" class="form-control" name="c_ganancia" value="` + v.Ganancia + `" readonly></td>
							<td><input style="width:120px;" type="text" class="form-control" name="c_precio" value="` + strconv.FormatFloat(articulo.PreVenta, 'f', 2, 32) + `" readonly></td>
                            <td><input style="width:120px;" type="text" class="form-control" name="c_importe" value="` + strconv.FormatFloat(v.Importe, 'f', 2, 32) + `" readonly></td>
                         </tr>`
				// <td><button type="button" class="btn btn-danger deleteButton">
				// <span class="glyphicon glyphicon-trash btn-xs"> </span></button></td>
			}

			ide := template.HTML(id)
			filas := template.HTML(elem)

			tmpl2_frame.ExecuteTemplate(w, "index_layout", map[string]interface{}{
				"almacenes":   almacenes,
				"proveedores": proveedores,
				"move":        ide,
				"productos":   filas,
			})
		} else {
			http.Redirect(w, r, "/", 302)
		}
	} else if r.Method == "POST" {
		fmt.Println("POST")
	}
}

func GetAllResultSearchElastic_index(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		page, _ := strconv.Atoi(r.FormValue("getPage"))
		fmt.Println("valores de IDS->", len(Ides_index))
		var tmpl_pag = ` <ul class="pagination pagination-lg" > `
		var numTotal int
		numTotal = len(Ides_index)
		NumPagina := float32(numTotal) / float32(tammuestra_index)
		NumPagina2 := int(NumPagina)
		if NumPagina > float32(NumPagina2) {
			NumPagina2++
		}
		fmt.Println(NumPagina2)
		if page == 1 {
			tmpl_pag = tmpl_pag + ` <li><a><i class="fa fa-angle-left"></i></a></li>`
		} else if page > 1 {
			tmpl_pag = tmpl_pag + ` <li>
                                <a onClick="getPagination(` + strconv.Itoa(page-1) + `);">
                                    <i class="fa fa-angle-left"></i>
                                </a>
                            </li>`
		}
		var i int = page - 2
		var y int = i + 4
		if NumPagina2 > 5 {
			if i >= 1 {
				if y <= NumPagina2-2 {
					for i <= y {
						if i == page {
							tmpl_pag = tmpl_pag + `<li class="page-active"><a onClick="getPagination(` + strconv.Itoa(i) + `);"> ` + strconv.Itoa(i) + ` </a></li>`
						} else {
							tmpl_pag = tmpl_pag + `<li><a onClick="getPagination(` + strconv.Itoa(i) + `);"> ` + strconv.Itoa(i) + ` </a></li>`
						}
						i++
					}
					tmpl_pag = tmpl_pag + `<li><a> ... </a></li>`
					tmpl_pag = tmpl_pag + `<li><a onClick="getPagination(` + strconv.Itoa(NumPagina2) + `);"> ` + strconv.Itoa(NumPagina2) + ` </a></li>`
				} else {
					var z int = NumPagina2 - 4

					tmpl_pag = tmpl_pag + `<li><a onClick="getPagination(` + strconv.Itoa(1) + `);"> ` + strconv.Itoa(1) + ` </a></li>`
					tmpl_pag = tmpl_pag + `<li><a> ... </a></li>`
					for z <= NumPagina2 {
						if z == page {
							tmpl_pag = tmpl_pag + `<li class="page-active"><a onClick="getPagination(` + strconv.Itoa(z) + `);"> ` + strconv.Itoa(z) + ` </a></li>`
						} else {
							tmpl_pag = tmpl_pag + `<li><a onClick="getPagination(` + strconv.Itoa(z) + `);"> ` + strconv.Itoa(z) + ` </a></li>`
						}
						z++
					}
				}
			} else if i <= 0 {
				var x int = 1
				for x <= 5 {
					if x == page {
						tmpl_pag = tmpl_pag + `<li class="page-active"><a onClick="getPagination(` + strconv.Itoa(x) + `);"> ` + strconv.Itoa(x) + ` </a></li>`
					} else {
						tmpl_pag = tmpl_pag + `<li><a onClick="getPagination(` + strconv.Itoa(x) + `);"> ` + strconv.Itoa(x) + ` </a></li>`
					}
					x++
				}
				tmpl_pag = tmpl_pag + `<li><a> ... </a></li>`
				tmpl_pag = tmpl_pag + `<li><a onClick="getPagination(` + strconv.Itoa(NumPagina2) + `);"> ` + strconv.Itoa(NumPagina2) + ` </a></li>`
			}
		} else {
			var x int = 1
			for x <= NumPagina2 {
				if i == page {
					tmpl_pag = tmpl_pag + `<li class="page-active"><a onClick="getPagination(` + strconv.Itoa(x) + `);"> ` + strconv.Itoa(x) + ` </a></li>`
				} else {
					tmpl_pag = tmpl_pag + `<li><a onClick="getPagination(` + strconv.Itoa(x) + `);"> ` + strconv.Itoa(x) + ` </a></li>`
				}
				x++
			}
			tmpl_pag = tmpl_pag + `<li><a> ... </a></li>`
			tmpl_pag = tmpl_pag + `<li><a onClick="getPagination(` + strconv.Itoa(NumPagina2) + `);"> ` + strconv.Itoa(NumPagina2) + ` </a></li>`
		}

		if page == NumPagina2 {
			tmpl_pag = tmpl_pag + `<li><a><i class="fa fa-angle-right"></i></a></li>`
		} else if page < NumPagina2 {
			tmpl_pag = tmpl_pag + `<li>
                                <a onClick="getPagination(` + strconv.Itoa(page+1) + `);">
                                    <i class="fa fa-angle-right"></i>
                                </a>
                            </li>`
		}

		tmpl_pag = tmpl_pag + ` </ul> `

		fmt.Fprintf(w, tmpl_pag)

	}

}

//GetSearchPaginationGeneral funcion que regresa artículos de correspondiente paginación
func GetSearchPaginationGeneral_index(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {

		pageS := r.FormValue("num_page")
		page, _ := strconv.Atoi(pageS)

		lim := tammuestra_index
		skip := (page * lim) - lim
		limite := skip + lim
		fmt.Println("Solicita la página: ", page, " del número: ", skip, " al: ", limite)
		result, _ := GetValuesDataMongodbPagination_index(limite, skip, page)
		jData, _ := json.Marshal(result)
		w.Header().Set("Content-Type", "application/json")
		w.Write(jData)
	}
}

//GetValuesDataMongodbPagination_index funcion obtiene de un conjunto de Ids
//limit es el rango final de la segunda página
//skip desde donde va a comenzar
//Si es una página final entonces debe ser el ´límite del tamaño del arreglo de Ids

func GetValuesDataMongodbPagination_index(limit int, skip int, page int) ([]venta_m.MovimientoIndex, int) {
	var arrayids []bson.ObjectId
	numpags := float32(numero_registros_index) / float32(tammuestra_index)
	numpags2 := int(numpags)
	if numpags > float32(numpags2) {
		numpags2++
		fmt.Println("se aumentó uno el total")
	}

	if page == numpags2 {
		final := len(Ides_index) % tammuestra_index
		fmt.Println(final)
		if final == 0 {
			for _, valor := range Ides_index[skip:limit] {
				hexa := bson.ObjectIdHex(valor)
				arrayids = append(arrayids, hexa)
			}
			fmt.Println("Se consultó última página: ", page, "del: ", skip, " al ", limit)
		} else {
			for _, valor := range Ides_index[skip : skip+final] {
				hexa := bson.ObjectIdHex(valor)
				arrayids = append(arrayids, hexa)
			}
			fmt.Println("Se consultó última página: ", page, "del: ", skip, " al ", skip+final)
		}

	} else {
		for _, valor := range Ides_index[skip:limit] {
			hexa := bson.ObjectIdHex(valor)
			arrayids = append(arrayids, hexa)
		}

	}

	result := venta_m.GetVentas(arrayids)

	for i := range result {
		result[i].AlmacenOrigen.Nombre = result[i].AlmacenOrigen.ID.Hex()
		result[i].AlmacenDestino.Nombre = result[i].AlmacenDestino.ID.Hex()
		result[i].UsuarioOrigen.Nombre = venta_m.ObtenerNombreUsuario(result[i].UsuarioOrigen.ID)
		result[i].UsuarioDestino.Nombre = venta_m.ObtenerNombreUsuario(result[i].UsuarioDestino.ID)
	}

	return result, numero_registros_index
}
