package venta

import (
	"fmt"
	"net/http"
	"strconv"

	"../../Modelos/BuscarArticulosElastic"

	_ "gopkg.in/mgo.v2/bson"
)

var Ids []string
var numero_registros int64

func SearchArticleElasticInSell(w http.ResponseWriter, r *http.Request) {
	Ids = nil
	numero_registros = 0
	if r.Method == "POST" {
		r.ParseForm()
		dato := r.FormValue("searchtext")
		//		fmt.Println(dato)

		if dato != "" {
			conexionElastic := search_elastic.ConectarElastic()
			docs := search_elastic.BuscarEnElastic(dato, conexionElastic)

			if docs.Hits.TotalHits > 0 {
				numero_registros = docs.Hits.TotalHits
				fmt.Printf("\n\t %d registros encontradas en %d  milisegundos\n", docs.Hits.TotalHits, docs.TookInMillis)
				for _, item := range docs.Hits.Hits {
					iid := item.Id
					Ids = append(Ids, iid)
				}
				fmt.Println("Número de reg->", numero_registros)
				//				fmt.Println(Ids)

			} else {
				fmt.Println("No se encontró algún registro")
			}

		} else {
			fmt.Println("Sin datos que consultar")
		}
		http.Redirect(w, r, "/valuePaginatioElasticResult", 302)
	} else if r.Method == "GET" {
	}
}

func GetProductsIdTo_Info_Paginatio(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Entrando en get product .... valuepaginatioelasticresu")
	if r.Method == "POST" {

		pages := r.FormValue("page")
		var page int = 1
		if pages != "" {
			page, _ = strconv.Atoi(pages)
		} else {
			page = 1
		}

		lim := 5
		skip := (lim * page) - lim

		var limite int
		if skip+lim > int(numero_registros) {
			limite = int(numero_registros)
		} else {
			limite = skip + lim
		}

		IdsCopy := Ids[skip:limite]

		var idsRange []string
		//		fmt.Println(Ids)
		for _, v := range IdsCopy {
			//				fmt.Println("k->", k, "  v->", v)
			idsRange = append(idsRange, v)
		}
		//		fmt.Println(idsRange)

		result := search_elastic.GetArticleMongoToRangePagination(idsRange)
		//		fmt.Println(result)

		for _, v := range result {
			//			fmt.Println(v.Descripcion)

			tmp_img := search_elastic.GetImagenIdSearch(v.Imagen)

			fmt.Fprintf(w, `<div class="row-fluid row">`)

			fmt.Fprintf(w, `<div class="span2 col-md-2">`)
			//			fmt.Fprintf(w, `<img src="../../Recursos/Local/Img/images.jpg" alt="Smiley face" style="width:100px">`)
			fmt.Fprintf(w, tmp_img)
			fmt.Fprintf(w, `</div>`)
			fmt.Fprintf(w, `<div class="span10 col-md-10">`)
			fmt.Fprintf(w, `<h4>%s</h4>`, v.Descripcion)
			fmt.Fprintf(w, `<button type="button" class="btn btn-primary btn-sm btn-add-v" onClick="SetAddRegistro('%s');" data-dismiss="modal" >Añadir producto</button>`, v.CodBarra)
			fmt.Fprintf(w, `</div>`)
			fmt.Fprintf(w, `</div>`)

		}

	}
}

func PlacePagination(w http.ResponseWriter, r *http.Request) {

	if r.Method == "POST" {

		var page int
		pageV := r.FormValue("page")
		if pageV != "" {
			page, _ = strconv.Atoi(pageV)
		} else {
			page = 1
		}

		var n int = int(numero_registros)

		lim := 5
		//		skip := (page * lim) - lim

		NumPagina := float32(n) / float32(lim)
		pp := int(NumPagina)
		if NumPagina > float32(pp) {
			pp++
		}
		fmt.Println("Numero de paginas->", pp)

		var i int = 1
		fmt.Fprintf(w, `<ul class="pagination justify-content-center">`)

		if page != 1 {
			fmt.Fprintf(w, `<li class="page-item">
							      	<a class="page-link" href="#" onClick="ReloadPage(%d);" tabindex="-1">Anterior</a>
							    </li>`, page-1)
		} else if page == 1 {
			fmt.Fprintf(w, `<li class="page-item disabled">
					      	<a class="page-link" href="#" tabindex="-1">Anterior</a>
					    </li>`)
		}

		for i <= pp {
			if i == page {
				fmt.Fprintf(w, `<li class="page-item active">
							      	<a class="page-link" href="#">%d</a>
							    </li>`, i)
			} else {
				fmt.Fprintf(w, `<li class="page-item">
									<a class="page-link" onClick="ReloadPage(%d);" href="#">%d</a>
								</li>`, i, i)
			}
			i++
		}
		if page == pp || pp == 0 {
			fmt.Fprintf(w, `<li class="page-item disabled">
					      	<a class="page-link" href="#">Siguiente</a>
					    </li></ul>`)
		} else if pp != 0 {
			fmt.Fprintf(w, `<li class="page-item">
					      	<a class="page-link" href="#" onClick="ReloadPage(%d);">Siguiente</a>
					    </li></ul>`, page+1)
		}

	}
}

/*
 *@melchor
 *Genero una copia del metodo de busqueda de caleb para poder darle un trato diferente al seleccionar el SKU
 */
func SearchArticleElasticInSell2(w http.ResponseWriter, r *http.Request) {
	Ids = nil
	numero_registros = 0
	if r.Method == "POST" {
		r.ParseForm()
		dato := r.FormValue("searchtext")
		//		fmt.Println(dato)

		if dato != "" {
			conexionElastic := search_elastic.ConectarElastic()
			docs := search_elastic.BuscarEnElastic(dato, conexionElastic)

			if docs.Hits.TotalHits > 0 {
				numero_registros = docs.Hits.TotalHits
				fmt.Printf("\n\t %d registros encontradas en %d  milisegundos\n", docs.Hits.TotalHits, docs.TookInMillis)
				for _, item := range docs.Hits.Hits {
					iid := item.Id
					Ids = append(Ids, iid)
				}
				fmt.Println("Número de reg->", numero_registros)
				//				fmt.Println(Ids)

			} else {
				fmt.Println("No se encontró algún registro")
			}

		} else {
			fmt.Println("Sin datos que consultar")
		}
		http.Redirect(w, r, "/valuePaginatioElasticResult2", 302)
	} else if r.Method == "GET" {
	}
}

/*
 *@melchor
 *Genero una copia del metodo de busqueda de caleb para poder darle un trato diferente al seleccionar el SKU
 */
func GetProductsIdTo_Info_Paginatio2(w http.ResponseWriter, r *http.Request) {

	if r.Method == "POST" {

		pages := r.FormValue("page")
		var page int = 1
		if pages != "" {
			page, _ = strconv.Atoi(pages)
		} else {
			page = 1
		}

		lim := 5
		skip := (lim * page) - lim

		var limite int
		if skip+lim > int(numero_registros) {
			limite = int(numero_registros)
		} else {
			limite = skip + lim
		}

		IdsCopy := Ids[skip:limite]

		var idsRange []string
		//		fmt.Println(Ids)
		for _, v := range IdsCopy {
			//				fmt.Println("k->", k, "  v->", v)
			idsRange = append(idsRange, v)
		}
		//		fmt.Println(idsRange)

		result := search_elastic.GetArticleMongoToRangePagination(idsRange)
		//fmt.Println(result)

		for _, v := range result {
			//fmt.Println(v.Descripcion)

			tmp_img := search_elastic.GetImagenIdSearch(v.Imagen)

			fmt.Fprintf(w, `<div class="row-fluid row">`)

			fmt.Fprintf(w, `<div class="span2 col-md-2">`)
			//			fmt.Fprintf(w, `<img src="../../Recursos/Local/Img/images.jpg" alt="Smiley face" style="width:100px">`)
			fmt.Fprintf(w, tmp_img)
			fmt.Fprintf(w, `</div>`)
			fmt.Fprintf(w, `<div class="span10 col-md-10">`)
			//fmt.Fprintf(w, `<h4>%v</h4>`, v.ID.Hex())
			fmt.Fprintf(w, `<h4>%s</h4>`, v.Descripcion)
			fmt.Fprintf(w, `<button type="button" class="btn btn-primary btn-sm btn-add-v" onClick="SetAddCodeArt('%s','%v');" data-dismiss="modal" >Seleccionar</button>`, v.Descripcion, v.ID.Hex())
			fmt.Fprintf(w, `</div>`)
			fmt.Fprintf(w, `</div>`)

		}

	}
}

/*
 *@melchor
 * Busqueda para modulo de traslados
 */
func SearchArticleElasticInSell3(w http.ResponseWriter, r *http.Request) {
	Ids = nil
	numero_registros = 0
	if r.Method == "POST" {
		r.ParseForm()
		dato := r.FormValue("searchtext")
		//		fmt.Println(dato)

		if dato != "" {
			conexionElastic := search_elastic.ConectarElastic()
			docs := search_elastic.BuscarEnElastic(dato, conexionElastic)

			if docs.Hits.TotalHits > 0 {
				numero_registros = docs.Hits.TotalHits
				fmt.Printf("\n\t %d registros encontradas en %d  milisegundos\n", docs.Hits.TotalHits, docs.TookInMillis)
				for _, item := range docs.Hits.Hits {
					iid := item.Id
					Ids = append(Ids, iid)
				}
				fmt.Println("Número de reg->", numero_registros)
				//				fmt.Println(Ids)

			} else {
				fmt.Println("No se encontró algún registro")
			}

		} else {
			fmt.Println("Sin datos que consultar")
		}
		http.Redirect(w, r, "/valuePaginatioElasticResult3", 302)
	} else if r.Method == "GET" {
	}
}

/*
 *@melchor
 *Modulo de busqueda para traslados
 */
func GetProductsIdTo_Info_Paginatio3(w http.ResponseWriter, r *http.Request) {

	if r.Method == "POST" {

		pages := r.FormValue("page")
		var page int = 1
		if pages != "" {
			page, _ = strconv.Atoi(pages)
		} else {
			page = 1
		}

		lim := 5
		skip := (lim * page) - lim

		var limite int
		if skip+lim > int(numero_registros) {
			limite = int(numero_registros)
		} else {
			limite = skip + lim
		}

		IdsCopy := Ids[skip:limite]

		var idsRange []string
		//		fmt.Println(Ids)
		for _, v := range IdsCopy {
			//				fmt.Println("k->", k, "  v->", v)
			idsRange = append(idsRange, v)
		}
		//		fmt.Println(idsRange)

		result := search_elastic.GetArticleMongoToRangePagination(idsRange)
		//fmt.Println(result)

		for _, v := range result {
			//fmt.Println(v.Descripcion)

			tmp_img := search_elastic.GetImagenIdSearch(v.Imagen)

			fmt.Fprintf(w, `<div class="row-fluid row">`)

			fmt.Fprintf(w, `<div class="span2 col-md-2">`)
			//			fmt.Fprintf(w, `<img src="../../Recursos/Local/Img/images.jpg" alt="Smiley face" style="width:100px">`)
			fmt.Fprintf(w, tmp_img)
			fmt.Fprintf(w, `</div>`)
			fmt.Fprintf(w, `<div class="span10 col-md-10">`)
			//fmt.Fprintf(w, `<h4>%v</h4>`, v.ID.Hex())
			fmt.Fprintf(w, `<h4>%s</h4>`, v.Descripcion)
			fmt.Fprintf(w, `<button type="button" class="btn btn-primary btn-sm btn-add-v" onClick="SetCodigoTraslado('%s','%v');" data-dismiss="modal" >Seleccionar</button>`, v.Descripcion, v.CodBarra)
			fmt.Fprintf(w, `</div>`)
			fmt.Fprintf(w, `</div>`)

		}

	}
}
