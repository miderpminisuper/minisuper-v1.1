package almacenes

import (
	"fmt"
	"html/template"
	"net/http"

	"../../Modelos/Almacenes"
	"../../Modelos/General"
	"../Session"
	_ "github.com/lib/pq"
	"gopkg.in/mgo.v2/bson"
)

type Almacen struct {
	Id      bson.ObjectId     `bson:"_id,omitempty"`
	Nombre  string            `bson:"nombre"`
	Tipo    string            `bson:"tipo"`
	Defecto string            `bson:"defecto"`
	Campos  map[string]string `bson:"datos"`
}

var tmpl_almacenes = template.Must(template.ParseFiles(
	"Vistas/index.html",
	"Vistas/Parciales/Plantilla/header.html",
	"Vistas/Parciales/Plantilla/footer.html",
	"Vistas/Parciales/Plantilla/styles.html",
	"Vistas/Parciales/Plantilla/scripts.html",
	"Vistas/Parciales/Paginas/Almacenes/index.html",
))

var tmpl_almacenes_frame = template.Must(template.ParseFiles(
	"Vistas/index.html",
	"Vistas/Parciales/Plantilla/header_blank.html",
	"Vistas/Parciales/Plantilla/footer_blank.html",
	"Vistas/Parciales/Plantilla/styles.html",
	"Vistas/Parciales/Plantilla/scripts.html",
	"Vistas/Parciales/Paginas/Almacenes/index.html",
))

func Index(w http.ResponseWriter, r *http.Request) {
	userName := Session.GetUserName(r)
	if userName != "" {
		result := almacenes_m.GetAlmacenes()
		tmpl_almacenes.ExecuteTemplate(w, "index_layout", result)
	} else {
		http.Redirect(w, r, "/login", 302)
	}
}

func AlmacenesFrame(w http.ResponseWriter, r *http.Request) {
	userName := Session.GetUserName(r)
	if userName != "" {
		result := almacenes_m.GetAlmacenes()
		tmpl_almacenes_frame.ExecuteTemplate(w, "index_layout", result)
	} else {
		http.Redirect(w, r, "/login", 302)
	}
}

func Crear(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {

		var tmp0 = `	<div class="page-header">
							<h3 class="text-center">Nuevo Almacén : </h3>
						</div>
							<div class="input-group input-group-md">
								<span class="input-group-addon">Nombre de almacén: </span>
								<input type="text" name="nombre_almacen" id="nombre_almacen" value="" class="form-control">
							</div>

							<div class="input-group input-group-md">
								<span class="input-group-addon">Tipo de almacén:
									<label class="radio-inline"><input type="radio" name="tipo_almacen" id="tipo_almacen" value ="fisico" checked>Físico</label>
									<label class="radio-inline"><input type="radio" name="tipo_almacen" id="tipo_almacen" value ="logico">Lógico</label>
								 </span>
							</div>

							<div class="input-group input-group-md">
								<span class="input-group-addon">Almacén por defecto:
									<label class="radio-inline"><input type="radio" name="almacen_defecto" id="almacen_defecto" value ="si">Si</label>
									<label class="radio-inline"><input type="radio" name="almacen_defecto" id="almacen_defecto" value ="no"checked>No</label>
								 </span>
							</div>

							<div class="input-group input-group-md">
							<span class="input-group-addon">Campos Extras:
								<input type="button" class="btn btn-success" onClick="Agregarcampo();" value="+" name="agregarcampo" id="agregarcampo">
								<input type="button" class="btn btn-danger" onClick="Eliminarcampo();" value="-" name="eliminarcampo" id="eliminarcampo">
								</span>
                   			</div>

							<div class="input-group input-group-md">
								<span class="input-group-addon">
									<input type="button" class="btn btn-primary" onClick="GuardarAlmacen();" value="Guardar" name="guardaralmacen" id="guardaralmacen">
								</span>
							</div>`

		fmt.Fprintf(w, tmp0)

	}
}

func Guardar(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {

		var m = make(map[string]string)
		var s1 = []string{}
		var s2 = []string{}
		var i int = 0
		var almacencillo Almacen

		id := bson.NewObjectId()

		nombre_almacen := r.FormValue("nombre_almacen")
		tipo_almacen := r.FormValue("tipo_almacen")
		defecto_almacen := r.FormValue("defecto_almacen")

		for k, v := range r.PostForm {
			if k == "claves_k[]" {
				s1 = v
			}
			if k == "valores_k[]" {
				s2 = v
			}
		}

		lim := len(s1)
		for i = 0; i < lim; i++ {
			m[s1[i]] = s2[i]
		}

		almacencillo = Almacen{Id: id, Nombre: nombre_almacen, Tipo: tipo_almacen, Defecto: defecto_almacen, Campos: m}
		insertado := almacenes_m.InsertarAlmacen(almacencillo)

		nombre_almacen_pg := id.Hex()

		dbserv := models.ConectarPostgres(models.USER_POSTGRESQL, models.PASS_POSGRESQL, models.BASE_POSTGRESQL, models.SERVIDOR_POSTGRESQL, models.PORT_POSTGRESQL, "disable")

		query := `CREATE TABLE ` + `"` + nombre_almacen_pg + `"` + `()`
		id_producto := `ALTER TABLE ` + `"` + nombre_almacen_pg + `"` + `ADD COLUMN id_producto VARCHAR(25)`
		cantidad := `ALTER TABLE ` + `"` + nombre_almacen_pg + `"` + `ADD COLUMN cantidad NUMERIC`
		estatus := `ALTER TABLE ` + `"` + nombre_almacen_pg + `"` + `ADD COLUMN estatus VARCHAR(30)`

		_, _ = dbserv.Exec(query)
		_, _ = dbserv.Exec(id_producto)
		_, _ = dbserv.Exec(cantidad)
		_, _ = dbserv.Exec(estatus)

		if insertado == true {
			fmt.Fprintf(w, "Insertado")
		} else {
			fmt.Fprintf(w, "No insertado")
		}

	}
}

func Editar(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		var contador int = 0
		ids := r.FormValue("id")
		result := almacenes_m.GetAlmacen(ids)

		var tmp0id = `<div class="input-group input-group-md">
						<span class="input-group-addon">Id: </span>
						<input type="text" name="id_" id="id_" value="%v" class="form-control" disabled>
					</div>`

		var tmp1nombre = `<div class="input-group input-group-md">
								<span class="input-group-addon">Nombre de almacén: </span>
								<input type="text" name="nombre_" id="nombre_" value="%v" class="form-control">
							</div>`

		var tmp1fis = `	<div class="input-group input-group-md">
								<span class="input-group-addon">Tipo de almacén:
									<label class="radio-inline"><input type="radio" name="tipo_" id="tipo_" value ="fisico" checked>Físico</label>
									<label class="radio-inline"><input type="radio" name="tipo_" id="tipo_" value ="logico">Lógico</label>
								 </span>
						</div>`

		var tmp1log = `	<div class="input-group input-group-md">
								<span class="input-group-addon">Tipo de almacén:
									<label class="radio-inline"><input type="radio" name="tipo_" id="tipo_" value ="fisico">Físico</label>
									<label class="radio-inline"><input type="radio" name="tipo_" id="tipo_" value ="logico"checked>Lógico</label>
								 </span>
						</div>`

		var tmp3defsi = `<div class="input-group input-group-md">
								<span class="input-group-addon">Almacén por defecto:
									<label class="radio-inline"><input type="radio" name="almacen_d" id="almacen_d" value ="si"checked>Si</label>
									<label class="radio-inline"><input type="radio" name="almacen_d" id="almacen_d" value ="no">No</label>
								 </span>
						</div>`

		var tmp3defno = `<div class="input-group input-group-md">
								<span class="input-group-addon">Almacén por defecto:
									<label class="radio-inline"><input type="radio" name="almacen_d" id="almacen_d" value ="si">Si</label>
									<label class="radio-inline"><input type="radio" name="almacen_d" id="almacen_d" value ="no"checked>No</label>
								 </span>
						</div>`

		var tmp4Datos = `<div class="input-group input-group-md claves" id="valores_%d">
											<span class="input-group-addon">Clave: </span>
											<input type="text" name="clave_%d" id="clave_%d" value="%v" class="form-control clave">
											<span class="input-group-addon">Valor: </span>
											<input type="text" name="valor_%d" id="valor_%d" value="%v" class="form-control valor">
											<span class="input-group-addon btn btn-danger eliminar">-</span>
										</div>`

		var tmp5edit = `<div class="input-group input-group-md">
							<span class="input-group-addon">								
								<input type="button" class="btn btn-lg btn-success" value="+" name="agregarcampo_e" id="agregarcampo_e">
							</span>
						</div>`

		fmt.Fprintf(w, tmp0id, result.Id.Hex())

		fmt.Fprintf(w, tmp1nombre, result.Nombre)

		if result.Tipo == "fisico" {
			fmt.Fprintf(w, tmp1fis)
		} else {
			fmt.Fprintf(w, tmp1log)
		}

		if result.Defecto == "si" {
			fmt.Fprintf(w, tmp3defsi)
		} else {
			fmt.Fprintf(w, tmp3defno)
		}

		fmt.Fprintf(w, tmp5edit)

		for key, value := range result.Campos {
			fmt.Fprintf(w, tmp4Datos, contador, contador, contador, key, contador, contador, value)
			contador++
		}

	}

}

func GuardarEditar(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {

		var m = make(map[string]string)
		var s1 = []string{}
		var s2 = []string{}
		var i int = 0
		var almacencillo Almacen

		id := r.FormValue("id")
		nombre_almacen := r.FormValue("nombre_almacen")
		tipo_almacen := r.FormValue("tipo_almacen")
		defecto_almacen := r.FormValue("defecto_almacen")

		for k, v := range r.PostForm {
			if k == "claves_k[]" {
				s1 = v
			}
			if k == "valores_k[]" {
				s2 = v
			}
		}

		lim := len(s1)
		for i = 0; i < lim; i++ {
			m[s1[i]] = s2[i]
		}

		objectid := bson.ObjectIdHex(id)

		almacencillo = Almacen{Nombre: nombre_almacen, Tipo: tipo_almacen, Defecto: defecto_almacen, Campos: m}

		insertado := almacenes_m.EditarAlmacen(almacencillo, objectid)

		if insertado == true {
			fmt.Fprintf(w, "Insertado")
		} else {
			fmt.Fprintf(w, "No insertado")
		}

	}
}

func Eliminar(w http.ResponseWriter, r *http.Request) {
	id := r.FormValue("id")
	objectid := bson.ObjectIdHex(id)

	fmt.Println(id)
	fmt.Println(objectid)

	eliminado := almacenes_m.EliminarAlmacen(objectid)
	if eliminado == true {
		fmt.Fprintf(w, "Eliminado")
	} else {
		fmt.Fprintf(w, "No eliminado")
	}
}
