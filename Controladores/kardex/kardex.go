package kardex

import (
	"fmt"
	"html/template"
	"net/http"
	_ "strconv"

	"../Session"
	"gopkg.in/mgo.v2/bson"

	"../../Modelos/Almacenes"
	_ "../../Modelos/Clientes"
	"../../Modelos/Kardex"
	_ "../../Modelos/Usuarios"
)

var tmpl_pagos = template.Must(template.ParseFiles(
	"Vistas/index.html",
	"Vistas/Parciales/Plantilla/header.html",
	"Vistas/Parciales/Plantilla/footer.html",
	"Vistas/Parciales/Plantilla/styles.html",
	"Vistas/Parciales/Plantilla/scripts.html",
	"Vistas/Parciales/Paginas/Kardex/index.html",
))

var tmpl_pagos_frame = template.Must(template.ParseFiles(
	"Vistas/index.html",
	"Vistas/Parciales/Plantilla/header_blank.html",
	"Vistas/Parciales/Plantilla/footer_blank.html",
	"Vistas/Parciales/Plantilla/styles.html",
	"Vistas/Parciales/Plantilla/scripts.html",
	"Vistas/Parciales/Paginas/Kardex/index.html",
))

/*
@melchormendoza
Trae la vista principal
@params
	null
@return
	null
*/
func Index(w http.ResponseWriter, r *http.Request) {
	userName := Session.GetUserName(r)
	if userName != "" {
		almacenes := almacenes_m.GetAlmacenes()
		tmpl_pagos.ExecuteTemplate(w, "index_layout", map[string]interface{}{
			"almacenes": almacenes,
		})
	} else {
		http.Redirect(w, r, "/login", 302)
	}
}

func IndexFrame(w http.ResponseWriter, r *http.Request) {
	userName := Session.GetUserName(r)
	if userName != "" {
		almacenes := almacenes_m.GetAlmacenes()
		tmpl_pagos_frame.ExecuteTemplate(w, "index_layout", map[string]interface{}{
			"almacenes": almacenes,
		})
	} else {
		http.Redirect(w, r, "/login", 302)
	}
}

/*
@melchormendoza
Genera el reporte de kardex de un articulo.
@params:
	ids: sku
	idAlmacen: Almacen
	finicial: fecha de inicio
	ffinal: fecha final
@return
	vista HTML
*/
func GetKardex(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		/*
		 * Recibo parametros para consulta de kardex
		 */
		ids := r.PostFormValue("id")
		idAlmacen := r.PostFormValue("idAlmacen")
		finicial := r.PostFormValue("finicial")
		ffinal := r.PostFormValue("ffinal")
		var saldo float64
		var array_ids []bson.ObjectId
		/*
		 * Voy por todos los movimientos para luego filtrar donde se encuentr el sku buscado.
		 */
		result := kardex_modelo.GetMovimientos(idAlmacen, finicial, ffinal)

		for _, valor := range result {
			for _, value := range valor.Productos {
				if value.Id.Hex() == ids {
					array_ids = append(array_ids, valor.Id)
				}
			}
		}
		/*
		 *Voy por los movimientos que contienen el sku buscado
		 */
		result2 := kardex_modelo.GetArrayMovimientos(array_ids)
		/*
		 *Voy por el inventario del sku a postgres
		 */
		inventario := kardex_modelo.GetInventario(idAlmacen, ids)

		if len(result2) > 0 {
			var nombre_alm_origen string
			var nombre_alm_destino string
			var nombre_us_origen string
			var nombre_us_destino string
			var cantidad float64
			var costo float64

			fmt.Fprintf(w, `<table class="table table-hover table-sm table-responsive">`)
			fmt.Fprintf(w, `<thead>`)
			fmt.Fprintf(w, `<tr>`)
			fmt.Fprintf(w, `<th>Fecha</th>`)
			fmt.Fprintf(w, `<th>Movimiento</th>`)
			fmt.Fprintf(w, `<th>Alm-origen</th>`)
			fmt.Fprintf(w, `<th>Alm-destino</th>`)
			fmt.Fprintf(w, `<th>Us-origen</th>`)
			fmt.Fprintf(w, `<th>Us-destino</th>`)
			fmt.Fprintf(w, `<th>Cantidad</th>`)
			fmt.Fprintf(w, `<th>Valor</th>`)
			fmt.Fprintf(w, `<th>Saldo</th>`)
			fmt.Fprintf(w, `<th>Referencia</th>`)
			fmt.Fprintf(w, `</tr>`)
			fmt.Fprintf(w, `</thead>`)
			for _, v := range result2 {
				fmt.Fprintf(w, `<tbody>`)
				fmt.Fprintf(w, `<tr>`)
				fmt.Fprintf(w, `<td>%s</td>`, v.FechaHora.Format("2006-01-02"))
				fmt.Fprintf(w, `<td>%s</td>`, v.TipoMovimiento)
				if v.Origen != "" {
					nombre_alm_origen = kardex_modelo.GetAlmacenById(v.Origen)
				}
				fmt.Fprintf(w, `<td>%s</td>`, nombre_alm_origen)

				if v.Destino != "" {
					nombre_alm_destino = kardex_modelo.GetAlmacenById(v.Destino)
				}
				fmt.Fprintf(w, `<td>%s</td>`, nombre_alm_destino)
				if v.UsOrigen != "" {
					nombre_us_origen = kardex_modelo.GetUsuarioById(v.UsOrigen)
				}
				fmt.Fprintf(w, `<td>%s</td>`, nombre_us_origen)
				if v.UsDestino != "" {
					nombre_us_destino = kardex_modelo.GetUsuarioById(v.UsDestino)
				}
				fmt.Fprintf(w, `<td>%s</td>`, nombre_us_destino)
				for _, x := range v.Productos {
					if x.Id.Hex() == ids {
						cantidad = x.Cantidad
					}
				}
				saldo = saldo + cantidad
				fmt.Fprintf(w, `<td>%v</td>`, cantidad)
				if ids != "" {
					costo = kardex_modelo.GetProductoById(ids)
				}
				fmt.Fprintf(w, `<td>%f</td>`, costo*float64(cantidad))
				fmt.Fprintf(w, `<td>%f</td>`, inventario+saldo)
				fmt.Fprintf(w, `<td><buttont type="button" class="btn btn-primary" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-target=".modal-doc" onclick="getDocumento('%v')">VER DOC</button></td>`, v.Id.Hex())

				fmt.Fprintf(w, `</tr>`)
				fmt.Fprintf(w, `</tbody>`)
			}

			fmt.Fprintf(w, `</table>`)
			fmt.Fprintf(w, `<script>$("#si").html(%f);</script>`, inventario)
			fmt.Fprintf(w, `<script>$("#sf").html(%f);</script>`, inventario+saldo)
		} else {
			fmt.Fprintf(w, `<div class="alert alert-warning" role="alert"><strong>Ups!</strong> No se encontraron movimientos.</div>`)
		}
	}

}

/*
@melchormendoza
Genera el documento

*/
func GetDocumento(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		/*
		 * Recibo parametros para consulta de kardex
		 */
		ids := r.PostFormValue("id")
		/*
		 *Voy por los movimientos que contienen el sku buscado
		 */
		result := kardex_modelo.GetMovimientoById(ids)
		/*
		 *Voy por el inventario del sku a postgres
		 */
		//inventario := kardex_modelo.GetInventario(idAlmacen, ids)
		if len(result) > 0 {

			var cantidad float64
			var total float64
			var nombre_art string
			//var costo float64
			fmt.Fprintf(w, `<table class="table table-hover table-sm table-responsive">`)
			for _, v := range result {
				fmt.Fprintf(w, `<thead>`)
				fmt.Fprintf(w, `<tr>`)
				fmt.Fprintf(w, `<th>Documento: %s</th><th></th>`, ids)
				fmt.Fprintf(w, `</tr>`)
				fmt.Fprintf(w, `<tr>`)
				fmt.Fprintf(w, `<th>Fecha: %s</th><th></th>`, v.FechaHora.Format("2006-01-02 12:01:30"))
				fmt.Fprintf(w, `</tr>`)
				fmt.Fprintf(w, `<tr>`)
				fmt.Fprintf(w, `<th>Tipo de movimiento: %s</th><th></th>`, v.TipoMovimiento)
				fmt.Fprintf(w, `</tr>`)
				fmt.Fprintf(w, `<tr>`)
				fmt.Fprintf(w, `<th>Descripción</th>`)
				fmt.Fprintf(w, `<th>Cantidad</th>`)
				fmt.Fprintf(w, `</tr>`)
				fmt.Fprintf(w, `</thead>`)
				fmt.Fprintf(w, `<tbody>`)

				for _, x := range v.Productos {
					//if x.Id.Hex() == ids {
					fmt.Fprintf(w, `<tr>`)
					cantidad = x.Cantidad
					if x.Id.Hex() != "" {
						nombre_art = kardex_modelo.GetProductoNombreById(x.Id.Hex())
					}
					fmt.Fprintf(w, `<td>%s</td>`, nombre_art)
					//fmt.Fprintf(w, `<td>%v</td>`, x.Id.Hex())
					fmt.Println(v.Id.Hex())
					fmt.Fprintf(w, `<td>%v</td>`, cantidad)
					fmt.Fprintf(w, `</tr>`)
					total = total + cantidad
				}
				fmt.Fprintf(w, `<script>$("#titulo-doc").html('Documento de referencia');</script>`)
				fmt.Fprintf(w, `</tbody>`)
			}
			fmt.Fprintf(w, `<tfoot>`)
			fmt.Fprintf(w, `<tr>`)
			fmt.Fprintf(w, `<th>TOTAL</th>`)
			fmt.Fprintf(w, `<th>%v</th>`, total)
			fmt.Fprintf(w, `</tr>`)
			fmt.Fprintf(w, `</tfoot>`)
			fmt.Fprintf(w, `</table>`)

		} else {
			fmt.Fprintf(w, `<div class="alert alert-warning" role="alert"><strong>Ups!</strong> No se encontraron movimientos.</div>`)
		}
	}
}
