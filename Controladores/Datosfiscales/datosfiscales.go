package fiscal

import (
	"fmt"
	"html/template"
	"net/http"
	"strconv"

	"../Session"
	"gopkg.in/mgo.v2/bson"

	"../../Modelos/General"

	"../../Modelos/DatosFiscales"
)

var tmpl_f = template.Must(template.ParseFiles(
	"Vistas/index.html",
	"Vistas/Parciales/Plantilla/header.html",
	"Vistas/Parciales/Plantilla/footer.html",
	"Vistas/Parciales/Plantilla/styles.html",
	"Vistas/Parciales/Plantilla/scripts.html",
	"Vistas/Parciales/Paginas/Fiscal/index.html",
))

var tmpl_f_frame = template.Must(template.ParseFiles(
	"Vistas/index.html",
	"Vistas/Parciales/Plantilla/header_blank.html",
	"Vistas/Parciales/Plantilla/footer_blank.html",
	"Vistas/Parciales/Plantilla/styles.html",
	"Vistas/Parciales/Plantilla/scripts.html",
	"Vistas/Parciales/Paginas/Fiscal/index.html",
))

func DatosFiscales(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		userName := Session.GetUserName(r)
		if userName != "" {
			result := fiscal_m.GetFiscalDataOne()

			var estados = []string{
				"Aguascalientes", "Baja California", "Baja California Sur", "Campeche", "Chiapas",
				"Chihuahua", "Coahuila", "Colima", "Ciudad de México", "Durango", "Estado de México", "Guanajuato",
				"Guerrero", "Hidalgo", "Jalisco", "Michoacán", "Morelos", "Nayarit", "Nuevo León",
				"Oaxaca", "Puebla", "Quintana Roo", "San Luis Potosí", "Sinaloa", "Sonora", "Tabasco",
				"Tamaulipas", "Tlaxcala", "Veracruz", "Yucatán", "Zacatecas"}

			var est = `<option value="">Seleccione Estado... </option>`

			for _, item := range estados {
				if item == result.Direccion.Estado {
					est = est + `<option selected value="` + item + `">` + item + `</option>`
				} else {
					est = est + `<option value="` + item + `">` + item + `</option>`
				}

			}

			var aditional = ``

			for k, v := range result.Adicionales {
				aditional = aditional + `
							<div>
								<div class='col-md-4' id='div` + k + `'>
									<div  class='input-group input-group-md'> `
				aditional = aditional + `<label for="` + k + `" class="input-group-addon">` + k + `</label> `
				aditional = aditional + `<input id="` + k + `" name="` + k + `" type="text" value="` + v + `" class="form-control" > `
				aditional = aditional + `</div> 
								</div>
								<input type='button' value='X' class='deleteButton btn btn-danger'>
							</div>`

			}

			tmpl_f.ExecuteTemplate(w, "index_layout", map[string]interface{}{
				"result":         result,
				"est":            template.HTML(est),
				"aditional_data": template.HTML(aditional),
			})
		} else {
			http.Redirect(w, r, "/login", 302)
		}
	} else if r.Method == "POST" {

		id := r.FormValue("id")

		name := r.FormValue("nombre")
		rfc := r.FormValue("rfc")

		email := r.FormValue("email")
		phone := r.FormValue("telefono")
		celphone := r.FormValue("celular")

		calle := r.FormValue("calle")
		noExt, _ := strconv.Atoi(r.FormValue("noExterior"))
		noInt, _ := strconv.Atoi(r.FormValue("noInterior"))
		colonia := r.FormValue("colonia")
		localidad := r.FormValue("localidad")
		municipio := r.FormValue("municipio")
		estado := r.FormValue("estado")
		codigo, _ := strconv.Atoi(r.FormValue("codigoPostal"))

		var campos = []string{"id", "nombre", "telefono", "celular", "email", "rfc", "calle", "noExterior", "noInterior", "colonia", "localidad", "municipio", "estado", "codigoPostal"}
		var m = make(map[string]string)
		for k, v := range r.PostForm {
			if contains(k, campos) == false {
				m[k] = v[0]
			}
		}

		contact := models.Contacto{Id: bson.NewObjectId(), Email: email, Telefono: phone, Celular: celphone}
		address := models.Direccion{Id: bson.NewObjectId(), Calle: calle, NumExterior: noExt, NumInterior: noInt, Colonia: colonia, Localidad: localidad, Municipio: municipio, Estado: estado, CodigoPostal: codigo}
		datosfiscales := models.DatosFiscales{Rfc: rfc, Nombre: name, Direccion: address, Contacto: contact, Adicionales: m}
		var b bool
		if id != "" {
			b = fiscal_m.EditFiscalDataId(bson.ObjectIdHex(id), rfc, name, address, contact, m)
		} else if id == "" {
			b = fiscal_m.InsertFiscalData(datosfiscales)
		}
		if b == true {
			fmt.Println("Save fiscal data :)")
			http.Redirect(w, r, "/datosfiscales", 302)
		}

	}
}

func DatosFiscales_frame(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		userName := Session.GetUserName(r)
		if userName != "" {
			result := fiscal_m.GetFiscalDataOne()

			var estados = []string{
				"Aguascalientes", "Baja California", "Baja California Sur", "Campeche", "Chiapas",
				"Chihuahua", "Coahuila", "Colima", "Ciudad de México", "Durango", "Estado de México", "Guanajuato",
				"Guerrero", "Hidalgo", "Jalisco", "Michoacán", "Morelos", "Nayarit", "Nuevo León",
				"Oaxaca", "Puebla", "Quintana Roo", "San Luis Potosí", "Sinaloa", "Sonora", "Tabasco",
				"Tamaulipas", "Tlaxcala", "Veracruz", "Yucatán", "Zacatecas"}

			var est = `<option value="">Seleccione Estado... </option>`

			for _, item := range estados {
				if item == result.Direccion.Estado {
					est = est + `<option selected value="` + item + `">` + item + `</option>`
				} else {
					est = est + `<option value="` + item + `">` + item + `</option>`
				}

			}

			var aditional = ``

			for k, v := range result.Adicionales {
				aditional = aditional + `
							<div>
								<div class='col-md-4' id='div` + k + `'>
									<div  class='input-group input-group-md'> `
				aditional = aditional + `<label for="` + k + `" class="input-group-addon">` + k + `</label> `
				aditional = aditional + `<input id="` + k + `" name="` + k + `" type="text" value="` + v + `" class="form-control" > `
				aditional = aditional + `</div> 
								</div>
								<input type='button' value='X' class='deleteButton btn btn-danger'>
							</div>`

			}

			tmpl_f_frame.ExecuteTemplate(w, "index_layout", map[string]interface{}{
				"result":         result,
				"est":            template.HTML(est),
				"aditional_data": template.HTML(aditional),
			})
		} else {
			http.Redirect(w, r, "/login", 302)
		}
	} else if r.Method == "POST" {

		id := r.FormValue("id")

		name := r.FormValue("nombre")
		rfc := r.FormValue("rfc")

		email := r.FormValue("email")
		phone := r.FormValue("telefono")
		celphone := r.FormValue("celular")

		calle := r.FormValue("calle")
		noExt, _ := strconv.Atoi(r.FormValue("noExterior"))
		noInt, _ := strconv.Atoi(r.FormValue("noInterior"))
		colonia := r.FormValue("colonia")
		localidad := r.FormValue("localidad")
		municipio := r.FormValue("municipio")
		estado := r.FormValue("estado")
		codigo, _ := strconv.Atoi(r.FormValue("codigoPostal"))

		var campos = []string{"id", "nombre", "telefono", "celular", "email", "rfc", "calle", "noExterior", "noInterior", "colonia", "localidad", "municipio", "estado", "codigoPostal"}
		var m = make(map[string]string)
		for k, v := range r.PostForm {
			if contains(k, campos) == false {
				m[k] = v[0]
			}
		}

		contact := models.Contacto{Id: bson.NewObjectId(), Email: email, Telefono: phone, Celular: celphone}
		address := models.Direccion{Id: bson.NewObjectId(), Calle: calle, NumExterior: noExt, NumInterior: noInt, Colonia: colonia, Localidad: localidad, Municipio: municipio, Estado: estado, CodigoPostal: codigo}
		datosfiscales := models.DatosFiscales{Rfc: rfc, Nombre: name, Direccion: address, Contacto: contact, Adicionales: m}
		var b bool
		if id != "" {
			b = fiscal_m.EditFiscalDataId(bson.ObjectIdHex(id), rfc, name, address, contact, m)
		} else if id == "" {
			b = fiscal_m.InsertFiscalData(datosfiscales)
		}
		if b == true {
			http.Redirect(w, r, "/datosfiscales", 302)
		}

	}
}

func contains(texto string, campos []string) bool {
	var b bool = false
	for _, v := range campos {
		if v == texto {
			b = true
		}
	}
	return b
}

func EliminarDatosFiscales(w http.ResponseWriter, r *http.Request) {
	userName := Session.GetUserName(r)
	if userName != "" {
		err := fiscal_m.EliminarDatosFiscales()
		if err != nil {
			fmt.Println("Ocurrió un error", err)
			fmt.Fprintf(w, err.Error())
		}
	} else {
		http.Redirect(w, r, "/login", 302)
	}
}
