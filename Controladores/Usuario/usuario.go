package user

import (
	"fmt"
	"html/template"
	"net/http"

	"strconv"

	"../../Controladores"
	"../Session"

	"../../Modelos/General"
	"../../Modelos/Usuarios"

	"github.com/gorilla/mux"

	"gopkg.in/mgo.v2/bson"
)

var tmpl_user = template.Must(template.ParseFiles(
	"Vistas/index.html",
	"Vistas/Parciales/Plantilla/header.html",
	"Vistas/Parciales/Plantilla/footer.html",
	"Vistas/Parciales/Plantilla/styles.html",
	"Vistas/Parciales/Plantilla/scripts.html",
	"Vistas/Parciales/Paginas/Usuario/index.html",
))

var plantilla_usuario_ventana = template.Must(template.ParseFiles(
	"Vistas/index.html",
	"Vistas/Parciales/Plantilla/header_blank.html",
	"Vistas/Parciales/Plantilla/footer_blank.html",
	"Vistas/Parciales/Plantilla/styles.html",
	"Vistas/Parciales/Plantilla/scripts.html",
	"Vistas/Parciales/Paginas/Usuario/index.html",
))

func IndexUser(w http.ResponseWriter, r *http.Request) {
	userName := Session.GetUserName(r)
	if userName != "" {
		result, err_conex := users.GetAllUser()
		if err_conex != nil {
			fmt.Printf(err_conex.Error())
		}
		tmpl_user.ExecuteTemplate(w, "index_layout", result)
	} else {
		http.Redirect(w, r, "/login", 302)
	}
}

var tmpl_userN = template.Must(template.ParseFiles(
	"Vistas/index.html",
	"Vistas/Parciales/Plantilla/header.html",
	"Vistas/Parciales/Plantilla/footer.html",
	"Vistas/Parciales/Plantilla/styles.html",
	"Vistas/Parciales/Plantilla/scripts.html",
	"Vistas/Parciales/Paginas/Usuario/new.html",
))

var tmpl_userN_frame = template.Must(template.ParseFiles(
	"Vistas/index.html",
	"Vistas/Parciales/Plantilla/header_blank.html",
	"Vistas/Parciales/Plantilla/footer_blank.html",
	"Vistas/Parciales/Plantilla/styles.html",
	"Vistas/Parciales/Plantilla/scripts.html",
	"Vistas/Parciales/Paginas/Usuario/new.html",
))

func MostrarUsuariosFrame(w http.ResponseWriter, r *http.Request) {
	userName := Session.GetUserName(r)
	if userName != "" {
		result, err_conex := users.GetAllUser()
		if err_conex != nil {
			fmt.Printf(err_conex.Error())
		}
		plantilla_usuario_ventana.ExecuteTemplate(w, "index_layout", result)
	} else {
		http.Redirect(w, r, "/login", 302)
	}
}

func UserNew(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		userName := Session.GetUserName(r)
		if userName != "" {
			tmpl_userN.ExecuteTemplate(w, "index_layout", nil)
		} else {
			http.Redirect(w, r, "/login", 302)
		}
	} else if r.Method == "POST" {
		r.ParseForm()
		name := r.FormValue("nombre")
		username := r.FormValue("usuario")
		password := r.FormValue("password")
		activeValue := r.FormValue("estatus")
		var active bool
		if activeValue == "activo" {
			active = true
		} else {
			active = false
		}

		email := r.FormValue("email")
		phone := r.FormValue("telefono")
		celphone := r.FormValue("celular")

		calle := r.FormValue("calle")
		noExt, _ := strconv.Atoi(r.FormValue("noExterior"))
		noInt, _ := strconv.Atoi(r.FormValue("noInterior"))
		colonia := r.FormValue("colonia")
		localidad := r.FormValue("localidad")
		municipio := r.FormValue("municipio")
		estado := r.FormValue("estado")
		codigo, _ := strconv.Atoi(r.FormValue("codigoPostal"))

		var campos = []string{"nombre", "telefono", "estatus", "celular", "email", "usuario", "confirmPassword", "password", "calle", "noExterior", "noInterior", "colonia", "localidad", "municipio", "estado", "codigoPostal"}
		var m = make(map[string]string)
		for k, v := range r.PostForm {
			if contains(k, campos) == false {
				m[k] = v[0]
			}
		}
		//		fmt.Println(m)

		contact := models.Contacto{Id: bson.NewObjectId(), Email: email, Telefono: phone, Celular: celphone}

		address := models.Direccion{Id: bson.NewObjectId(), Calle: calle, NumExterior: noExt, NumInterior: noInt, Colonia: colonia, Localidad: localidad, Municipio: municipio, Estado: estado, CodigoPostal: codigo}
		user := models.Usuarios{Nombre: name, Usuario: username, Password: controllers.EncriptaTexto(password), Activo: active, Contacto: contact, Direccion: address, Adicionales: m}

		err_conex := users.NewUser(user)
		if err_conex != nil {
			fmt.Printf(err_conex.Error())
		} else {
			http.Redirect(w, r, "/usuarios_frame", 302)
		}

	}
}

func UserNew_frame(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		userName := Session.GetUserName(r)
		if userName != "" {
			tmpl_userN_frame.ExecuteTemplate(w, "index_layout", nil)
		} else {
			http.Redirect(w, r, "/login", 302)
		}
	} else if r.Method == "POST" {
		r.ParseForm()
		name := r.FormValue("nombre")
		username := r.FormValue("usuario")
		password := r.FormValue("password")

		activeValue := r.FormValue("estatus")
		var active bool
		if activeValue == "activo" {
			active = true
		} else {
			active = false
		}

		email := r.FormValue("email")
		phone := r.FormValue("telefono")
		celphone := r.FormValue("celular")

		calle := r.FormValue("calle")
		noExt, _ := strconv.Atoi(r.FormValue("noExterior"))
		noInt, _ := strconv.Atoi(r.FormValue("noInterior"))
		colonia := r.FormValue("colonia")
		localidad := r.FormValue("localidad")
		municipio := r.FormValue("municipio")
		estado := r.FormValue("estado")
		codigo, _ := strconv.Atoi(r.FormValue("codigoPostal"))

		var campos = []string{"nombre", "telefono", "estatus", "celular", "email", "usuario", "confirmPassword", "password", "calle", "noExterior", "noInterior", "colonia", "localidad", "municipio", "estado", "codigoPostal"}
		var m = make(map[string]string)
		for k, v := range r.PostForm {
			if contains(k, campos) == false {
				m[k] = v[0]
			}
		}
		contact := models.Contacto{Id: bson.NewObjectId(), Email: email, Telefono: phone, Celular: celphone}
		fmt.Println("INSERTANDO EL USUARIO: ", controllers.EncriptaTexto(password))
		address := models.Direccion{Id: bson.NewObjectId(), Calle: calle, NumExterior: noExt, NumInterior: noInt, Colonia: colonia, Localidad: localidad, Municipio: municipio, Estado: estado, CodigoPostal: codigo}
		user := models.Usuarios{Nombre: name, Usuario: username, Password: controllers.EncriptaTexto(password), Activo: active, Contacto: contact, Direccion: address, Adicionales: m}

		err_conex := users.NewUser(user)
		if err_conex != nil {
			fmt.Fprintf(w, err_conex.Error())
		} else {
			http.Redirect(w, r, "/usuarios_frame", 302)
		}

	}
}

func contains(texto string, campos []string) bool {
	var b bool = false
	for _, v := range campos {
		if v == texto {
			b = true
		}
	}
	return b
}

var tmpl_userE = template.Must(template.ParseFiles(
	"Vistas/index.html",
	"Vistas/Parciales/Plantilla/header_blank.html",
	"Vistas/Parciales/Plantilla/footer_blank.html",
	"Vistas/Parciales/Plantilla/styles.html",
	"Vistas/Parciales/Plantilla/scripts.html",
	"Vistas/Parciales/Paginas/Usuario/edit.html",
))

var tmpl_userM = template.Must(template.ParseFiles(
	"Vistas/index.html",
	"Vistas/Parciales/Plantilla/header_blank.html",
	"Vistas/Parciales/Plantilla/footer_blank.html",
	"Vistas/Parciales/Plantilla/styles.html",
	"Vistas/Parciales/Plantilla/scripts.html",
	"Vistas/Parciales/Paginas/Usuario/view.html",
))

func EditUserId(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		userName := Session.GetUserName(r)
		if userName != "" {
			vars := mux.Vars(r)
			id := vars["id"]
			result, err_conex := users.GetUserId(bson.ObjectIdHex(id))
			if err_conex != nil {
				fmt.Printf(err_conex.Error())
			}

			var estados = []string{
				"Aguascalientes", "Baja California", "Baja California Sur", "Campeche", "Chiapas",
				"Chihuahua", "Coahuila", "Colima", "Ciudad de México", "Durango", "Estado de México", "Guanajuato",
				"Guerrero", "Hidalgo", "Jalisco", "Michoacán", "Morelos", "Nayarit", "Nuevo León",
				"Oaxaca", "Puebla", "Quintana Roo", "San Luis Potosí", "Sinaloa", "Sonora", "Tabasco",
				"Tamaulipas", "Tlaxcala", "Veracruz", "Yucatán", "Zacatecas"}

			var est = `<option selected="" value="">Seleccione Estado... </option>`

			for _, item := range estados {
				if item == result.Direccion.Estado {
					est = est + `<option selected value="` + item + `">` + item + `</option>`
				} else {
					est = est + `<option value="` + item + `">` + item + `</option>`
				}
			}

			var activ = ``
			if result.Activo == true {
				activ = `<div class="form-group has-feedback input-group-md">
			            <div class="input-group input-group-md">
			                <input id="activo" name="estatus" type="radio" class="input" value="activo" checked/>
			                <label for="activo" class="input-label">Activo</label>
			            </div>
			        </div>
					<div class="form-group has-feedback input-group-md">
			            <div class="input-group input-group-md">
			                <input id="inactivo" name="estatus" type="radio" class="input" value="inactivo"/>
			                <label for="inactivo" class="input-label">Inactivo</label>
			            </div>

			        </div>`
			} else if result.Activo == false {
				activ = `
					<div class="form-group has-feedback input-group-md">
			            <div class="input-group input-group-md">
			                <input id="activo" name="estatus" type="radio" class="input" value="activo" />
			                <label for="activo" class="input-label">Activo</label>
			            </div>
			        </div>
					<div class="form-group has-feedback input-group-md">
			            <div class="input-group input-group-md">
			                <input id="inactivo" name="estatus" type="radio" class="input" value="inactivo" checked/>
			                <label for="inactivo" class="input-label">Inactivo</label>
			            </div>

			        </div>`
			}

			//		fmt.Println(result.Adicionales)

			var aditional = ``

			for k, v := range result.Adicionales {
				aditional = aditional + `
							<div>
								<div class='col-md-4' id='div` + k + `'>
									<div  class='input-group input-group-md'> `
				aditional = aditional + `<label for="` + k + `" class="input-group-addon">` + k + `</label> `
				aditional = aditional + `<input id="` + k + `" name="` + k + `" type="text" value="` + v + `" class="form-control" > `
				aditional = aditional + `</div> 
								</div>
								<input type='button' value='X' class='deleteButton btn btn-danger'>
							</div>`

			}
			//		fmt.Println(aditional)
			//		valor := `caleb cruz garcia`

			tmpl_userE.ExecuteTemplate(w, "index_layout", map[string]interface{}{
				"result":         result,
				"active_radio":   template.HTML(activ),
				"est":            template.HTML(est),
				"aditional_data": template.HTML(aditional),
			})
		} else {
			http.Redirect(w, r, "/login", 302)
		}
	} else if r.Method == "POST" {
		id := r.FormValue("id")

		name := r.FormValue("nombre")
		username := r.FormValue("usuario")
		password := r.FormValue("password")
		activeValue := r.FormValue("estatus")
		var active bool = false
		if activeValue == "activo" {
			active = true
		} else {
			active = false
		}

		email := r.FormValue("email")
		phone := r.FormValue("telefono")
		celphone := r.FormValue("celular")

		calle := r.FormValue("calle")
		noExt, _ := strconv.Atoi(r.FormValue("noExterior"))
		noInt, _ := strconv.Atoi(r.FormValue("noInterior"))
		colonia := r.FormValue("colonia")
		localidad := r.FormValue("localidad")
		municipio := r.FormValue("municipio")
		estado := r.FormValue("estado")
		codigo, _ := strconv.Atoi(r.FormValue("codigoPostal"))

		var campos = []string{"nombre", "id", "telefono", "estatus", "celular", "email", "usuario", "confirmPassword", "password", "calle", "noExterior", "noInterior", "colonia", "localidad", "municipio", "estado", "codigoPostal"}
		var m = make(map[string]string)
		for k, v := range r.PostForm {
			if contains(k, campos) == false {
				m[k] = v[0]
			}
		}
		contact := models.Contacto{Id: bson.NewObjectId(), Email: email, Telefono: phone, Celular: celphone}
		address := models.Direccion{Id: bson.NewObjectId(), Calle: calle, NumExterior: noExt, NumInterior: noInt, Colonia: colonia, Localidad: localidad, Municipio: municipio, Estado: estado, CodigoPostal: codigo}

		err_conex := users.EditUserId(bson.ObjectIdHex(id), name, username, controllers.EncriptaTexto(password), active, contact, address, m)
		if err_conex != nil {
			fmt.Printf(err_conex.Error())
		} else {
			http.Redirect(w, r, "/usuarios_frame", 302)
		}

	}
}

func MostrarUsuarioId(w http.ResponseWriter, r *http.Request) {
	userName := Session.GetUserName(r)
	if userName != "" {
		vars := mux.Vars(r)
		id := vars["id"]
		result, err_conex := users.GetUserId(bson.ObjectIdHex(id))
		if err_conex != nil {
			fmt.Printf(err_conex.Error())
		}
		var estados = []string{
			"Aguascalientes", "Baja California", "Baja California Sur", "Campeche", "Chiapas",
			"Chihuahua", "Coahuila", "Colima", "Ciudad de México", "Durango", "Estado de México", "Guanajuato",
			"Guerrero", "Hidalgo", "Jalisco", "Michoacán", "Morelos", "Nayarit", "Nuevo León",
			"Oaxaca", "Puebla", "Quintana Roo", "San Luis Potosí", "Sinaloa", "Sonora", "Tabasco",
			"Tamaulipas", "Tlaxcala", "Veracruz", "Yucatán", "Zacatecas"}

		var est = `<option selected="" value="">Seleccione Estado... </option>`

		for _, item := range estados {
			if item == result.Direccion.Estado {
				est = est + `<option selected value="` + item + `">` + item + `</option>`
			} else {
				est = est + `<option value="` + item + `">` + item + `</option>`
			}
		}

		var activ = ``
		if result.Activo == true {
			activ = `<div class="form-group has-feedback input-group-md">
			            <div class="input-group input-group-md">
			                <input id="activo" name="estatus" type="radio" class="input" value="activo" checked disabled/>
			                <label for="activo" class="input-label">Activo</label>
			            </div>
			        </div>
					<div class="form-group has-feedback input-group-md">
			            <div class="input-group input-group-md">
			                <input id="inactivo" name="estatus" type="radio" class="input" value="inactivo" disabled/>
			                <label for="inactivo" class="input-label">Inactivo</label>
			            </div>

			        </div>`
		} else if result.Activo == false {
			activ = `
					<div class="form-group has-feedback input-group-md">
			            <div class="input-group input-group-md">
			                <input id="activo" name="estatus" type="radio" class="input" value="activo" disabled />
			                <label for="activo" class="input-label">Activo</label>
			            </div>
			        </div>
					<div class="form-group has-feedback input-group-md">
			            <div class="input-group input-group-md">
			                <input id="inactivo" name="estatus" type="radio" class="input" value="inactivo" checked disabled/>
			                <label for="inactivo" class="input-label">Inactivo</label>
			            </div>

			        </div>`
		}
		var aditional = ``

		for k, v := range result.Adicionales {
			aditional = aditional + `
							<div>
								<div class='col-md-4' id='div` + k + `'>
									<div  class='input-group input-group-md'> `
			aditional = aditional + `<label for="` + k + `" class="input-group-addon">` + k + `</label> `
			aditional = aditional + `<input id="` + k + `" name="` + k + `" type="text" value="` + v + `" class="form-control" > `
			aditional = aditional + `</div> 
								</div>
								
							</div>`

		}
		tmpl_userM.ExecuteTemplate(w, "index_layout", map[string]interface{}{
			"result":         result,
			"active_radio":   template.HTML(activ),
			"est":            template.HTML(est),
			"aditional_data": template.HTML(aditional),
		})
	} else {
		http.Redirect(w, r, "/login", 302)
	}
}

func EliminarUsuario(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["id"]
	err := users.EliminaUsuario(id)
	if err != nil {
		fmt.Fprintf(w, err.Error())
	} else {
		http.Redirect(w, r, "/usuarios_frame", 302)
	}
}
