package controllers

import (
	"crypto/md5"
	"encoding/hex"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"net/smtp"

	"../Modelos/Accesos"

	"./Session"
)

var tmpl_login = template.Must(template.ParseFiles(
	"Vistas/login.html",
	"Vistas/Parciales/Plantilla/footer.html",
	"Vistas/Parciales/Paginas/Login/content.html",
))

var tmpl_usr = template.Must(template.ParseFiles(
	"Vistas/flat.html",
	"Vistas/Parciales/Plantilla/footer.html",
))

//############################################### F  U  N  C  I  O  N  E S ########################################################

func Login(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		name := Session.GetUserName(r)
		fmt.Println(name)
		if name != "" {

			http.Redirect(w, r, "/", 302)

		} else {
			tmpl_login.ExecuteTemplate(w, "login_layout", nil)
		}

	} else if r.Method == "POST" {
		username := r.FormValue("txt_username")
		userpass := r.FormValue("txt_password")

		redirectTarget := "/login"
		if username != "" && userpass != "" {

			var resultado *modelo_acceso.Usuario
			var errores error
			var str string = userpass
			hasher := md5.New()
			hasher.Write([]byte(str))
			userpass = hex.EncodeToString(hasher.Sum(nil))
			resultado, errores = modelo_acceso.ComprobarUsuario(username, userpass)
			if errores != nil {
				//w.WriteHeader(404)
				http.Redirect(w, r, redirectTarget, 302)
				w.Write([]byte(errores.Error()))
			}
			//verifica si la consulta no devolvió un registro, redireccionar al login para intentar nuevamente
			if resultado == nil {
				Session.ClearSession(w)

				redirectTarget = "/login"
				//mensaje de salida
				//var mensaje string = "Usuario o contraseña incorrectos."
			} else { /* Si la consulta regresó coincidencias, se iniciará sesion correctamente */
				Session.SetSession(username, w)
				redirectTarget = "/"
			}
		}
		//dependiendo del resultado de la consulta, redirecciona al loguin o a la vista principal
		http.Redirect(w, r, redirectTarget, 302)
	}
}

func Logout(w http.ResponseWriter, r *http.Request) {
	Session.ClearSession(w)
	http.Redirect(w, r, "/login", 302)
}

func RecuperacionContraseña(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		tmpl_usr.ExecuteTemplate(w, "recovery_layout", nil)
	} else if r.Method == "POST" {
		r.ParseForm()
		correo := r.FormValue("mensaje")
		fmt.Println("correo: ", correo)
		if correo != "" {
			secreto, err := modelo_acceso.ExistMailandUpdate(correo)

			if err == nil {
				body := "<p>Estimado usuario:\n Su contrase&ntilde;a ha sido reestablecida al valor <strong>" + secreto + "</strong> .</p> \n Verifique su ingreso correcto."
				msg := "To: " + correo + "\r\n" +
					"Content-type: text/html" + "\r\n" +
					"Subject: Cambio de credenciales" + "\r\n\r\n" +
					body + "\r\n"
				// Set up authentication information.
				auth := smtp.PlainAuth(
					"",
					"test.miderp@hotmail.com",
					"D3m0st3n3s",
					"smtp.live.com",
				)
				// Connect to the server, authenticate, set the sender and recipient,
				// and send the email all in one step.
				err := smtp.SendMail(
					"smtp.live.com:587",
					auth,
					"test.miderp@hotmail.com",
					[]string{correo},
					[]byte(msg),
				)
				//err := smtp.SendMail("smtp.gmail.com:587", smtp.PlainAuth("", from, password, "smtp.gmail.com"), from, []string{to}, []byte(msg))
				if err != nil {
					log.Println(err)
					fmt.Fprintf(w, "Ha sucedido un error al procesar su solicitud,\n intente de nuevo.")
				} else {
					fmt.Fprintf(w, "Su solicitud ha sido procesada.")
				}
			} else {
				fmt.Println("User not found...")
				fmt.Fprintf(w, "Usuario no encontrado...")
			}

		} else {
			fmt.Fprintf(w, "Correo Invalido")
		}
	}

}
