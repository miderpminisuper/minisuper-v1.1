package client

import (
	"fmt"
	"html/template"
	"net/http"
	"strconv"

	"../../Modelos/Clientes"
	"../../Modelos/General"
	"../Session"
	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2/bson"
)

var tmpl_client = template.Must(template.ParseFiles(
	"Vistas/index.html",
	"Vistas/Parciales/Plantilla/header.html",
	"Vistas/Parciales/Plantilla/footer.html",
	"Vistas/Parciales/Plantilla/styles.html",
	"Vistas/Parciales/Plantilla/scripts.html",
	"Vistas/Parciales/Paginas/Cliente/index.html",
))

var tmpl_client_frame = template.Must(template.ParseFiles(
	"Vistas/index.html",
	"Vistas/Parciales/Plantilla/header_blank.html",
	"Vistas/Parciales/Plantilla/footer_blank.html",
	"Vistas/Parciales/Plantilla/styles.html",
	"Vistas/Parciales/Plantilla/scripts.html",
	"Vistas/Parciales/Paginas/Cliente/index_frame.html",
))

func IndexClient(w http.ResponseWriter, r *http.Request) {
	userName := Session.GetUserName(r)
	if userName != "" {
		result := clients.GetAllClient()
		tmpl_client.ExecuteTemplate(w, "index_layout", result)
	} else {
		http.Redirect(w, r, "/login", 302)
	}
}

func IndexClientFrame(w http.ResponseWriter, r *http.Request) {
	userName := Session.GetUserName(r)
	if userName != "" {
		result := clients.GetAllClient()
		tmpl_client_frame.ExecuteTemplate(w, "index_layout", result)
	} else {
		http.Redirect(w, r, "/login", 302)
	}
}

var tmpl_clientN = template.Must(template.ParseFiles(
	"Vistas/index.html",
	"Vistas/Parciales/Plantilla/header.html",
	"Vistas/Parciales/Plantilla/footer.html",
	"Vistas/Parciales/Plantilla/styles.html",
	"Vistas/Parciales/Plantilla/scripts.html",
	"Vistas/Parciales/Paginas/Cliente/new.html",
))

var tmpl_clientN_frame = template.Must(template.ParseFiles(
	"Vistas/index.html",
	"Vistas/Parciales/Plantilla/header_blank.html",
	"Vistas/Parciales/Plantilla/footer_blank.html",
	"Vistas/Parciales/Plantilla/styles.html",
	"Vistas/Parciales/Plantilla/scripts.html",
	"Vistas/Parciales/Paginas/Cliente/new_frame.html",
))

func ClientNew(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		userName := Session.GetUserName(r)
		if userName != "" {
			tmpl_clientN.ExecuteTemplate(w, "index_layout", nil)
		} else {
			http.Redirect(w, r, "/login", 302)
		}
	} else if r.Method == "POST" {
		name := r.FormValue("nombre")
		rfc := r.FormValue("rfc")
		activeValue := r.FormValue("estatus")
		var active bool
		if activeValue == "activo" {
			active = true
		} else {
			active = false
		}

		email := r.FormValue("email")
		phone := r.FormValue("telefono")
		celphone := r.FormValue("celular")

		calle := r.FormValue("calle")
		noExt, _ := strconv.Atoi(r.FormValue("noExterior"))
		noInt, _ := strconv.Atoi(r.FormValue("noInterior"))
		colonia := r.FormValue("colonia")
		localidad := r.FormValue("localidad")
		municipio := r.FormValue("municipio")
		estado := r.FormValue("estado")
		codigo, _ := strconv.Atoi(r.FormValue("codigoPostal"))

		var campos = []string{"nombre", "telefono", "estatus", "celular", "email", "rfc", "calle", "noExterior", "noInterior", "colonia", "localidad", "municipio", "estado", "codigoPostal"}
		var m = make(map[string]string)
		for k, v := range r.PostForm {
			if contains(k, campos) == false {
				m[k] = v[0]
			}
		}
		id_cliente := bson.NewObjectId()
		contact := models.Contacto{Id: bson.NewObjectId(), Email: email, Telefono: phone, Celular: celphone}

		address := models.Direccion{Id: bson.NewObjectId(), Calle: calle, NumExterior: noExt, NumInterior: noInt, Colonia: colonia, Localidad: localidad, Municipio: municipio, Estado: estado, CodigoPostal: codigo}
		client := models.Cliente{Id: id_cliente, Rfc: rfc, Nombre: name, Activo: active, Contacto: contact, Direccion: address, Adicionales: m}

		b := clients.NewClient(client)

		nombre_almacen_pg := id_cliente.Hex()

		dbserv := models.ConectarPostgres(models.USER_POSTGRESQL, models.PASS_POSGRESQL, models.BASE_POSTGRESQL, models.SERVIDOR_POSTGRESQL, models.PORT_POSTGRESQL, "disable")

		query := `CREATE TABLE ` + `"` + nombre_almacen_pg + `"` + `()`
		id_producto := `ALTER TABLE ` + `"` + nombre_almacen_pg + `"` + `ADD COLUMN id_producto VARCHAR(25)`
		cantidad := `ALTER TABLE ` + `"` + nombre_almacen_pg + `"` + `ADD COLUMN cantidad NUMERIC`
		estatus := `ALTER TABLE ` + `"` + nombre_almacen_pg + `"` + `ADD COLUMN estatus VARCHAR(30)`

		_, _ = dbserv.Exec(query)
		_, _ = dbserv.Exec(id_producto)
		_, _ = dbserv.Exec(cantidad)
		_, _ = dbserv.Exec(estatus)

		if b != nil {
			fmt.Fprintf(w, b.Error())
		} else {
			http.Redirect(w, r, "/client", 302)
		}
	}
}

func ClientNewFrame(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		userName := Session.GetUserName(r)
		if userName != "" {
			tmpl_clientN_frame.ExecuteTemplate(w, "index_layout", nil)
		} else {
			http.Redirect(w, r, "/login", 302)
		}
	} else if r.Method == "POST" {
		name := r.FormValue("nombre")
		rfc := r.FormValue("rfc")
		activeValue := r.FormValue("estatus")
		var active bool
		if activeValue == "activo" {
			active = true
		} else {
			active = false
		}

		email := r.FormValue("email")
		phone := r.FormValue("telefono")
		celphone := r.FormValue("celular")

		calle := r.FormValue("calle")
		noExt, _ := strconv.Atoi(r.FormValue("noExterior"))
		noInt, _ := strconv.Atoi(r.FormValue("noInterior"))
		colonia := r.FormValue("colonia")
		localidad := r.FormValue("localidad")
		municipio := r.FormValue("municipio")
		estado := r.FormValue("estado")
		codigo, _ := strconv.Atoi(r.FormValue("codigoPostal"))

		var campos = []string{"nombre", "telefono", "estatus", "celular", "email", "rfc", "calle", "noExterior", "noInterior", "colonia", "localidad", "municipio", "estado", "codigoPostal"}
		var m = make(map[string]string)
		for k, v := range r.PostForm {
			if contains(k, campos) == false {
				m[k] = v[0]
			}
		}
		id_cliente := bson.NewObjectId()
		contact := models.Contacto{Id: bson.NewObjectId(), Email: email, Telefono: phone, Celular: celphone}

		address := models.Direccion{Id: bson.NewObjectId(), Calle: calle, NumExterior: noExt, NumInterior: noInt, Colonia: colonia, Localidad: localidad, Municipio: municipio, Estado: estado, CodigoPostal: codigo}
		client := models.Cliente{Id: id_cliente, Rfc: rfc, Nombre: name, Activo: active, Contacto: contact, Direccion: address, Adicionales: m}

		b := clients.NewClient(client)

		nombre_almacen_pg := id_cliente.Hex()

		dbserv := models.ConectarPostgres(models.USER_POSTGRESQL, models.PASS_POSGRESQL, models.BASE_POSTGRESQL, models.SERVIDOR_POSTGRESQL, models.PORT_POSTGRESQL, "disable")

		query := `CREATE TABLE ` + `"` + nombre_almacen_pg + `"` + `()`
		id_producto := `ALTER TABLE ` + `"` + nombre_almacen_pg + `"` + `ADD COLUMN id_producto VARCHAR(25)`
		cantidad := `ALTER TABLE ` + `"` + nombre_almacen_pg + `"` + `ADD COLUMN cantidad NUMERIC`
		estatus := `ALTER TABLE ` + `"` + nombre_almacen_pg + `"` + `ADD COLUMN estatus VARCHAR(30)`

		_, _ = dbserv.Exec(query)
		_, _ = dbserv.Exec(id_producto)
		_, _ = dbserv.Exec(cantidad)
		_, _ = dbserv.Exec(estatus)

		if b != nil {
			fmt.Fprintf(w, b.Error())
		} else {
			http.Redirect(w, r, "/client_frame", 302)
		}
	}
}

func contains(texto string, campos []string) bool {
	var b bool = false
	for _, v := range campos {
		if v == texto {
			b = true
		}
	}
	return b
}

var tmpl_clientE = template.Must(template.ParseFiles(
	"Vistas/index.html",
	"Vistas/Parciales/Plantilla/header.html",
	"Vistas/Parciales/Plantilla/footer.html",
	"Vistas/Parciales/Plantilla/styles.html",
	"Vistas/Parciales/Plantilla/scripts.html",
	"Vistas/Parciales/Paginas/Cliente/edit.html",
))

var tmpl_clientE_frame = template.Must(template.ParseFiles(
	"Vistas/index.html",
	"Vistas/Parciales/Plantilla/header_blank.html",
	"Vistas/Parciales/Plantilla/footer_blank.html",
	"Vistas/Parciales/Plantilla/styles.html",
	"Vistas/Parciales/Plantilla/scripts.html",
	"Vistas/Parciales/Paginas/Cliente/edit_frame.html",
))

func EditClientId(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		userName := Session.GetUserName(r)
		if userName != "" {
			vars := mux.Vars(r)
			id := vars["id"]
			result := clients.GetClientId(bson.ObjectIdHex(id))

			var estados = []string{
				"Aguascalientes", "Baja California", "Baja California Sur", "Campeche", "Chiapas",
				"Chihuahua", "Coahuila", "Colima", "Ciudad de México", "Durango", "Estado de México", "Guanajuato",
				"Guerrero", "Hidalgo", "Jalisco", "Michoacán", "Morelos", "Nayarit", "Nuevo León",
				"Oaxaca", "Puebla", "Quintana Roo", "San Luis Potosí", "Sinaloa", "Sonora", "Tabasco",
				"Tamaulipas", "Tlaxcala", "Veracruz", "Yucatán", "Zacatecas"}

			var est = `<option value="">Seleccione Estado... </option>`

			for _, item := range estados {
				if item == result.Direccion.Estado {
					est = est + `<option selected value="` + item + `">` + item + `</option>`
				} else {
					est = est + `<option value="` + item + `">` + item + `</option>`
				}

			}

			var activ = ``
			if result.Activo == true {
				activ = `<div class="input-group input-group-md">
	                                                    <input id="activo" name="estatus" type="radio" class="input" value="activo" checked/>
	                                                    <label for="activo" class="input-label">Activo</label>
	                                                </div>
	                                                <div class="input-group input-group-md">
	                                                    <input id="inactivo" name="estatus" type="radio" class="input" value="inactivo"/>
	                                                    <label for="inactivo" class="input-label">Inactivo</label>
	                                                </div>`
			} else if result.Activo == false {
				activ = `
					<div class="input-group input-group-md">
	                                                    <input id="activo" name="estatus" type="radio" class="input" value="activo"/>
	                                                    <label for="activo" class="input-label">Activo</label>
	                                                </div>
	                                                <div class="input-group input-group-md">
	                                                    <input id="inactivo" name="estatus" type="radio" class="input" value="inactivo" checked/>
	                                                    <label for="inactivo" class="input-label">Inactivo</label>
	                                                </div>`
			}

			var aditional = ``

			for k, v := range result.Adicionales {
				aditional = aditional + `
							<div>
								<div class='col-md-4' id='div` + k + `'>
									<div  class='input-group input-group-md'> `
				aditional = aditional + `<label for="` + k + `" class="input-group-addon">` + k + `</label> `
				aditional = aditional + `<input id="` + k + `" name="` + k + `" type="text" value="` + v + `" class="form-control" > `
				aditional = aditional + `</div> 
								</div>
								<input type='button' value='X' class='deleteButton btn btn-danger'>
							</div>`

			}

			tmpl_clientE.ExecuteTemplate(w, "index_layout", map[string]interface{}{
				"result":         result,
				"active_radio":   template.HTML(activ),
				"est":            template.HTML(est),
				"aditional_data": template.HTML(aditional),
			})
		} else {
			http.Redirect(w, r, "/login", 302)
		}
	} else if r.Method == "POST" {
		id := r.FormValue("id")
		rfc := r.FormValue("rfc")
		name := r.FormValue("nombre")
		activeValue := r.FormValue("estatus")
		var active bool
		if activeValue == "activo" {
			active = true
		} else {
			active = false
		}

		email := r.FormValue("email")
		phone := r.FormValue("telefono")
		celphone := r.FormValue("celular")

		calle := r.FormValue("calle")
		noExt, _ := strconv.Atoi(r.FormValue("noExterior"))
		noInt, _ := strconv.Atoi(r.FormValue("noInterior"))
		colonia := r.FormValue("colonia")
		localidad := r.FormValue("localidad")
		municipio := r.FormValue("municipio")
		estado := r.FormValue("estado")
		codigo, _ := strconv.Atoi(r.FormValue("codigoPostal"))

		var campos = []string{"nombre", "id", "telefono", "estatus", "celular", "email", "rfc", "calle", "noExterior", "noInterior", "colonia", "localidad", "municipio", "estado", "codigoPostal"}
		var m = make(map[string]string)
		for k, v := range r.PostForm {
			if contains(k, campos) == false {
				m[k] = v[0]
			}
		}

		contact := models.Contacto{Id: bson.NewObjectId(), Email: email, Telefono: phone, Celular: celphone}
		address := models.Direccion{Id: bson.NewObjectId(), Calle: calle, NumExterior: noExt, NumInterior: noInt, Colonia: colonia, Localidad: localidad, Municipio: municipio, Estado: estado, CodigoPostal: codigo}

		err := clients.EditClientId(bson.ObjectIdHex(id), rfc, name, active, contact, address, m)
		if err != nil {
			fmt.Fprintf(w, err.Error())
		} else {
			http.Redirect(w, r, "/client", 302)
		}
	}
}

func EditClientIdFrame(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		userName := Session.GetUserName(r)
		if userName != "" {
			vars := mux.Vars(r)
			id := vars["id"]
			result := clients.GetClientId(bson.ObjectIdHex(id))

			var estados = []string{
				"Aguascalientes", "Baja California", "Baja California Sur", "Campeche", "Chiapas",
				"Chihuahua", "Coahuila", "Colima", "Ciudad de México", "Durango", "Estado de México", "Guanajuato",
				"Guerrero", "Hidalgo", "Jalisco", "Michoacán", "Morelos", "Nayarit", "Nuevo León",
				"Oaxaca", "Puebla", "Quintana Roo", "San Luis Potosí", "Sinaloa", "Sonora", "Tabasco",
				"Tamaulipas", "Tlaxcala", "Veracruz", "Yucatán", "Zacatecas"}

			var est = `<option value="">Seleccione Estado... </option>`

			for _, item := range estados {
				if item == result.Direccion.Estado {
					est = est + `<option selected value="` + item + `">` + item + `</option>`
				} else {
					est = est + `<option value="` + item + `">` + item + `</option>`
				}

			}

			var activ = ``
			if result.Activo == true {
				activ = `<div class="input-group input-group-md">
	                                                    <input id="activo" name="estatus" type="radio" class="input" value="activo" checked/>
	                                                    <label for="activo" class="input-label">Activo</label>
	                                                </div>
	                                                <div class="input-group input-group-md">
	                                                    <input id="inactivo" name="estatus" type="radio" class="input" value="inactivo"/>
	                                                    <label for="inactivo" class="input-label">Inactivo</label>
	                                                </div>`
			} else if result.Activo == false {
				activ = `
					<div class="input-group input-group-md">
	                                                    <input id="activo" name="estatus" type="radio" class="input" value="activo"/>
	                                                    <label for="activo" class="input-label">Activo</label>
	                                                </div>
	                                                <div class="input-group input-group-md">
	                                                    <input id="inactivo" name="estatus" type="radio" class="input" value="inactivo" checked/>
	                                                    <label for="inactivo" class="input-label">Inactivo</label>
	                                                </div>`
			}

			var aditional = ``

			for k, v := range result.Adicionales {
				aditional = aditional + `
							<div>
								<div class='col-md-4' id='div` + k + `'>
									<div  class='input-group input-group-md'> `
				aditional = aditional + `<label for="` + k + `" class="input-group-addon">` + k + `</label> `
				aditional = aditional + `<input id="` + k + `" name="` + k + `" type="text" value="` + v + `" class="form-control" > `
				aditional = aditional + `</div> 
								</div>
								<input type='button' value='X' class='deleteButton btn btn-danger'>
							</div>`

			}

			tmpl_clientE_frame.ExecuteTemplate(w, "index_layout", map[string]interface{}{
				"result":         result,
				"active_radio":   template.HTML(activ),
				"est":            template.HTML(est),
				"aditional_data": template.HTML(aditional),
			})
		} else {
			http.Redirect(w, r, "/login", 302)
		}
	} else if r.Method == "POST" {
		id := r.FormValue("id")
		rfc := r.FormValue("rfc")
		name := r.FormValue("nombre")
		activeValue := r.FormValue("estatus")
		var active bool
		if activeValue == "activo" {
			active = true
		} else {
			active = false
		}

		email := r.FormValue("email")
		phone := r.FormValue("telefono")
		celphone := r.FormValue("celular")

		calle := r.FormValue("calle")
		noExt, _ := strconv.Atoi(r.FormValue("noExterior"))
		noInt, _ := strconv.Atoi(r.FormValue("noInterior"))
		colonia := r.FormValue("colonia")
		localidad := r.FormValue("localidad")
		municipio := r.FormValue("municipio")
		estado := r.FormValue("estado")
		codigo, _ := strconv.Atoi(r.FormValue("codigoPostal"))

		var campos = []string{"nombre", "id", "telefono", "estatus", "celular", "email", "rfc", "calle", "noExterior", "noInterior", "colonia", "localidad", "municipio", "estado", "codigoPostal"}
		var m = make(map[string]string)
		for k, v := range r.PostForm {
			if contains(k, campos) == false {
				m[k] = v[0]
			}
		}

		contact := models.Contacto{Id: bson.NewObjectId(), Email: email, Telefono: phone, Celular: celphone}
		address := models.Direccion{Id: bson.NewObjectId(), Calle: calle, NumExterior: noExt, NumInterior: noInt, Colonia: colonia, Localidad: localidad, Municipio: municipio, Estado: estado, CodigoPostal: codigo}

		err := clients.EditClientId(bson.ObjectIdHex(id), rfc, name, active, contact, address, m)
		if err != nil {
			fmt.Fprintf(w, err.Error())
		} else {
			http.Redirect(w, r, "/client_frame", 302)
		}
	}
}

func EliminarCliente(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["id"]
	err := clients.EliminaCliente(id)
	if err != nil {
		fmt.Fprintf(w, err.Error())
	} else {
		http.Redirect(w, r, "/client", 302)
	}
}

func EliminarClienteFrame(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["id"]
	err := clients.EliminaCliente(id)
	if err != nil {
		fmt.Fprintf(w, err.Error())
	} else {
		http.Redirect(w, r, "/client_frame", 302)
	}
}
