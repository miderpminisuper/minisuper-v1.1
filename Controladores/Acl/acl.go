package acl

import (
	_ "fmt"
	"html/template"
	"net/http"
	_ "strconv"

	"../Session"
	_ "gopkg.in/mgo.v2/bson"

	"../../Modelos/Almacenes"
	"../../Modelos/Clientes"
	_ "../../Modelos/Usuarios"
)

/*
type FormaPago struct {
	Id       bson.ObjectId `bson:"_id,omitempty"`
	Nombre   string        `bson:"nombre"`
	Cambio   bool          `bson:"devuelve_cambio"`
	Comision float64       `bson:"comision"`
	Estatus  string        `bson:"status"`
}
*/
var tmpl_pagos = template.Must(template.ParseFiles(
	"Vistas/index.html",
	"Vistas/Parciales/Plantilla/header.html",
	"Vistas/Parciales/Plantilla/footer.html",
	"Vistas/Parciales/Plantilla/styles.html",
	"Vistas/Parciales/Plantilla/scripts.html",
	"Vistas/Parciales/Paginas/Acl/index.html",
))

var tmpl_pagos_frame = template.Must(template.ParseFiles(
	"Vistas/index.html",
	"Vistas/Parciales/Plantilla/header_blank.html",
	"Vistas/Parciales/Plantilla/footer_blank.html",
	"Vistas/Parciales/Plantilla/styles.html",
	"Vistas/Parciales/Plantilla/scripts.html",
	"Vistas/Parciales/Paginas/Acl/index.html",
))

func Index(w http.ResponseWriter, r *http.Request) {
	userName := Session.GetUserName(r)
	if userName != "" {
		almacenes := almacenes_m.GetAlmacenes()
		//usuarios := users.GetAllUser()
		clientes := clients.GetAllClient()
		tmpl_pagos.ExecuteTemplate(w, "index_layout", map[string]interface{}{
			"almacenes": almacenes,
			"clientes":  clientes,
		})
	} else {
		http.Redirect(w, r, "/login", 302)
	}
}

/*
func FormasDePago(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		result := pagos_m.GetFormasPago()
		tmpl_pagos_frame.ExecuteTemplate(w, "index_layout", result)
	}
}

func Crear(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		var tmp0 = `<div class="input-group input-group-md">
								<span class="input-group-addon">Nombre: </span>
								<input type="text" name="nombre_nueva_forma" id="nombre_nueva_forma" value="" class="form-control">
							</div>

							<div class="input-group input-group-md">
								<span class="input-group-addon">Requiere cambio:
									<label class="radio-inline"><input type="radio" name="cambio_nueva_forma" id="cambio_nueva_forma" value ="si" checked>Si</label>
									<label class="radio-inline"><input type="radio" name="cambio_nueva_forma" id="cambio_nueva_forma" value ="no">No</label>
								 </span>
							</div>

							<div class="input-group input-group-md">
								<span class="input-group-addon">Comisión:</span>
									<input type="text" name="comision_forma" id="comision_forma" value="" class="form-control text-center" value="0.0">
									<span class="input-group-addon">&#37</span>
							</div>

							<div class="input-group input-group-md">
							<span class="input-group-addon">Estatus:</span>
								<select id="estatus" name="estatus" class="form-control">
                        			<option selected="" value="on">Activo</option>
                        			<option value="off">Desactivado</option>
                   				</select>
                   			</div>

							<div class="input-group input-group-md">
								<span class="input-group-addon">
									<input type="button" class="btn btn-primary" onClick="GuardarFormaPago();" value="Guardar" name="guardarforma" id="guardarforma">
								 </span>
							</div>`

		// var tmp1 = ``
		fmt.Fprintf(w, tmp0)

	}
}

func Guardar(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {

		var cambio_pago_bool bool = false
		var formadepago FormaPago

		nombre_pago := r.PostFormValue("nombre_pago")
		cambio_pago := r.PostFormValue("cambio_pago")
		comision_pago := r.PostFormValue("comision_pago")
		estatus := r.PostFormValue("estatus")

		comision_pago_float, _ := strconv.ParseFloat(comision_pago, 64)

		if cambio_pago == "si" {
			cambio_pago_bool = true
		}

		formadepago = FormaPago{Nombre: nombre_pago, Cambio: cambio_pago_bool, Comision: comision_pago_float, Estatus: estatus}
		insertado := pagos_m.InsertarPago(formadepago)

		if insertado == true {
			fmt.Fprintf(w, "Insertado")
		} else {
			fmt.Fprintf(w, "No insertado")
		}

	}

}

func Editar(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		ids := r.PostFormValue("id")
		result := pagos_m.GetFormadepago(ids)

		// var tmp0 = `<h1> %v </h1>`

		var tmp0id = `<div class="input-group input-group-md">
						<span class="input-group-addon">Id: </span>
						<input type="text" name="id_" id="id_" value="%v" class="form-control" disabled>
					</div>`

		var tmp1 = `<div class="input-group input-group-md">
						<span class="input-group-addon">Nombre: </span>
						<input type="text" name="nombre_forma_pago" id="nombre_forma_pago" value="%v" class="form-control">
					</div>`

		var tmp2si = `<div class="input-group input-group-md">
						<span class="input-group-addon">Requiere cambio:
							<label class="radio-inline"><input type="radio" name="cambio_" id="cambio_" value ="si" checked>Si</label>
							<label class="radio-inline"><input type="radio" name="cambio_" id="cambio_" value ="no">No</label>
						 </span>
					</div>`

		var tmp2no = `<div class="input-group input-group-md">
						<span class="input-group-addon">Requiere cambio:
							<label class="radio-inline"><input type="radio" name="cambio_" id="cambio_" value ="si">Si</label>
							<label class="radio-inline"><input type="radio" name="cambio_" id="cambio_" value ="no"checked>No</label>
						 </span>
					</div>`

		var tmp3com = `<div class="input-group input-group-md">
						<span class="input-group-addon">Comision: </span>
						<input type="text" name="comision_" id="comision_" value="%v" class="form-control">
					</div>`

		var tmp4edit = `<div class="input-group input-group-md">
								<span class="input-group-addon">
									<input type="button" class="btn btn-primary" onClick="GuardarFormaEditada();" value="Guardar" name="guardareditar" id="guardareditar">
								 </span>
						</div>`

		var tmp5staton = `<div class="input-group input-group-md">
						<span class="input-group-addon">Estatus:</span>
						<select id="estatus_edit" name="estatus_edit" class="form-control">
                        	<option selected="" value="on">Activo</option>
                        	<option value="off">Desactivado</option>
                   		</select>
                   </div>`

		var tmp5statoff = `<div class="input-group input-group-md">
						<span class="input-group-addon">Estatus:</span>
						<select id="estatus_edit" name="estatus_edit" class="form-control">
                        	<option value="on">Activo</option>
                        	<option selected="" value="off">Desactivado</option>
                   		</select>
                   </div>`

		fmt.Fprintf(w, tmp0id, result.Id.Hex())

		fmt.Fprintf(w, tmp1, result.Nombre)

		if result.Cambio == true {
			fmt.Fprintf(w, tmp2si)
		} else {
			fmt.Fprintf(w, tmp2no)
		}

		fmt.Fprintf(w, tmp3com, result.Comision)

		if result.Estatus == "on" {
			fmt.Fprintf(w, tmp5staton)
		} else {
			fmt.Fprintf(w, tmp5statoff)
		}

		fmt.Fprintf(w, tmp4edit)

	}

}

func GuardarEditar(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {

		r.ParseForm()

		var cambio_pago_bool bool = false
		var formadepago FormaPago
		var insertado bool = false

		id := r.FormValue("id")
		nombre_pago := r.FormValue("nombre_pago")
		cambio_pago := r.FormValue("cambio_pago")
		comision_pago := r.FormValue("comision_pago")
		estatus := r.FormValue("estatus")

		fmt.Println("id -> ", id)
		fmt.Println("nombre_pago -> ", nombre_pago)
		fmt.Println("cambio_pago -> ", cambio_pago)
		fmt.Println("comision_pago -> ", comision_pago)
		fmt.Println("estatus -> ", estatus)

		comision_pago_float, _ := strconv.ParseFloat(comision_pago, 64)

		if cambio_pago == "si" {
			cambio_pago_bool = true
		}

		objectid := bson.ObjectIdHex(id)

		formadepago = FormaPago{Nombre: nombre_pago, Cambio: cambio_pago_bool, Comision: comision_pago_float, Estatus: estatus}
		insertado = pagos_m.EditarFormadepago(formadepago, objectid)

		if insertado == true {
			fmt.Fprintf(w, "Insertado")
		} else {
			fmt.Fprintf(w, "No insertado")
		}

	}

}*/
