package Articulos

import (
	"encoding/json"
	"fmt"
	"html/template"
	"io"
	"net/http"
	"os"
	"strconv"

	"../../Controladores/Session"

	"../../Modelos/Articulos"
	"../../Modelos/General"
	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2/bson"
)

var tf = template.FuncMap{"add": add}

var tmpl = template.Must(template.New("").Funcs(tf).ParseFiles(
	"Vistas/index.html",
	"Vistas/Parciales/Plantilla/header.html",
	"Vistas/Parciales/Plantilla/footer.html",
	"Vistas/Parciales/Plantilla/styles.html",
	"Vistas/Parciales/Plantilla/scripts.html",
	"Vistas/Parciales/Paginas/Articulos/nuevotool.html",
	"Vistas/Parciales/Paginas/Articulos/scripts.html",
	"Vistas/Parciales/Paginas/Articulos/styles.html",
))

var tmpl_frame = template.Must(template.New("").Funcs(tf).ParseFiles(
	"Vistas/index.html",
	"Vistas/Parciales/Plantilla/header_blank.html",
	"Vistas/Parciales/Plantilla/footer_blank.html",
	"Vistas/Parciales/Plantilla/styles.html",
	"Vistas/Parciales/Plantilla/scripts.html",
	"Vistas/Parciales/Paginas/Articulos/new_frame.html",
	"Vistas/Parciales/Paginas/Articulos/scripts.html",
	"Vistas/Parciales/Paginas/Articulos/styles.html",
))

var tmpl2 = template.Must(template.New("").Funcs(tf).ParseFiles(
	"Vistas/index.html",
	"Vistas/Parciales/Plantilla/header.html",
	"Vistas/Parciales/Plantilla/footer.html",
	"Vistas/Parciales/Plantilla/styles.html",
	"Vistas/Parciales/Plantilla/scripts.html",
	"Vistas/Parciales/Paginas/Articulos/pruebaedit.html",
	"Vistas/Parciales/Paginas/Articulos/editScripts.html",
	"Vistas/Parciales/Paginas/Articulos/styles.html",
))

var tmpl2_frame = template.Must(template.New("").Funcs(tf).ParseFiles(
	"Vistas/index.html",
	"Vistas/Parciales/Plantilla/header_blank.html",
	"Vistas/Parciales/Plantilla/footer_blank.html",
	"Vistas/Parciales/Plantilla/styles.html",
	"Vistas/Parciales/Plantilla/scripts.html",
	"Vistas/Parciales/Paginas/Articulos/editar_frame.html",
	"Vistas/Parciales/Paginas/Articulos/editScripts.html",
	"Vistas/Parciales/Paginas/Articulos/styles.html",
))

var tmpl3 = template.Must(template.New("").Funcs(tf).ParseFiles(
	"Vistas/index.html",
	"Vistas/Parciales/Plantilla/header.html",
	"Vistas/Parciales/Plantilla/footer.html",
	"Vistas/Parciales/Plantilla/styles.html",
	"Vistas/Parciales/Plantilla/scripts.html",
	"Vistas/Parciales/Paginas/Articulos/index.html",
	"Vistas/Parciales/Paginas/Articulos/scripts.html",
	"Vistas/Parciales/Paginas/Articulos/styles.html",
))

var tmpl3_frame = template.Must(template.New("").Funcs(tf).ParseFiles(
	"Vistas/index.html",
	"Vistas/Parciales/Plantilla/header_blank.html",
	"Vistas/Parciales/Plantilla/footer_blank.html",
	"Vistas/Parciales/Plantilla/styles.html",
	"Vistas/Parciales/Plantilla/scripts.html",
	"Vistas/Parciales/Paginas/Articulos/index_frame.html",
	"Vistas/Parciales/Paginas/Articulos/scripts.html",
	"Vistas/Parciales/Paginas/Articulos/styles.html",
))

//ArticuloElastic estructura de datos de artículo para insertar en elastic
type ArticuloElastic struct {
	CodBarra    string            `json:"codigobarra"`
	Descripcion string            `json:"descripcion"`
	Imagen      bson.ObjectId     `json:"imagen,omitempty"`
	Fotos       []bson.ObjectId   `json:"fotos,omitempty"`
	Tipo        string            `json:"tipo"`
	Unidad      string            `json:"unidad"`
	Entero      bool              `json:"usa_fraccion"`
	NumDec      int               `json:"num_dec"`
	Etiquetas   map[string]string `json:"etiquetas"`
	PreCompra   float64           `json:"costo"`
	PreVenta    float64           `json:"precio"`
}

//CargaPantalla es una funcion que redirige
//al usuario a la pantalla de alta sin pasar por validaciones
func CargaPantalla(w http.ResponseWriter, r *http.Request) {
	userName := Session.GetUserName(r)
	if userName != "" {
		uni := ModeloArticulos.GeneraSelectUnidades()
		unidades := template.HTML(uni)

		estat := `<option value="" selected>--SELECCIONA--</option><option value="ACTIVO">ACTIVO</option><option value="INACTIVO">INACTIVO</option><option value="CANCELADO">CANCELADO</option>`
		estatus := template.HTML(estat)

		tmpl.ExecuteTemplate(w, "index_layout", map[string]interface{}{
			"unidades": unidades,
			"estatus":  estatus,
		})
	} else {
		http.Redirect(w, r, "/", 302)
	}
}

func CargaPantalla_frame(w http.ResponseWriter, r *http.Request) {
	userName := Session.GetUserName(r)
	if userName != "" {
		uni := ModeloArticulos.GeneraSelectUnidades()
		unidades := template.HTML(uni)

		estat := `<option value="" selected>--SELECCIONA--</option><option value="ACTIVO">ACTIVO</option><option value="INACTIVO">INACTIVO</option><option value="CANCELADO">CANCELADO</option>`
		estatus := template.HTML(estat)

		tmpl_frame.ExecuteTemplate(w, "index_layout", map[string]interface{}{
			"unidades": unidades,
			"estatus":  estatus,
		})
	} else {
		http.Redirect(w, r, "/", 302)
	}
}

//CargaPantallaIndex es una funcion que redirige
//al usuario a la pantalla de alta sin pasar por validaciones
func CargaPantallaIndex(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		userName := Session.GetUserName(r)
		if userName != "" {
			tmpl3.ExecuteTemplate(w, "index_layout", nil)
		} else {
			http.Redirect(w, r, "/", 302)
		}
	}
}

//CargaPantallaIndex es una funcion que redirige
//al usuario a la pantalla de alta sin pasar por validaciones
//_frame significa que solo se cargará la pantalla de productos, dejando en blanco la cabezera y el pie
func CargaPantallaIndex_frame(w http.ResponseWriter, r *http.Request) {
	userName := Session.GetUserName(r)
	if userName != "" {
		tmpl3_frame.ExecuteTemplate(w, "index_layout", nil)
	} else {
		http.Redirect(w, r, "/", 302)
	}
}

//VerificaCodigo valida que el código ingresado exista en la base
//de ser así carga la pantalla de alta de artículos
func VerificaCodigo(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		r.ParseForm()
		code := r.Form["code"]
		if code[0] != "" {
			n := ModeloArticulos.ConsultaCodigo(code[0])
			if n > 0 {
				fmt.Fprintf(w, "<span>El código ya ha sido asiganado a otro Artículo</span>")
			}
		} else {
			fmt.Fprintf(w, "<span>El código no debe estar vacío</span>")
		}

	}
}

//VerificaCodigoEdit valida que el código ingresado exista en la base
//de ser así carga la pantalla de alta de artículos
func VerificaCodigoEdit(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		r.ParseForm()
		code := r.Form["code"]
		oculto := r.Form["oculto"]
		if code[0] != oculto[0] {
			if code[0] != "" {
				n := ModeloArticulos.ConsultaCodigo(code[0])
				if n > 0 {
					fmt.Fprintf(w, "<span>El código ya ha sido asiganado a otro Artículo 1</span>")
				}
			} else {
				fmt.Fprintf(w, "<span>El código no debe estar vacío 2</span>")
			}
		}
	}
}

//EditProducto funcion que muestra la vista de artículos
//con los datos del producto a editar
func EditProducto(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		userName := Session.GetUserName(r)
		if userName != "" {
			vars := mux.Vars(r)
			code := vars["code"]

			if code != "" {
				n := ModeloArticulos.ConsultaCodigo(code)
				if n > 0 {
					producto, data := ModeloArticulos.ConsultaCodigoYRegresaEstructuras(code)
					var Product ModeloArticulos.Producto

					//Consulta de Etiquetas y armado de template

					var llavevalor = ""
					if len(producto.Etiquetas) > 0 {
						for k, v := range producto.Etiquetas {
							llavevalor += `<tr><td><input type="text" class="form-control" name="idecampo" value="` + k + `" readonly></td>
											<td><input type="text" class="form-control" name="idevalor" value="` + v + `" readonly></td>
											<td><button type="button" class="btn btn-danger deleteButton">
												<span class="glyphicon glyphicon-trash btn-xs"> </span></button>
											</td></tr>`
						}
					}

					etiquetass := template.HTML(llavevalor)

					//Carga de Imagenes
					tagimg := ``
					if producto.Imagen != "" {
						ruta, nom := ModeloArticulos.RegresaTagImagen(producto.Imagen.Hex())
						tagimg = `<img id="ImagenProducto" alt="Responsive image" name="ImagenProducto" width="100px" height="100px";" src="` + ruta + `">`
						Product.NomImg = nom
					}

					tagimg2 := template.HTML(tagimg)

					//Tipo de producto
					switch producto.Tipo {
					case "fisico":
						Product.TipoFisico = "checked"
					case "logico":
						Product.TipoLogico = "checked"
					}

					//Carga Combo de Unidades
					// uni := ModeloArticulos.ConsultaUnidadesDistintas()
					// templ := `<option value="">--SELECCIONA--</option>`
					// for _, v := range uni {
					// 	if producto.Unidad == v {
					// 		templ += `<option value=` + v + ` selected>` + v + `</option>`
					// 	} else {
					// 		templ += `<option value=` + v + `>` + v + `</option>`
					// 	}
					// }
					// unidades := template.HTML(templ)

					uni := ModeloArticulos.GeneraSelectUnidadesfijo(producto.Unidad)
					unidades := template.HTML(uni)

					estat := `<option value="">--SELECCIONA--</option>`
					estats := [3]string{"ACTIVO", "INACTIVO", "CANCELADO"}
					for _, v := range estats {
						if data.Estatus == v {
							estat += `<option value=` + v + ` selected>` + v + `</option>`
						} else {
							estat += `<option value=` + v + `>` + v + `</option>`
						}
					}
					estatus := template.HTML(estat)

					desci := `<option value="">--SELECCIONA--</option>`
					if producto.Entero == true {
						Product.Entero = "checked"
						for i := 0; i < 4; i++ {
							if producto.NumDec == i {
								desci += `<option value=` + strconv.Itoa(i) + ` selected>` + strconv.Itoa(i) + `</option>`
							} else {
								desci += `<option value=` + strconv.Itoa(i) + `>` + strconv.Itoa(i) + `</option>`
							}
						}
					} else {
						for i := 0; i < 4; i++ {
							desci += `<option value=` + strconv.Itoa(i) + `>` + strconv.Itoa(i) + `</option>`
						}
					}
					decimales := template.HTML(desci)

					costo := strconv.FormatFloat(producto.PreCompra, 'f', 2, 32)
					precio := strconv.FormatFloat(producto.PreVenta, 'f', 2, 32)

					Product.Articulo = producto
					Product.Datos = data

					tmpl2.ExecuteTemplate(w, "index_layout", map[string]interface{}{
						"result":       Product,
						"etiquetas":    etiquetass,
						"imagen":       tagimg2,
						"unidades":     unidades,
						"estatus":      estatus,
						"costo":        costo,
						"precio":       precio,
						"numDecimales": decimales,
					})

				} else {
					fmt.Fprintf(w, "No se encontró Registro")
				}
			} else {
				fmt.Fprintf(w, "No se ingresó código")
			}
		}
	} else if r.Method == "POST" {
		codigo := r.FormValue("codigobarra")
		fmt.Fprintf(w, codigo)
		http.Redirect(w, r, "/", 302)
	}
}

//EditProducto funcion que muestra la vista de artículos
//con los datos del producto a editar
func EditProducto_frame(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		userName := Session.GetUserName(r)
		if userName != "" {
			vars := mux.Vars(r)
			code := vars["code"]

			if code != "" {
				n := ModeloArticulos.ConsultaCodigo(code)
				if n > 0 {
					producto, data := ModeloArticulos.ConsultaCodigoYRegresaEstructuras(code)
					var Product ModeloArticulos.Producto

					//Consulta de Etiquetas y armado de template

					var llavevalor = ""
					if len(producto.Etiquetas) > 0 {
						for k, v := range producto.Etiquetas {
							llavevalor += `<tr><td><input type="text" class="form-control" name="idecampo" value="` + k + `" readonly></td>
											<td><input type="text" class="form-control" name="idevalor" value="` + v + `" readonly></td>
											<td><button type="button" class="btn btn-danger deleteButton">
												<span class="glyphicon glyphicon-trash btn-xs"> </span></button>
											</td></tr>`
						}
					}

					etiquetass := template.HTML(llavevalor)

					//Carga de Imagenes
					tagimg := ``
					if producto.Imagen != "" {
						ruta, nom := ModeloArticulos.RegresaTagImagen(producto.Imagen.Hex())
						tagimg = `<img id="ImagenProducto" alt="Responsive image" name="ImagenProducto" width="100px" height="100px";" src="` + ruta + `">`
						Product.NomImg = nom
					}

					tagimg2 := template.HTML(tagimg)

					//Tipo de producto
					switch producto.Tipo {
					case "fisico":
						Product.TipoFisico = "checked"
					case "logico":
						Product.TipoLogico = "checked"
					}
					uni := ModeloArticulos.GeneraSelectUnidadesfijo(producto.Unidad)
					unidades := template.HTML(uni)

					estat := `<option value="">--SELECCIONA--</option>`
					estats := [3]string{"ACTIVO", "INACTIVO", "CANCELADO"}
					for _, v := range estats {
						if data.Estatus == v {
							estat += `<option value=` + v + ` selected>` + v + `</option>`
						} else {
							estat += `<option value=` + v + `>` + v + `</option>`
						}
					}
					estatus := template.HTML(estat)

					desci := `<option value="">--SELECCIONA--</option>`
					if producto.Entero == true {
						Product.Entero = "checked"
						for i := 0; i < 4; i++ {
							if producto.NumDec == i {
								desci += `<option value=` + strconv.Itoa(i) + ` selected>` + strconv.Itoa(i) + `</option>`
							} else {
								desci += `<option value=` + strconv.Itoa(i) + `>` + strconv.Itoa(i) + `</option>`
							}
						}
					} else {
						for i := 0; i < 4; i++ {
							desci += `<option value=` + strconv.Itoa(i) + `>` + strconv.Itoa(i) + `</option>`
						}
					}
					decimales := template.HTML(desci)

					costo := strconv.FormatFloat(producto.PreCompra, 'f', 2, 32)
					precio := strconv.FormatFloat(producto.PreVenta, 'f', 2, 32)

					Product.Articulo = producto
					Product.Datos = data

					tmpl2_frame.ExecuteTemplate(w, "index_layout", map[string]interface{}{
						"result":       Product,
						"etiquetas":    etiquetass,
						"imagen":       tagimg2,
						"unidades":     unidades,
						"estatus":      estatus,
						"costo":        costo,
						"precio":       precio,
						"numDecimales": decimales,
					})

				} else {
					fmt.Fprintf(w, "No se encontró Registro")
				}
			} else {
				fmt.Fprintf(w, "No se ingresó código")
			}
		}
	} else if r.Method == "POST" {
		codigo := r.FormValue("codigobarra")
		fmt.Fprintf(w, codigo)
		http.Redirect(w, r, "/", 302)
	}
}

func add(x, y int) int {
	return x + y
}

//GeneraCodigo prueba
func GeneraCodigo(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		codigo := ModeloArticulos.GeneraCodigo()
		fmt.Fprintf(w, codigo)
	}
}

//InsertaArticulo es una funcion que inserta a mongo, postgres y elastic los datos del nuevo artículo
func InsertaArticulo(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		http.Redirect(w, r, "/", 302)
	} else if r.Method == "POST" {

		r.ParseMultipartForm(0)

		var Product ModeloArticulos.Articulo

		codigo := r.FormValue("codigobarra")
		descripcion := r.FormValue("descripcion")
		tipos := r.FormValue("tipos")
		unidades := r.FormValue("unidades")
		fraccion := r.FormValue("fraccion")
		decimales := r.FormValue("decimales")
		idecampo := r.Form["idecampo"]
		idevalor := r.Form["idevalor"]
		costo := r.FormValue("costo")
		precio := r.FormValue("precio")

		file, header, err := r.FormFile("imagen[]")

		if err == nil {
			check(err, "Error al seleccionar la imagen")
			defer file.Close()
			nombrefile := header.Filename

			dirpath := "./Recursos/Local/Imagenes"

			//Comprobar directorio y crearlo
			if _, err := os.Stat(dirpath); os.IsNotExist(err) {
				fmt.Println("el directorio no existe")
				os.MkdirAll(dirpath, 0777)
			} else {
				fmt.Println("el directorio ya existe")
			}

			//subir imagen al servidor local
			out, err := os.Create("./Recursos/Local/Imagenes/" + nombrefile)
			check(err, "Unable to create the file for writing. Check your write access privilege")
			defer out.Close()
			_, err = io.Copy(out, file)
			check(err, "Error al escribir la imagen al directorio")
			fmt.Println(nombrefile)
			pathimg := "./Recursos/Local/Imagenes"

			idsImg := ModeloArticulos.UploadImageToMongodb(pathimg, nombrefile)
			fmt.Println(w, "File uploaded successfully ")

			Product.Imagen = idsImg
			fmt.Println("id de imagen: ", idsImg)

		}
		etiquetas := make(map[string]string)

		t := len(idecampo)
		for i := 0; i < t-1; i++ {
			etiquetas[idecampo[i]] = idevalor[i]
		}

		Product.Etiquetas = etiquetas
		Product.ID = bson.NewObjectId()

		if codigo != "" {
			n := ModeloArticulos.ConsultaCodigo(codigo)
			if n > 0 {
				Product.CodBarra = ModeloArticulos.GeneraCodigo()
			} else {
				Product.CodBarra = codigo
			}
		} else {
			Product.CodBarra = ModeloArticulos.GeneraCodigo()
		}

		Product.Descripcion = descripcion
		Product.Tipo = tipos
		Product.Unidad = unidades

		if fraccion == "fraccion" {
			Product.Entero = true
			Product.NumDec, _ = strconv.Atoi(decimales)
		} else {
			Product.Entero = false
			Product.NumDec = 0
		}

		Product.PreCompra, _ = strconv.ParseFloat(costo, 64)
		Product.PreVenta, _ = strconv.ParseFloat(precio, 64)

		ModeloArticulos.InsertaProductoEnMongo(Product)
		articulo := ArticuloElastic{CodBarra: Product.CodBarra, Descripcion: Product.Descripcion, Imagen: Product.Imagen, Fotos: Product.Fotos, Tipo: Product.Tipo, Unidad: Product.Unidad, Entero: Product.Entero, NumDec: Product.NumDec, Etiquetas: Product.Etiquetas, PreCompra: Product.PreCompra, PreVenta: Product.PreVenta}
		ModeloArticulos.InsertElasticValue(models.INDICE_ELASTICSEARCH, models.COLECCION_ELASTICSEARCH, Product.ID.Hex(), articulo)
		http.Redirect(w, r, "/del/"+Product.CodBarra, 302)

	}
}

//InsertaArticulo es una funcion que inserta a mongo, postgres y elastic los datos del nuevo artículo
func InsertaArticuloFrame(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		http.Redirect(w, r, "/", 302)
	} else if r.Method == "POST" {

		r.ParseMultipartForm(0)

		var Product ModeloArticulos.Articulo

		codigo := r.FormValue("codigobarra")
		descripcion := r.FormValue("descripcion")
		tipos := r.FormValue("tipos")
		unidades := r.FormValue("unidades")
		fraccion := r.FormValue("fraccion")
		decimales := r.FormValue("decimales")
		idecampo := r.Form["idecampo"]
		idevalor := r.Form["idevalor"]
		costo := r.FormValue("costo")
		precio := r.FormValue("precio")

		file, header, err := r.FormFile("imagen[]")

		if err == nil {
			check(err, "Error al seleccionar la imagen")
			defer file.Close()
			nombrefile := header.Filename

			dirpath := "./Recursos/Local/Imagenes"

			//Comprobar directorio y crearlo
			if _, err := os.Stat(dirpath); os.IsNotExist(err) {
				fmt.Println("el directorio no existe")
				os.MkdirAll(dirpath, 0777)
			} else {
				fmt.Println("el directorio ya existe")
			}

			//subir imagen al servidor local
			out, err := os.Create("./Recursos/Local/Imagenes/" + nombrefile)
			check(err, "Unable to create the file for writing. Check your write access privilege")
			defer out.Close()
			_, err = io.Copy(out, file)
			check(err, "Error al escribir la imagen al directorio")
			fmt.Println(nombrefile)
			pathimg := "./Recursos/Local/Imagenes"

			idsImg := ModeloArticulos.UploadImageToMongodb(pathimg, nombrefile)
			fmt.Println(w, "File uploaded successfully ")

			Product.Imagen = idsImg
			fmt.Println("id de imagen: ", idsImg)

		}
		etiquetas := make(map[string]string)

		t := len(idecampo)
		for i := 0; i < t-1; i++ {
			etiquetas[idecampo[i]] = idevalor[i]
		}

		Product.Etiquetas = etiquetas
		Product.ID = bson.NewObjectId()

		if codigo != "" {
			n := ModeloArticulos.ConsultaCodigo(codigo)
			if n > 0 {
				Product.CodBarra = ModeloArticulos.GeneraCodigo()
			} else {
				Product.CodBarra = codigo
			}
		} else {
			Product.CodBarra = ModeloArticulos.GeneraCodigo()
		}

		Product.Descripcion = descripcion
		Product.Tipo = tipos
		Product.Unidad = unidades

		if fraccion == "fraccion" {
			Product.Entero = true
			Product.NumDec, _ = strconv.Atoi(decimales)
		} else {
			Product.Entero = false
			Product.NumDec = 0
		}

		Product.PreCompra, _ = strconv.ParseFloat(costo, 64)
		Product.PreVenta, _ = strconv.ParseFloat(precio, 64)

		ModeloArticulos.InsertaProductoEnMongo(Product)
		articulo := ArticuloElastic{CodBarra: Product.CodBarra, Descripcion: Product.Descripcion, Imagen: Product.Imagen, Fotos: Product.Fotos, Tipo: Product.Tipo, Unidad: Product.Unidad, Entero: Product.Entero, NumDec: Product.NumDec, Etiquetas: Product.Etiquetas, PreCompra: Product.PreCompra, PreVenta: Product.PreVenta}
		ModeloArticulos.InsertElasticValue(models.INDICE_ELASTICSEARCH, models.COLECCION_ELASTICSEARCH, Product.ID.Hex(), articulo)
		http.Redirect(w, r, "/del_frame/"+Product.CodBarra, 302)

	}
}

//ActualizaArticulo actualiza el artículo
func ActualizaArticulo(w http.ResponseWriter, r *http.Request) {

	if r.Method == "GET" {
		http.Redirect(w, r, "/", 302)
	} else if r.Method == "POST" {
		r.ParseMultipartForm(0)
		fmt.Println(r.PostForm)

		var Product ModeloArticulos.Articulo

		//get value form
		ids := r.FormValue("id")
		codigo := r.FormValue("codigobarra")
		codigobase := r.FormValue("codigobarraoculto")
		descripcion := r.FormValue("descripcion")
		tipos := r.FormValue("tipos")
		unidades := r.FormValue("unidades")
		fraccion := r.FormValue("fraccion")
		decimales := r.FormValue("decimales")
		idecampo := r.Form["idecampo"]
		idevalor := r.Form["idevalor"]
		costo := r.FormValue("costo")
		precio := r.FormValue("precio")

		fmt.Println(r.FormFile("imagen[]"))
		file, header, err := r.FormFile("imagen[]")
		if err == nil {
			//get img form
			fmt.Println("Entró con imagen nueva")

			check(err, "Error al seleccionar la imagen")

			nombrefile := header.Filename

			dirpath := "./Recursos/Local/Imagenes"

			//Comprobar directorio y crearlo
			if _, err := os.Stat(dirpath); os.IsNotExist(err) {
				fmt.Println("el directorio no existe")
				os.MkdirAll(dirpath, 0777)
			} else {
				fmt.Println("el directorio ya existe")
			}

			out, err := os.Create("./Recursos/Local/Imagenes/" + nombrefile)
			check(err, "Unable to create the file for writing. Check your write access privilege")
			defer out.Close()
			_, err = io.Copy(out, file)
			check(err, "Error al escribir la imagen al directorio")
			fmt.Println(w, "File uploaded successfully ")
			fmt.Println(nombrefile)
			pathimg := "./Recursos/Local/Imagenes"

			idsImg := ModeloArticulos.UploadImageToMongodb(pathimg, nombrefile)
			Product.Imagen = idsImg
			defer file.Close()
		}

		etiquetas := make(map[string]string)

		t := len(idecampo)
		for i := 0; i < t-1; i++ {
			etiquetas[idecampo[i]] = idevalor[i]
		}

		Product.Etiquetas = etiquetas
		Product.ID = bson.ObjectIdHex(ids)

		if codigo != "" {
			if codigo != codigobase {
				n := ModeloArticulos.ConsultaCodigo(codigo)
				if n > 0 {
					Product.CodBarra = ModeloArticulos.GeneraCodigo()
				} else {
					Product.CodBarra = codigo
				}
			} else {
				Product.CodBarra = codigo
			}
		} else {
			Product.CodBarra = ModeloArticulos.GeneraCodigo()
		}

		Product.Descripcion = descripcion
		Product.Tipo = tipos
		Product.Unidad = unidades

		if fraccion == "fraccion" {
			Product.Entero = true
			Product.NumDec, _ = strconv.Atoi(decimales)
		} else {
			Product.Entero = false
			Product.NumDec = 0
		}

		Product.PreCompra, _ = strconv.ParseFloat(costo, 64)
		Product.PreVenta, _ = strconv.ParseFloat(precio, 64)

		ModeloArticulos.ActualizaProductoEnMongo(Product)

		ModeloArticulos.DeleteElasticValue(models.INDICE_ELASTICSEARCH, models.COLECCION_ELASTICSEARCH, Product.ID.Hex())

		articulo := ArticuloElastic{CodBarra: Product.CodBarra, Descripcion: Product.Descripcion, Imagen: Product.Imagen, Fotos: Product.Fotos, Tipo: Product.Tipo, Unidad: Product.Unidad, Entero: Product.Entero, NumDec: Product.NumDec, Etiquetas: Product.Etiquetas, PreCompra: Product.PreCompra, PreVenta: Product.PreVenta}
		fmt.Println(articulo)
		ModeloArticulos.InsertElasticValue(models.INDICE_ELASTICSEARCH, models.COLECCION_ELASTICSEARCH, Product.ID.Hex(), articulo)
		http.Redirect(w, r, "/del/"+Product.CodBarra, 302)
	}
}

//ActualizaArticulo actualiza el artículo
func ActualizaArticuloFrame(w http.ResponseWriter, r *http.Request) {

	if r.Method == "GET" {
		http.Redirect(w, r, "/", 302)
	} else if r.Method == "POST" {
		r.ParseMultipartForm(0)
		fmt.Println(r.PostForm)

		var Product ModeloArticulos.Articulo

		ids := r.FormValue("id")
		codigo := r.FormValue("codigobarra")
		codigobase := r.FormValue("codigobarraoculto")
		descripcion := r.FormValue("descripcion")
		tipos := r.FormValue("tipos")
		unidades := r.FormValue("unidades")
		fraccion := r.FormValue("fraccion")
		decimales := r.FormValue("decimales")
		idecampo := r.Form["idecampo"]
		idevalor := r.Form["idevalor"]
		costo := r.FormValue("costo")
		precio := r.FormValue("precio")

		fmt.Println(r.FormFile("imagen[]"))
		file, header, err := r.FormFile("imagen[]")
		if err == nil {
			check(err, "Error al seleccionar la imagen")

			nombrefile := header.Filename

			dirpath := "./Recursos/Local/Imagenes"

			//Comprobar directorio y crearlo
			if _, err := os.Stat(dirpath); os.IsNotExist(err) {
				fmt.Println("el directorio no existe")
				os.MkdirAll(dirpath, 0777)
			} else {
				fmt.Println("el directorio ya existe")
			}

			out, err := os.Create("./Recursos/Local/Imagenes/" + nombrefile)
			check(err, "Unable to create the file for writing. Check your write access privilege")
			defer out.Close()
			_, err = io.Copy(out, file)
			check(err, "Error al escribir la imagen al directorio")
			fmt.Println(w, "File uploaded successfully ")
			fmt.Println(nombrefile)
			pathimg := "./Recursos/Local/Imagenes"

			idsImg := ModeloArticulos.UploadImageToMongodb(pathimg, nombrefile)
			Product.Imagen = idsImg
			defer file.Close()
		}

		etiquetas := make(map[string]string)

		t := len(idecampo)
		for i := 0; i < t-1; i++ {
			etiquetas[idecampo[i]] = idevalor[i]
		}

		Product.Etiquetas = etiquetas
		Product.ID = bson.ObjectIdHex(ids)

		if codigo != "" {
			if codigo != codigobase {
				n := ModeloArticulos.ConsultaCodigo(codigo)
				if n > 0 {
					Product.CodBarra = ModeloArticulos.GeneraCodigo()
				} else {
					Product.CodBarra = codigo
				}
			} else {
				Product.CodBarra = codigo
			}
		} else {
			Product.CodBarra = ModeloArticulos.GeneraCodigo()
		}

		Product.Descripcion = descripcion
		Product.Tipo = tipos
		Product.Unidad = unidades

		if fraccion == "fraccion" {
			Product.Entero = true
			Product.NumDec, _ = strconv.Atoi(decimales)
		} else {
			Product.Entero = false
			Product.NumDec = 0
		}

		Product.PreCompra, _ = strconv.ParseFloat(costo, 64)
		Product.PreVenta, _ = strconv.ParseFloat(precio, 64)

		ModeloArticulos.ActualizaProductoEnMongo(Product)

		ModeloArticulos.DeleteElasticValue(models.INDICE_ELASTICSEARCH, models.COLECCION_ELASTICSEARCH, Product.ID.Hex())

		articulo := ArticuloElastic{CodBarra: Product.CodBarra, Descripcion: Product.Descripcion, Imagen: Product.Imagen, Fotos: Product.Fotos, Tipo: Product.Tipo, Unidad: Product.Unidad, Entero: Product.Entero, NumDec: Product.NumDec, Etiquetas: Product.Etiquetas, PreCompra: Product.PreCompra, PreVenta: Product.PreVenta}
		fmt.Println(articulo)
		ModeloArticulos.InsertElasticValue(models.INDICE_ELASTICSEARCH, models.COLECCION_ELASTICSEARCH, Product.ID.Hex(), articulo)
		http.Redirect(w, r, "/del_frame/"+Product.CodBarra, 302)
	}
}

//EliminarDirectorio elimina los archivos creados temporalmente y redirecciona
//a la edición de artículos.
func EliminarDirectorio(w http.ResponseWriter, r *http.Request) {
	var er = os.RemoveAll("./Recursos/Local/Imagenes")
	check(er, "No se pudo eliminar el directorio")
	http.Redirect(w, r, "/indexarticulos", 302)
}

//EliminarDirectorio elimina los archivos creados temporalmente y redirecciona
//a la edición de artículos.
func EliminarDirectorioFrame(w http.ResponseWriter, r *http.Request) {
	var er = os.RemoveAll("./Recursos/Local/Imagenes")
	check(er, "No se pudo eliminar el directorio")
	http.Redirect(w, r, "/indexarticulos_frame", 302)
}

func check(err error, mensaje string) {
	if err != nil {
		fmt.Println("____")
		fmt.Println(mensaje)
		fmt.Println("____")
		panic(err)
	}
}

//Variables globales para búsqueda y paginación
var Ids []string
var numero_registros int
var tammuestra int = 8

//CargaArticulos funcion que regresa los primeros artoculos de la base y recupoera todos en una variable global
func CargaArticulos(w http.ResponseWriter, r *http.Request) {

	if r.Method == "POST" {
		result := ModeloArticulos.GetAllArticles()

		if len(result) > 0 {

			Ids = nil
			for _, item := range result {
				iid := item.ID.Hex()
				Ids = append(Ids, iid)
			}

			numero_registros = len(Ids)
			var arrayids []bson.ObjectId

			reg := len(Ids)
			if reg < tammuestra {
				for _, valor := range Ids {
					hexa := bson.ObjectIdHex(valor)
					arrayids = append(arrayids, hexa)
				}
			} else {
				for _, valor := range Ids[0:tammuestra] {
					hexa := bson.ObjectIdHex(valor)
					arrayids = append(arrayids, hexa)
				}
			}

			result = ModeloArticulos.GetArticles(arrayids)
			result = ModeloArticulos.RegresaSrcImagenes(result)

			jData, _ := json.Marshal(result)
			w.Header().Set("Content-Type", "application/json")
			w.Write(jData)

		} else {
			fmt.Println("No se encontraron Datos en la Base de Datos")
		}

	} else if r.Method == "GET" {
	}
}

//BuscaArticulos funcion que busca artoculos en elastic
func BuscaArticulos(w http.ResponseWriter, r *http.Request) {
	Ids = nil
	if r.Method == "POST" {
		r.ParseForm()
		dato := r.FormValue("dato")
		if dato != "" {
			conexionElastic := ModeloArticulos.ConectarElastic()
			docs := ModeloArticulos.BuscarEnElastic(dato, conexionElastic)

			if docs.Hits.TotalHits > 0 {
				numero_registros = int(docs.Hits.TotalHits)
				for _, item := range docs.Hits.Hits {
					iid := item.Id
					Ids = append(Ids, iid)
				}
				var arrayids []bson.ObjectId

				reg := len(Ids)
				if reg < tammuestra {
					for _, valor := range Ids {
						hexa := bson.ObjectIdHex(valor)
						arrayids = append(arrayids, hexa)
					}
				} else {
					for _, valor := range Ids[0:tammuestra] {
						hexa := bson.ObjectIdHex(valor)
						arrayids = append(arrayids, hexa)
					}
				}

				result := ModeloArticulos.GetArticles(arrayids)
				result = ModeloArticulos.RegresaSrcImagenes(result)

				ModeloArticulos.FlushElastic(conexionElastic)

				jData, _ := json.Marshal(result)

				w.Header().Set("Content-Type", "application/json")

				w.Write(jData)

			} else {
				fmt.Println("No se encontró algún registro")
			}

		} else {
			fmt.Println("Sin datos que consultar")
		}
	}
}

//GetSearchPaginationGeneral funcion que regresa artículos de correspondiente paginación
func GetSearchPaginationGeneral(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {

		pageS := r.FormValue("num_page")
		page, _ := strconv.Atoi(pageS)

		lim := tammuestra
		skip := (page * lim) - lim
		limite := skip + lim
		fmt.Println("Solicita la página: ", page, " del número: ", skip, " al: ", limite)
		result, _ := GetValuesDataMongodbPagination(limite, skip, page)
		jData, _ := json.Marshal(result)
		w.Header().Set("Content-Type", "application/json")
		w.Write(jData)
	}
}

//GetValuesDataMongodbPagination funcion obtiene de un conjunto de Ids
func GetValuesDataMongodbPagination(limit int, skip int, page int) ([]ModeloArticulos.Articulo, int) {
	var arrayids []bson.ObjectId
	//limit es el rango final de la segunda página
	//skip desde donde va a comenzar
	//Si es una página final entonces debe ser el ´límite del tamaño del arreglo de Ids

	numpags := float32(numero_registros) / float32(tammuestra)
	numpags2 := int(numpags)
	if numpags > float32(numpags2) {
		numpags2++
		fmt.Println("se aumentó uno el total")
	}

	if page == numpags2 {
		final := len(Ids) % tammuestra
		fmt.Println(final)
		if final == 0 {
			for _, valor := range Ids[skip:limit] {
				hexa := bson.ObjectIdHex(valor)
				arrayids = append(arrayids, hexa)
			}
			fmt.Println("Se consultó última página: ", page, "del: ", skip, " al ", limit)
		} else {
			for _, valor := range Ids[skip : skip+final] {
				hexa := bson.ObjectIdHex(valor)
				arrayids = append(arrayids, hexa)
			}
			fmt.Println("Se consultó última página: ", page, "del: ", skip, " al ", skip+final)
		}

	} else {
		for _, valor := range Ids[skip:limit] {
			hexa := bson.ObjectIdHex(valor)
			arrayids = append(arrayids, hexa)
		}

	}

	result := ModeloArticulos.GetArticles(arrayids)
	result = ModeloArticulos.RegresaSrcImagenes(result)

	return result, numero_registros
}

//GetAllResultSearchElastic function for know pagination number
func GetAllResultSearchElastic(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {

		page, _ := strconv.Atoi(r.FormValue("getPage"))

		fmt.Println("valores de IDS->", len(Ids))

		var tmpl_pag = ` <ul class="pagination pagination-lg" > `

		var numTotal int

		// if numero_registros <= 10000 {
		// 	numTotal = int(numero_registros)
		// } else if int(numero_registros) > 10000 {
		// 	numTotal = 10000
		// }

		numTotal = len(Ids)
		NumPagina := float32(numTotal) / float32(tammuestra)
		NumPagina2 := int(NumPagina)
		if NumPagina > float32(NumPagina2) {
			NumPagina2++
		}
		fmt.Println(NumPagina2)

		if page == 1 {
			tmpl_pag = tmpl_pag + ` <li><a><i class="fa fa-angle-left"></i></a></li>`
		} else if page > 1 {
			tmpl_pag = tmpl_pag + ` <li>
                                <a onClick="getPagination(` + strconv.Itoa(page-1) + `);">
                                    <i class="fa fa-angle-left"></i>
                                </a>
                            </li>`
		}

		var i int = page - 2
		var y int = i + 4
		if NumPagina2 > 5 {
			if i >= 1 {
				if y <= NumPagina2-2 {
					for i <= y {
						if i == page {
							tmpl_pag = tmpl_pag + `<li class="page-active"><a onClick="getPagination(` + strconv.Itoa(i) + `);"> ` + strconv.Itoa(i) + ` </a></li>`
						} else {
							tmpl_pag = tmpl_pag + `<li><a onClick="getPagination(` + strconv.Itoa(i) + `);"> ` + strconv.Itoa(i) + ` </a></li>`
						}
						i++
						//fmt.Println(i)
					}
					tmpl_pag = tmpl_pag + `<li><a> ... </a></li>`
					tmpl_pag = tmpl_pag + `<li><a onClick="getPagination(` + strconv.Itoa(NumPagina2) + `);"> ` + strconv.Itoa(NumPagina2) + ` </a></li>`
				} else {
					var z int = NumPagina2 - 4

					tmpl_pag = tmpl_pag + `<li><a onClick="getPagination(` + strconv.Itoa(1) + `);"> ` + strconv.Itoa(1) + ` </a></li>`
					tmpl_pag = tmpl_pag + `<li><a> ... </a></li>`
					for z <= NumPagina2 {
						if z == page {
							tmpl_pag = tmpl_pag + `<li class="page-active"><a onClick="getPagination(` + strconv.Itoa(z) + `);"> ` + strconv.Itoa(z) + ` </a></li>`
						} else {
							tmpl_pag = tmpl_pag + `<li><a onClick="getPagination(` + strconv.Itoa(z) + `);"> ` + strconv.Itoa(z) + ` </a></li>`
						}
						z++
					}
				}
			} else if i <= 0 {
				var x int = 1
				for x <= 5 {
					if x == page {
						tmpl_pag = tmpl_pag + `<li class="page-active"><a onClick="getPagination(` + strconv.Itoa(x) + `);"> ` + strconv.Itoa(x) + ` </a></li>`
					} else {
						tmpl_pag = tmpl_pag + `<li><a onClick="getPagination(` + strconv.Itoa(x) + `);"> ` + strconv.Itoa(x) + ` </a></li>`
					}
					x++
				}
				tmpl_pag = tmpl_pag + `<li><a> ... </a></li>`
				tmpl_pag = tmpl_pag + `<li><a onClick="getPagination(` + strconv.Itoa(NumPagina2) + `);"> ` + strconv.Itoa(NumPagina2) + ` </a></li>`
			}
		} else {
			var x int = 1
			for x <= NumPagina2 {
				if i == page {
					tmpl_pag = tmpl_pag + `<li class="page-active"><a onClick="getPagination(` + strconv.Itoa(x) + `);"> ` + strconv.Itoa(x) + ` </a></li>`
				} else {
					tmpl_pag = tmpl_pag + `<li><a onClick="getPagination(` + strconv.Itoa(x) + `);"> ` + strconv.Itoa(x) + ` </a></li>`
				}
				x++
			}
			tmpl_pag = tmpl_pag + `<li><a> ... </a></li>`
			tmpl_pag = tmpl_pag + `<li><a onClick="getPagination(` + strconv.Itoa(NumPagina2) + `);"> ` + strconv.Itoa(NumPagina2) + ` </a></li>`
		}

		if page == NumPagina2 {
			tmpl_pag = tmpl_pag + `<li><a><i class="fa fa-angle-right"></i></a></li>`
		} else if page < NumPagina2 {
			tmpl_pag = tmpl_pag + `<li>
                                <a onClick="getPagination(` + strconv.Itoa(page+1) + `);">
                                    <i class="fa fa-angle-right"></i>
                                </a>
                            </li>`
		}

		tmpl_pag = tmpl_pag + ` </ul> `

		fmt.Fprintf(w, tmpl_pag)

	}

}
