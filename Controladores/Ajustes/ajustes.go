package ajustes

import (
	"fmt"
	"html/template"
	"net/http"
	"strconv"
	"time"

	"../../Controladores/Session"
	"../../Modelos/Ajustes"

	"gopkg.in/mgo.v2/bson"
)

type Movimiento struct {
	ID             bson.ObjectId `bson:"_id,omitempty"`
	Tipo           string        `bson:"tipo_movimiento"`
	Date           time.Time     `bson:"fecha_hora"`
	AlmacenOrigen  bson.ObjectId `bson:"almacen_origen,omitempty"`
	AlmacenDestino bson.ObjectId `bson:"almacen_destino,omitempty"`
	Usuario        bson.ObjectId `bson:"usuario,omitempty"`
	Productos      []Producto    `bson:"productos"`
}

type MovimientoA struct {
	ID             bson.ObjectId `bson:"_id,omitempty"`
	Tipo           string        `bson:"tipo_movimiento"`
	Date           time.Time     `bson:"fecha_hora"`
	AlmacenOrigen  bson.ObjectId `bson:"almacen_origen,omitempty"`
	AlmacenDestino bson.ObjectId `bson:"almacen_destino,omitempty"`
	Usuario        bson.ObjectId `bson:"usuario,omitempty"`
	Productos      []ProductoA   `bson:"productos"`
}

type Producto struct {
	ID            bson.ObjectId `bson:"_id,omitempty"`
	CodigoDeBarra string        `bson:"codigobarra"`
	Nombre        string        `bson:"nombre"`
	CantidadA     float64       `bson:"cantidad_anterior"`
	CantidadB     float64       `bson:"cantidad_solicitada"`
	CantidadC     float64       `bson:"cantidad_final"`
	Operacion     string        `bson:"operacion"`
}
type ProductoA struct {
	ID            bson.ObjectId `bson:"_id,omitempty"`
	CodigoDeBarra string        `bson:"codigobarra"`
	Nombre        string        `bson:"nombre"`
	CantidadA     float64       `bson:"existencia_origen"`
	CantidadB     float64       `bson:"existencia_destino"`
	CantidadC     float64       `bson:"cantidad_solicitada"`
	CantidadD     float64       `bson:"cantidad_origen"`
	CantidadE     float64       `bson:"cantidad_destino"`
}

type Almacen struct {
	Id      bson.ObjectId     `bson:"_id,omitempty"`
	Nombre  string            `bson:"nombre"`
	Tipo    string            `bson:"tipo"`
	Defecto string            `bson:"defecto"`
	Campos  map[string]string `bson:"datos"`
}

var tmpl_ajustes_frame = template.Must(template.ParseFiles(
	"Vistas/index.html",
	"Vistas/Parciales/Plantilla/header_blank.html",
	"Vistas/Parciales/Plantilla/footer_blank.html",
	"Vistas/Parciales/Plantilla/styles.html",
	"Vistas/Parciales/Plantilla/scripts.html",
	"Vistas/Parciales/Paginas/Ajustes/index.html",
))

func AjustesFrame(w http.ResponseWriter, r *http.Request) {
	userName := Session.GetUserName(r)
	if userName != "" {
		result := ajustes_m.GetAlmacenes()
		tmpl_ajustes_frame.ExecuteTemplate(w, "index_layout", result)
	} else {
		http.Redirect(w, r, "/login", 302)
	}
}

var tmpl_ajustes = template.Must(template.ParseFiles(
	"Vistas/index.html",
	"Vistas/Parciales/Plantilla/header.html",
	"Vistas/Parciales/Plantilla/footer.html",
	"Vistas/Parciales/Plantilla/styles.html",
	"Vistas/Parciales/Plantilla/scripts.html",
	"Vistas/Parciales/Paginas/Ajustes/index.html",
))

func Index(w http.ResponseWriter, r *http.Request) {
	userName := Session.GetUserName(r)
	if userName != "" {
		result := ajustes_m.GetAlmacenes()
		tmpl_ajustes.ExecuteTemplate(w, "index_layout", result)
	} else {
		http.Redirect(w, r, "/login", 302)
	}
}

func AjusteoTraslado(w http.ResponseWriter, r *http.Request) {

	alm_origen := r.FormValue("origen")
	alm_destino := r.FormValue("destino")
	tipo := r.FormValue("tipo")
	var tmp_tipo_ajuste = ``

	fmt.Println(alm_origen)
	fmt.Println(alm_destino)
	fmt.Println(tipo)

	if tipo == "ajuste" {
		tmp_tipo_ajuste = `	<div class="page-header">
								<h3 class="text-center">Ajuste : </h3>
							</div>
							<div class="input-group input-group-md">
									<span class="input-group-addon">Codigo de Barra:</span>
									<input type="text" name="elarticulo" onKeydown="Javascript: if (event.keyCode==13) GetArticulo();"  id="elarticulo" class="form-control selectpicker" autofocus>
									<label class="input-group-addon" for="elarticulo">
									<span  class="glyphicon glyphicon-circle-arrow-down"></span>
									</label>													
									
									<span class="input-group-addon">
										<buttont type="button" class="btn btn-primary" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-target=".bd-example-modal-lg">Buscar</button>										
									</span>																
							</div>`

		fmt.Fprintf(w, tmp_tipo_ajuste)

	} else if tipo == "traslado" {
		tmp_tipo_ajuste = `	<div class="page-header">
								<h3 class="text-center" >Traslado: </h3>
							</div>
							<div class="input-group input-group-md">
									<span class="input-group-addon">Codigo de Barra:</span>
									<input type="text" name="elarticulo" onKeydown="Javascript: if (event.keyCode==13) GetArticulo();"  id="elarticulo" class="form-control selectpicker" autofocus>
									<label class="input-group-addon" for="elarticulo">
									<span  class="glyphicon glyphicon-circle-arrow-down"></span>
									</label>													
									
									<span class="input-group-addon">
										<buttont type="button" class="btn btn-primary" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-target=".bd-example-modal-lg">Buscar</button>
									</span>																
							</div>`

		fmt.Fprintf(w, tmp_tipo_ajuste)

	}

}

func GetProductoUnico(w http.ResponseWriter, r *http.Request) {
	userName := Session.GetUserName(r)
	if userName != "" {
		cod_barra := r.FormValue("cod_b_art")
		alm_origen := r.FormValue("origen")
		alm_destino := r.FormValue("destino")
		num_art := r.FormValue("articulos_agregados")
		tipo := ""

		if alm_origen == alm_destino {
			tipo = "ajuste"
		} else {
			tipo = "traslado"
		}

		articulo_mongo := ajustes_m.GetProductoMongo(cod_barra)

		if articulo_mongo.ID == "" {
			fmt.Fprintf(w, `<h3 class="text-center">Articulo no encontrado</h3>`)
		} else {

			// ir por este articulo a el almacen de origen y destino.

			articulo_origen, articulo_destino := ajustes_m.GetProductoPostgres(articulo_mongo.ID.Hex(), alm_origen, alm_destino)

			fmt.Println(articulo_origen)
			fmt.Println(articulo_destino)

			precio_compra_string := strconv.FormatFloat(articulo_mongo.PreCompra, 'f', -1, 64)
			cantidad_origen := strconv.FormatFloat(articulo_origen.Cantidad, 'f', -1, 64)
			cantidad_destino := strconv.FormatFloat(articulo_destino.Cantidad, 'f', -1, 64)

			tmp_ajuste := `	<tr class="renglon">
								<td id="codigo_b` + num_art + `">` + articulo_mongo.CodBarra + `</td>
				  				<td id="desc_b` + num_art + `">` + articulo_mongo.Descripcion + `</td>
				  				<td id="precio_b` + num_art + `">` + precio_compra_string + `</td> 
				  				<td id="origen_b` + num_art + `">` + cantidad_origen + `</td>
				  				
				  				<td id="operacion_b` + num_art + `"> 

				  				<label class="radio-inline"><input type="radio" name="operacion` + articulo_mongo.CodBarra + `" id="operacion` + articulo_mongo.CodBarra + `" value ="sumar" checked>Suma</label>
								<label class="radio-inline"><input type="radio" name="operacion` + articulo_mongo.CodBarra + `" id="operacion` + articulo_mongo.CodBarra + `" value ="restar">Resta</label>

				  				</td>

				  				<td id="cantidad_b` + num_art + `"><input type="text" name="cantidad` + articulo_mongo.CodBarra + `" id="` + articulo_mongo.CodBarra + `" value="0" requerided  pattern="\d*"></td>
				  				<td><span class="btn btn-danger eliminar">-</span></td>
							</tr>`

			tmp_traslado := `	<tr class="renglon">
								<td id="codigo_b` + num_art + `">` + articulo_mongo.CodBarra + `</td>
				  				<td id="desc_b` + num_art + `">` + articulo_mongo.Descripcion + `</td>
				  				<td id="precio_b` + num_art + `">` + precio_compra_string + `</td> 
				  				<td id="origen_b` + num_art + `">` + cantidad_origen + `</td>
				  				<td id="destino_b` + num_art + `">` + cantidad_destino + `</td>				  				

				  				<td id="cantidad_b` + num_art + `"><input type="text" name="cantidad` + articulo_mongo.CodBarra + `" id="` + articulo_mongo.CodBarra + `" value="0" requerided="true"  pattern="\d*"></input></td>
				  				<td><span class="btn btn-danger eliminar">-</span></td>
							</tr>`

			switch {
			//1
			case articulo_destino.Estatus == "ACTIVO":
				switch {
				case articulo_origen.Estatus == "ACTIVO":
					switch {
					case tipo == "ajuste":
						fmt.Fprintf(w, tmp_ajuste)
						break
					case tipo == "traslado":
						fmt.Fprintf(w, tmp_traslado)
						break
					}
					break
				case articulo_origen.Estatus == "DESACTIVADO":
					fmt.Fprintf(w, `<h3 class="text-center">Origen: Desactivado     Destino: Activo</h3>`)
					break
				case articulo_origen.Estatus == "BLOQUEADO":
					fmt.Fprintf(w, `<h3 class="text-center">Origen: Bloqueado      Destino: Activo</h3>`)
					break
				case articulo_origen.Estatus == "":
					fmt.Fprintf(w, `<h3 class="text-center">Origen: No Existe     Destino: Activo</h3>`)

				}
				break
				//2
			case articulo_origen.Estatus == "ACTIVO":
				switch {
				case articulo_destino.Estatus == "ACTIVO":
					switch {
					case tipo == "ajuste":
						fmt.Fprintf(w, tmp_ajuste)
						break
					case tipo == "traslado":
						fmt.Fprintf(w, tmp_traslado)
						break
					}
					break
				case articulo_destino.Estatus == "DESACTIVADO":
					fmt.Fprintf(w, `<h3 class="text-center">Origen: Activo      Destino: Desactivado</h3>`)
					break
				case articulo_destino.Estatus == "BLOQUEADO":
					fmt.Fprintf(w, `<h3 class="text-center">Origen: Activo      Destino: Bloqueado</h3>`)
					break
				case articulo_destino.Estatus == "":
					switch {
					case tipo == "ajuste":
						fmt.Fprintf(w, tmp_ajuste)
						break
					case tipo == "traslado":
						fmt.Fprintf(w, tmp_traslado)
						break
					}
					break
				}
				break
				//3
			case articulo_destino.Estatus == "":
				switch {
				case articulo_origen.Estatus == "":
					fmt.Fprintf(w, `<h3 class="text-center">Origen: No Existe      Destino: No Existe</h3>`)
					break
				}
				break
				//4
			case articulo_origen.Estatus == "":
				switch {
				case articulo_destino.Estatus == "":
					fmt.Fprintf(w, `<h3 class="text-center">Origen: No Existe      Destino: No Existe</h3>`)
					break
				}
				break
			}

		}
	} else {
		http.Redirect(w, r, "/login", 302)
	}
}

func RealizarMovimiento(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		userName := Session.GetUserName(r)
		fmt.Println(userName)

		//ir por el ID del usuario
		r.ParseForm()

		var origen string
		var destino string
		origen = r.FormValue("origen")
		destino = r.FormValue("destino")

		object_origen := bson.ObjectIdHex(origen)
		object_destino := bson.ObjectIdHex(destino)

		usuario_mongo := ajustes_m.GetUsuario(userName)
		fmt.Println(usuario_mongo)

		var codigos = []string{}
		var nombres = []string{}
		// var precios = []string{}
		var existencias = []string{}
		var existenciasd = []string{}
		var operaciones = []string{}
		var cantidades = []string{}

		for k, v := range r.PostForm {

			if k == "codigos[]" {
				codigos = v
			}
			if k == "nombres[]" {
				nombres = v
			}
			// if k == "precios[]" {
			// 	precios = v
			// }
			if k == "existencias[]" {
				existencias = v
			}
			if k == "operaciones[]" && origen == destino {
				operaciones = v
			}
			if k == "cantidades[]" {
				cantidades = v
			}
			if k == "existenciasd[]" && origen != destino {
				existenciasd = v
			}
		}

		lim := len(codigos)
		var total float64
		var totald float64
		var movimiento_ajuste Movimiento
		var producto_ajuste Producto
		var productos_ajuste []Producto

		var movimiento_traslado MovimientoA
		var producto_traslado ProductoA
		var productos_traslado []ProductoA

		if origen == destino {

			for i := 0; i < lim; i++ {
				nombresito := nombres[i]
				codigodebarra := codigos[i]
				operacion := operaciones[i]
				cantidad := cantidades[i]
				existe := existencias[i]

				articulo_mongo_id_string := ajustes_m.GetProductoMongoID(codigodebarra)
				object_articulo := bson.ObjectIdHex(articulo_mongo_id_string)

				existe_float, _ := strconv.ParseFloat(existe, 64)
				cantidad_float, _ := strconv.ParseFloat(cantidad, 64)

				if operacion == "sumar" {
					total = cantidad_float + existe_float

				} else if operacion == "restar" {
					total = existe_float - cantidad_float
				}

				producto_ajuste = Producto{ID: object_articulo, CodigoDeBarra: codigodebarra, Nombre: nombresito, CantidadA: existe_float, CantidadB: cantidad_float, CantidadC: total, Operacion: operacion}
				productos_ajuste = append(productos_ajuste, producto_ajuste)
				ajustes_m.RealizarAjuste(origen, articulo_mongo_id_string, total)

			}

			movimiento_ajuste = Movimiento{Tipo: "Ajuste", Date: time.Now(), AlmacenOrigen: object_origen, AlmacenDestino: object_destino, Usuario: usuario_mongo.Id, Productos: productos_ajuste}
			fmt.Println(movimiento_ajuste)
			insertado := ajustes_m.GuardarMovimiento(movimiento_ajuste)
			if insertado == true {
				fmt.Println("Movimiento de ajuste realizado")
			}

		} else {

			for i := 0; i < lim; i++ {

				nombresito := nombres[i]
				codigodebarra := codigos[i]
				cantidad := cantidades[i]
				existe := existencias[i]
				existed := existenciasd[i]

				articulo_mongo_id_string := ajustes_m.GetProductoMongoID(codigodebarra)
				object_articulo := bson.ObjectIdHex(articulo_mongo_id_string)

				existe_float, _ := strconv.ParseFloat(existe, 64)
				existed_float, _ := strconv.ParseFloat(existed, 64)
				cantidad_float, _ := strconv.ParseFloat(cantidad, 64)

				articulo_origen, articulo_destino := ajustes_m.GetProductoPostgres(articulo_mongo_id_string, origen, destino)

				totald = articulo_destino.Cantidad + cantidad_float
				total = articulo_origen.Cantidad - cantidad_float

				producto_traslado = ProductoA{ID: object_articulo, CodigoDeBarra: codigodebarra, Nombre: nombresito, CantidadA: existe_float, CantidadB: existed_float, CantidadC: cantidad_float, CantidadD: total, CantidadE: totald}

				productos_traslado = append(productos_traslado, producto_traslado)

				if totald < 0 {
					fmt.Fprintf(w, "no")

				} else {

					ajustes_m.RealizarTraslado(origen, destino, articulo_mongo_id_string, total, totald)
				}

			}
			movimiento_traslado = MovimientoA{Tipo: "Traslado", Date: time.Now(), AlmacenOrigen: object_origen, AlmacenDestino: object_destino, Usuario: usuario_mongo.Id, Productos: productos_traslado}
			fmt.Println(movimiento_traslado)
			insertado := ajustes_m.GuardarMovimiento(movimiento_traslado)
			if insertado == true {
				fmt.Println("Movimiento de traslado realizado")
			}
		}

	}
}
