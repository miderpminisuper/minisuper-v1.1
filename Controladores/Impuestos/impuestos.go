package impuestos

import (
	"fmt"
	"html/template"
	"net/http"
	"strconv"

	"../../Modelos/Impuestos"
	"../Session"
	"gopkg.in/mgo.v2/bson"
)

type MetaImpuesto struct {
	Id        bson.ObjectId `bson:"_id,omitempty"`
	Grupo     string        `bson:"grupo_impuesto"`
	Prioridad int           `bson:"prioridad"`
	Impuestos []Impuesto    `bson:"impuestos"`
}

type Impuesto struct {
	Nombre      string  `bson:"nombre"`
	Tipo        string  `bson:"tipo"`
	Valor       float64 `bson:"valor"`
	Descripcion string  `bson:"descripcion"`
}

var tmpl_impuestos_frame = template.Must(template.ParseFiles(
	"Vistas/index.html",
	"Vistas/Parciales/Plantilla/header_blank.html",
	"Vistas/Parciales/Plantilla/footer_blank.html",
	"Vistas/Parciales/Plantilla/styles.html",
	"Vistas/Parciales/Plantilla/scripts.html",
	"Vistas/Parciales/Paginas/Impuestos/index.html",
))

func ImpuestosFrame(w http.ResponseWriter, r *http.Request) {
	userName := Session.GetUserName(r)
	if userName != "" {
		result := impuestos_m.GetImpuestos()
		tmpl_impuestos_frame.ExecuteTemplate(w, "index_layout", result)
	} else {
		http.Redirect(w, r, "/login", 302)
	}
}

var tmpl_impuestos = template.Must(template.ParseFiles(
	"Vistas/index.html",
	"Vistas/Parciales/Plantilla/header.html",
	"Vistas/Parciales/Plantilla/footer.html",
	"Vistas/Parciales/Plantilla/styles.html",
	"Vistas/Parciales/Plantilla/scripts.html",
	"Vistas/Parciales/Paginas/Impuestos/index.html",
))

func Index(w http.ResponseWriter, r *http.Request) {
	userName := Session.GetUserName(r)
	if userName != "" {
		result := impuestos_m.GetImpuestos()
		tmpl_impuestos.ExecuteTemplate(w, "index_layout", result)
	} else {
		http.Redirect(w, r, "/login", 302)
	}
}

func Newer(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		meta_impuesto := r.FormValue("meta_impuesto")
		prioridad := r.FormValue("prioridad")

		int_prioridad, _ := strconv.Atoi(prioridad)

		var impuestesillo MetaImpuesto
		var sub_impuestos []Impuesto

		impuestesillo = MetaImpuesto{Grupo: meta_impuesto, Prioridad: int_prioridad, Impuestos: sub_impuestos}

		insertado := impuestos_m.InsertarImpuesto(impuestesillo)

		if insertado == true {
			fmt.Fprintf(w, "Insertado")
		} else {
			fmt.Fprintf(w, "No insertado")
		}

	}

}

func New(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {

		var tmp0 = `	<div class="page-header">
						<h3 class="text-center">Nuevo Meta-Impuesto : </h3>
					</div>
							<div class="input-group input-group-md">
								<span class="input-group-addon">Nombre: </span>
								<input type="text" name="nombre_grupo" id="nombre_grupo" value="" class="form-control">
							</div>`

		tmp0 = tmp0 + `<div class="input-group input-group-md">
								<span class="input-group-addon">Prioridad: </span>
								<select name="prioridad_n" id="prioridad_n" class="selectpicker form-control">`

		for ind := 1; ind <= 10; ind++ {
			tmp0 = tmp0 + `<option value="` + strconv.Itoa(ind) + `">` + strconv.Itoa(ind) + ` </option>`
		}
		tmp0 = tmp0 + `</select></div>`

		var tmp1 = `<div class="input-group input-group-md">
								<span class="input-group-addon">
									<input type="button" class="btn btn-primary" onClick="GuardarImpuestoNuevo();" value="Guardar" name="guardarimpuestonuevo" id="guardarimpuestonuevo">
								</span>
							</div>`

		fmt.Fprintf(w, tmp0)
		fmt.Fprintf(w, tmp1)

	}

}

func Guardar(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {

		var nombres = []string{}
		var valores = []string{}
		var descripcion = []string{}
		var tipos = []string{}

		var impuestos []Impuesto
		var impuesto Impuesto
		var metaimpuesto MetaImpuesto

		id := r.FormValue("id")
		meta_impuesto := r.FormValue("meta_impuesto")
		prioridad := r.FormValue("prioridad")

		for k, v := range r.PostForm {

			if k == "nombres[]" {
				nombres = v
			}
			if k == "tipos[]" {
				tipos = v
			}
			if k == "valores[]" {
				valores = v
			}
			if k == "descripcion[]" {
				descripcion = v
			}
		}

		lim := len(nombres)
		for i := 0; i < lim; i++ {

			flotante, _ := strconv.ParseFloat(valores[i], 64)

			impuesto = Impuesto{Nombre: nombres[i], Tipo: tipos[i], Valor: flotante, Descripcion: descripcion[i]}
			impuestos = append(impuestos, impuesto)
		}
		objectid := bson.ObjectIdHex(id)

		int_prioridad, _ := strconv.Atoi(prioridad)

		metaimpuesto = MetaImpuesto{Grupo: meta_impuesto, Prioridad: int_prioridad, Impuestos: impuestos}

		insertado := impuestos_m.EditarImpuesto(metaimpuesto, objectid)

		if insertado == true {
			fmt.Fprintf(w, "Insertado")
		} else {
			fmt.Fprintf(w, "No insertado")
		}

		// --
	}

}

func Editar(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		var contador int = 0
		ids := r.FormValue("id")
		result := impuestos_m.GetImpuesto(ids)

		fmt.Println("\n \n", result)

		var tmp0id = `<div class="input-group input-group-md">
						<span class="input-group-addon">Id: </span>
						<input type="text" name="id_" id="id_" value="%v" class="form-control" disabled>
					</div>`

		var tmp1grupo = `<div class="input-group input-group-md">
								<span class="input-group-addon">Nombre de Meta Impuesto: </span>
								<input type="text" name="grupo_" id="grupo_" value="%v" class="form-control">
							</div>`

		var tmp2prioridad = `<div class="input-group input-group-md">
								<span class="input-group-addon">Prioridad: </span>
								<select name="prioridad_" id="prioridad_" class="selectpicker form-control">`

		for ind := 1; ind <= 10; ind++ {
			if ind == result.Prioridad {
				tmp2prioridad = tmp2prioridad + `<option selected="" value="` + strconv.Itoa(ind) + `">` + strconv.Itoa(ind) + ` </option>`

			} else {
				tmp2prioridad = tmp2prioridad + `<option value="` + strconv.Itoa(ind) + `">` + strconv.Itoa(ind) + ` </option>`
			}
		}
		tmp2prioridad = tmp2prioridad + `</select></div>`

		var tmp4DatosPorcentaje = `<div class="input-group input-group-md claves" id="valores_%d">
											<span class="input-group-addon">Nombre: </span>
											<input type="text" name="nombre_%d" id="nombre_%d" value="%v" class="form-control nombre">

											<span class="input-group-addon">Tipo:
		 									<label class="radio-inline"><input type="radio" name="impuesto_tipo_%d" id="impuesto_tipo_%d" value ="porcentaje"checked>%%</label>
				 							<label class="radio-inline"><input type="radio" name="impuesto_tipo_%d" id="impuesto_tipo_%d" value ="moneda">$</label>
											</span>

											<span class="input-group-addon">Valor: </span>
											<input type="text" name="valor_%d" id="valor_%d" value="%v" class="form-control valor">
											<span class="input-group-addon">Descripcion: </span>

											<input type="text" name="desc_%d" id="desc_%d" value="%v" class="form-control descripcion">
											<span class="input-group-addon btn btn-danger eliminar">-</span>
										</div>`

		var tmp4DatosMoneda = `<div class="input-group input-group-md claves" id="valores_%d">
											<span class="input-group-addon">Nombre: </span>

											<input type="text" name="nombre_%d" id="nombre_%d" value="%v" class="form-control nombre">
											<span class="input-group-addon">Tipo:
		 									<label class="radio-inline"><input type="radio" name="impuesto_tipo_%d" id="impuesto_tipo_%d" value ="porcentaje">%%</label>
				 							<label class="radio-inline"><input type="radio" name="impuesto_tipo_%d" id="impuesto_tipo_%d" value ="moneda"checked>$</label>
											</span>

											<span class="input-group-addon">Valor: </span>
											<input type="text" name="valor_%d" id="valor_%d" value="%v" class="form-control valor">
											<span class="input-group-addon">Descripcion: </span>

											<input type="text" name="desc_%d" id="desc_%d" value="%v" class="form-control descripcion">
											<span class="input-group-addon btn btn-danger eliminar">-</span>
										</div>`

		var tmpAgregarImpuesto = `<div class="input-group input-group-md">
									<span class="input-group-addon">
									<input type="button" class="btn btn-lg btn-success" onClick="Agregarcampoimpuesto('%v');" value="+" name="agregarcampo_" id="agregarcampo_">
									</span>
								  </div>`

		fmt.Fprintf(w, tmp0id, result.Id.Hex())

		fmt.Fprintf(w, tmp1grupo, result.Grupo)

		fmt.Fprintf(w, tmp2prioridad)

		fmt.Fprintf(w, tmpAgregarImpuesto, result.Id.Hex())

		for _, value := range result.Impuestos {
			impuestesillo := value
			if impuestesillo.Tipo == "porcentaje" {
				fmt.Fprintf(w, tmp4DatosPorcentaje, contador, contador, contador, impuestesillo.Nombre, contador, contador, contador, contador, contador, contador, impuestesillo.Valor, contador, contador, impuestesillo.Descripcion)
			} else if impuestesillo.Tipo == "moneda" {
				fmt.Fprintf(w, tmp4DatosMoneda, contador, contador, contador, impuestesillo.Nombre, contador, contador, contador, contador, contador, contador, impuestesillo.Valor, contador, contador, impuestesillo.Descripcion)
			}
			contador++
		}
	}

}

func Eliminar(w http.ResponseWriter, r *http.Request) {
	id := r.FormValue("id")
	objectid := bson.ObjectIdHex(id)

	fmt.Println(id)
	fmt.Println(objectid)

	eliminado := impuestos_m.EliminarImpuesto(objectid)
	if eliminado == true {
		fmt.Fprintf(w, "Eliminado")
	} else {
		fmt.Fprintf(w, "No eliminado")
	}
}
