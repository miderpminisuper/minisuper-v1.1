package movimientos

import (
	"../../Modelos/Movimientos"
	"fmt"
	_ "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"html/template"
	"net/http"
	_ "reflect"
	"strconv"
)

var tmpl_moviminentos_frame = template.Must(template.ParseFiles(
	"Vistas/index.html",
	"Vistas/Parciales/Plantilla/header_blank.html",
	"Vistas/Parciales/Plantilla/footer_blank.html",
	"Vistas/Parciales/Plantilla/styles.html",
	"Vistas/Parciales/Plantilla/scripts.html",
	"Vistas/Parciales/Paginas/Ajustes/index.html",
	"Vistas/Parciales/Paginas/Movimientos/index.html",
))

func MovimientosFrame(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		tmpl_moviminentos_frame.ExecuteTemplate(w, "index_layout", nil)
	}

}

var tmpl_moviminentos = template.Must(template.ParseFiles(
	"Vistas/index.html",
	"Vistas/Parciales/Plantilla/header.html",
	"Vistas/Parciales/Plantilla/footer.html",
	"Vistas/Parciales/Plantilla/styles.html",
	"Vistas/Parciales/Plantilla/scripts.html",
	"Vistas/Parciales/Paginas/Ajustes/index.html",
	"Vistas/Parciales/Paginas/Movimientos/index.html",
))

func Index(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		tmpl_moviminentos.ExecuteTemplate(w, "index_layout", nil)
	}

}

func ObtenerMovimientos(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		r.ParseForm()
		tipo_movimiento := r.FormValue("tipo_movimiento")
		movimientos := movimientos_m.Obtenertipodemovimientos(tipo_movimiento)

		switch {
		//---------------------------------------------------------------------------------------------------------------VENTA
		case tipo_movimiento == "venta":
			if movimientos.([]movimientos_m.MovimientosVenta) != nil {

				var nombre_usuario UserName
				var nombre_usuario2 UserName
				var nombre_alm_origen AlmacenName
				var nombre_alm_destino AlmacenName

				s := movimientos_m.ConectMgoSession("192.168.1.110")

				c_usuarios := s.DB("minisuper").C("usuarios")

				c_almacenes := s.DB("minisuper").C("almacenes")

				var template_ventas = `		
								<table class="table table-bordered text-center" >
			  						<thead id="cabeceraventa">
			  							<tr>
			  								<th>Id</th>
			    							<th>Tipo</th>
			    							<th>Fecha</th>
			    							<th>Usuario Or</th>
			    							<th>Usuario Ds</th>
			    							<th>Alm Origen</th>
			    							<th>Alm Destino</th>
			    							<th>Total</th>
			    							<th>Productos</th>
			  							</tr>
			  						</thead>
			  						<tbody>`

				for _, value := range movimientos.([]movimientos_m.MovimientosVenta) {

					template_ventas = template_ventas + `<tr>
											<td><a id="` + value.ID.Hex() + `" onclick="GetProductosUnicos('` + value.ID.Hex() + `');" data-toggle="modal" data-target="#myModal1" href="#">` + value.ID.Hex() + `</a></td>
											<td>` + value.Tipo + `</td>
											<td>` + value.Date.Format("2006-01-02") + `</td>`

					c_usuarios.Find(bson.M{"_id": value.UsuarioOrigen}).Select(bson.M{"nombre_completo": 1}).One(&nombre_usuario)
					c_usuarios.Find(bson.M{"_id": value.UsuarioDestino}).Select(bson.M{"nombre_completo": 1}).One(&nombre_usuario2)

					c_almacenes.Find(bson.M{"_id": value.AlmacenOrigen}).Select(bson.M{"nombre": 1}).One(&nombre_alm_origen)
					c_almacenes.Find(bson.M{"_id": value.AlmacenDestino}).Select(bson.M{"nombre": 1}).One(&nombre_alm_destino)

					total_venta := strconv.FormatFloat(value.TotalVenta, 'f', 2, 64)

					template_ventas = template_ventas + `<td>` + nombre_usuario.Nombre + `</td>
														<td>` + nombre_usuario2.Nombre + `</td>
														<td>` + nombre_alm_origen.Nombre + `</td>
														<td>` + nombre_alm_destino.Nombre + `</td>
														<td>` + total_venta + `</td>`

					numero_prod := len(value.Productos)
					numero_prod_srting := strconv.Itoa(numero_prod)

					template_ventas = template_ventas + `<td>` + numero_prod_srting + `</td>`

					template_ventas = template_ventas + `</tr>`

				}

				template_ventas = template_ventas + `</tbody></table>`

				fmt.Fprintf(w, template_ventas)
				defer s.Close()
			} else {
				fmt.Fprintf(w, `<h1 class="text-center"> No existen movimientos para %v </h1>`, tipo_movimiento)
			}
			break
			//---------------------------------------------------------------------------------------------------------------COMPRA
		case tipo_movimiento == "compra":
			if movimientos.([]movimientos_m.MovimientoCompras) != nil {

				var nombre_proveedor UserName
				var nombre_usuario UserName
				var nombre_alm_destino AlmacenName

				s := movimientos_m.ConectMgoSession("192.168.1.110")

				c_usuarios := s.DB("minisuper").C("usuarios")

				c_almacenes := s.DB("minisuper").C("almacenes")

				c_proveedores := s.DB("minisuper").C("proveedores")

				template_compra := `		
								<table class="table table-bordered text-center" >
			  						<thead id="cabeceracompra">
			  							<tr>
			  								<th>Id</th>
			    							<th>Tipo</th>
			    							<th>Fecha</th>
			    							<th>Usuario Or</th>
			    							<th>Usuario Ds</th>
			    							<th>Alm Origen</th>
			    							<th>Alm Destino</th>
			    							<th>Productos</th>
			  							</tr>
			  						</thead>
			  						<tbody>`
				for _, value := range movimientos.([]movimientos_m.MovimientoCompras) {

					template_compra = template_compra + `<tr>
											<td><a id="` + value.ID.Hex() + `" onclick="GetProductosUnicos('` + value.ID.Hex() + `');" data-toggle="modal" data-target="#myModal1" href="#">` + value.ID.Hex() + `</a></td>
											<td>` + value.Tipo + `</td>
											<td>` + value.Date.Format("2006-01-02") + `</td>`

					c_proveedores.Find(bson.M{"_id": value.UsuarioOrigen}).Select(bson.M{"nombre_completo": 1}).One(&nombre_proveedor)

					c_usuarios.Find(bson.M{"_id": value.UsuarioDestino}).Select(bson.M{"nombre_completo": 1}).One(&nombre_usuario)

					c_almacenes.Find(bson.M{"_id": value.AlmacenDestino}).Select(bson.M{"nombre": 1}).One(&nombre_alm_destino)

					template_compra = template_compra + `<td>` + nombre_proveedor.Nombre + `</td>
														<td>` + nombre_usuario.Nombre + `</td>
														<td>` + nombre_proveedor.Nombre + `</td>
														<td>` + nombre_alm_destino.Nombre + `</td>`

					numero_prod := len(value.Productos)
					numero_prod_srting := strconv.Itoa(numero_prod)

					template_compra = template_compra + `<td>` + numero_prod_srting + `</td>`

					template_compra = template_compra + `</tr>`
				}

				template_compra = template_compra + `</tbody></table>`

				fmt.Fprintf(w, template_compra)
				defer s.Close()
			} else {
				fmt.Fprintf(w, `<h1 class="text-center"> No existen movimientos para %v </h1>`, tipo_movimiento)
			}
			break
			//--------------------------------------------------------------------------------------------------------------DEVOLUCION
		case tipo_movimiento == "devolucion":
			if movimientos.([]movimientos_m.MovimientosDevolucion) != nil {

				var usuario_origen UserName
				var usuario_destino UserName
				var nombre_almacen_origen AlmacenName
				var nombre_almacen_destino AlmacenName

				s := movimientos_m.ConectMgoSession("192.168.1.110")

				c_usuarios := s.DB("minisuper").C("usuarios")

				c_almacenes := s.DB("minisuper").C("almacenes")

				var template_devolucion = `		
								<table class="table table-bordered text-center" >
			  						<thead id="cabeceradevolucion">
			  							<tr>
			  								<th>Id</th>
			    							<th>Tipo</th>
			    							<th>Fecha</th>
			    							<th>Usuario Or</th>
			    							<th>Usuario Ds</th>
			    							<th>Alm Origen</th>
			    							<th>Alm Destino</th>
			    							<th>Productos</th>
			  							</tr>
			  						</thead>
			  						<tbody>`
				for _, value := range movimientos.([]movimientos_m.MovimientosDevolucion) {

					template_devolucion = template_devolucion + `<tr>
											<td><a id="` + value.Id.Hex() + `" onclick="GetProductosUnicos('` + value.Id.Hex() + `');" data-toggle="modal" data-target="#myModal1" href="#">` + value.Id.Hex() + `</a></td>
											<td>` + value.TipoMovimiento + `</td>
											<td>` + value.FechaHora.Format("2006-01-02") + `</td>`

					c_usuarios.Find(bson.M{"_id": value.UsOrigen}).Select(bson.M{"nombre_completo": 1}).One(&usuario_origen)
					c_usuarios.Find(bson.M{"_id": value.UsDestino}).Select(bson.M{"nombre_completo": 1}).One(&usuario_destino)

					c_almacenes.Find(bson.M{"_id": value.Origen}).Select(bson.M{"nombre": 1}).One(&nombre_almacen_origen)
					c_almacenes.Find(bson.M{"_id": value.Destino}).Select(bson.M{"nombre": 1}).One(&nombre_almacen_destino)

					template_devolucion = template_devolucion + `<td>` + usuario_origen.Nombre + `</td>
																<td>` + usuario_destino.Nombre + `</td>
																<td>` + nombre_almacen_origen.Nombre + `</td>
																<td>` + nombre_almacen_destino.Nombre + `</td>`

					numero_prod := len(value.Productos)
					numero_prod_srting := strconv.Itoa(numero_prod)

					template_devolucion = template_devolucion + `<td>` + numero_prod_srting + `</td>`
					template_devolucion = template_devolucion + `</tr>`
				}
				template_devolucion = template_devolucion + `</tbody></table>`

				fmt.Fprintf(w, template_devolucion)
				defer s.Close()
			} else {
				fmt.Fprintf(w, `<h1 class="text-center"> No existen movimientos para %v </h1>`, tipo_movimiento)
			}

			break
			//---------------------------------------------------------------------------------------------------------------GARANTIA
		case tipo_movimiento == "garantia":
			if movimientos.([]movimientos_m.MovimientosGarantia) != nil {

				var usuario_origen UserName
				var usuario_destino UserName
				var nombre_almacen_origen AlmacenName
				var nombre_almacen_destino AlmacenName

				s := movimientos_m.ConectMgoSession("192.168.1.110")

				c_usuarios := s.DB("minisuper").C("usuarios")

				c_almacenes := s.DB("minisuper").C("almacenes")

				var template_garantia = `		
								<table class="table table-bordered text-center" >
			  						<thead id="cabeceragarantia">
			  							<tr>
			  								<th>Id</th>
			    							<th>Tipo</th>
			    							<th>Fecha</th>
			    							<th>Usuario Or</th>
			    							<th>Usuario Ds</th>
			    							<th>Alm Origen</th>
			    							<th>Alm Destino</th>
			    							<th>Productos</th>
			  							</tr>
			  						</thead>
			  						<tbody>`

				for _, value := range movimientos.([]movimientos_m.MovimientosGarantia) {

					template_garantia = template_garantia + `<tr>
											<td><a id="` + value.Id.Hex() + `" onclick="GetProductosUnicos('` + value.Id.Hex() + `');" data-toggle="modal" data-target="#myModal1" href="#">` + value.Id.Hex() + `</a></td>
											<td>` + value.TipoMovimiento + `</td>
											<td>` + value.FechaHora.Format("2006-01-02") + `</td>`

					c_usuarios.Find(bson.M{"_id": value.UsOrigen}).Select(bson.M{"nombre_completo": 1}).One(&usuario_origen)
					c_usuarios.Find(bson.M{"_id": value.UsDestino}).Select(bson.M{"nombre_completo": 1}).One(&usuario_destino)

					c_almacenes.Find(bson.M{"_id": value.Origen}).Select(bson.M{"nombre": 1}).One(&nombre_almacen_origen)
					c_almacenes.Find(bson.M{"_id": value.Destino}).Select(bson.M{"nombre": 1}).One(&nombre_almacen_destino)

					template_garantia = template_garantia + `<td>` + usuario_origen.Nombre + `</td>
																<td>` + usuario_destino.Nombre + `</td>
																<td>` + nombre_almacen_origen.Nombre + `</td>
																<td>` + nombre_almacen_destino.Nombre + `</td>`

					numero_prod := len(value.Productos)
					numero_prod_srting := strconv.Itoa(numero_prod)

					template_garantia = template_garantia + `<td>` + numero_prod_srting + `</td>`
					template_garantia = template_garantia + `</tr>`
				}
				template_garantia = template_garantia + `</tbody></table>`

				fmt.Fprintf(w, template_garantia)
				defer s.Close()
			} else {
				fmt.Fprintf(w, `<h1 class="text-center"> No existen movimientos para %v </h1>`, tipo_movimiento)
			}
			break
			//---------------------------------------------------------------------------------------------------------------AJUSTE
		case tipo_movimiento == "ajuste":
			if movimientos.([]movimientos_m.MovimientoAjuste) != nil {

				var nombre_usuario UserName
				var nombre_almacen_origen AlmacenName

				s := movimientos_m.ConectMgoSession("192.168.1.110")

				c_usuarios := s.DB("minisuper").C("usuarios")

				c_almacenes := s.DB("minisuper").C("almacenes")

				template_ajuste := `		
								<table class="table table-bordered text-center" >
			  						<thead id="cabeceraajuste">
			  							<tr>
			  								<th>Id</th>
			    							<th>Tipo</th>
			    							<th>Fecha</th>
			    							<th>Almacen Modificado</th>
			    							<th>Usuario</th>
			    							<th>Productos</th>
			  							</tr>
			  						</thead>
			  						<tbody>`

				for _, value := range movimientos.([]movimientos_m.MovimientoAjuste) {

					template_ajuste = template_ajuste + `<tr>
											<td><a id="` + value.ID.Hex() + `" onclick="GetProductosUnicos('` + value.ID.Hex() + `');" data-toggle="modal" data-target="#myModal1" href="#">` + value.ID.Hex() + `</a></td>
											<td>` + value.Tipo + `</td>
											<td>` + value.Date.Format("2006-01-02") + `</td>`

					c_usuarios.Find(bson.M{"_id": value.Usuario}).Select(bson.M{"nombre_completo": 1}).One(&nombre_usuario)
					c_almacenes.Find(bson.M{"_id": value.AlmacenOrigen}).Select(bson.M{"nombre": 1}).One(&nombre_almacen_origen)

					template_ajuste = template_ajuste + `<td>` + nombre_almacen_origen.Nombre + `</td>
														<td>` + nombre_usuario.Nombre + `</td>`

					numero_prod := len(value.Productos)
					numero_prod_srting := strconv.Itoa(numero_prod)

					template_ajuste = template_ajuste + `<td>` + numero_prod_srting + `</td>`
					template_ajuste = template_ajuste + `</tr>`
				}
				template_ajuste = template_ajuste + `</tbody></table>`

				fmt.Fprintf(w, template_ajuste)
				defer s.Close()
			} else {
				fmt.Fprintf(w, `<h1 class="text-center"> No existen movimientos para %v </h1>`, tipo_movimiento)
			}
			break
			//----------------------------------------------------------------------------------------------------------------TRASLADO
		case tipo_movimiento == "traslado":
			if movimientos.([]movimientos_m.MovimientoTraslado) != nil {
				var nombre_almacen_origen AlmacenName
				var nombre_almacen_destino AlmacenName
				var nombre_usuario UserName

				s := movimientos_m.ConectMgoSession("192.168.1.110")

				c_usuarios := s.DB("minisuper").C("usuarios")

				c_almacenes := s.DB("minisuper").C("almacenes")

				template_traslado := `		
								<table class="table table-bordered text-center" >
			  						<thead id="cabeceratraslado">
			  							<tr>
			  								<th>Id</th>
			    							<th>Tipo</th>
			    							<th>Fecha</th>
			    							<th>Origen</th>
			    							<th>Destino</th>
			    							<th>Usuario</th>			    							
			    							<th>Productos</th>
			  							</tr>
			  						</thead>
			  					<tbody>`
				for _, value := range movimientos.([]movimientos_m.MovimientoTraslado) {

					template_traslado = template_traslado + `<tr>
											<td><a id="` + value.ID.Hex() + `" onclick="GetProductosUnicos('` + value.ID.Hex() + `');" data-toggle="modal" data-target="#myModal1" href="#">` + value.ID.Hex() + `</a></td>
											<td>` + value.Tipo + `</td>
											<td>` + value.Date.Format("2006-01-02") + `</td>`

					c_almacenes.Find(bson.M{"_id": value.AlmacenOrigen}).Select(bson.M{"nombre": 1}).One(&nombre_almacen_origen)
					c_almacenes.Find(bson.M{"_id": value.AlmacenDestino}).Select(bson.M{"nombre": 1}).One(&nombre_almacen_destino)
					c_usuarios.Find(bson.M{"_id": value.Usuario}).Select(bson.M{"nombre_completo": 1}).One(&nombre_usuario)

					template_traslado = template_traslado + `<td>` + nombre_almacen_origen.Nombre + `</td>
															<td>` + nombre_almacen_destino.Nombre + `</td>
															<td>` + nombre_usuario.Nombre + `</td>`

					numero_prod := len(value.Productos)
					numero_prod_srting := strconv.Itoa(numero_prod)

					template_traslado = template_traslado + `<td>` + numero_prod_srting + `</td>`
					template_traslado = template_traslado + `</tr>`
				}
				template_traslado = template_traslado + `</tbody></table>`

				fmt.Fprintf(w, template_traslado)
				defer s.Close()
			} else {
				fmt.Fprintf(w, `<h1 class="text-center"> No existen movimientos para %v </h1>`, tipo_movimiento)
			}
			break
			//---------------------------------------------------------------------------------------------------------------COTIZACION
		case tipo_movimiento == "cotizacion":
			if movimientos.([]movimientos_m.MovimientoCotizacion) != nil {

				var nombre_cliente ClienteName
				var nombre_usuario UserName
				var nombre_almacen_origen AlmacenName
				var nombre_almacen_destino ClienteName

				s := movimientos_m.ConectMgoSession("192.168.1.110")

				c_clientes := s.DB("minisuper").C("clientes")

				c_usuarios := s.DB("minisuper").C("usuarios")

				c_almacenes := s.DB("minisuper").C("almacenes")

				template_cotizacion := `		
								<table class="table table-bordered text-center" >
			  						<thead id="cabeceracotizacion">
			  							<tr>
			  								<th>Id</th>
			    							<th>Tipo</th>
			    							<th>Fecha</th>
			    							<th>Cliente</th>
			    							<th>Usuario</th>
			    							<th>Origen</th>
			    							<th>Destino</th>
			    							<th>Productos</th>
			  							</tr>
			  						</thead>
			  						<tbody>`

				for _, value := range movimientos.([]movimientos_m.MovimientoCotizacion) {

					template_cotizacion = template_cotizacion + `<tr>
											<td><a id="` + value.ID.Hex() + `" onclick="GetProductosUnicos('` + value.ID.Hex() + `');" data-toggle="modal" data-target="#myModal1" href="#">` + value.ID.Hex() + `</a></td>
											<td>` + value.Tipo + `</td>
											<td>` + value.FechaHora.Format("2006-01-02") + `</td>`

					c_clientes.Find(bson.M{"_id": value.IdCliente}).Select(bson.M{"nombre_completo": 1}).One(&nombre_cliente)

					c_usuarios.Find(bson.M{"_id": value.IdUsuario}).Select(bson.M{"nombre_completo": 1}).One(&nombre_usuario)

					c_almacenes.Find(bson.M{"_id": value.AlmacenOrigen}).Select(bson.M{"nombre": 1}).One(&nombre_almacen_origen)

					c_clientes.Find(bson.M{"_id": value.AlmacenDestino}).Select(bson.M{"nombre_completo": 1}).One(&nombre_almacen_destino)

					template_cotizacion = template_cotizacion + `
											<td>` + nombre_cliente.Nombre + `</td>
											<td>` + nombre_usuario.Nombre + `</td>
											<td>` + nombre_almacen_origen.Nombre + `</td>
											<td> Almacen ` + nombre_almacen_destino.Nombre + `</td>`

					numero_prod := len(value.Productos)
					numero_prod_srting := strconv.Itoa(numero_prod)

					template_cotizacion = template_cotizacion + `<td>` + numero_prod_srting + `</td>`
					template_cotizacion = template_cotizacion + `</tr>`
				}

				template_cotizacion = template_cotizacion + `</tbody></table>`

				fmt.Fprintf(w, template_cotizacion)

				defer s.Close()
			} else {
				fmt.Fprintf(w, `<h1 class="text-center"> No existen movimientos para %v </h1>`, tipo_movimiento)
			}
			break
			//---------------------------------------------------------------------------------------------------------------------
		}

	}
}

type ClienteName struct {
	Nombre string `bson:"nombre_completo"`
}

type UserName struct {
	Nombre string `bson:"nombre_completo"`
}

type AlmacenName struct {
	Nombre string `bson:"nombre"`
}

type ProductoName struct {
	Nombre string `bson:"descripcion"`
	Codigo string `bson:"codigobarra"`
}

func ProductosMovimiento(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		r.ParseForm()

		id_movimiento := r.FormValue("idmovimiento")
		tipo_movimiento := r.FormValue("tipo_movimiento")

		movimiento := movimientos_m.GetMovimientoUnico(id_movimiento, tipo_movimiento)

		switch {
		//------------------------------------------------------------------------------------------------------------------VENTA
		case tipo_movimiento == "venta":

			var nombre_producto ProductoName
			var tipo_impuesto string
			var impuesto string

			s := movimientos_m.ConectMgoSession("192.168.1.110")
			c_productos := s.DB("minisuper").C("productos")

			tmp_productos := `		
					<table class="table table-bordered text-center" >
			  			<thead id="cabeceraproductoventa">
			  				<tr>			  					
			    				<th>Codigo</th>
			    				<th>Nombre</th>
			    				<th>Cantidad</th>
			    				<th>Precio</th>
			    				<th>Descuentos</th>
			    				<th>Impuestos</th>
			    				<th>Importe</th>			    				
			  				</tr>
			  			</thead>
						<tbody>`

			for _, value := range movimiento.(movimientos_m.MovimientosVenta).Productos {

				c_productos.Find(bson.M{"_id": value.ID}).Select(bson.M{"descripcion": 1, "codigobarra": 1}).One(&nombre_producto)

				cantidad_solicitada := strconv.FormatFloat(value.Cantidad, 'f', -1, 64)
				precio := strconv.FormatFloat(value.CostoTotal, 'f', -1, 64)
				descuento := strconv.FormatFloat(value.Descuento, 'f', -1, 64)
				importe_total := strconv.FormatFloat(value.Importe, 'f', -1, 64)

				for index2, value2 := range value.Impuestos {
					tipo_impuesto = index2
					impuesto = value2

				}

				tmp_productos = tmp_productos + `<tr>												
												<td>` + nombre_producto.Codigo + `</td>												
												<td>` + nombre_producto.Nombre + `</td>
												<td>` + cantidad_solicitada + `</td>
												<td>` + precio + `</td>
												<td>` + descuento + `</td>
												<td>` + tipo_impuesto + " " + impuesto + `</td>
												<td>` + importe_total + `</td>`

				tmp_productos = tmp_productos + `</tr>`

			}

			tmp_productos = tmp_productos + `</tbody></table>`
			var formapago string
			total_venta := strconv.FormatFloat(movimiento.(movimientos_m.MovimientosVenta).TotalVenta, 'f', -1, 64)

			for _, value3 := range movimiento.(movimientos_m.MovimientosVenta).FormasPago.Formas {
				formapago = value3.Nombre
			}

			tmp_productos = tmp_productos + `<div class="container"><div class="row"><h3>Forma de Pago: <small>` + formapago + ` </small></h3><h3>Total: <small> ` + total_venta + `</small></h3></div></div>`
			fmt.Fprintf(w, tmp_productos)
			defer s.Close()
			break
			//--------------------------------------------------------------------------------------------------------------COMPRA
		case tipo_movimiento == "compra":

			var nombre_producto ProductoName

			s := movimientos_m.ConectMgoSession("192.168.1.110")
			c_productos := s.DB("minisuper").C("productos")
			tmp_productos := `		
					<table class="table table-bordered text-center" >
			  			<thead id="cabeceraproductocompra">
			  				<tr>			  					
			    				<th>Codigo</th>
			    				<th>Nombre</th>
			    				<th>Cantidad</th>
			    				<th>Precio</th>
			    				<th>Importe</th>
			  				</tr>
			  			</thead>
						<tbody>`

			for _, value := range movimiento.(movimientos_m.MovimientoCompras).Productos {

				c_productos.Find(bson.M{"_id": value.ID}).Select(bson.M{"descripcion": 1, "codigobarra": 1}).One(&nombre_producto)

				cantidad_solicitada := strconv.FormatFloat(value.Cantidad, 'f', -1, 64)
				importe := strconv.FormatFloat(value.Importe, 'f', -1, 64)
				precio := strconv.FormatFloat(value.Costo, 'f', -1, 64)

				tmp_productos = tmp_productos + `<tr>												
												<td>` + nombre_producto.Codigo + `</td>												
												<td>` + nombre_producto.Nombre + `</td>
												<td>` + cantidad_solicitada + `</td>
												<td>` + precio + `</td>
												<td>` + importe + `</td>`

				tmp_productos = tmp_productos + `</tr>`

			}

			tmp_productos = tmp_productos + `</tbody></table>`
			fmt.Fprintf(w, tmp_productos)
			break
			//--------------------------------------------------------------------------------------------------------------DEVOLUCION
		case tipo_movimiento == "devolucion":
			var nombre_producto ProductoName
			s := movimientos_m.ConectMgoSession("192.168.1.110")
			c_productos := s.DB("minisuper").C("productos")

			tmp_productos := `		
					<table class="table table-bordered text-center" >
			  			<thead id="cabeceraproductodevolucion">
			  				<tr>			  					
			    				<th>Codigo</th>
			    				<th>Nombre</th>
			    				<th>Cantidad</th>
			    				<th>Costo</th>
			    				<th>Importe</th>
			  				</tr>
			  			</thead>
						<tbody>`

			for _, value := range movimiento.(movimientos_m.MovimientosDevolucion).Productos {

				c_productos.Find(bson.M{"_id": value.Id}).Select(bson.M{"descripcion": 1, "codigobarra": 1}).One(&nombre_producto)

				cantidad_solicitada := strconv.FormatFloat(value.Cantidad, 'f', -1, 64)
				importe := strconv.FormatFloat(value.Importe, 'f', -1, 64)
				precio := strconv.FormatFloat(value.Costo, 'f', -1, 64)

				tmp_productos = tmp_productos + `<tr>												
												<td>` + nombre_producto.Codigo + `</td>												
												<td>` + nombre_producto.Nombre + `</td>
												<td>` + cantidad_solicitada + `</td>
												<td>` + precio + `</td>
												<td>` + importe + `</td>`

				tmp_productos = tmp_productos + `</tr>`

			}

			tmp_productos = tmp_productos + `</tbody></table>`
			fmt.Fprintf(w, tmp_productos)
			defer s.Close()
			break
			//--------------------------------------------------------------------------------------------------------------GARANTIA
		case tipo_movimiento == "garantia":
			var nombre_producto ProductoName
			s := movimientos_m.ConectMgoSession("192.168.1.110")
			c_productos := s.DB("minisuper").C("productos")
			tmp_productos := `		
					<table class="table table-bordered text-center" >
			  			<thead id="cabeceraproductogarantia">
			  				<tr>			  					
			    				<th>Codigo</th>
			    				<th>Nombre</th>
			    				<th>Cantidad</th>
			    				<th>Costo</th>
			    				<th>Importe</th>
			  				</tr>
			  			</thead>
						<tbody>`

			for _, value := range movimiento.(movimientos_m.MovimientosGarantia).Productos {

				c_productos.Find(bson.M{"_id": value.Id}).Select(bson.M{"descripcion": 1, "codigobarra": 1}).One(&nombre_producto)

				cantidad_solicitada := strconv.FormatFloat(value.Cantidad, 'f', -1, 64)
				importe := strconv.FormatFloat(value.Importe, 'f', -1, 64)
				precio := strconv.FormatFloat(value.Costo, 'f', -1, 64)

				tmp_productos = tmp_productos + `<tr>												
												<td>` + nombre_producto.Codigo + `</td>												
												<td>` + nombre_producto.Nombre + `</td>
												<td>` + cantidad_solicitada + `</td>
												<td>` + precio + `</td>
												<td>` + importe + `</td>`

				tmp_productos = tmp_productos + `</tr>`
			}

			tmp_productos = tmp_productos + `</tbody></table>`
			fmt.Fprintf(w, tmp_productos)
			break
			//--------------------------------------------------------------------------------------------------------------AJUSTE
		case tipo_movimiento == "ajuste":

			tmp_productos := `		
					<table class="table table-bordered text-center" >
			  			<thead id="cabeceraproductoajuste">
			  				<tr>			  					
			    				<th>Codigo</th>
			    				<th>Nombre</th>			    				
			    				<th>Cant Anterior</th>
			    				<th>Cant Solicitada</th>
			    				<th>Cant Final</th>
			    				<th>Operacion</th>			    				
			  				</tr>
			  			</thead>
						<tbody>`

			for _, value := range movimiento.(movimientos_m.MovimientoAjuste).Productos {

				cantidad_anterior := strconv.FormatFloat(value.CantidadA, 'f', -1, 64)
				cantidad_solicitada := strconv.FormatFloat(value.CantidadB, 'f', -1, 64)
				cantidad_final := strconv.FormatFloat(value.CantidadC, 'f', -1, 64)

				tmp_productos = tmp_productos + `<tr>												
												<td>` + value.CodigoDeBarra + `</td>												
												<td>` + value.Nombre + `</td>
												<td>` + cantidad_anterior + `</td>
												<td>` + cantidad_solicitada + `</td>
												<td>` + cantidad_final + `</td>
												<td>` + value.Operacion + `</td>`

				tmp_productos = tmp_productos + `</tr>`

			}
			tmp_productos = tmp_productos + `</tbody></table>`

			fmt.Fprintf(w, tmp_productos)

			break
			//---------------------------------------------------------------------------------------------------------------TRASLADO
		case tipo_movimiento == "traslado":
			tmp_productos := `		
					<table class="table table-bordered text-center" >
			  			<thead id="cabeceraproductotraslado">
			  				<tr>			  					
			    				<th>Codigo</th>
			    				<th>Nombre</th>
			    				<th>Alm Origen</th>
			    				<th>Alm Destino</th>
			    				<th>Cantidad</th>			    				
			  				</tr>
			  			</thead>
						<tbody>`

			for _, value := range movimiento.(movimientos_m.MovimientoTraslado).Productos {

				cantidad_solicitada := strconv.FormatFloat(value.CantidadC, 'f', -1, 64)
				cantidad_final_origen := strconv.FormatFloat(value.CantidadD, 'f', -1, 64)
				cantidad_final_destino := strconv.FormatFloat(value.CantidadE, 'f', -1, 64)

				tmp_productos = tmp_productos + `<tr>												
												<td>` + value.CodigoDeBarra + `</td>												
												<td>` + value.Nombre + `</td>
												<td>` + cantidad_final_origen + `</td>
												<td>` + cantidad_final_destino + `</td>
												<td>` + cantidad_solicitada + `</td>`

				tmp_productos = tmp_productos + `</tr>`

			}

			tmp_productos = tmp_productos + `</tbody></table>`
			fmt.Fprintf(w, tmp_productos)
			break
			//----------------------------------------------------------------------------------------------------------------COTIZACION
		case tipo_movimiento == "cotizacion":

			var nombre_producto ProductoName
			s := movimientos_m.ConectMgoSession("192.168.1.110")
			c_productos := s.DB("minisuper").C("productos")

			tmp_productos := `		
					<table class="table table-bordered text-center" >
			  			<thead id="cabeceraproductocotizacion">
			  				<tr>			  					
			    				<th>Codigo</th>
			    				<th>Nombre</th>
			    				<th>Cantidad</th>
			    				<th>Precio</th>
			    				<th>Descuentos</th>
			    				<th>Total</th>			    							    			
			  				</tr>
			  			</thead>
						<tbody>`

			for _, value := range movimiento.(movimientos_m.MovimientoCotizacion).Productos {

				c_productos.Find(bson.M{"_id": value.ID}).Select(bson.M{"descripcion": 1, "codigobarra": 1}).One(&nombre_producto)

				cantidad_solicitada := strconv.FormatFloat(value.Cantidad, 'f', -1, 64)
				precio := strconv.FormatFloat(value.Importe, 'f', -1, 64)
				descuento := strconv.FormatFloat(value.Descuento, 'f', -1, 64)
				importe_total := strconv.FormatFloat(value.ImporteTotal, 'f', -1, 64)

				tmp_productos = tmp_productos + `<tr>												
												<td>` + nombre_producto.Codigo + `</td>												
												<td>` + nombre_producto.Nombre + `</td>
												<td>` + cantidad_solicitada + `</td>
												<td>` + precio + `</td>
												<td>` + descuento + `</td>
												<td>` + importe_total + `</td>`

				tmp_productos = tmp_productos + `</tr>`

			}

			tmp_productos = tmp_productos + `</tbody></table>`
			fmt.Fprintf(w, tmp_productos)
			defer s.Close()
			break
			//-----------------------------------------------------------------------------------------------------------------END
		}

	}

}
