package medidas

import (
	"fmt"

	"../../Modelos/Medidas"
	"../Session"

	"html/template"
	"net/http"
	_ "strconv"

	"gopkg.in/mgo.v2/bson"
)

type Metamedida struct {
	Id     bson.ObjectId `bson:"_id,omitempty"`
	Grupo  string        `bson:"grupo_medida"`
	Medida []Medidas     `bson:"medidas"`
}

type Medidas struct {
	Nombre      string `bson:"nombre"`
	Abreviatura string `bson:"abreviatura"`
}

var tmpl_medidas = template.Must(template.ParseFiles(
	"Vistas/index.html",
	"Vistas/Parciales/Plantilla/header.html",
	"Vistas/Parciales/Plantilla/footer.html",
	"Vistas/Parciales/Plantilla/styles.html",
	"Vistas/Parciales/Plantilla/scripts.html",
	"Vistas/Parciales/Paginas/Medidas/index.html",
))

func Index(w http.ResponseWriter, r *http.Request) {
	userName := Session.GetUserName(r)
	if userName != "" {
		result := medidas_m.GetMedidas()
		fmt.Println(result)
		tmpl_medidas.ExecuteTemplate(w, "index_layout", result)
	} else {
		http.Redirect(w, r, "/login", 302)
	}
}

var tmpl_medidas_frame = template.Must(template.ParseFiles(
	"Vistas/index.html",
	"Vistas/Parciales/Plantilla/header_blank.html",
	"Vistas/Parciales/Plantilla/footer_blank.html",
	"Vistas/Parciales/Plantilla/styles.html",
	"Vistas/Parciales/Plantilla/scripts.html",
	"Vistas/Parciales/Paginas/Medidas/index.html",
))

func MedidasFrame(w http.ResponseWriter, r *http.Request) {
	userName := Session.GetUserName(r)
	if userName != "" {
		result := medidas_m.GetMedidas()
		tmpl_medidas_frame.ExecuteTemplate(w, "index_layout", result)
	} else {
		http.Redirect(w, r, "/login", 302)
	}
}

func Editar(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		ids := r.FormValue("id")
		result := medidas_m.GetMedida(ids)
		var tmp0id = `<div class="input-group input-group-md">
						<span class="input-group-addon">Id: </span>
						<input type="text" name="id_" id="id_" value="%v" class="form-control" disabled>
					</div>`

		var tmp1grupo = `<div class="input-group input-group-md">
								<span class="input-group-addon">Nombre de Meta-Unidad: </span>
								<input type="text" name="grupo_" id="grupo_" value="%v" class="form-control">
							</div>`

		var tmp4MedidasDisp = `<div class="input-group input-group-md claves" id="valores_%d">
											<span class="input-group-addon">Nombre: </span>
											<input type="text" name="nombre_%d" id="nombre_%d" value="%v" class="form-control nombre">

											<span class="input-group-addon">Abreviatura: </span>
											<input type="text" name="valor_%d" id="valor_%d" value="%v" class="form-control abreviatura">

											<span class="input-group-addon btn btn-danger eliminar">-</span>
										</div>`

		var tmpAgregarImpuesto = `<div class="input-group input-group-md">
									<span class="input-group-addon">
									<input type="button" class="btn btn-lg btn-success" onClick="Agregarcampomedida('%v');" value="+" name="agregarcampo_" id="agregarcampo_">
									</span>
								  </div>`

		fmt.Fprintf(w, tmp0id, result.Id.Hex())
		fmt.Fprintf(w, tmp1grupo, result.Grupo)
		fmt.Fprintf(w, tmpAgregarImpuesto, result.Id.Hex())
		for contador, value := range result.Medida {
			fmt.Fprintf(w, tmp4MedidasDisp, contador, contador, contador, value.Nombre, contador, contador, value.Abreviatura)
		}
	}

}

func Guardar(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {

		var nombres = []string{}
		var abreviaturas = []string{}
		var medidas []Medidas
		var medida Medidas
		var metamedida Metamedida

		id := r.FormValue("id")
		meta_medida := r.FormValue("meta_medida")

		for k, v := range r.PostForm {

			if k == "nombres[]" {
				nombres = v
			}
			if k == "abreviaturas[]" {
				abreviaturas = v
			}
		}

		for i := 0; i < len(nombres); i++ {

			medida = Medidas{Nombre: nombres[i], Abreviatura: abreviaturas[i]}
			medidas = append(medidas, medida)
		}

		objectid := bson.ObjectIdHex(id)

		metamedida = Metamedida{Grupo: meta_medida, Medida: medidas}

		insertado := medidas_m.EditarMedida(metamedida, objectid)

		if insertado == true {
			fmt.Fprintf(w, "Insertado")
		} else {
			fmt.Fprintf(w, "No insertado")
		}

		// --
	}

}

func New(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {

		var tmp0 = `	<div class="page-header">
						<h3 class="text-center">Nueva Meta-Unidad : </h3>
					</div>
							<div class="input-group input-group-md">
								<span class="input-group-addon">Nombre: </span>
								<input type="text" name="nombre_grupo" id="nombre_grupo" value="" class="form-control">
							</div>`

		var tmp1 = `<div class="input-group input-group-md">
								<span class="input-group-addon">
									<input type="button" class="btn btn-primary" onClick="GuardarMedidaNueva();" value="Guardar" name="GuardarMedidaNueva" id="GuardarMedidaNueva">
								</span>
							</div>`

		fmt.Fprintf(w, tmp0)
		fmt.Fprintf(w, tmp1)

	}

}

func Newer(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		meta_medida := r.FormValue("meta_medida")

		var new_meta_medida Metamedida
		var medidas []Medidas

		new_meta_medida = Metamedida{Grupo: meta_medida, Medida: medidas}

		insertado := medidas_m.InsertarMedida(new_meta_medida)

		if insertado == true {
			fmt.Fprintf(w, "Insertado")
		} else {
			fmt.Fprintf(w, "No insertado")
		}

	}

}

func Eliminar(w http.ResponseWriter, r *http.Request) {
	id := r.FormValue("id")
	objectid := bson.ObjectIdHex(id)
	eliminado := medidas_m.EliminarMedida(objectid)
	if eliminado == true {
		fmt.Fprintf(w, "Eliminado")
	} else {
		fmt.Fprintf(w, "No eliminado")
	}
}
