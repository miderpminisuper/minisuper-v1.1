package controllers

import (
	"crypto/md5"
	"encoding/hex"
	"fmt"
	"html/template"
	"net/http"

	"./Session"
)

var tmpl_index = template.Must(template.ParseFiles(

	"Vistas/index.html",
	"Vistas/Parciales/Plantilla/header.html",
	"Vistas/Parciales/Plantilla/footer.html",
	"Vistas/Parciales/Plantilla/styles.html",
	"Vistas/Parciales/Plantilla/scripts.html",
	"Vistas/Parciales/Paginas/Dashboard/content.html",
))

//############################################### F  U  N  C  I  O  N  E S ########################################################
func Index(w http.ResponseWriter, r *http.Request) {
	userName := Session.GetUserName(r)
	if userName != "" {
		//SesionUser := Users{Name: userName}
		tmpl_index.ExecuteTemplate(w, "index_layout", nil)
	} else {
		http.Redirect(w, r, "/login", 302)
	}
}

func GetUserSession(w http.ResponseWriter, r *http.Request) {
	user := Session.GetUserName(r)
	fmt.Fprintf(w, user)
}

//Autor: Ramon Cruz Juarez
//Funcion Administracion
//Modulo para establecer las opciones de administracion
func Administracion(w http.ResponseWriter, r *http.Request) {
	userName := Session.GetUserName(r)
	if userName != "" {
		fmt.Fprintf(w, `<div class="alert alert-info" role="alert">
						<center><label>Sección Administración</label></center>
					`)

		fmt.Fprintf(w, `<div id='tabs' id='menuadmin' class='col-md-2 col-lg-2'>
							<div id='menuadmin' class='col-md-2 col-lg-2'>
								<ul>
									<li><a href='#tabs-1'>Datos de la empresa</a></li>
									<li><a href='#tabs-2'>Usuarios</a></li>
									<li><a href='#tabs-3'>Clientes</a></li>
									<li><a href='#tabs-4'>Proveedores</a></li>
									<li><a href='#tabs-5'>Medios de pago</a></li>
									<li><a href='#tabs-6'>Almacenes</a></li>
									<li><a href='#tabs-7'>Impuestos</a></li>
									<li><a href='#tabs-8'>Unidades - Medidas</a></li>
								</ul>
							</div>
							<div id='frames' class='col-md-10 col-lg-10'">
								<div id='tabs-1'><iframe src="/datosfiscales_frame" width="100%" height="700"></iframe></div>
								<div id='tabs-2'><iframe src="/usuarios_frame" width="100%" height="700"></iframe></div>								
								<div id='tabs-3'><iframe src="/client_frame" width="100%" height="700"></iframe></div>								
								<div id='tabs-4'><iframe src="/proveedores_frame" width="100%" height="700"></iframe></div>																	
								<div id='tabs-5'><iframe src="formadepago_frame" width="100%" height="700"></iframe></div>
								<div id='tabs-6'><iframe src="almacenes_frame" width="100%" height="700"></iframe></div>
								<div id='tabs-7'><iframe src="impuestos_frame" width="100%" height="700"></iframe></div>
								<div id='tabs-8'><iframe src="medidas_frame" width="100%" height="700"></iframe></div>
							</div>
						</div>
					`)
		fmt.Fprintf(w, `</div>`)
	} else {
		http.Redirect(w, r, "/login", 302)
	}
}

//Autor: Ramon Cruz Juárez
//Funcion Inventario
//Modulo que tenga la plantilla general de lo que contiene el inventario
//submenu almacenes, traslados/reajustes y kardex
func Inventario(w http.ResponseWriter, r *http.Request) {
	userName := Session.GetUserName(r)
	if userName != "" {
		fmt.Fprintf(w, `<div class="alert alert-info" role="alert">
						<center><label>Sección Inventario</label></center>
					`)

		fmt.Fprintf(w, `
						<div id='tabs' id='menuadmin' class='col-md-2 col-lg-2'>
							<div id='menuinventario' class='col-md-2 col-lg-2'>
								<ul>
									<li><a href='#tabs-1'>Almacenes</a></li>
									<li><a href='#tabs-2'>Traslados-Ajustes</a></li>
									<li><a href='#tabs-3'>Kardex</a></li>
									<li><a href='#tabs-4'>Movimientos</a></li>
								</ul>
							</div>

							<div id='frames' class='col-md-10 col-lg-10'">
								<div id='tabs-1'>
									<iframe src="/almacenes_frame" width="100%" height="700"></iframe>
								</div>

								<div id='tabs-2'>
									<iframe src="/ajustetraslado_frame" width="100%" height="700"></iframe>
								</div>
								<div id='tabs-3'>
									<iframe src="/kardex_frame" width="100%" height="700"></iframe>
								</div>
								<div id='tabs-4'>
									<iframe src="/movimientos_frame" width="100%" height="700"></iframe>
								</div>
							</div>
						</div>
					`)
		fmt.Fprintf(w, `</div>`)
	} else {
		http.Redirect(w, r, "/login", 302)
	}
}

//Autor: Ramon Cruz Juárez
//Funcion Productos
//Modulo que tenga la plantilla general de lo que contiene los productos
//index de productos y alta de artículos
func Productos(w http.ResponseWriter, r *http.Request) {
	userName := Session.GetUserName(r)
	if userName != "" {
		fmt.Fprintf(w, `<div class="alert alert-info" role="alert">
					<center><label><h3>Artículos</h3></label></center>
					`)

		fmt.Fprintf(w, `<iframe src="indexarticulos_frame" width="100%" height="700"></iframe>
					</div>`)
	} else {
		http.Redirect(w, r, "/login", 302)
	}
}

//Autor: Ramon Cruz Juarez
//Funcion Compras
//Modulo para establecer las opciones principales de compras
func Compras(w http.ResponseWriter, r *http.Request) {
	userName := Session.GetUserName(r)
	if userName != "" {
		fmt.Fprintf(w, `<div class="alert alert-info" role="alert">
						<center><label>Módulo Compras</label></center>
					`)

		fmt.Fprintf(w, `<div id='tabs' id='menucompras' class='col-md-2 col-lg-2'>
							<div id='menucompras' class='col-md-2 col-lg-2'>
								<ul>
									<li><a href='#tabs-1'>Agregar nueva compra</a></li>
									<li><a href='#tabs-2'>Alta de productos</a></li>
									<li><a href='#tabs-3'>Productos</a></li>
									<li><a href='#tabs-4'>Proveedores</a></li>
									<li><a href='#tabs-5'>Compras realizadas</a></li>
								</ul>
							</div>
							<div id='frames' class='col-md-10 col-lg-10'">
								<div id='tabs-1'><iframe src="/compras_frame" width="100%" height="700"></iframe></div>
								<div id='tabs-2'><iframe src="/articulos_frame" width="100%" height="700"></iframe></div>
								<div id='tabs-3'><iframe src="/indexarticulos_frame" width="100%" height="700"></iframe></div>
								<div id='tabs-4'><iframe src="/proveedores_frame" width="100%" height="700"></iframe></div>	
								<div id='tabs-5'><iframe src="/indexcompras_frame" width="100%" height="700"></iframe></div>
							</div>
						</div>
					`)
		fmt.Fprintf(w, `</div>`)
	} else {
		http.Redirect(w, r, "/login", 302)
	}
}

//Autor: Ramon Cruz Juarez
//Funcion Ventas
//Modulo para establecer las opciones principales de Ventas
func Ventas(w http.ResponseWriter, r *http.Request) {
	userName := Session.GetUserName(r)
	if userName != "" {
		fmt.Fprintf(w, `<div class="alert alert-info" role="alert">
						<center><label>Módulo Ventas</label></center>
					`)

		fmt.Fprintf(w, `
						<div id='tabs' id='menuventas' class='col-md-2 col-lg-2'>
							<div id='menuventas' class='col-md-2 col-lg-2'>
								<ul>
									<li><a href='#tabs-1'>Realizar venta</a></li>
									<li><a href='#tabs-2'>Productos</a></li>
									<li><a href='#tabs-3'>Ventas realizadas</a></li>
									<li><a href='#tabs-4'>Devolucion y Garantia</a></li>
								</ul>
							</div>
							<div id='frames' class='col-md-10 col-lg-10'">
								<div id='tabs-1'><iframe src="/venta/new_frame" width="100%" height="700"></iframe></div>
								<div id='tabs-2'><iframe src="/indexarticulos_frame" width="100%" height="700"></iframe></div>
								<div id='tabs-3'><iframe src="/indexventas_frame" width="100%" height="700"></iframe></div>
								<div id='tabs-4'><iframe src="/devolucion_frame" width="100%" height="700"></iframe></div>								
							</div>
						</div>
					`)
		fmt.Fprintf(w, `</div>`)
	} else {
		http.Redirect(w, r, "/login", 302)
	}
}

//Autor: Ramon Cruz Juarez
//Funcion EncriptaTexto
//funcion que encripta un texto dado
//parametros de entrada: valor *Texto a encriptar
//parametros de salida: texto encriptado
func EncriptaTexto(valor string) string {
	hasher := md5.New()
	hasher.Write([]byte(valor))

	return hex.EncodeToString(hasher.Sum(nil))
}
