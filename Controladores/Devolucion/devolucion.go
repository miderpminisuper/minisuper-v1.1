package devolucion

import (
	_ "encoding/json"
	"fmt"
	"html/template"
	_ "io"
	_ "log"
	"net/http"
	"strconv"
	"time"

	"../Session"
	"gopkg.in/mgo.v2/bson"

	"../../Modelos/Almacenes"
	"../../Modelos/Devolucion"
)

var tmpl_pagos = template.Must(template.ParseFiles(
	"Vistas/index.html",
	"Vistas/Parciales/Plantilla/header.html",
	"Vistas/Parciales/Plantilla/footer.html",
	"Vistas/Parciales/Plantilla/styles.html",
	"Vistas/Parciales/Plantilla/scripts.html",
	"Vistas/Parciales/Paginas/Devolucion/index.html",
))

var tmpl_pagos_frame = template.Must(template.ParseFiles(
	"Vistas/index.html",
	"Vistas/Parciales/Plantilla/header_blank.html",
	"Vistas/Parciales/Plantilla/footer_blank.html",
	"Vistas/Parciales/Plantilla/styles.html",
	"Vistas/Parciales/Plantilla/scripts.html",
	"Vistas/Parciales/Paginas/Devolucion/index.html",
))

type Movimientos struct {
	Id             bson.ObjectId  `bson:"_id,omitempty"`
	TipoMovimiento string         `bson:"tipo_movimiento"`
	FechaHora      time.Time      `bson:"fecha_hora"`
	Origen         bson.ObjectId  `bson:"almacen_origen,omitempty"`
	Destino        bson.ObjectId  `bson:"almacen_destino,omitempty"`
	UsOrigen       bson.ObjectId  `bson:"usuario_origen,omitempty"`
	UsDestino      bson.ObjectId  `bson:"usuario_destino,omitempty"`
	Productos      []ProductosAll `bson:"productos"`
	Referencia     bson.ObjectId  `bson:"referencia,omitempty"`
}
type ProductosAll struct {
	Id            bson.ObjectId `bson:"_id,omitempty"`
	Cantidad      float64       `bson:"cantidad"`
	Costo         float64       `bson:"costo"`
	Precio        float64       `bson:"precio"`
	Importe       float64       `bson:"importe"`
	Ganancia      float64       `bson:"ganancia"`
	totalImpuesto float64       `bson:"total_impuesto"`
}

/*
@melchormendoza
Trae la vista principal
@params
	null
@return
	null
*/

func Index(w http.ResponseWriter, r *http.Request) {
	userName := Session.GetUserName(r)
	if userName != "" {
		almacenes := almacenes_m.GetAlmacenes()
		tmpl_pagos.ExecuteTemplate(w, "index_layout", map[string]interface{}{
			"almacenes": almacenes,
		})
	} else {
		http.Redirect(w, r, "/login", 302)
	}
}

func IndexFrame(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		almacenes := almacenes_m.GetAlmacenes()
		tmpl_pagos_frame.ExecuteTemplate(w, "index_layout", map[string]interface{}{
			"almacenes": almacenes,
		})
	} else {
		http.Redirect(w, r, "/login", 302)
	}
}

func GetDocumento(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		/*
		 * Recibo parametros para consulta de kardex
		 */
		fmt.Println("2. Controlador/GetDocumento")

		ids := r.PostFormValue("id")

		/*
		 *Voy por los movimientos que contienen el sku buscado
		 */
		result2 := devolucion_modelo.GetMovimientoById(ids)
		/*
		 *Voy por el inventario del sku a postgres
		 */
		//inventario := kardex_modelo.GetInventario(idAlmacen, ids)

		if len(result2) > 0 {
			var cantidad float64
			var total float64
			var nombre_art string
			fmt.Fprintf(w, `<div class="panel-body"></div>`)
			fmt.Fprintf(w, `<table id="devolucion" class="table table-hover table-sm table-responsive">`)
			fmt.Fprintf(w, `<thead>`)
			fmt.Fprintf(w, `<tr>`)
			fmt.Fprintf(w, `<th>Descripción</th>`)
			fmt.Fprintf(w, `<th>Cantidad</th>`)
			fmt.Fprintf(w, `<th>Devolución</th>`)
			fmt.Fprintf(w, `<th>Precio</th>`)
			fmt.Fprintf(w, `<th>Importe</th>`)
			fmt.Fprintf(w, `</tr>`)
			fmt.Fprintf(w, `</thead>`)
			for _, v := range result2 {
				fmt.Fprintf(w, `<tbody>`)
				//fmt.Fprintf(w, `<tr>`)

				//fmt.Fprintf(w, `<td>%v</td>`, cantidad)
				for _, x := range v.Productos {
					//if x.Id.Hex() == ids {
					fmt.Fprintf(w, `<tr id="%v">`, x.Id.Hex())
					cantidad = x.Cantidad
					if x.Id.Hex() != "" {
						nombre_art = devolucion_modelo.GetProductoNombreById(x.Id.Hex())
					}
					fmt.Fprintf(w, `<td>%s</td>`, nombre_art)
					//fmt.Fprintf(w, `<td>%v</td>`, x.Id.Hex())
					//fmt.Println(v.Id.Hex())
					fmt.Fprintf(w, `<td><span id="cantidad%v">%v</span></td>`, x.Id.Hex(), cantidad)
					fmt.Fprintf(w, `<td>`)
					fmt.Fprintf(w, `<div class="input-group number-spinner"">`)
					fmt.Fprintf(w, `<span class="input-group-btn"><button class="btn btn-info" data-dir="dwn" onclick="cambiaImporte('%v',%v,-1)"><span class="glyphicon glyphicon-minus" id="%v"></span></button></span>`, x.Id.Hex(), cantidad, x.Id.Hex())
					fmt.Fprintf(w, `<input type="text" class="form-control" id="dev%v" value="0" onkeypress="cambiaImporte('%v',%v,0)">`, x.Id.Hex(), x.Id.Hex(), cantidad)
					fmt.Fprintf(w, `<span class="input-group-btn"><button class="btn btn-info" data-dir="up" onclick="cambiaImporte('%v',%v,1)"><span class="glyphicon glyphicon-plus" id="%v"></span></button></span>`, x.Id.Hex(), cantidad, x.Id.Hex())
					fmt.Fprintf(w, `</div>`)
					fmt.Fprintf(w, `</td>`)
					fmt.Fprintf(w, `<td><input type="text" class="form-control" id="pre%v" value="%f" disabled></td>`, x.Id.Hex(), x.Precio)
					fmt.Fprintf(w, `<td><input type="text" class="form-control" id="imp%v" value="0" disabled></td>`, x.Id.Hex())
					fmt.Fprintf(w, `</tr>`)
					total = total + cantidad
				}
				fmt.Fprintf(w, `</tbody>`)
				fmt.Fprintf(w, `<tfoot>`)
				fmt.Fprintf(w, `<tr>`)
				fmt.Fprintf(w, `<td><button type="button" class="btn btn-success" onclick="guardaDevolucion('%v')">Aplicar</button></td>`, v.Id.Hex())
				//fmt.Fprintf(w, `<td><buttont type="button" class="btn btn-primary" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-target=".bd-example-modal-lg">Buscar</button></td>`, v.Id.Hex())
				fmt.Fprintf(w, `</tr>`)
				fmt.Fprintf(w, `</tfoot>`)
			}

			fmt.Fprintf(w, `</table>`)
			//fmt.Fprintf(w, `<script>$("#si").html(%f);</script>`, inventario)
			//fmt.Fprintf(w, `<script>$("#sf").html(%f);</script>`, inventario+saldo)
		} else {
			fmt.Fprintf(w, `<div class="alert alert-warning" role="alert"><strong>Ups!</strong> No se encontraron movimientos.</div>`)
		}
	}
}

func SetDocumento(w http.ResponseWriter, r *http.Request) {

	if r.Method == "POST" {
		//Declaracion de variables
		var mov Movimientos
		var almacenOrigenMovOriginal bson.ObjectId
		var almacenDestinoMovOriginal bson.ObjectId
		var usuarioOrigenMovOriginal bson.ObjectId
		var usuarioDestinoMovOriginal bson.ObjectId
		var destino bson.ObjectId

		var productos []ProductosAll
		var producto ProductosAll
		//var m = make(map[string]string)
		var s1 = []string{}
		var s2 = []string{}
		var s3 = []string{}
		var i int = 0

		fmt.Println("1. Controlador/SetDocumento")
		//Parametros de entrada
		//Movimiento original
		idDocumento := r.FormValue("idDocumento")
		//Tipo de movimiento(devolucion/garantia)
		tipoMovimiento := r.FormValue("tipoMovimiento")
		//Almacen seleccionado como destino del movimiento
		idAlmacenDestino := r.FormValue("idAlmacen")
		//Obtenemos datos del movimiento original
		data := devolucion_modelo.GetMovimientoById(idDocumento)
		//Iteramos el resultado para localizar el almacen origen y destino
		if len(data) > 0 {
			for _, v := range data {
				almacenOrigenMovOriginal = v.Origen
				almacenDestinoMovOriginal = v.Destino
				usuarioOrigenMovOriginal = v.UsOrigen
				usuarioDestinoMovOriginal = v.UsDestino
			}
		}
		//Si el movimiento es de tipo devolucion, entonces asignamos el almacen origen, como destino
		//Es decir la devolucion entra al almacen de donde salio de manera automatica.
		if tipoMovimiento == "devolucion" {
			destino = almacenOrigenMovOriginal
		} else {
			//Valido, si viene vacio el almacen destino, le asigno el origen del documento original
			if idAlmacenDestino == "0" {
				destino = almacenOrigenMovOriginal
			} else {
				destino = bson.ObjectIdHex(idAlmacenDestino)
			}
		}
		//Leo los articulos y las cantidades
		for k, v := range r.PostForm {
			if k == "articulos[]" {
				s1 = v
			}
			if k == "devoluciones[]" {
				s2 = v
			}
			if k == "precios[]" {
				s3 = v
			}
		}
		lim := len(s1)
		for i = 0; i < lim; i++ {
			artid := bson.ObjectIdHex(s1[i])
			dev, _ := strconv.ParseFloat(s2[i], 64)
			pre, _ := strconv.ParseFloat(s3[i], 64)
			producto = ProductosAll{Id: artid, Cantidad: dev, Costo: pre, Precio: pre, Importe: dev * pre}
			productos = append(productos, producto)
			//Actualizamos inventairo en postgreSQL
			devolucion_modelo.RealizarAjuste(destino.Hex(), artid.Hex(), dev)
		}
		fmt.Println(productos)
		//Obtengo el objectId del usuario logueado.
		//Pendiente
		//InsertoMovimiento en Mongo
		//Creo el ObjectId
		objId := bson.NewObjectId()
		mov = Movimientos{Id: objId, TipoMovimiento: tipoMovimiento, FechaHora: time.Now(), Destino: destino, UsDestino: usuarioOrigenMovOriginal, UsOrigen: usuarioDestinoMovOriginal, Origen: almacenDestinoMovOriginal, Productos: productos, Referencia: bson.ObjectIdHex(idDocumento)}
		insertado := devolucion_modelo.SetMovimiento(mov)
		fmt.Println(insertado)
		if insertado == true {
			fmt.Fprintf(w, objId.Hex())
		} else {
			fmt.Fprintf(w, "-1")
		}
	}

}
